function add_room_in_list(house_id, number_of_rooms) {
    add_room_details(house_id, number_of_rooms)
    $('#room_adder' + house_id).css('display', 'none')
    $('#main_switcher' + house_id).css('display', '-webkit-inline-box')
}


function unique(array) {
    return array.filter(function(el, index, arr) {
        return index == arr.indexOf(el);
    });
}



function add_room_details(house_id, number_of_rooms) {


    var date = new Date($('#inputCheckIn1').val());
    var day_in = date.getDate();
    var month_in = date.getMonth() + 1;
    var year_in = date.getFullYear();
    let check_in_value = month_in + "-" + day_in + "-" + year_in
        // document.getElementById("inputCheckIn1").valueAsDate = check_in_value;

    var date2 = new Date($('#inputCheckOut1').val());
    var day_out = date2.getDate();
    var month_out = date2.getMonth() + 1;
    var year_out = date2.getFullYear();
    let check_out_value = month_out + "-" + day_out + "-" + year_out
        // document.getElementById("inputCheckOut1").valueAsDate = check_in_value;

    // change number of night
    // To calculate the time difference of two dates
    var today = new Date();

    var Difference_Check_In_and_Current = date.getTime() - today.getTime();

    var Difference_In_Time = date2.getTime() - date.getTime();

    // To calculate the no. of days between two dates
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

    if (date.getDate() == today.getDate()) {

    } else if (Difference_Check_In_and_Current < 0) {
        alertify.error('Please select correct check in date.');
        return false;
    }

    if (Difference_In_Days < 1) {
        alertify.error('Please select correct check out date.');
        return false;
    }



    update_date()


    $('.sdcvsjvcds').css('display', 'grid')
        // $('#body_id').css('padding-bottom', '328px')
        // console.log("am here nac")
    $('#cart_item_id').val(house_id)


    let next_room_remaining_rooms_in_house = $('#next_room_remaining_rooms_in_house_' + house_id).val();
    next_room_remaining_rooms_in_house = Number(next_room_remaining_rooms_in_house) + 1;
    if (next_room_remaining_rooms_in_house > number_of_rooms) {
        return false;
    }

    if ($('#cart_item_id').val()) {
        $('#book_btn').css('display', 'unset')
    }
    $('#room_left' + house_id).text(Number(number_of_rooms) - next_room_remaining_rooms_in_house)
    $('#room_left' + house_id).css('color', 'rgb(22, 187, 64)')
    $('#room_left' + house_id).css('font-weight', 'bold')

    let unique_value = new Date().valueOf();


    let faker_id = house_id + next_room_remaining_rooms_in_house + unique_value
    $('#room_main_section_' + house_id).append(
        `<div class="form-groups sdcsdc dfvcsdcs` + house_id + `" id="room_selection_` + house_id + `_` + next_room_remaining_rooms_in_house + `">
        Room ` + next_room_remaining_rooms_in_house + ` &nbsp; <div class = "jfbvkjf">
        Adult(12 + years) <select class = "form-control adulttt` + faker_id + ` adult` + house_id + ` adult_class" onchange="save_adult_details(` + house_id + `)" name="adult_` + house_id + `_` + next_room_remaining_rooms_in_house + `" id="adult_` + house_id + `_` + next_room_remaining_rooms_in_house + `">
                <option value="1">1</option>
                <option value="2" selected="">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
        <input type="hidden" name="booking_details_id_` + house_id + `_` + next_room_remaining_rooms_in_house + `"  id="booking_details_id_` + house_id + `_` + next_room_remaining_rooms_in_house + `">  
        </div>
        <div  class="jfbvkjf">
            Child (0-12 yrs) 
            <select class="form-control  children` + house_id + `  children_class" name="child_` + house_id + `_` + next_room_remaining_rooms_in_house + `" onchange="save_children_details(` + house_id + `)" id="child_` + house_id + `_` + next_room_remaining_rooms_in_house + `">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
         </div>
        </div>`
    )

    $('.adult_class').val($('#adult_selector').val())
    $('.children_class').val($('#children_selector').val())

    let new_next_room_remaining_rooms_in_house = next_room_remaining_rooms_in_house;
    $('#next_room_remaining_rooms_in_house_' + house_id).val(new_next_room_remaining_rooms_in_house)

    //    cart details


    let last_id = []
    var room_main_div_ = $('.room_main_dev').map(function(index) {
        last_id.push(this.id)
        $('.room_main_dev_container').append(`<li class='fsdfdfs room_main_dev main__div` + house_id + `' id='room_holder` +
                house_id + `'><strong></strong>
            <span>` + $('#item_name_' + house_id).text() + `</span>
        
        <span><span class="sub_adult` + house_id + `">2</span> Adults <span class="sub_children` + house_id + `">0</span> Child <span class="sub_room` + house_id + `">0</span> Rooms</span> 
        <span><i onclick='focus_in_house(` + house_id + `)' class='fa fa-edit  fa-2x' style='font-size: medium;padding-left:10px;color: #00BFFF;cursor:pointer;'></i></span> 
        
        <br>
        
            <span style='text-align: right;'>
            
            <span><strong>USD <span class="mid_sub_total` + house_id + `"></span></strong></span>
            
            </span>
        
        </li>`)
            // $('#item_name_' + house_id).text() + "</span></li> <li> 4 Adults 0 Child 2 Rooms <span><i onclick='focus_in_house(" + house_id + ")' class='fa fa-edit  fa-2x' style='font-size: medium;padding-left:10px;padding-right: 12px;color: #00BFFF;cursor:pointer;'></i><i class='fa fa-trash  fa-2x'  style='font-size: medium;color: red;cursor:pointer;'></i></span></li><hr>")

    })
    last_id = unique(last_id)
    for (let i = 0; i < last_id.length; i++) {
        if (last_id[i] != "cart_item__1") {
            let for_loop_var_holder = last_id[i]
            $('.main__div' + house_id).css('display', 'none')
            $('.main__div' + house_id).remove()
            $('.room_main_dev_container').append(`<li class='fsdfdfs room_main_dev main__div` + house_id + `' id='room_holder` +
                house_id + `'><strong></strong> <span '>` +
                $('#item_name_' + house_id).text() + `</span>
        
        <span><span class="sub_adult` + house_id + `">2</span> Adults <span class="sub_children` + house_id + `">0</span> Child <span class="sub_room` + house_id + `">0</span> Rooms</span> 
        <span><i onclick='focus_in_house(` + house_id + `)' class='fa fa-edit  fa-2x' style='font-size: medium;padding-left:10px;color: #00BFFF;cursor:pointer;'></i></span> 
        
        <br>
        
            <span style='text-align: right;'>
            
            <span><strong>USD <span class="mid_sub_total` + house_id + `"></span></strong></span>
            
            </span>
        
        </li>`)
        }
    }

    // create new item if the list above has no data.
    //price
    let total_price = Number($('#item_price_' + house_id).text())

    // alert("here")

    let current_value = Number($('#cart_price').text())
    let new_value = current_value + total_price * $('#cart_number_of_days_holder').val()
    $('#cart_price').text(new_value)
    $('#cart_currency').text($('#item_currency_' + house_id).text())


    let sub_room_total = new_next_room_remaining_rooms_in_house * total_price
    $('.mid_sub_total' + house_id).text(sub_room_total * $('#cart_number_of_days_holder').val())

    // price done



    // save the booking details room data
    $('.sub_room' + house_id).text(new_next_room_remaining_rooms_in_house)



    // add total adult and children number in the booking details
    save_adult_details(house_id)
    save_children_details(house_id)

}

function update_date_checker() {
    // check is it ok to change date details


    var checker = $('.sdcsdc').map(function(index) {
        let last_id = this.id
        if (last_id) {
            if (!alertify.errorAlert) {
                //define a new errorAlert base on alert
                alertify.dialog('errorAlert', function factory() {
                    return {
                        build: function() {
                            var errorHeader = '<span class="fa fa-times-circle fa-2x" ' +
                                'style="vertical-align:middle;color:#e10000;">' +
                                '</span> Sorry!.';
                            this.setHeader(errorHeader);
                        }
                    };
                }, true, 'alert');
            }
            //launch it.
            // since this was transient, we can launch another instance at the same time.

            alertify.errorAlert('To change "Check In" or "Check Out" date after setting room details will require reloading the page.!', function(e) {
                if (e) {
                    location.reload();

                    alertify.success("Page is reloading.")
                    return true;
                } else {
                    alertify.error("Page fail to load.");
                    return false;
                }
            });

            return false;
        }

    })
    update_date()
}

function update_date() {

    var date = new Date($('#inputCheckIn1').val());
    var day_in = date.getDate();
    var month_in = date.getMonth() + 1;
    var year_in = date.getFullYear();
    let check_in_value = month_in + "-" + day_in + "-" + year_in
        // document.getElementById("inputCheckIn1").valueAsDate = check_in_value;

    var date2 = new Date($('#inputCheckOut1').val());
    var day_out = date2.getDate();
    var month_out = date2.getMonth() + 1;
    var year_out = date2.getFullYear();
    let check_out_value = month_out + "-" + day_out + "-" + year_out
        // document.getElementById("inputCheckOut1").valueAsDate = check_in_value;

    // change number of night
    // To calculate the time difference of two dates
    var today = new Date();

    var Difference_Check_In_and_Current = date.getTime() - today.getTime();

    var Difference_In_Time = date2.getTime() - date.getTime();

    // To calculate the no. of days between two dates
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

    // alert(Difference_Check_In_and_Current)
    if (date.getDate() == today.getDate()) {

    } else if (Difference_Check_In_and_Current < 0) {
        alertify.error('Please select correct check in date.');
        return false;
    }

    if (Difference_In_Days < 1) {
        alertify.error('Please select correct check out date.');
        return false;
    }



    // To calculate the no. of days between two da




    $('#cart_date').text(check_in_value + " to " + check_out_value)

    $('#cart_reference').text(Difference_In_Days)
    $('#cart_number_of_days_holder').val(Difference_In_Days)

}


function focus_in_house(house_id) {
    document.getElementById(house_id).scrollIntoView()
    document.getElementById(house_id).focus();
    window.location = "#house_id";

    $('.svkjdfkjhbds').css("border", "2px solid white")
    $('#min_room_division' + house_id).css("border", "2px solid #fcc51e")
}

function remove_room_details(house_id, number_of_rooms) {
    if ($('#next_room_remaining_rooms_in_house_' + house_id).val() == 1) {
        $('#main_switcher' + house_id).css('display', 'none')
        $('#room_adder' + house_id).css('display', 'unset')
            // remove 
        $('#room_holder' + house_id).css('display', 'none')
        $('#room_holder' + house_id).remove()
    }
    console.log("kk", house_id)

    if ($('#next_room_remaining_rooms_in_house_' + house_id).val() == 0) {
        console.log("kk1", house_id)
        console.log("am here onasis1")
        console.log(house_id)
        console.log(house_id)
        return false;
    }


    // find last element in the row and kill it
    last_jfbvkjf_id = null;
    var ids = $('.dfvcsdcs' + house_id).map(function(index) {
        let last_jfbvkjf_id = this.id
        $('#temp_last_room_value_holder' + house_id).val(last_jfbvkjf_id);
    })
    console.log("killed ID is:", $('#temp_last_room_value_holder' + house_id).val())
    $('#' + $('#temp_last_room_value_holder' + house_id).val()).remove();
    $('#' + $('#temp_last_room_value_holder' + house_id).val()).css('display', 'none');
    // done killing
    let next_room_remaining_rooms_in_house = $('#next_room_remaining_rooms_in_house_' + house_id).val();
    let new_next_room_remaining_rooms_in_house = Number(next_room_remaining_rooms_in_house) - 1;
    $('#next_room_remaining_rooms_in_house_' + house_id).val(new_next_room_remaining_rooms_in_house)

    $('#room_left' + house_id).text(Number(number_of_rooms) - new_next_room_remaining_rooms_in_house)
    $('#room_left' + house_id).css('color', 'rgb(22, 187, 64)')
    $('#room_left' + house_id).css('font-weight', 'bold')

    //price

    let total_price = $('#item_price_' + house_id).text()
    let current_value = Number($('#cart_price').text())
    let new_value = current_value - total_price * $('#cart_number_of_days_holder').val()

    $('#room_left' + house_id).text()


    $('#cart_price').text(new_value)
    $('#cart_currency').text($('#item_currency_' + house_id).text())
        // price done
    $('#cart_item_id').val(house_id)
    let sub_room_total = new_next_room_remaining_rooms_in_house * total_price
    $('.mid_sub_total' + house_id).text(sub_room_total * $('#cart_number_of_days_holder').val())


    // price done


    // save the booking details room data
    $('.sub_room' + house_id).text(new_next_room_remaining_rooms_in_house)


    // add total adult and children number in the booking details
    save_adult_details(house_id)
    save_children_details(house_id)

}


function save_adult_details(house_id) {

    const num = [];
    var ids = $('.adult' + house_id).map(function(index) {
        let res = $('#' + this.id).val();
        num.push(parseInt(res));

    });
    // let sum = 0;
    var sum = num.reduce(function(a, b) {
        return a + b;
    }, 0);
    $('.sub_adult' + house_id).text(sum)

}



function save_children_details(house_id) {

    const num = [];
    var ids = $('.children' + house_id).map(function(index) {
        let res = $('#' + this.id).val();
        num.push(parseInt(res));

    });
    // let sum = 0;
    var sum = num.reduce(function(a, b) {
        return a + b;
    }, 0);
    $('.sub_children' + house_id).text(sum)

}