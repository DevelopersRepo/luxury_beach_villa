@extends('layouts.guest.app')
@push('head')
<style>
footer {
 	position:absolute;
   	bottom:0;
   	width:100%;
   	height:60px; 
}
.thank-you-pop{
	width:100%;
 	padding:20px;
	text-align:center;
}
.thank-you-pop h3.cupon-pop{
	font-size: 25px;
    margin-bottom: 40px;
	color:#222;
	display:inline-block;
	text-align:center;
	padding:10px 20px;
	border:2px dashed #222;
	clear:both;
	font-weight:normal;
}
.thank-you-pop h3.cupon-pop span{
	color:#03A9F4;
}
.waves-effect {
    position: relative;
    cursor: pointer;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
}

</style>

@endpush
@section('body')
<div class="col-md-12" style="margin-top: 35px;" id="form-container">
	<div class="row" style="  padding: 10px;">
	
	<div class="container h-100">
	    <div class="row align-items-center h-100 " style="margin-top: 100px;">
	        <div class="col-6 mx-auto">
	        	<div class="jumbotron">
	            	<div class="thank-you-pop">
	            		@if($status == "true")
							<img src="http://goactionstations.co.uk/wp-content/uploads/2017/03/Green-Round-Tick.png" alt="">
							<h1>{{$msg}}!</h1>
							
						
						@else
							<img src="https://thumbs.dreamstime.com/b/red-cross-symbol-icon-as-delete-remove-fail-failure-incorr-incorrect-answer-89999776.jpg" height="210" width="210"  alt="">
							<h1>{{$msg}}!</h1>
						@endif
						
							<button href="{{ route('home') }}" type="button" class="btn btn-success waves-effect waves-light">Ok</button>
							
 					</div>
 				</div>
	        </div>
	    </div>
	</div>

</div>
</div>
@endsection