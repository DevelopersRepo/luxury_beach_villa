<script>

    setTimeout(function(){
        $('.alert').fadeOut('fast');
    },25000);


</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


@if(count($errors)>0)
@foreach($errors->all() as $error)
<div class="alert" style="margin-top:-40px;margin-bottom:-10px;">
    <code style="color:white;font-size:14px;" class="label label-danger">
        {{ $error }}
    </code>
    <div>
@endforeach
@endif


@if(session('success'))
<div class="container alert">

<h2 style="color:#16c161;text-shadow: #1E1D1B 1px 0 10px;" class="label label-success">
     <strong>{{ session('success' )}}</strong>
</h2>
</div>

@endif

@if(session('error'))
<div class="container alert">

<h2  style="color:#bb1321;text-shadow: #1E1D1B 1px 0 10px;"  class="label label-danger">
    <strong>{{ session('error' )}}</strong>
</h2>
</div>
@endif 