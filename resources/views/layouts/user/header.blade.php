<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Luxury Beach Villa</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{  asset('assets/website/assets/img/favicon.jpeg') }}" rel="icon">
  <link href="{{  asset('assets/website/assets/img/favicon.jpeg') }}" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/user/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/user/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/user/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/user/assets/vendor/quill/quill.snow.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/user/assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/user/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/user/assets/css/style.css') }}" rel="stylesheet">

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JC7H3PM5TG"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-JC7H3PM5TG');
</script>


  <style>
.dataTable-wrapper.no-footer .dataTable-container {
  border-bottom: 1px solid #d9d9d900;
}
    </style>
  @stack('head')
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
      <a href="index.html" class="logo d-flex align-items-center" style="padding-left: 20px;">
        <img src="{{  asset('assets/website/assets/img/favicon.jpeg') }}" style="max-height: 40px;
        margin-right: 40px;" alt="">
        {{-- <span class="d-none d-lg-block">NiceAdmin</span> --}}
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->

   
    
    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">
    

        <li class="nav-item dropdown">

          <a class="nav-link nav-icon" href="{{  url('/') }}"  target="_blank">
            <span class="badge rounded-pill"   style="background:#00BFFF">
              
              <i class="bi bi-box-arrow-in-up-right"></i>
              View Website
  


            </span>

           
          </a><!-- End Messages Icon -->

       
          
        </li><!-- End Messages Nav -->



        
        <li class="nav-item dropdown pe-3">

          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
            <span class="d-none d-md-block dropdown-toggle ps-2">{{  Auth::user()->name }}</span>
          </a><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
            <li class="dropdown-header">
              <h6>{{  Auth::user()->name }}</h6>
              <span>Admin</span>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>


            {{-- <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-person"></i>
                <span>My Profile</span>
              </a>
            </li> --}}


            {{-- <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-person"></i>
                <span>My Profile</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-gear"></i>
                <span>Account Settings</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="pages-faq.html">
                <i class="bi bi-question-circle"></i>
                <span>Need Help?</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li> --}}

            <li>

              <a  href="{{ route('logout') }}"  style="" class="dropdown-item d-flex align-items-center" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="bi bi-box-arrow-in-right"></i>
                <span>Log out</span>
                              <form id="logout-form" action="{{ route('logout') }}" 
                                method="POST" style="display: none;">{{ csrf_field() }}</form>
                            </a>


              {{-- <a class="dropdown-item d-flex align-items-center" href="#">
                <i class="bi bi-box-arrow-right"></i>
                <span>Sign Out</span>
              </a> --}}
            </li>

          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->

      </ul>
    </nav><!-- End Icons Navigation -->

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('home')  }}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->


      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('home/booked')  }}">
          <i class="bi bi-book"></i>
          <span>Booked</span>
        </a>
      </li>
    
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('home/check_in')  }}">
          <i class="bi bi-box-arrow-down"></i>
          <span>Check In</span>
        </a>
      </li><!-- End Contact Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('home/check_out')  }}">
          <i class="bi bi-box-arrow-up"></i>
          <span>Check Out</span>
        </a>
      </li><!-- End Register Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('home/content_manager')  }}">
          <i class="bi bi-justify-right"></i>
          <span>Content Manager</span>
        </a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('reports/index')  }}">
          <i class="bi bi-bar-chart-line"></i>

          <span>Reports</span>
        </a>
      </li>
      

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{  url('home/users')  }}">
          <i class="bi bi-file-person"></i>

          <span>Users</span>
        </a>
      </li>
      
      <!-- End Login Page Nav -->
{{-- 
      <li class="nav-item">
        <a class="nav-link collapsed" href="pages-error-404.html">
          <i class="bi bi-dash-circle"></i>
          <span>Error 404</span>
        </a>
      </li><!-- End Error 404 Page Nav --> --}}

      <li class="nav-item">

        <a  href="{{ route('logout') }}"  style="" class="nav-link collapsed" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="bi bi-box-arrow-in-right"></i>
          <span>Log out</span>
												<form id="logout-form" action="{{ route('logout') }}" 
													method="POST" style="display: none;">{{ csrf_field() }}</form>
											</a>

      
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->
