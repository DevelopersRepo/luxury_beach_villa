
  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>Luxury Beach Villa</span></strong>. All Rights Reserved
    </div>   
	
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/user/assets/js/custom/jquery.min.js') }}"></script>

<script src="{{ asset('assets/user/assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/chart.js/chart.min.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/quill/quill.min.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/user/assets/vendor/php-email-form/validate.js') }}"></script>
<script src="{{  asset('assets/website/js/custom/notify.js') }}"></script>                  <!-- http://kenwheeler.github.io/slick/ -->

<!-- Template Main JS File -->
<script src="{{ asset('assets/user/assets/js/main.js') }}"></script>
@stack('script')
@yield('blade_script')

</body>

</html>