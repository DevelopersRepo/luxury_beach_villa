

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Luxury Beach Villa</title>
		<!--
			Tooplate 2095 Level
			
			https://www.tooplate.com/view/2095-level
			
			-->
		<!-- load stylesheets -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
		<!-- Google web font "Open Sans" -->
		<link rel="stylesheet" href="{{  asset('assets/website/font-awesome-4.7.0/css/font-awesome.min.css') }}">
		<!-- Font Awesome -->
		<link href="{{  asset('assets/website/assets/vendor/aos/aos.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{  asset('assets/website/css/bootstrap.min.css') }}">
		<!-- Bootstrap style -->
		<link rel="stylesheet" type="text/css" href="{{  asset('assets/website/slick/slick.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{  asset('assets/website/slick/slick-theme.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{  asset('assets/website/css/datepicker.css') }}"/>
		<link rel="stylesheet" href="{{  asset('assets/website/css/tooplate-style.css') }}">
		<!-- Templatemo style -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">


		<meta content="intl_homepage" name="section">
		<meta name="referrer" content="unsafe-url">
		<meta content="2022-03-24T14:45:54Z" property="og:pubdate">
		<meta content="2022-03-24T14:45:54Z" name="pubdate">
		<meta content="2023-03-28T17:05:17Z" name="lastmod">
		<meta content="https://luxurybeachvilla.co.tz/" property="og:url">
		<meta content="“Come In As Guests. Leave As Family.”" name="description">
		<meta content="“Come In As Guests. Leave As Family.”" name="twitter:description">
		<meta content="Offers Photos Gallery Menu Breakfast Suites Rooms Small or niche museums
			Venues that host live music events Venues that host sports events Specific buildings like hospitals or theatres
			athens hotel with rooftop pool santorini hotel with in room hot tub rhodes hotel with free breakfast
			thessloniki hotel with meeting rooms corfu hotel with spa mykonos hotel with gym facilities
			romantic charming  boutique   upscale picturesque authentic scenic unforgettable historical timeless
			hotels near me booking hilton honors motel holiday inn motels near me best western cheap hotels cheap hotels near me 
			hilton garden inn comfort inn all inclusive resorts hyatt regancy priceline hotel booking hotel google hotels last minute hotel deals
			hotel deals hotel room" name="keywords">
		<meta content="luxurybeachvilla" property="og:site_name">
		<meta content="summary_large_image" name="twitter:card">
		<meta content="website" property="og:type">
		<meta property="vr:canonical" content="https://luxurybeachvilla.co.tz/">
		
		{{-- <script>


document.body.style.zoom = "50%";

			</script> --}}

			<style>


				</style>
		<link rel="alternate" href="https://luxurybeachvilla.co.tz/" hreflang="en-gb">
		<link rel="alternate" href="https://luxurybeachvilla.co.tz/" hreflang="en">
		<link rel="alternate" href="https://luxurybeachvilla.co.tz/" hreflang="kisw">
		<link rel="alternate" href="https://luxurybeachvilla.co.tz/" hreflang="kiswahili">
		<link rel="alternate" href="https://luxurybeachvilla.co.tz/" hreflang="en-us">
			
				
				<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-JC7H3PM5TG"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-JC7H3PM5TG');
		</script>


			<!-- CSS -->
			<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
			<!-- Default theme -->
			<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
			<!-- Semantic UI theme -->
			<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
			<!-- Bootstrap theme -->
			<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
	


		
		<style>

			.alertify .ajs-modal {
  z-index: 3434343435;
}


			@media (min-width: 575.98px) {
			.pc-friendly-table-inline{
			display: table-caption;
			text-align: center; 
			}

		


			}
			@media (max-width: 575.98px) {
			#mainNav {
			background: rgb(45, 45, 54);
			}

			.vres_social {
  margin-top: -31px;
}


			}
			.navbar-toggler {
			border-color: rgb(255, 255, 255);
			}
			.tm-top-bar .navbar-expand-lg .navbar-nav .nav-link {
			padding: 17px;
			}
			.nav-link.active, .nav-link:hover {
			background: #634e4fb3;
			}
			.tm-top-bar {
			background: #00BFFF;
			}
			.nav-link.active, .nav-link:hover {
			color: white;
			background: #634e4fb3;
			}
			.nav_title{
			white-space: pre;
			font-weight: bold;
			text-transform: capitalize;
			}
			.fa-2x {
			font-size: 1em;
			}
			.tm-top-bar.active .navbar-expand-lg .navbar-nav .nav-link{
			padding:9px;
			}
			.tm-top-bar {
			height: 73px;
			}
			.row > * {margin-top: -10px;
			top: -10px;}
			.tm-top-bar-bg {
			height: 0px;
			}
			/* footer css */
			footer {
			width: 100%;
			float: left;
			background: #2D2D36;
			}
			.container {
			max-width: 1170px;
			width: 100%;
			margin: 0 auto;
			}
			footer p {
			color: rgba(255,255,255,.7);
			text-align: center;
			padding: 15px 0 13px;
			margin: 0;
			font-size: 14px;
			display: inline-block;
			}
			.vres_social {
			float: right;
			padding: 3px 0;
			}
			.vres_social p {
			padding: 10px 0;
			}
			.addthis_toolbox, .fb_reset {
			display: inline-block;
			vertical-align: middle;
			margin: 5px 0;
			}
			.addthis_toolbox a.at300b, .addthis_toolbox a.at300m {
			width: auto;
			}
			.addthis_default_style .at300b, .addthis_default_style .at300bo, .addthis_default_style .at300m {
			padding: 0 2px;
			}
			.addthis_default_style .addthis_separator, .addthis_default_style .at4-icon, .addthis_default_style .at300b, .addthis_default_style .at300bo, .addthis_default_style .at300bs, .addthis_default_style .at300m {
			float: left;
			}

			/* .cancel_modal_class{
				z-index: 3434343439;
			} */
			.addthis_toolbox a {
			margin-bottom: 5px;
			line-height: initial;
			}
			.addthis_toolbox a {
			margin-bottom: 0 !important;
			height: 20px;
			}
			.addthis_toolbox a, .fb_iframe_widget {
			vertical-align: middle;
			}
			.vres_social a {
			padding: 8px 0;
			text-align: center;
			width: 40px;
			display: inline-block;
			font-size: 16px;
			color: #fff;
			margin-left: 5px;
			}
			.addthis_toolbox.addthis_20x20_style span {
			line-height: 20px;
			}
			a .at-icon-wrapper {
			cursor: pointer;
			}
			.at-icon-wrapper {
			display: inline-block;
			overflow: hidden;
			}
			.at300b, .at300bo, .at300bs, .at300m {
			cursor: pointer;
			}
			svg:not(:root) {
			overflow: hidden;
			}
			.at-icon {
			fill: #fff;
			border: 0;
			}
			.fa-stack, .ui-button, .ui-spinner, .ui-spinner-input, audio, canvas, iframe, img, svg, video {
			vertical-align: middle;
			}
			.tm-top-bar .navbar-expand-lg .navbar-nav .nav-link {
			color: white;
			}
			.staryellow {
			color: #EABA00;
			font-size: 15px;
			}

			.modal {
	z-index: 3434343434;
	background: #0a0808d4;
	}
	.nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
	color: #555;
	cursor: default;
	background-color: #fff;
	border: 1px solid #ddd;
	border-bottom-color: rgb(221, 221, 221);
	border-bottom-color: transparent;
	}
	.nav-tabs > li > a {
	margin-right: 2px;
	line-height: 1.42857143;
	border: 1px solid transparent;
	border-radius: 4px 4px 0 0;
	}
	.nav > li > a {
	position: relative;
	display: block;
	padding: 10px 15px;
	}
	.nav > li > a {
	position: relative;
	display: block;
	padding: 10px 15px;
	}
	.nav-tabs > li > a {
	margin-right: 2px;
	line-height: 1.42857143;
	border: 1px solid transparent;
	border-radius: 4px 4px 0 0;
	}
	.nav-tabs > li > a {
	margin-right: 2px;
	line-height: 1.42857143;
	border: 1px solid transparent;
	border-radius: 4px 4px 0 0;
	}
	.nav > li > a {
	position: relative;
	display: block;
	padding: 10px 15px;
	}
	.nav-tabs > li {
	float: left;
	margin-bottom: -1px;
	}
	.nav > li {
	position: relative;
	display: block;
	}
	.nav-tabs {
	border-bottom: 1px solid #ddd;
	}
	.nav {
	padding-left: 0;
	margin-bottom: 0;
	list-style: none;
	}
	a {
	color: #337ab7;
	text-decoration: none;
	}
	a {
	background-color: transparent;
	}
	
	.tm-section-pad {
	padding-top: 25px;
	}

	.success-fail{
		display:none
	}

	.success-alert{
		display:none
	}
	.success-success{
		display:none
	}

	.navbar-toggler-icon {
  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgb(45, 45, 54)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
}



		</style>
		@stack('head')

		 <style>
		.modal {
			z-index: 343434;
		  
		  }		
		  .cancel_modal_class {
			z-index: 3434349;
		  
		  }	 </style> 

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="body_id" >
		<div class="tm-main-content" id="top">
		<div class="tm-top-bar-bg"></div>
		<div class="tm-top-bar" id="tm-top-bar" style="padding-left: 34px;padding-right: 34px;">
			<!-- Top Navbar -->
			<div class="">
				<div class="row">
					<nav class="navbar navbar-expand-lg narbar-light">
						<a class="navbar-brand mr-auto" href="{{  url('/')  }}">
						<img  style="max-height: 83px;" src="{{  asset('assets/website/img/favicon.png') }}" alt="">
						{{-- LBV --}}
						</a>
						<button type="button" id="nav-toggle" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
						</button>
						<div id="mainNav" class="collapse navbar-collapse ">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item">
									<a class="nav-link pc-friendly-table-inline" href="{{ url('/#top') }}">
									<i class="fa fa-home fa-2x"></i>
									Home <span class="sr-only">(current)</span></a>
								</li>
								
								<li class="nav-item">
									<a data-toggle="modal" data-target="#hotel-infos" class="nav-link pc-friendly-table-inline" >
									<i class="fa fa-info fa-2x"></i>
									<span class="nav_title">Airbnb Resort Infos</span>
									</a>
								</li>
								<li class="nav-item" data-toggle="modal" data-target="#login-modal" >
									<a class="nav-link pc-friendly-table-inline">
									<i class="fa fa-lock fa-2x" ></i>
									<span class="nav_title">Login</span>
									</a>
								</li>
								<li class="nav-item" data-toggle="modal" data-target="#contact-modal" >
									<a class="nav-link pc-friendly-table-inline">
									<i class="fa fa-envelope fa-2x"></i>
									<span class="nav_title">Contact Us</span>
									</a>									
								</li>
								<li class="nav-item" data-toggle="modal" data-target="#map-modal">
									<a  class="nav-link pc-friendly-table-inline">
									<i class="fa fa-map fa-2x"></i>
									<span class="nav_title">Map</span>
									</a>
								</li>
								<li class="nav-item" data-toggle="modal" data-target="#cancel-booking-modal" >
									<a class="nav-link pc-friendly-table-inline">
									<i class="fa fa-calendar fa-2x" ></i>
									<span class="nav_title">Cancel Booking</span>
									</a>
									
								</li>
								{{-- <li class="nav-item" style="display: contents;">
									<a class="nav-link pc-friendly-table-inline" style="margin-top: 18px;">
									<span class="nav_title">USD</span> 						
									<i class="fa fa-angle-down fa-2x"  id="cancel_booking_nav_icon"></i>
									</a>
								</li> --}}
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>

