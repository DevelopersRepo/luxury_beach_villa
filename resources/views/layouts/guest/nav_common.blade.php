<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">
        <div class="logo me-auto">
            <a href="{{  url('/')  }}"><img src="{{  asset('assets/website/assets/img/favicon.png') }}" alt="" class="img-fluid"></imh</a>
            <!-- Uncomment below if you prefer to use an image logo -->
        </div>
        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a class="nav-link scrollto active" href="{{  url('/') }}">Home</a></li>
                <li><a class="nav-link scrollto" href="{{  url('/t#activities') }}">Activities</a></li>
                <li><a class="nav-link scrollto" href="{{  url('/t#facilities') }}">Facilities</a></li>
                {{-- <li><a class="nav-link scrollto " href="{{  url('/test/view#accomodation') }}">Accomodation</a></li> --}}
                {{-- <li><a class="nav-link scrollto" href="{{  url('gallery') }}">Gallery</a></li> --}}
                <li><a class="nav-link scrollto" href="{{  url('/t#gallery') }}">Gallery</a></li>

                <li><a class="nav-link scrollto" href="{{  url('/#contacts') }}">Contacts</a></li>
                <li style="padding: 24px;">
                    <div class="btn-wrap">
                        <a href="{{  url('check_available') }}">
                        <button type="button" class="btn btn-primary">
                        Check Availability
                        </button>
                        </a>
                    </div>
                    {{-- <a class="nav-link scrollto" href="#contact">About Us</a> --}}
                </li>
            </ul>
            <i class="bi mobile-nav-toggle bi-list"></i>
        </nav>
        <!-- .navbar -->
    </div>
    </header>