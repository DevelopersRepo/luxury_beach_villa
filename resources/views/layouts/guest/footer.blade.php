

<!-- ======= Contact Section ======= -->
<div class="modal fade jd-modal" id="hotel-infos" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Airbnb Resort Information</h5>
				<button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button>
			</div>
			<div class="modal-body" >
				<div class="container">
					<ul class="nav nav-tabs">
						<li class="major_list active" onclick="add_Active_2('1')" id="menu_info11"><a data-toggle="tab" href="#menu_info1">Airbnb Resort Details & Policy</a></li>
						<li class="major_list" onclick="add_Active_2('2')" id="menu_info22"><a data-toggle="tab" href="#menu_info2">Airbnb Resort Gallery</a></li>
						<li class="major_list" onclick="add_Active_2('3')" id="menu_info33"><a data-toggle="tab" href="#menu_info3" >Airbnb Resort Map</a></li>
					</ul>
					<div class="tab-content">
						<div id="menu_info1" class="tab-pane active in"  style="padding-top: 20px">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								<div class="col-md-12">
									{{-- 
									<figure>
										<img src="{{  asset('assets/website/img/house800x600/20211229_121653.jpg')  }}" alt="" class="img-fluid">
									</figure>
									--}}
									<div class="">
										<h5>
											<strong>Luxury Beach Villa  </strong> 
											<small>
												<i class="fa fa-star staryellow"></i>
												<!--<i class="fa fa-star stargrey"></i>-->
												<i class="fa fa-star staryellow"></i>
												<!--<i class="fa fa-star stargrey"></i>-->
												<i class="fa fa-star staryellow"></i>
												<!--<i class="fa fa-star stargrey"></i>-->
											</small>
										</h5>
										<h7><strong>Address: </strong></h7>
										<br>
										Mbezi Beach White sands road, Dar es Salaam 
										<h7>
											<br>
											<strong>Phone: </strong>
										</h7>
										+255756883598  
										&nbsp;&nbsp;<strong>Reservation Phone : </strong></h7> +254 733 280 000  
										&nbsp;&nbsp;<strong>Email : </strong></h7> luxury@luxurybeachvilla.co.tz  
										&nbsp;&nbsp;<strong>Airbnb Resort Type : </strong></h7> Airbnb Resort
										<div class="col-md-12" style="
											border-bottom: 1px solid #ccc;margin-top: 10px;margin: auto;
											margin-top: auto;
											margin-bottom: auto;
											margin-top: 10px;
											margin-bottom: 10px;"></div>
										<div class="row" style="padding-left: 6px;">
											<div class="container">
												<h7><strong>Check-In Time: </strong></h7>
												<br><span style="font-size: 14px;
													color: #868080;">12:00 PM</span> 
											</div>
										</div>
										<br>
										<div class="row" style="padding-left: 6px;">
											<div class="container">
												<h7><strong>Check-Out Time: </strong></h7>
												<br><span style="font-size: 14px;
													color: #868080;">11:00 AM </span> 
											</div>
										</div>
										<br>
										<div class="row" style="padding-left: 6px;">
											<div class="container">
												<h7><strong>Airbnb Resort Information: </strong></h7>
												<br><span style="font-size: 14px;
													color: #868080;">Located in Mbezi Beach White Sands road, Dar es Salaam. </span> 
											</div>
										</div>
										<br>
									</div>
								</div>
							</div>
						</div>
						<div id="menu_info2" class="tab-pane fade"  style="padding-top: 20px">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								<!-- Carousel wrapper -->
								<div class="container">
									<div class="row" >
										@if(count($gallery)>0)
										@foreach($gallery as $data)
										<a style="padding: 10px;" href="{{ asset('/storage/uploads/'.$data->name) }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
										<img src="{{ asset('/storage/uploads/'.$data->name) }}" class="img-fluid rounded">
										</a>
										@endforeach
										@endif
									</div>
								</div>
							</div>
						</div>
						<div id="menu_info3" class="tab-pane fade" style="padding-top: 24px">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								@include('lib.embedded_map')


														</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- cancel booking modal --}}
<div class="modal cancel_modal_class"  id="cancel_booking_modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><strong>You've cancel the trip!.</strong></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="fddffsdfs" method="POST" action="{{ route('cancel_trip_description') }}">
				@csrf
				<div class="modal-body">
				<p>We’re sorry to hear that you wish to cancel your subscription. We have received your cancellation request and are processing it. If you don’t mind, could you please provide us with some feedback on why you chose to cancel your subscription?.</p>
			
					
					<div class="">
							
							<div class="" id="loginModal" style="
								background: white;">
								<div class="form-input" style="padding: 3px;">
									<input name="cancelled_booking_id" type="hidden" class="form-control" id="cancelled_booking_id">
									<input name="cancel_booking_description" type="text" class="form-control" id="cancel_booking_description" placeholder="Please say something.">
								</div>
							</div>

					</div>
				
				</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" style="background-color: #00BFFF;border-color: #ee505700;">Save changes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal" style="border-color: #ee505700;
				border-radius: 0;
				cursor: pointer;
				font-size: 0.7rem;
				font-weight: 600;
				padding: 13px 30px;
				text-transform: uppercase;">Close</button>
			</div>
		</form>

		</div>
	</div>
</div>
{{-- cancel booking modal ends here --}}
{{-- login modal --}}
<div class="modal fade jd-modal" id="login-modal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Login</h5>
				<button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button>
			</div>
			<div class="modal-body" >
				{{-- {{ Form::open('action'=> route('login'), 'method'=>'POST') }} --}}
				<form id="login_form_data" method="POST" action="{{ route('login') }}">
					@csrf
					<div class="container">
						<div class="col-md-8" style="margin: auto;
							text-shadow: none;">
							<div class="" id="loginModal" style="padding: 5px;
								background: white;">
								<div class="form-input" style="padding: 3px;">
									<label>Email Address:</label>
									<input name="email" type="email" class="form-control" id="email" placeholder="Email Address">
								</div>
								<div class="form-input" style="padding: 3px;">
									<label style="color: green;
										font-weight: bold;" class="success-alert">Checking...</label><br class="success-alert">
									<label style="color: green;
										font-weight: bold;" class="success-success">Success.</label><br class="success-success">
									<label style="color: red;
										font-weight: bold;" class="success-fail">Credentials are invalid!.</label><br class="success-fail">
									<label>Password:</label>
									<input name="password" type="password" class="form-control" id="password" placeholder="*******">
								</div>
								<div class="form-input" style="padding: 3px;">
									<a href="#"><label style="color: #00BFFF">Forget Password ?</label></a>
									{{-- <button  type="button" onclick="check_user()" class="btn btn-primary" style="background: #fcc51e; --}}
										<button  type="button" onclick="check_user()" class="btn btn-primary" style="background: #fcc51e;
										color: #614c00;
										width: 100%;
										padding: 9px;border-color: #6860603d;
										border-radius: 2px;" >Login</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				{{-- {{ Form::close() }} --}}
			</div>
		</div>
	</div>
</div>
{{-- login modal done --}}
{{-- contact page goes here --}}
<div class="modal fade jd-modal" id="contact-modal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Contact Us</h5>
				<button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button>
			</div>
			<div class="modal-body" >
				<div class="container">
					<div class="col-md-8" style="margin: auto;
						text-shadow: none;">
						<div class="" id="contact-form" style="padding: 5px;
							background: white;">
							<form id="feedback_form" action="{{  route('feedback.store') }}"  method="post" >
								@csrf
								<div class="form-input" style="padding: 3px;">
									<label>Name:</label>
									<input name="feedback_name" type="text" class="form-control" id="feedback_name" placeholder="Name">
								</div>
								<div class="form-input" style="padding: 3px;">
									<label>Phone:</label>
									<div class="" style="display: flex;">
										<select class="form-control" id="feedback_country_code" name="feedback_country_code" style="    margin-right: 5px;height: calc(2.25rem + 9px);width: 40%;">
											<option value="" selected="selected">Country Code</option>
											<option value="+995 44 +7 840, 940|Abkhazia">Abkhazia (+995 44 +7 840, 940)</option>
											<option value="+93|Afghanistan">Afghanistan (+93)</option>
											<option value="+355|Albania">Albania (+355)</option>
											<option value="+213|Algeria">Algeria (+213)</option>
											<option value="+1 684|American Samoa">American Samoa (+1 684)</option>
											<option value="+376|Andorra">Andorra (+376)</option>
											<option value="+244|Angola">Angola (+244)</option>
											<option value="+1 264|Anguilla">Anguilla (+1 264)</option>
											<option value="+1 268|Antigua and Barbuda">Antigua and Barbuda (+1 268)</option>
											<option value="+54|Argentina">Argentina (+54)</option>
											<option value="+374|Armenia">Armenia (+374)</option>
											<option value="+297|Aruba">Aruba (+297)</option>
											<option value="+247|Ascension Island">Ascension Island (+247)</option>
											<option value="+61|Australia">Australia (+61)</option>
											<option value="+672 1x|Australian Antarctic Territory">Australian Antarctic Territory (+672 1x)</option>
											<option value="+43|Austria">Austria (+43)</option>
											<option value="+994|Azerbaijan">Azerbaijan (+994)</option>
											<option value="+1 242|Bahamas">Bahamas (+1 242)</option>
											<option value="+973|Bahrain">Bahrain (+973)</option>
											<option value="+880|Bangladesh">Bangladesh (+880)</option>
											<option value="+1 246|Barbados">Barbados (+1 246)</option>
											<option value="+375|Belarus">Belarus (+375)</option>
											<option value="+32|Belgium">Belgium (+32)</option>
											<option value="+501|Belize">Belize (+501)</option>
											<option value="+229|Benin">Benin (+229)</option>
											<option value="+1 441|Bermuda">Bermuda (+1 441)</option>
											<option value="+975|Bhutan">Bhutan (+975)</option>
											<option value="+591|Bolivia">Bolivia (+591)</option>
											<option value="+599 7|Bonaire">Bonaire (+599 7)</option>
											<option value="+387|Bosnia and Herzegovina">Bosnia and Herzegovina (+387)</option>
											<option value="+267|Botswana">Botswana (+267)</option>
											<option value="+55|Brazil">Brazil (+55)</option>
											<option value="+246|British Indian Ocean Territory">British Indian Ocean Territory (+246)</option>
											<option value="+1 284|British Virgin Islands">British Virgin Islands (+1 284)</option>
											<option value="+673|Brunei">Brunei (+673)</option>
											<option value="+359|Bulgaria">Bulgaria (+359)</option>
											<option value="+226|Burkina Faso">Burkina Faso (+226)</option>
											<option value="+257|Burundi">Burundi (+257)</option>
											<option value="+855|Cambodia">Cambodia (+855)</option>
											<option value="+237|Cameroon">Cameroon (+237)</option>
											<option value="+1|Canada">Canada (+1)</option>
											<option value="+238|Cape Verde">Cape Verde (+238)</option>
											<option value="+1 345|Cayman Islands">Cayman Islands (+1 345)</option>
											<option value="+236|Central African Republic">Central African Republic (+236)</option>
											<option value="+235|Chad">Chad (+235)</option>
											<option value="+56|Chile">Chile (+56)</option>
											<option value="+86|China">China (+86)</option>
											<option value="+61 8 9164|Christmas Island">Christmas Island (+61 8 9164)</option>
											<option value="+61 8 9162|Cocos Islands">Cocos Islands (+61 8 9162)</option>
											<option value="+57|Colombia">Colombia (+57)</option>
											<option value="+682|Cook Islands">Cook Islands (+682)</option>
											<option value="+506|Costa Rica">Costa Rica (+506)</option>
											<option value="+225|Côte d" ivoire'="">Côte d'Ivoire (+225)</option>
											<option value="+385|Croatia">Croatia (+385)</option>
											<option value="+53|Cuba">Cuba (+53)</option>
											<option value="+599 9|Curacao">Curacao (+599 9)</option>
											<option value="+357|Cyprus">Cyprus (+357)</option>
											<option value="+420|Czech Republic">Czech Republic (+420)</option>
											<option value="+243|Democratic Republic of the Congo">Democratic Republic of the Congo (+243)</option>
											<option value="+45|Denmark">Denmark (+45)</option>
											<option value="+253|Djibouti">Djibouti (+253)</option>
											<option value="+1 767|Dominica">Dominica (+1 767)</option>
											<option value="+1 809 / 829 / 849|Dominican Republic">Dominican Republic (+1 809 / 829 / 849)</option>
											<option value="+670|East Timor">East Timor (+670)</option>
											<option value="+593|Ecuador">Ecuador (+593)</option>
											<option value="+20|Egypt">Egypt (+20)</option>
											<option value="+503|El Salvador">El Salvador (+503)</option>
											<option value="+240|Equatorial Guinea">Equatorial Guinea (+240)</option>
											<option value="+291|Eritrea">Eritrea (+291)</option>
											<option value="+372|Estonia">Estonia (+372)</option>
											<option value="+251|Ethiopia">Ethiopia (+251)</option>
											<option value="+500|Falkland Islands">Falkland Islands (+500)</option>
											<option value="+298|Faroe Islands">Faroe Islands (+298)</option>
											<option value="+691|Federated States of Micronesia">Federated States of Micronesia (+691)</option>
											<option value="+679|Fiji">Fiji (+679)</option>
											<option value="+358|Finland">Finland (+358)</option>
											<option value="+33|France">France (+33)</option>
											<option value="+594|French Guiana">French Guiana (+594)</option>
											<option value="+689|French Polynesia">French Polynesia (+689)</option>
											<option value="+241|Gabon">Gabon (+241)</option>
											<option value="+220|Gambia">Gambia (+220)</option>
											<option value="+995|Georgia">Georgia (+995)</option>
											<option value="+49|Germany">Germany (+49)</option>
											<option value="+233|Ghana">Ghana (+233)</option>
											<option value="+350|Gibraltar">Gibraltar (+350)</option>
											<option value="+881|Global Mobile Satellite System">Global Mobile Satellite System (+881)</option>
											<option value="+30|Greece">Greece (+30)</option>
											<option value="+299|Greenland">Greenland (+299)</option>
											<option value="+1 473|Grenada">Grenada (+1 473)</option>
											<option value="+590|Guadeloupe">Guadeloupe (+590)</option>
											<option value="+1 671|Guam">Guam (+1 671)</option>
											<option value="+502|Guatemala">Guatemala (+502)</option>
											<option value="+44 1481|Guernsey">Guernsey (+44 1481)</option>
											<option value="+224|Guinea">Guinea (+224)</option>
											<option value="+245|Guinea-Bissau">Guinea-Bissau (+245)</option>
											<option value="+592|Guyana">Guyana (+592)</option>
											<option value="+509|Haiti">Haiti (+509)</option>
											<option value="+504|Honduras">Honduras (+504)</option>
											<option value="+852|Hong Kong">Hong Kong (+852)</option>
											<option value="+36|Hungary">Hungary (+36)</option>
											<option value="+354|Iceland">Iceland (+354)</option>
											<option value="+91|India">India (+91)</option>
											<option value="+62|Indonesia">Indonesia (+62)</option>
											<option value="+800|International Freephone UIFN">International Freephone UIFN (+800)</option>
											<option value="+979|International Premium Rate Service">International Premium Rate Service (+979)</option>
											<option value="+98|Iran">Iran (+98)</option>
											<option value="+964|Iraq">Iraq (+964)</option>
											<option value="+353|Ireland">Ireland (+353)</option>
											<option value="+44 1624|Isle of Man">Isle of Man (+44 1624)</option>
											<option value="+972|Israel">Israel (+972)</option>
											<option value="+39|Italy">Italy (+39)</option>
											<option value="+1 876|Jamaica">Jamaica (+1 876)</option>
											<option value="+81|Japan">Japan (+81)</option>
											<option value="+44 1534|Jersey">Jersey (+44 1534)</option>
											<option value="+962|Jordan">Jordan (+962)</option>
											<option value="+7 6xx, 7xx|Kazakhstan">Kazakhstan (+7 6xx, 7xx)</option>
											<option value="+254|Kenya">Kenya (+254)</option>
											<option value="+686|Kiribati">Kiribati (+686)</option>
											<option value="+383|Kosovo">Kosovo (+383)</option>
											<!--Prashant - 12/08/2021 - purpose: change country code [RES-2866] -->
											<option value="+965|Kuwait">Kuwait (+965)</option>
											<option value="+996|Kyrgyzstan">Kyrgyzstan (+996)</option>
											<option value="+856|Laos">Laos (+856)</option>
											<option value="+371|Latvia">Latvia (+371)</option>
											<option value="+961|Lebanon">Lebanon (+961)</option>
											<option value="+266|Lesotho">Lesotho (+266)</option>
											<option value="+231|Liberia">Liberia (+231)</option>
											<option value="+218|Libya">Libya (+218)</option>
											<option value="+423|Liechtenstein">Liechtenstein (+423)</option>
											<option value="+370|Lithuania">Lithuania (+370)</option>
											<option value="+352|Luxembourg">Luxembourg (+352)</option>
											<option value="+853|Macau">Macau (+853)</option>
											<option value="+389|Macedonia">Macedonia (+389)</option>
											<option value="+261|Madagascar">Madagascar (+261)</option>
											<option value="+265|Malawi">Malawi (+265)</option>
											<option value="+60|Malaysia">Malaysia (+60)</option>
											<option value="+960|Maldives">Maldives (+960)</option>
											<option value="+223|Mali">Mali (+223)</option>
											<option value="+356|Malta">Malta (+356)</option>
											<option value="+692|Marshall Islands">Marshall Islands (+692)</option>
											<option value="+596|Martinique">Martinique (+596)</option>
											<option value="+222|Mauritania">Mauritania (+222)</option>
											<option value="+230|Mauritius">Mauritius (+230)</option>
											<option value="+262 269 / 639|Mayotte">Mayotte (+262 269 / 639)</option>
											<option value="+52|Mexico">Mexico (+52)</option>
											<option value="+373|Moldova">Moldova (+373)</option>
											<option value="+377|Monaco">Monaco (+377)</option>
											<option value="+976|Mongolia">Mongolia (+976)</option>
											<option value="+382|Montenegro">Montenegro (+382)</option>
											<option value="+1 664|Montserrat">Montserrat (+1 664)</option>
											<option value="+212|Morocco">Morocco (+212)</option>
											<option value="+258|Mozambique">Mozambique (+258)</option>
											<option value="+95|Myanmar">Myanmar (+95)</option>
											<option value="+374 47 / 97|Nagorno-Karabakh">Nagorno-Karabakh (+374 47 / 97)</option>
											<option value="+264|Namibia">Namibia (+264)</option>
											<option value="+674|Nauru">Nauru (+674)</option>
											<option value="+977|Nepal">Nepal (+977)</option>
											<option value="+31|Netherlands">Netherlands (+31)</option>
											<option value="+687|New Caledonia">New Caledonia (+687)</option>
											<option value="+64|New Zealand">New Zealand (+64)</option>
											<option value="+505|Nicaragua">Nicaragua (+505)</option>
											<option value="+227|Niger">Niger (+227)</option>
											<option value="+234|Nigeria">Nigeria (+234)</option>
											<option value="+683|Niue">Niue (+683)</option>
											<option value="+672 3|Norfolk Island">Norfolk Island (+672 3)</option>
											<option value="+850|North Korea">North Korea (+850)</option>
											<option value="+1 670|Northern Mariana Islands">Northern Mariana Islands (+1 670)</option>
											<option value="+47|Norway">Norway (+47)</option>
											<option value="+968|Oman">Oman (+968)</option>
											<option value="+92|Pakistan">Pakistan (+92)</option>
											<option value="+680|Palau">Palau (+680)</option>
											<option value="+970|Palestinian territories">Palestinian territories (+970)</option>
											<option value="+507|Panama">Panama (+507)</option>
											<option value="+675|Papua New Guinea">Papua New Guinea (+675)</option>
											<option value="+595|Paraguay">Paraguay (+595)</option>
											<option value="+51|Peru">Peru (+51)</option>
											<option value="+63|Philippines">Philippines (+63)</option>
											<option value="+48|Poland">Poland (+48)</option>
											<option value="+351|Portugal">Portugal (+351)</option>
											<option value="+1 787 / 939|Puerto Rico">Puerto Rico (+1 787 / 939)</option>
											<option value="+974|Qatar">Qatar (+974)</option>
											<!--<option value='+886|Republic of China (Taiwan)'>Republic of China (Taiwan) (+886)</option>-->
											<option value="+242|Republic of the Congo">Republic of the Congo (+242)</option>
											<option value="+262|Réunion">Réunion (+262)</option>
											<option value="+40|Romania">Romania (+40)</option>
											<option value="+7|Russia">Russia (+7)</option>
											<option value="+250|Rwanda">Rwanda (+250)</option>
											<option value="+599 4|Saba">Saba (+599 4)</option>
											<option value="+290|Saint Helena">Saint Helena (+290)</option>
											<option value="+1 869|Saint Kitts and Nevis">Saint Kitts and Nevis (+1 869)</option>
											<option value="+1 758|Saint Lucia">Saint Lucia (+1 758)</option>
											<option value="+1 784|Saint Vincent and the Grenadines">Saint Vincent and the Grenadines (+1 784)</option>
											<option value="+508|Saint-Pierre and Miquelon">Saint-Pierre and Miquelon (+508)</option>
											<option value="+685|Samoa">Samoa (+685)</option>
											<option value="+378|San Marino">San Marino (+378)</option>
											<option value="+239|Sao Tome and Principe">Sao Tome and Principe</option>
											<option value="+966|Saudi Arabia">Saudi Arabia (+966)</option>
											<option value="+221|Senegal">Senegal (+221)</option>
											<option value="+381|Serbia">Serbia (+381)</option>
											<option value="+248|Seychelles">Seychelles (+248)</option>
											<option value="+232|Sierra Leone">Sierra Leone (+232)</option>
											<option value="+65|Singapore">Singapore (+65)</option>
											<option value="+599 3|Sint Eustatius">Sint Eustatius (+599 3)</option>
											<option value="+599 5|Sint Maarten">Sint Maarten (+599 5)</option>
											<option value="+421|Slovakia">Slovakia (+421)</option>
											<option value="+386|Slovenia">Slovenia (+386)</option>
											<option value="+677|Solomon Islands">Solomon Islands (+677)</option>
											<option value="+252|Somalia">Somalia (+252)</option>
											<option value="+27|South Africa">South Africa (+27)</option>
											<option value="+82|South Korea">South Korea (+82)</option>
											<option value="+211|South Sudan">South Sudan (+211)</option>
											<option value="+34|Spain">Spain (+34)</option>
											<option value="+94|Sri Lanka">Sri Lanka (+94)</option>
											<option value="+249|Sudan">Sudan (+249)</option>
											<option value="+597|Suriname">Suriname (+597)</option>
											<option value="+268|Swaziland">Swaziland (+268)</option>
											<option value="+46|Sweden">Sweden (+46)</option>
											<option value="+41|Switzerland">Switzerland (+41)</option>
											<option value="+963|Syria">Syria (+963)</option>
											<option value="+886|Taiwan">Taiwan (+886)</option>
											<!--Parth - 30 Apr 2019 - RES-1963 - Add Taiwan Country code.-->
											<option value="+992|Tajikistan">Tajikistan (+992)</option>
											<option value="+255|Tanzania">Tanzania (+255)</option>
											<option value="+888|Telecommunications for Disaster Relief by OCHA">Telecommunications for Disaster Relief by OCHA (+888)</option>
											<option value="+66|Thailand">Thailand (+66)</option>
											<option value="+228|Togo">Togo (+228)</option>
											<option value="+690|Tokelau">Tokelau (+690)</option>
											<option value="+676|Tonga">Tonga (+676)</option>
											<option value="+1 868|Trinidad and Tobago">Trinidad and Tobago (+1 868)</option>
											<option value="+290 8|Tristan da Cunha">Tristan da Cunha (+290 8)</option>
											<option value="+216|Tunisia">Tunisia (+216)</option>
											<option value="+90|Turkey">Turkey (+90)</option>
											<option value="+993|Turkmenistan">Turkmenistan (+993)</option>
											<option value="+1 649|Turks and Caicos Islands">Turks and Caicos Islands (+1 649)</option>
											<option value="+688|Tuvalu">Tuvalu (+688)</option>
											<option value="+1 340|U.S. Virgin Islands">U.S. Virgin Islands (+1 340)</option>
											<option value="+256|Uganda">Uganda (+256)</option>
											<option value="+380|Ukraine">Ukraine (+380)</option>
											<option value="+971|United Arab Emirates">United Arab Emirates (+971)</option>
											<option value="+44|United Kingdom">United Kingdom (+44)</option>
											<option value="+1|United States of America">United States of America (+1)</option>
											<option value="+598|Uruguay">Uruguay (+598)</option>
											<option value="+998|Uzbekistan">Uzbekistan (+998)</option>
											<option value="+678|Vanuatu">Vanuatu (+678)</option>
											<option value="+39 06 698|Vatican City">Vatican City (+39 06 698)</option>
											<option value="+58|Venezuela">Venezuela (+58)</option>
											<option value="+84|Vietnam">Vietnam (+84)</option>
											<option value="+681|Wallis and Futuna">Wallis and Futuna (+681)</option>
											<option value="+212 5288 / 5289|Western Sahara">Western Sahara (+212 5288 / 5289)</option>
											<option value="+967|Yemen">Yemen (+967)</option>
											<option value="+260|Zambia">Zambia (+260)</option>
											<option value="+255 24|Zanzibar">Zanzibar (+255 24)</option>
											<option value="+263|Zimbabwe">Zimbabwe (+263)</option>
										</select>
										<input name="feedback_phone" type="number" class="form-control" id="feedback_phone" placeholder="Phone No">
									</div>
								</div>
								<div class="form-input" style="padding: 3px;">
									<label>Email Address:</label>
									<input name="feedback_email" type="email" class="form-control" id="feedback_email" placeholder="Email">
								</div>
								<div class="form-input" style="padding: 3px;">
									<label>Subject:</label>
									<input name="subject" type="text" required class="form-control" id="subject" placeholder="Subject">
								</div>
								{{-- 
								<div class="form-input" style="padding: 3px;">
									<input name="email"  class="form-control" placeholder="Recaptcha">
								</div>
								--}}
								<div class="form-input" style="padding: 3px;">
									<label>Message:</label>
									<textarea name="feedback_message" placeholder="Message"  class="form-control" id="feedback_message"></textarea>
								</div>
								<div class="form-input" style="padding: 3px;">
									<button onclick="send_feedback()" type="button"  class="btn btn-primary" style="background: #fcc51e;
										color: #614c00;
										width: 100%;
										padding: 9px;border-color: #6860603d;
										border-radius: 2px;" >Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- contact page done here --}}
{{-- map goes here --}}
<div class="modal fade jd-modal" id="map-modal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Our Map</h5>
				<button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button>
			</div>
			<div class="modal-body" >
				<div class="container">
					<div class="col-md-12" style="margin: auto;
						text-shadow: none;">
						<div class="" id="map-container" style="padding: 5px;
							background: white;">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								{{-- new --}}

								@include('lib.embedded_map')
								{{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.6000988917567!2d39.222497314771715!3d-6.696349995158616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0!2zNsKwNDEnNDYuOSJTIDM5wrAxMycyOC45IkU!5e0!3m2!1sen!2stz!4v1647865919144!5m2!1sen!2stz" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- map ends here --}}
{{-- cancel booking --}}
<div class="modal fade jd-modal" id="cancel-booking-modal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Cancel Booking</h5>
				<button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button>
			</div>
			<div class="modal-body" >
				<div class="container">
					<div class="col-md-8" style="margin: auto;
						text-shadow: none;">
						<div class="" id="map-container" style="padding: 5px;
							background: white;">
							<form method="POST" action="{{  route('cancel_booking')  }}" id="cancel_booking_form">
								@csrf
								<div class="" id="cancel_booking_container" style="padding: 5px;
									background: white;">
									<div class="form-input" style="padding: 3px;">
										<label>Reservation Number: </label>
										<input name="cancel_number_reserve" type="text" class="form-control" id="cancel_number_reserve" placeholder="Enter Reservation Number">
									</div>
									<div class="form-input" style="padding: 3px;">
										<label>Email Address: </label>
										<input name="cancel_email_reserve" type="email" class="form-control" id="cancel_email_reserve" placeholder="Enter Email">
									</div>
									<div class="form-input" style="padding: 3px;">
										<button name="email" onclick="cancel_booking()" type="button" class="btn btn-primary" id="email" style="background: #fcc51e;
											color: #614c00;
											width: 100%;
											padding: 9px;border-color: #6860603d;
											border-radius: 2px;" >Cancel Booking</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
{{-- done cancel booking --}}
<footer style="display: block;" id="footer_main" style="padding-right:40px;">
	<div class="" style="padding-right: 25px;">
		<div class="">
			<!--Priyanka Maurya - 04 Sep 2018 - Purpose : Hide the OEMLogo In epayment page - HideSocialLinkePayment -->
			<div style="padding-left: 21px;">
				<img style="max-height: 83px;" src="{{  asset('/assets/website/img/favicon.png') }}" alt="">
				{{-- <img style="display: " src="https://d1k7zr0dliejeg.cloudfront.net/booking/hotelsonline_logo.png" alt="HotelOnline" height="30"> <span style="display: ">HotelOnline</span> <!--Pratik Metaliya -1.0.53.61- -08-12-2017- Purpose: get oem logo and favicon from amazon s3 bucket--> --}}
				<!--<a class=oemlogo style="display: " href="http://www.hotelonline.co" target='_blank'>
					<img src="https://d1k7zr0dliejeg.cloudfront.net/booking/hotelsonline_logo.png" alt="HotelOnline" height="30"/> //Pratik Metaliya -1.0.53.61- -08-12-2017- Purpose: get oem logo and favicon from amazon s3 bucket
					</a>--> 
				<!--<p>HotelOnline &copy; 2022. All Rights Reserved.</p>-->
				<!--Priyanka Maurya - 04 Sep 2018 - Purpose : Hide the Social Media Link In Epayment Page - HideSocialLinkePayment-->
				<div class="vres_social" style="display: inline-flex;padding-top: 26px;display: -webkit-box;">
					<span style="color: white;">Share this Airbnb Resort &nbsp;<em class="fa fa-angle-right"></em></span>
					<div class="addthis_toolbox addthis_default_style addthis_20x20_style">
						<a class="addthis_button_facebook at300b" title="Facebook" href="#">
							<span class="at-icon-wrapper" style="height: 27px;width: 50px;background-color: rgb(59, 89, 152); line-height: 20px; ">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" class="at-icon at-icon-facebook" style="width: 20px; height: 20px;" title="Facebook" alt="Facebook">
									<title id="at-svg-facebook-1">Facebook</title>
									<g>
										<path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>
									</g>
								</svg>
							</span>
						</a>
						<a class="addthis_button_twitter at300b" title="Twitter" href="#">
							<span class="at-icon-wrapper" style="height: 27px;width: 50px;background-color: rgb(29, 161, 242); line-height: 20px; ">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" class="at-icon at-icon-twitter" style="width: 20px; height: 20px;" title="Twitter" alt="Twitter">
									<title id="at-svg-twitter-2">Twitter</title>
									<g>
										<path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path>
									</g>
								</svg>
							</span>
						</a>
						<a class="addthis_button_linkedin at300b" target="_blank" title="LinkedIn" href="#">
							<span class="at-icon-wrapper" style="height: 27px;width: 50px;background-color: rgb(0, 119, 181); line-height: 20px; ">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-linkedin-3" class="at-icon at-icon-linkedin" style="width: 20px; height: 20px;" title="LinkedIn" alt="LinkedIn">
									<title id="at-svg-linkedin-3">LinkedIn</title>
									<g>
										<path d="M26 25.963h-4.185v-6.55c0-1.56-.027-3.57-2.175-3.57-2.18 0-2.51 1.7-2.51 3.46v6.66h-4.182V12.495h4.012v1.84h.058c.558-1.058 1.924-2.174 3.96-2.174 4.24 0 5.022 2.79 5.022 6.417v7.386zM8.23 10.655a2.426 2.426 0 0 1 0-4.855 2.427 2.427 0 0 1 0 4.855zm-2.098 1.84h4.19v13.468h-4.19V12.495z" fill-rule="evenodd"></path>
									</g>
								</svg>
							</span>
						</a>
						<a class="addthis_button_email at300b" target="_blank" title="Email" href="#">
							<span class="at-icon-wrapper" style="height: 27px;width: 50px;background-color: rgb(132, 132, 132); line-height: 20px; ">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-email-4" class="at-icon at-icon-email" style="width: 20px; height: 20px;" title="Email" alt="Email">
									<title id="at-svg-email-4">Email</title>
									<g>
										<g fill-rule="evenodd"></g>
										<path d="M27 22.757c0 1.24-.988 2.243-2.19 2.243H7.19C5.98 25 5 23.994 5 22.757V13.67c0-.556.39-.773.855-.496l8.78 5.238c.782.467 1.95.467 2.73 0l8.78-5.238c.472-.28.855-.063.855.495v9.087z"></path>
										<path d="M27 9.243C27 8.006 26.02 7 24.81 7H7.19C5.988 7 5 8.004 5 9.243v.465c0 .554.385 1.232.857 1.514l9.61 5.733c.267.16.8.16 1.067 0l9.61-5.733c.473-.283.856-.96.856-1.514v-.465z"></path>
									</g>
								</svg>
							</span>
						</a>
						<a class="instagram-root at300b" href="https://www.instagram.com/luxurybeachvillainn/" target="_blank"   title="Instagram">
							<span class="at-icon-wrapper" style="height: 27px;width: 50px;background-color: rgb(132, 132, 132); line-height: 20px; ">
								<svg style="height: 16px;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 132.004 132">
									<defs>
										<linearGradient id="b">
											<stop offset="0" stop-color="#3771c8"/>
											<stop stop-color="#3771c8" offset=".128"/>
											<stop offset="1" stop-color="#60f" stop-opacity="0"/>
										</linearGradient>
										<linearGradient id="a">
											<stop offset="0" stop-color="#fd5"/>
											<stop offset=".1" stop-color="#fd5"/>
											<stop offset=".5" stop-color="#ff543e"/>
											<stop offset="1" stop-color="#c837ab"/>
										</linearGradient>
										<radialGradient id="c" cx="158.429" cy="578.088" r="65" xlink:href="#a" gradientUnits="userSpaceOnUse" gradientTransform="matrix(0 -1.98198 1.8439 0 -1031.402 454.004)" fx="158.429" fy="578.088"/>
										<radialGradient id="d" cx="147.694" cy="473.455" r="65" xlink:href="#b" gradientUnits="userSpaceOnUse" gradientTransform="matrix(.17394 .86872 -3.5818 .71718 1648.348 -458.493)" fx="147.694" fy="473.455"/>
									</defs>
									<path fill="url(#c)" d="M65.03 0C37.888 0 29.95.028 28.407.156c-5.57.463-9.036 1.34-12.812 3.22-2.91 1.445-5.205 3.12-7.47 5.468C4 13.126 1.5 18.394.595 24.656c-.44 3.04-.568 3.66-.594 19.188-.01 5.176 0 11.988 0 21.125 0 27.12.03 35.05.16 36.59.45 5.42 1.3 8.83 3.1 12.56 3.44 7.14 10.01 12.5 17.75 14.5 2.68.69 5.64 1.07 9.44 1.25 1.61.07 18.02.12 34.44.12 16.42 0 32.84-.02 34.41-.1 4.4-.207 6.955-.55 9.78-1.28 7.79-2.01 14.24-7.29 17.75-14.53 1.765-3.64 2.66-7.18 3.065-12.317.088-1.12.125-18.977.125-36.81 0-17.836-.04-35.66-.128-36.78-.41-5.22-1.305-8.73-3.127-12.44-1.495-3.037-3.155-5.305-5.565-7.624C116.9 4 111.64 1.5 105.372.596 102.335.157 101.73.027 86.19 0H65.03z" transform="translate(1.004 1)"/>
									<path fill="url(#d)" d="M65.03 0C37.888 0 29.95.028 28.407.156c-5.57.463-9.036 1.34-12.812 3.22-2.91 1.445-5.205 3.12-7.47 5.468C4 13.126 1.5 18.394.595 24.656c-.44 3.04-.568 3.66-.594 19.188-.01 5.176 0 11.988 0 21.125 0 27.12.03 35.05.16 36.59.45 5.42 1.3 8.83 3.1 12.56 3.44 7.14 10.01 12.5 17.75 14.5 2.68.69 5.64 1.07 9.44 1.25 1.61.07 18.02.12 34.44.12 16.42 0 32.84-.02 34.41-.1 4.4-.207 6.955-.55 9.78-1.28 7.79-2.01 14.24-7.29 17.75-14.53 1.765-3.64 2.66-7.18 3.065-12.317.088-1.12.125-18.977.125-36.81 0-17.836-.04-35.66-.128-36.78-.41-5.22-1.305-8.73-3.127-12.44-1.495-3.037-3.155-5.305-5.565-7.624C116.9 4 111.64 1.5 105.372.596 102.335.157 101.73.027 86.19 0H65.03z" transform="translate(1.004 1)"/>
									<path fill="#fff" d="M66.004 18c-13.036 0-14.672.057-19.792.29-5.11.234-8.598 1.043-11.65 2.23-3.157 1.226-5.835 2.866-8.503 5.535-2.67 2.668-4.31 5.346-5.54 8.502-1.19 3.053-2 6.542-2.23 11.65C18.06 51.327 18 52.964 18 66s.058 14.667.29 19.787c.235 5.11 1.044 8.598 2.23 11.65 1.227 3.157 2.867 5.835 5.536 8.503 2.667 2.67 5.345 4.314 8.5 5.54 3.054 1.187 6.543 1.996 11.652 2.23 5.12.233 6.755.29 19.79.29 13.037 0 14.668-.057 19.788-.29 5.11-.234 8.602-1.043 11.656-2.23 3.156-1.226 5.83-2.87 8.497-5.54 2.67-2.668 4.31-5.346 5.54-8.502 1.18-3.053 1.99-6.542 2.23-11.65.23-5.12.29-6.752.29-19.788 0-13.036-.06-14.672-.29-19.792-.24-5.11-1.05-8.598-2.23-11.65-1.23-3.157-2.87-5.835-5.54-8.503-2.67-2.67-5.34-4.31-8.5-5.535-3.06-1.187-6.55-1.996-11.66-2.23-5.12-.233-6.75-.29-19.79-.29zm-4.306 8.65c1.278-.002 2.704 0 4.306 0 12.816 0 14.335.046 19.396.276 4.68.214 7.22.996 8.912 1.653 2.24.87 3.837 1.91 5.516 3.59 1.68 1.68 2.72 3.28 3.592 5.52.657 1.69 1.44 4.23 1.653 8.91.23 5.06.28 6.58.28 19.39s-.05 14.33-.28 19.39c-.214 4.68-.996 7.22-1.653 8.91-.87 2.24-1.912 3.835-3.592 5.514-1.68 1.68-3.275 2.72-5.516 3.59-1.69.66-4.232 1.44-8.912 1.654-5.06.23-6.58.28-19.396.28-12.817 0-14.336-.05-19.396-.28-4.68-.216-7.22-.998-8.913-1.655-2.24-.87-3.84-1.91-5.52-3.59-1.68-1.68-2.72-3.276-3.592-5.517-.657-1.69-1.44-4.23-1.653-8.91-.23-5.06-.276-6.58-.276-19.398s.046-14.33.276-19.39c.214-4.68.996-7.22 1.653-8.912.87-2.24 1.912-3.84 3.592-5.52 1.68-1.68 3.28-2.72 5.52-3.592 1.692-.66 4.233-1.44 8.913-1.655 4.428-.2 6.144-.26 15.09-.27zm29.928 7.97c-3.18 0-5.76 2.577-5.76 5.758 0 3.18 2.58 5.76 5.76 5.76 3.18 0 5.76-2.58 5.76-5.76 0-3.18-2.58-5.76-5.76-5.76zm-25.622 6.73c-13.613 0-24.65 11.037-24.65 24.65 0 13.613 11.037 24.645 24.65 24.645C79.617 90.645 90.65 79.613 90.65 66S79.616 41.35 66.003 41.35zm0 8.65c8.836 0 16 7.163 16 16 0 8.836-7.164 16-16 16-8.837 0-16-7.164-16-16 0-8.837 7.163-16 16-16z"/>
								</svg>
							</span>
						</a>
						<a class="addthis_button_compact at300m" href="#">
							<span class="at-icon-wrapper" style="height: 27px;width: 50px;background-color: rgb(255, 101, 80); line-height: 20px; ">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5" class="at-icon at-icon-addthis" style="width: 20px; height: 20px;" title="More" alt="More">
									<title id="at-svg-addthis-5">AddThis</title>
									<g>
										<path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path>
									</g>
								</svg>
							</span>
						</a>
						<a class="addthis_button_pinterest at300b"></a>
						{{-- <a class="addthis_counter addthis_bubble_style" style="display: inline-block;" href="#"></a></a>		 --}}
						<div class="atclear"></div>
					</div>
					<script defer="" type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-511347aa39e0cb0d#async=1"></script> <!--Pinal - 27 March 2018 - Purpose : Booking Engine Optimization , added defer -->                 
					<div id="fb-root" class="fb_root fb_reset">
						<div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
							<div></div>
						</div>
					</div>
					<!--Pinal - 1.0.49.54 - 17 June 2016 - Purpose - Instagram Badge -->
					{{-- 
					<div class="instagram-root">
						<a href="https://www.instagram.com/HotelOnline International/?ref=badge" target="_blank" class="insta-click" style="padding: 0px;" title="Instagram">
							<!--<img src="/booking/templates/resui/img/instacon.png" />-->
							<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAADAFBMVEUAAAD/2365MqvYL4FOY9JXXs9wUcd7ScXNLZbTLo3XL4X+13raMXbZK3qSOL7+2DxiaOiBPKzqcj1iWspoV8mKOqXCMKKXOJ3HL52iNZavNorAQnf+znH9xWnPUGT9uGD7p1fbXVT5klD0eEzkaEh3O7XvfjftW02CQsVLZdPULX+sMrTnRVn+1nnZLXrxiDdsObmFPsSWOL9IZtT9u1BVWNGfNbn9zGyKOsT3qElJZNhiR8z25/H99ff64uz++fnpmb7//////fz98fP/9O/86+n/7+P+6eJWVcvxscf/3avMO4u6JYTdLHn+yF38qkj4dTf3hC/3ayX69vv47PT55O365+zx0OTmvuPTvOP53ODwydz+5tu5l9bDl9XttNDllcZkRcTglsHrlrzCgbtiPLtqO7hxObikNrj4t7WTL7V5NrGILLCiLa34tar+0aW2LKXwqqK6Pp60P5u4PJncWpD4qo7IHYupJonFOIixJYi8H4G1GYDaMnvTHnnhN3jXHXLgMW/bKG/dKm3nRWvPFmb+y2TfHmPmP1vZGVrlLlfxXVP9uU/9s0n0a0TuSUP8mULlLz76jTr6lzPxWzDtSDD47/T55u/+7+7719r2xdfvvNTHlNPIkNKoiNL93NHhpND5yMzCgMyMZ8f+58XyuMXfjMXPlMRURsL93cHXl8HDh8HypMD0ub/Vir+1Xb6XN75ZQL2YOLvfm7nOY7bri7SlY7R+QrTLcbJuMLKuNbF/MbHQcrDnoq+zP62sKal9NKf/3aKsSqJ/LaCvRZ/8u578wZ25IpzmY5i0RpeMKJW/HpTtdY+9P46bJo3VOobRPISaF4TyfoLHPoL+z4HOHYHMInv7qHXkP3TXJ3S2KXP8r3H0g3HZHnHqWm/URm7OEmzEGWrvb2nkNWHWFGH+v2D2ll/gU13IOF3UMF3+wFbpY1PdGVPsSE3WRkvgQUrwcUnqOEn1fUP7nkL4cDvhVDvnJDvqODXpYC72Yyvvbif0UCP6fB/5chn4aBb5bgyZnpEAAAAAPHRSTlMA4+Tk4+Pj4+Pj4+GMGxUUEOTk4+Pj4+Pj4+Pj4+Pj4+Pj4+Pj39/e3NvV08vJx8fFwL69u7SIg399eHibd+7pAAABr0lEQVQY0wGkAVv+AAAQOjMpBAQFBRMUBgYHBygxOA4AABA1SEhIqLFlZmdru2xsamqysDIOADthYWVmo55dXV5enJ2ir21taGg2ADBnubdZU1Q8PDw8PDw8U1i+v7orACVrwFk9ZMLEc3N0cnJ0uD1YcHACABHDtkFk0Xh6fExMfHugrrxCtMcCABXKq5bJekx5YldXYkubX0tUpssWABfOpzzNe3lbP19JPVvQ1kuXYHcYABl4qlXT4UBCdYWFdT9A4M9VYHcIABras1bf6VqliYOC5Ek+2X0+QNUIABvovUXn71qtkYjj7Ek+gn0+QH4JAB7ucUXrlW4/4veR3j2siX8+Y34JACH1cUXwlI6fPWlpQ5mEh39WY4AKACT4dkTyUlJQxVxcbovxithDtYAKABL65kHX//z9/lL5+5WP9sxByN0DACZRUalE25CQklFQ9I6L0kOag00DAC+Tk0+kQkRGRkZHR0eYP6GEh00qADlPjY2M1MFKSkpvb8Z23O2KgU0MAA805YaGTk5O6oxP85JQlI+IgS4NAAAPNy0LAQEBCxwdHyAiIycsDA0AE02tyQbXaaAAAAAASUVORK5CYII=" alt="Instagram"> <!--Pinal - 6 April 2018 - Purpose : Booking Engine Optimization--> <!-- Pinal - 25 March 2020 - Purpose : ADA compliance -->
						</a>
					</div>
					--}}
					<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
						fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
					</script>
					{{-- 
					<div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/hotelonlineinternational?_rdc=4&amp;_rdr" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=326&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhotelonlineinternational%3F_rdc%3D4%26_rdr&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=true"><span style="vertical-align: bottom; width: 90px; height: 28px;"><iframe name="f364cf6eb504b5" data-testid="fb:like Facebook Social Plugin" title="fb:like Facebook Social Plugin" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" style="border: medium none; visibility: visible; width: 90px; height: 28px;" src="https://www.facebook.com/v2.6/plugins/like.php?action=like&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df15fce03aabaabe%26domain%3Dlive.ipms247.com%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Flive.ipms247.com%252Ff296a2c0f25436c%26relation%3Dparent.parent&amp;container_width=326&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhotelonlineinternational%3F_rdc%3D4%26_rdr&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=false&amp;show_faces=true" class="" width="1000px" height="1000px" frameborder="0"></iframe></span></div>
					--}}
					<!--Pinal - 1.0.45.50 - 22 April 2015 - Purpose - Facebook Page URL for Facebook like on BE page-->
				</div>
			</div>
		</div>
	</div>
</footer>
{{-- 
<footer class="tm-bg-dark-blue" style="background: #2D2D36;">
	<div class="container">
		<div class="row">
			<p class="col-sm-12 text-center tm-font-light tm-color-white p-4 tm-margin-b-0">
				Copyright &copy; <span class="tm-current-year">2018</span>  <a rel="nofollow" href="https://www.tooplate.com" class="tm-color-primary tm-font-normal" target="_parent">Luxury Beach Villa</a>
			</p>
		</div>
	</div>
</footer>
--}}
</div>
<!-- load JS files -->
<style>
	.at-icon-wrapper{
	height: 27px;width: 50px;
	}
</style>
<script src="{{  asset('assets/website/js/jquery-1.11.3.min.js') }}"></script>             <!-- jQuery (https://jquery.com/download/) -->
<script src="{{  asset('assets/website/js/popper.min.js') }}"></script>                    <!-- https://popper.js.org/ -->       
<script src="{{  asset('assets/website/js/bootstrap.min.js') }}"></script>                 <!-- https://getbootstrap.com/ -->
<script src="{{  asset('assets/website/js/datepicker.min.js') }}"></script>                <!-- https://github.com/qodesmith/datepicker -->
<script src="{{  asset('assets/website/js/jquery.singlePageNav.min.js') }}"></script>      <!-- Single Page Nav (https://github.com/ChrisWojcik/single-page-nav) -->
<script src="{{  asset('assets/website/slick/slick.min.js') }}"></script>                  <!-- http://kenwheeler.github.io/slick/ -->
<script src="{{  asset('assets/website/js/custom/notify.js') }}"></script>                  <!-- http://kenwheeler.github.io/slick/ -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
<style>
	.swal2-container {
	z-index: 3434343435;
	}
</style>
<script>
	$(document).on("click", '[data-toggle="lightbox"]', function(event) {
	  event.preventDefault();
	  $(this).ekkoLightbox();
	});
	
	
		function add_Active_2($element){
			// console.log("am here")
			// console.log('#menu_info'+$element+$element)
			// alert('#menu_info'+$element+$element)
		    major_list_deactivate1()
		    $('#menu_info'+$element+$element).addClass('active')
		    $('#menu_info'+$element).addClass('active in')	
		}
		
		
		function major_list_deactivate1(){	
		
			var ids = $('.major_list').map(function(index) {
		    // this callback function will be called once for each matching element
		    let id_name = this.id;
		    $('#'+id_name).removeClass('active')
		    })
		
			var ids = $('.tab-pane').map(function(index) {
		    // this callback function will be called once for each matching element
		    let id_name = this.id;
		    $('#'+id_name).removeClass('active in')
		    })
		}
		
		
		
		function redirect_to_map(){
		url="{{ url('/map') }}"
		
		location.replace(url)
		
		}
				
		function cancel_booking(){
		
			if(!$('#cancel_email_reserve').val()){
		        $("#cancel_email_reserve").notify("Please specify email address.");
		        return false;
		    }
	
			if(!$('#cancel_email_reserve').val()){
		        $("#cancel_email_reserve").notify("Please specify email address.");
		        return false;
		    }
	
	
		// Swal.fire({
		//  title: "Please enter correct reservation number!.,",
		//  showClass: {
		//    popup: 'animate__animated animate__fadeInDown'
		//  },
		//  hideClass: {
		//    popup: 'animate__animated animate__fadeOutUp'
		//  }
		// })
		
		
		// Swal.fire({
		//  icon: 'error',
		//  title: 'Sorry!',
		//  text: 'Please enter correct reservation number!.'
		// })
		
		
	
		// document.getElementById("cancel_booking_form").submit(); 
	
		$.ajax({
				url:"{{  route('cancel_booking') }}",
				method: 'POST',
				// contentType: false,
				// 	processData: true,
					dataType: 'json',
					data:{
						cancel_email_reserve:$('#cancel_email_reserve').val(),
						cancel_number_reserve: $('#cancel_number_reserve').val(),
							 "_token": "{{ csrf_token() }}"
								 },
						beforeSend: function(data){
					
						},
				success: function(result) {	
	console.log("am here")
	console.log(result)
	console.log(result.message)
					
						if(result.message == "success"){
	console.log("am here2")
	$('#cancelled_booking_id').val(result.booking_id);
				
							$('#cancel_booking_modal').modal('show');
		
	
							
	//  Swal.fire({
	//  title: "We’re sorry to hear that you wish to cancel your subscription. We have received your cancellation request and are processing it. If you don’t mind, could you please provide us with some feedback on why you chose to cancel your subscription?.",
	//  input: 'text',
	//  inputPlaceholder: 'Description',
	//  inputAttributes: {
	//    autocapitalize: 'off'
	//  },
	//  showCancelButton: true,
	//  confirmButtonText: 'Save',
	//  showLoaderOnConfirm: true,
	//  preConfirm: (login) => {
	
		  
	//  },
	//  allowOutsideClick: () => !Swal.isLoading()
	// }).then((result) => {
	
		
	// })
	
	
	
	
						}else{
							
	
					// Extend existing 'alert' dialog
	if(!alertify.errorAlert){
	 //define a new errorAlert base on alert
	 alertify.dialog('errorAlert',function factory(){
	   return{
	           build:function(){
	               var errorHeader = '<span class="fa fa-times-circle fa-2x" '
	               +    'style="vertical-align:middle;color:#e10000;">'
	               + '</span> Sorry!.';
	               this.setHeader(errorHeader);
	           }
	       };
	   },true,'alert');
	}
	//launch it.
	// since this was transient, we can launch another instance at the same time.
	alertify
	   .errorAlert("The provided details do not match with any booking details please confirm the email and reservation number.!");
	
	
							// alertify
							// .alert("We’re sorry to hear that you wish to cancel your subscription. We have received your cancellation request and are processing it.", function(){
								// .alert("We’re sorry to hear that you wish to cancel your subscription. We have received your cancellation request and are processing it. If you don’t mind, could you please provide us with some feedback on why you chose to cancel your subscription?", function(){
							// 	alertify.message('OK');
							// }).setHeader('Thanks for cancel with us!.');
								}				
				},
				error: function(data) {
	
		
				}
	  });	
	
	
	
		
		}
		function send_feedback(){
		
			if(!$('#feedback_name').val()){
		        $("#feedback_name").notify("Please write a name.");
		        return false;
		    }
		
			if(!$('#feedback_country_code').val()){
		        $("#feedback_country_code").notify("Please specify your country code.");
		        return false;
		    }
	
			
	
			// alert("am here")
			
			if(!$('#feedback_phone').val()){
		        $("#feedback_phone").notify("Please write a phone number.");
		        return false;
		    }else{
				
			}
		
		
			
			if(!$('#feedback_email').val()){
		        $("#feedback_email").notify("Please specify your email.");
		        return false;
		    }
		
			
			if(!validate_email($('#feedback_email').val())){
			$("#feedback_email").notify("Please enter valid email address.");
			return false;
			}
			
		
		if(!$('#subject').val()){
		        $("#subject").notify("Please write a subject.");
		        return false;
		    }
	
			
		if(!$('#feedback_message').val()){
		        $("#feedback_message").notify("Please leave a message.");
		        return false;
		    }
		
	
		
			
		
			
				let timerInterval
		
		Swal.fire({
		 title: 'Thanks for contacting us!.',
		 html:
		   'I will take <strong></strong> seconds.<br/><br/>' 
		 ,
		 timer: 60000,
		 didOpen: () => {
		   const content = Swal.getHtmlContainer()
		   const $ = content.querySelector.bind(content)
		
		   const stop = $('#stop')
		   const resume = $('#resume')
		   const toggle = $('#toggle')
		   const increase = $('#increase')
		
		   Swal.showLoading()
		
		   function toggleButtons () {
		     stop.disabled = !Swal.isTimerRunning()
		     resume.disabled = Swal.isTimerRunning()
		   }
		
		   stop.addEventListener('click', () => {
		     Swal.stopTimer()
		     toggleButtons()
		   })
		
		   resume.addEventListener('click', () => {
		     Swal.resumeTimer()
		     toggleButtons()
		   })
		
		   toggle.addEventListener('click', () => {
		     Swal.toggleTimer()
		     toggleButtons()
		   })
		
		   increase.addEventListener('click', () => {
		     Swal.increaseTimer(5000)
		   })
		
		   timerInterval = setInterval(() => {
		     Swal.getHtmlContainer().querySelector('strong')
		       .textContent = (Swal.getTimerLeft() / 1000)
		         .toFixed(0)
		   }, 50)
		 },
		 willClose: () => {
		   clearInterval(timerInterval)
	
		 }
		})
		
		document.getElementById("feedback_form").submit(); 
		
		
		
		
		// submit data
		
		
		}
		
		
		function validate_email(email) {
		   var x = email;
		   var atposition = x.indexOf("@");
		   var dotposition = x.lastIndexOf(".");
		   if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= x.length) {
		       // alert("Please enter a valid e-mail address \n atpostion:" + atposition + "\n dotposition:" + dotposition);
		       return false;
		   } else {
		       return true;
		   }
		}
		
		
		
		// login details
		function check_user(){
			
			$('.success-alert').css('display', 'none')
					$('.success-fail').css('display', 'none')
					$('.success-success').css('display', 'none')
	
		if(!$('#email').val()){
		   $("#email").notify("Please specify email address.");
		   return false;
		}
		
		
		if(!validate_email($('#email').val())){
		$("#email").notify("Please enter valid email address.");
		return false;
		}
		
		
		
		if(!$('#password').val()){
		   $("#password").notify("Please specify password.");
		   return false;
		}
	
	
		$('.success-alert').css('display', 'unset')
		console.log("am here0")
		
	        //   let formData = document.querySelector('#login_form');
				$.ajax({
				url:"{{  url('login_checker') }}",
				method: 'POST',
				// contentType: false,
				// 	processData: true,
					dataType: 'json',
					data:{
							email:$('#email').val(),
							 password: $('#password').val(),
							 "_token": "{{ csrf_token() }}"
								 },
						beforeSend: function(data){
					$('.success-alert').css('display', 'unset')
							console.log("before send")
							// console.log(new FormData(formData))
						},
				success: function(result) {	
	
					$('.success-alert').css('display', 'none')
	
	
	
	
					console.log("am here")
					
					console.log(result.message)
	
						if(result.message == "success"){
							$('.success-success').css('display', 'unset')
		document.getElementById("login_form_data").submit(); 
	
							// alertify.success('You have successful change password.');
							// location.reload();
						}else{
							$('.success-fail').css('display', 'unset')
							console.log("am here2")
	
							// alertify.error(result.error)
						}				
				},
				error: function(data) {
					alertify.error(data)		
				}
	  });	
	
	
	
	
		
		}
		
		
</script>
<script>
	/* Google map
	------------------------------------------------*/
	var map = '';
	var center;
	
	function initialize() {
		var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(-23.013104,-43.394365),
			scrollwheel: false
		};
	
		map = new google.maps.Map(document.getElementById('google-map'),  mapOptions);
	
		google.maps.event.addDomListener(map, 'idle', function() {
		  calculateCenter();
	  });
	
		google.maps.event.addDomListener(window, 'resize', function() {
		  map.setCenter(center);
	  });
	}
	
	function calculateCenter() {
		center = map.getCenter();
	}
	
	function loadGoogleMap(){
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDVWt4rJfibfsEDvcuaChUaZRS5NXey1Cs&v=3.exp&sensor=false&' + 'callback=initialize';
		document.body.appendChild(script);
	} 
	
	function setCarousel() {
		
		if ($('.tm-article-carousel').hasClass('slick-initialized')) {
			$('.tm-article-carousel').slick('destroy');
		} 
	
		if($(window).width() < 438){
			// Slick carousel
			$('.tm-article-carousel').slick({
				infinite: false,
				dots: true,
				slidesToShow: 1,
				slidesToScroll: 1
			});
		}
		else {
		 $('.tm-article-carousel').slick({
				infinite: false,
				dots: true,
				slidesToShow: 2,
				slidesToScroll: 1
			});   
		}
	}
	
	function setPageNav(){
		if($(window).width() > 991) {
			$('#tm-top-bar').singlePageNav({
				currentClass:'active',
				offset: 79
			});   
		}
		else {
			$('#tm-top-bar').singlePageNav({
				currentClass:'active',
				offset: 65
			});   
		}
	}
	
	function togglePlayPause() {
		vid = $('.tmVideo').get(0);
	
		if(vid.paused) {
			vid.play();
			$('.tm-btn-play').hide();
			$('.tm-btn-pause').show();
		}
		else {
			vid.pause();
			$('.tm-btn-play').show();
			$('.tm-btn-pause').hide();   
		}  
	}
	
	$(document).ready(function(){
	
		$(window).on("scroll", function() {
			if($(window).scrollTop() > 100) {
				$(".tm-top-bar").addClass("active");
			} else {
				//remove the background property so it comes transparent again (defined in your css)
			   $(".tm-top-bar").removeClass("active");
			}
		});      
	
		// Google Map
		loadGoogleMap();  
	
		// Date Picker
		const pickerCheckIn = datepicker('#inputCheckIn');
		const pickerCheckOut = datepicker('#inputCheckOut');
		
		// Slick carousel
		setCarousel();
		setPageNav();
	
		$(window).resize(function() {
		  setCarousel();
		  setPageNav();
		});
	
		// Close navbar after clicked
		$('.nav-link').click(function(){
			$('#mainNav').removeClass('show');
		});
	
		// Control video
		$('.tm-btn-play').click(function() {
			togglePlayPause();                                      
		});
	
		$('.tm-btn-pause').click(function() {
			togglePlayPause();                                      
		});
	
		// Update the current year in copyright
		$('.tm-current-year').text(new Date().getFullYear());                           
	});
	
</script>         
<script>
	function main_nav(element){
		var ids = $('.nav-item').map(function(index) {
		    // this callback function will be called once for each matching element
		    let id_name = this.id;
		    $('#'+id_name).removeClass('active')
			$('#'+id_name).css('background', '#007bff')
		    $('#'+id_name+"_icon").css('color', 'black')
		    $('#'+id_name+"_title").css('color', 'black')
		    $('#'+id_name+"_div").css('display', 'none')
		    })
	
		    $('#'+element).css('background', 'white')
		    $('#'+element+"_icon").css('color', '#007bff')
		    $('#'+element+"_title").css('color', '#007bff')
		    $('#'+element+"_div").css('display', 'unset')
		
	}
	
			
</script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
@stack('script')
<style>
	.swal2-title {
	font-size: 16px;
	}
</style>
</body>
</html>

