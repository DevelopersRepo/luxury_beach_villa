@include('layouts.guest.header')
  <!-- ======= Hero Section ======= -->
  <main id="main" class="main">

@yield('body')

</main>
@include('layouts.guest.footer')