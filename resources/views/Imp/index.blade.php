
{{-- https://html.design/demo/pride_door/index.html --}}

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<script data-ezscrex='false' data-cfasync='false' data-pagespeed-no-defer>var __ez=__ez||{};__ez.stms=Date.now();__ez.evt={};__ez.script={};__ez.ck=__ez.ck||{};__ez.template={};__ez.template.isOrig=true;__ez.queue=(function(){var count=0,incr=0,items=[],timeDelayFired=false,hpItems=[],lpItems=[],allowLoad=true;var obj={func:function(name,funcName,parameters,isBlock,blockedBy,deleteWhenComplete,proceedIfError){var self=this;this.name=name;this.funcName=funcName;this.parameters=parameters===null?null:(parameters instanceof Array)?parameters:[parameters];this.isBlock=isBlock;this.blockedBy=blockedBy;this.deleteWhenComplete=deleteWhenComplete;this.isError=false;this.isComplete=false;this.isInitialized=false;this.proceedIfError=proceedIfError;this.isTimeDelay=false;this.process=function(){log("... func = "+name);self.isInitialized=true;self.isComplete=true;log("... func.apply: "+name);var funcs=self.funcName.split('.');var func=null;if(funcs.length>3){}else if(funcs.length===3){func=window[funcs[0]][funcs[1]][funcs[2]];}else if(funcs.length===2){func=window[funcs[0]][funcs[1]];}else{func=window[self.funcName];}
			if(typeof func!=='undefined'&&func!==null){func.apply(null,this.parameters);}
			if(self.deleteWhenComplete===true)delete items[name];if(self.isBlock===true){log("----- F'D: "+self.name);processAll();}}},file:function(name,path,isBlock,blockedBy,async,defer,proceedIfError){var self=this;this.name=name;this.path=path;this.async=async;this.defer=defer;this.isBlock=isBlock;this.blockedBy=blockedBy;this.isInitialized=false;this.isError=false;this.isComplete=false;this.proceedIfError=proceedIfError;this.isTimeDelay=false;this.process=function(){self.isInitialized=true;log("... file = "+name);var scr=document.createElement('script');scr.src=path;if(async===true)scr.async=true;else if(defer===true)scr.defer=true;scr.onerror=function(){log("----- ERR'D: "+self.name);self.isError=true;if(self.isBlock===true){processAll();}};scr.onreadystatechange=scr.onload=function(){var state=scr.readyState;log("----- F'D: "+self.name);if((!state||/loaded|complete/.test(state))){self.isComplete=true;if(self.isBlock===true){processAll();}}};document.getElementsByTagName('head')[0].appendChild(scr);}},fileLoaded:function(name,isComplete){this.name=name;this.path="";this.async=false;this.defer=false;this.isBlock=false;this.blockedBy=[];this.isInitialized=true;this.isError=false;this.isComplete=isComplete;this.proceedIfError=false;this.isTimeDelay=false;this.process=function(){};}};function init(){window.addEventListener("load",function(){setTimeout(function(){timeDelayFired=true;log('TDELAY -----');processAll();},5000);},false);}
			function addFile(name,path,isBlock,blockedBy,async,defer,proceedIfError,priority){var item=new obj.file(name,path,isBlock,blockedBy,async,defer,proceedIfError);if(priority===true){hpItems[name]=item}else{lpItems[name]=item}
			items[name]=item;checkIfBlocked(item);}
			function setallowLoad(settobool){allowLoad=settobool}
			function addFunc(name,func,parameters,isBlock,blockedBy,autoInc,deleteWhenComplete,proceedIfError,priority){if(autoInc===true)name=name+"_"+incr++;var item=new obj.func(name,func,parameters,isBlock,blockedBy,deleteWhenComplete,proceedIfError);if(priority===true){hpItems[name]=item}else{lpItems[name]=item}
			items[name]=item;checkIfBlocked(item);}
			function addTimeDelayFile(name,path){var item=new obj.file(name,path,false,[],false,false,true);item.isTimeDelay=true;log(name+' ... '+' FILE! TDELAY');lpItems[name]=item;items[name]=item;checkIfBlocked(item);}
			function addTimeDelayFunc(name,func,parameters){var item=new obj.func(name,func,parameters,false,[],true,true);item.isTimeDelay=true;log(name+' ... '+' FUNCTION! TDELAY');lpItems[name]=item;items[name]=item;checkIfBlocked(item);}
			function checkIfBlocked(item){if(isBlocked(item)===true||allowLoad==false)return;item.process();}
			function isBlocked(item){if(item.isTimeDelay===true&&timeDelayFired===false){log(item.name+" blocked = TIME DELAY!");return true;}
			if(item.blockedBy instanceof Array){for(var i=0;i<item.blockedBy.length;i++){var block=item.blockedBy[i];if(items.hasOwnProperty(block)===false){log(item.name+" blocked = "+block);return true;}else if(item.proceedIfError===true&&items[block].isError===true){return false;}else if(items[block].isComplete===false){log(item.name+" blocked = "+block);return true;}}}
			return false;}
			function markLoaded(filename){if(!filename||0===filename.length){return;}
			if(filename in items){var item=items[filename];if(item.isComplete===true){log(item.name+' '+filename+': error loaded duplicate')}else{item.isComplete=true;item.isInitialized=true;}}else{items[filename]=new obj.fileLoaded(filename,true);}
			log("markLoaded dummyfile: "+items[filename].name);}
			function logWhatsBlocked(){for(var i in items){if(items.hasOwnProperty(i)===false)continue;var item=items[i];isBlocked(item)}}
			function log(msg){var href=window.location.href;var reg=new RegExp('[?&]ezq=([^&#]*)','i');var string=reg.exec(href);var res=string?string[1]:null;if(res==="1")console.debug(msg);}
			function processAll(){count++;if(count>200)return;log("let's go");processItems(hpItems);processItems(lpItems);}
			function processItems(list){for(var i in list){if(list.hasOwnProperty(i)===false)continue;var item=list[i];if(item.isComplete===true||isBlocked(item)||item.isInitialized===true||item.isError===true){if(item.isError===true){log(item.name+': error')}else if(item.isComplete===true){log(item.name+': complete already')}else if(item.isInitialized===true){log(item.name+': initialized already')}}else{item.process();}}}
			init();return{addFile:addFile,addDelayFile:addTimeDelayFile,addFunc:addFunc,addDelayFunc:addTimeDelayFunc,items:items,processAll:processAll,setallowLoad:setallowLoad,markLoaded:markLoaded,logWhatsBlocked:logWhatsBlocked,};})();__ez.evt.add=function(e,t,n){e.addEventListener?e.addEventListener(t,n,!1):e.attachEvent?e.attachEvent("on"+t,n):e["on"+t]=n()},__ez.evt.remove=function(e,t,n){e.removeEventListener?e.removeEventListener(t,n,!1):e.detachEvent?e.detachEvent("on"+t,n):delete e["on"+t]};__ez.script.add=function(e){var t=document.createElement("script");t.src=e,t.async=!0,t.type="text/javascript",document.getElementsByTagName("head")[0].appendChild(t)};__ez.dot={};__ez.vep=(function(){var pixels=[],pxURL="/detroitchicago/grapefruit.gif";function AddPixel(vID,pixelData){if(__ez.dot.isDefined(vID)&&__ez.dot.isValid(pixelData)){pixels.push({type:'video',video_impression_id:vID,domain_id:__ez.dot.getDID(),t_epoch:__ez.dot.getEpoch(0),data:__ez.dot.dataToStr(pixelData)});}}
			function Fire(){if(typeof document.visibilityState!=='undefined'&&document.visibilityState==="prerender"){return;}
			if(__ez.dot.isDefined(pixels)&&pixels.length>0){while(pixels.length>0){var j=5;if(j>pixels.length){j=pixels.length;}
			var pushPixels=pixels.splice(0,j);var pixelURL=__ez.dot.getURL(pxURL)+"?orig="+(__ez.template.isOrig===true?1:0)+"&v="+btoa(JSON.stringify(pushPixels));__ez.dot.Fire(pixelURL);}}
			pixels=[];}
			return{Add:AddPixel,Fire:Fire};})();
		</script><script data-ezscrex='false' data-cfasync='false' data-pagespeed-no-defer>__ez.pel=(function(){var pixels=[],pxURL="/porpoiseant/army.gif";function AddAndFirePixel(adSlot,pixelData){AddPixel(adSlot,pixelData,0,0,0,0,0);Fire();}
			function AddAndFireOrigPixel(adSlot,pixelData){AddPixel(adSlot,pixelData,0,0,0,0,0,true);Fire();}
			function GetCurrentPixels(){return pixels;}
			function AddPixel(adSlot,pixelData,revenue,est_revenue,bid_floor_filled,bid_floor_prev,stat_source_id,isOrig){if(!__ez.dot.isDefined(adSlot)||__ez.dot.isAnyDefined(adSlot.getSlotElementId,adSlot.ElementId)==false){return;}
			if(typeof isOrig==='undefined'){isOrig=false;}
			var ad_position_id=parseInt(__ez.dot.getTargeting(adSlot,'ap'));var impId=__ez.dot.getSlotIID(adSlot),adUnit=__ez.dot.getAdUnit(adSlot,isOrig);var compId=parseInt(__ez.dot.getTargeting(adSlot,"compid"));var lineItemId=0;var creativeId=0;var ezimData=getEzimData(adSlot);if(typeof ezimData=='object'){if(ezimData.creative_id!==undefined){creativeId=ezimData.creative_id;}
			if(ezimData.line_item_id!==undefined){lineItemId=ezimData.line_item_id;}}
			if(__ez.dot.isDefined(impId,adUnit)&&__ez.dot.isValid(pixelData)){if((impId!=="0"||isOrig===true)&&adUnit!==""){pixels.push({type:"impression",impression_id:impId,domain_id:__ez.dot.getDID(),unit:adUnit,t_epoch:__ez.dot.getEpoch(0),revenue:revenue,est_revenue:est_revenue,ad_position:ad_position_id,ad_size:"",bid_floor_filled:bid_floor_filled,bid_floor_prev:bid_floor_prev,stat_source_id:stat_source_id,country_code:__ez.dot.getCC(),pageview_id:__ez.dot.getPageviewId(),comp_id:compId,line_item_id:lineItemId,creative_id:creativeId,data:__ez.dot.dataToStr(pixelData),is_orig:isOrig||__ez.template.isOrig,});}}}
			function AddPixelById(impFullId,pixelData,isOrig,revenue){var vals=impFullId.split('/');if(__ez.dot.isDefined(impFullId)&&vals.length===3&&__ez.dot.isValid(pixelData)){var adUnit=vals[0],impId=vals[2];var pix={type:"impression",impression_id:impId,domain_id:__ez.dot.getDID(),unit:adUnit,t_epoch:__ez.dot.getEpoch(0),pageview_id:__ez.dot.getPageviewId(),data:__ez.dot.dataToStr(pixelData),is_orig:isOrig||__ez.template.isOrig};if(typeof revenue!=='undefined'){pix.revenue=revenue;}
			pixels.push(pix);}}
			function Fire(){if(typeof document.visibilityState!=='undefined'&&document.visibilityState==="prerender")return;if(__ez.dot.isDefined(pixels)&&pixels.length>0){var allPixels=[pixels.filter(function(pixel){return pixel.is_orig}),pixels.filter(function(pixel){return!pixel.is_orig})];allPixels.forEach(function(pixels){while(pixels.length>0){var isOrig=pixels[0].is_orig||false;var j=5;if(j>pixels.length){j=pixels.length;}
			var pushPixels=pixels.splice(0,j);var pixelURL=__ez.dot.getURL(pxURL)+"?orig="+(isOrig===true?1:0)+"&sts="+btoa(JSON.stringify(pushPixels));if(typeof window.isAmp!=='undefined'&&isAmp&&typeof window._ezaq!=='undefined'&&_ezaq.hasOwnProperty("domain_id")){pixelURL+="&visit_uuid="+_ezaq['visit_uuid'];}
			__ez.dot.Fire(pixelURL);}});}
			pixels=[];}
			function getEzimData(adSlot){if(typeof _ezim_d=="undefined"){return false;}
			var adUnitName=__ez.dot.getAdUnitPath(adSlot).split('/').pop();if(typeof _ezim_d==='object'&&_ezim_d.hasOwnProperty(adUnitName)){return _ezim_d[adUnitName];}
			for(var ezimKey in _ezim_d){if(ezimKey.split('/').pop()===adUnitName){return _ezim_d[ezimKey];}}
			return false;}
			return{Add:AddPixel,AddAndFire:AddAndFirePixel,AddAndFireOrig:AddAndFireOrigPixel,AddById:AddPixelById,Fire:Fire,GetPixels:GetCurrentPixels,};})();
		</script>
		<!-- basic -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<!-- mobile metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1"/>
		<!-- site metas -->
		<title>Luxury Beach Villa</title>
		<meta name="keywords" content=""/>
		<meta name="description" content=""/>
		<meta name="author" content=""/>
		<!-- site icons -->
		<link rel="icon" href="images/fevicon.png" type="image/gif"/>
		<!-- bootstrap css -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/bootstrap.min.css') }}"/>
		<!-- Vendor CSS-->
		<link href="{{ asset('ModifiedAssets/css/daterangepicker.css') }}" rel="stylesheet"/>
		<!-- Owl Stylesheets -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/owl.carousel.min.css') }}"/>
		<!-- nice select -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/nice-select.css') }}"/>
		<!-- animate css -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/animate.css') }}"/>
		<!-- Site css -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/style.css') }}"/>
		<!-- responsive css -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/responsive.css') }}"/>
		<!-- custom css -->
		<link rel="stylesheet" href="{{ asset('ModifiedAssets/css/custom.css') }}"/>
		
        <style>
            .text-white{
                color:white;
            }
             .text-black{
                color:black;
            }

            #banner {
    min-height: 0px;

            }
            #banner {
 
    background-attachment: unset;
}
.label{
    text-shadow: 0 0 black;
}
            </style>
        <!-- Icons font CSS-->
		<!-- Main CSS-->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<![endif]-->
		<link rel='canonical' href='https://html.design/demo/pride_door/index.html' />
		<script type="text/javascript">var ezouid = "1";</script>
		{{-- <base href="http://127.0.0.1:8000"> --}}
		<script type='text/javascript'>
			var ezoTemplate = 'old_site_noads';
			if(typeof ezouid == 'undefined')
			{
			    var ezouid = 'none';
			}
			var ezoFormfactor = '1';
			var ezo_elements_to_check = Array();
		</script><!-- START EZHEAD -->
		<script data-ezscrex="false" type='text/javascript'>
			var soc_app_id = '0';
			var did = 317139;
			var ezdomain = 'html.design';
			var ezoicSearchable = 1;
		</script>
		<!--{jquery}-->
		<!-- END EZHEAD -->
		<script data-ezscrex="false" type="text/javascript" data-cfasync="false">var _ezaq = {"ad_cache_level":0,"ad_lazyload_version":0,"ad_load_version":0,"city":"Dar es Salaam","country":"TZ","days_since_last_visit":-1,"domain_id":317139,"engaged_time_visit":118,"ezcache_level":2,"ezcache_skip_code":0,"form_factor_id":1,"framework_id":1,"is_return_visitor":false,"is_sitespeed":0,"last_page_load":"1652774339774","last_pageview_id":"3621ab4f-eb8e-4441-5d20-9d7599f4b3dc","lt_cache_level":0,"metro_code":0,"page_ad_positions":"","page_view_count":42,"page_view_id":"d8bba23b-6ecc-4203-4a90-2cdf815120df","position_selection_id":0,"postal_code":"","pv_event_count":0,"response_size_orig":65596,"response_time_orig":3,"serverid":"13.232.244.82:22322","state":"02","t_epoch":1652774661,"template_id":120,"time_on_site_visit":1216,"url":"https://html.design/demo/pride_door/index.html","user_id":0,"weather_precipitation":0,"weather_summary":"","weather_temperature":0,"word_count":664,"worst_bad_word_level":0};var _ezExtraQueries = "&ez_orig=1";</script>
		<script data-ezscrex='false' data-pagespeed-no-defer data-cfasync='false'>
			function create_ezolpl(pvID, rv) {
			    var d = new Date();
			    d.setTime(d.getTime() + (365*24*60*60*1000));
			    var expires = "expires="+d.toUTCString();
			    __ez.ck.setByCat("ezux_lpl_317139=" + new Date().getTime() + "|" + pvID + "|" + rv + "; " + expires, 3);
			}
			function attach_ezolpl(pvID, rv) {
			    if (document.readyState === "complete") {
			        create_ezolpl(pvID, rv);
			    }
			    if(window.attachEvent) {
			        window.attachEvent("onload", create_ezolpl, pvID, rv);
			    } else {
			        if(window.onload) {
			            var curronload = window.onload;
			            var newonload = function(evt) {
			                curronload(evt);
			                create_ezolpl(pvID, rv);
			            };
			            window.onload = newonload;
			        } else {
			            window.onload = create_ezolpl.bind(null, pvID, rv);
			        }
			    }
			}
			
			__ez.queue.addFunc("attach_ezolpl", "attach_ezolpl", ["d8bba23b-6ecc-4203-4a90-2cdf815120df", "false"], false, ['/detroitchicago/boise.js'], true, false, false, false);
		</script>
		<script type="text/javascript">var _audins_dom="html_design",_audins_did=317139;__ez.queue.addFile('/detroitchicago/cmbv2.js', '/detroitchicago/cmbv2.js?gcb=195-0&cb=04-1y02-5y06-12y07-1y19-5y0b-5y0d-14y13-3y17-4y1c-2y21-3y55-1&cmbcb=69&sj=x04x02x06x07x19x0bx0dx13x17x1cx21x55', true, [], true, false, true, false);</script>
		<script type="text/javascript" defer>__ez.queue.addFile('/detroitchicago/cmbdv2.js', '/detroitchicago/cmbdv2.js?gcb=195-0&cb=03-5y0c-5y18-4&cmbcb=69&sj=x03x0cx18', true, ['/detroitchicago/cmbv2.js'], true, false, true, false);</script>
	</head>
	<body id="default_theme" class="home_1">
		<!-- header -->
		<header id="main_header" class="header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-2">
						<div class="full logo_section">
							<a class="logo_main" href="index.html"><img style="width:133px;margin-top: -26px;margin-left: -7px;" src="{{ asset('ModifiedAssets/images/logo.png') }}" alt="#"/></a>
							<div class="float-right d-lg-none">
								<ul class="login_section">
									<li><a href="login.html">Login</a></li>
									<li><a href="sign_up.html">Sign up</a></li>
								</ul>
								{{-- <div class="search_container">
									<div class="search">
										<input type="text" placeholder="Search..."/>
									</div>
									<div class="search_btn search_open">
									</div>
								</div> --}}
							</div>
						</div>
					</div>
					<div class="col-lg-7">
						<div class="full menu_section">
							<nav class="navbar navbar-expand-lg navbar-light">
								<div class="button_menu_section d-lg-none">
									<a class="navbar-brand" href="#">Menu</a>
									<button id="nav-icon" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span></span>
									<span></span>
									<span></span>
									</button>
								</div>
								<div class="collapse navbar-collapse" id="navbarSupportedContent">
									<ul class="navbar-nav mr-auto">
										
									
										{{-- <li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
											<div class="dropdown-menu wow fadeInDown" data-wow-duration="0.5s">
												<a class="dropdown-item" href="services.html">Services Page One (Default)</a>
												<a class="dropdown-item" href="services_2.html">Services Page Two</a>
												<a class="dropdown-item" href="services_3.html">Services Page Three</a>
											</div>
										</li> --}}
                                        <li class="nav-item active">
                                            <a class="nav-link" href="#">Home</a>
                                           
                                         </li>                        
										<li class="nav-item">
											<a class="nav-link" href="#">Airbnb Resort Info</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#">Contact Us</a>
										</li>
                                        <li class="nav-item">
											<a class="nav-link" href="#">Cancel Booking</a>
										</li>
                                        <li class="nav-item">
											<a class="nav-link" href="#">Map</a>
										</li>

									</ul>
								</div>
							</nav>
						</div>
					</div>
					<div class="col-lg-3 d-none d-lg-block">
						<div class="full">
							<ul class="login_section">
								<li><a href="#">Login</a></li>
								{{-- <li><a href="sign_up.html">Sign up</a></li> --}}
							</ul>
							{{-- <div class="search_container">
								<div class="search">
									<input type="text" placeholder="Search..."/>
								</div>
								<div class="search_btn search_open">
								</div>
							</div> --}}
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		<!-- section -->
		<section id="banner" class="banner" style="background-image: url('{{ asset('/assets/website/img/house/front_view.jpg') }}');">
			<div class="sidebar_icons">
				<ul class="social_icon">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="full banner_cont">
							<h4 class="text-black" style="color:black;text-shadow: #f8f9fa 1px 0 10px;">Welcome To</h4>
							<h3 class="text-white" style="text-shadow: #1e1d1b 1px 0 10px;">Luxury Beach<br/>Hotel</h3>
							<a class="main_btn" href="about.html" style="background:#00BFFF">Book Now<i class="fa fa-bottom"></i></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<!-- section -->
		<section id="book_room_section" class="section">
			<div class="large_container white_bg padding-top_9 padding-bottom_4">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="full">
									<h2 class="reveal fw_700 wow">Book Room</h2>
								</div>
							</div>
						</div>
						<div class="row book_form">
							<form method="post" action="#" id="book_form" class="form col-lg-12">
								<div class="row">
									
									{{-- <div class="col-lg-3 col-sm-6">
										<div class="full field">
											<label>Rooms</label>
											<select class="full book_select niceSelect">
												<option data-display="----">1 Room</option>
												<option value="1">2 Rooms</option>
												<option value="2">3 Rooms</option>
												<option value="3">4 Rooms</option>
											</select>
										</div>
									</div> --}}
									
									<div class="col-lg-3 col-sm-6">
										<div class="full field">
											<label class="label">Check-In</label>
											<input class="input--style-1" type="text" name="checkin" placeholder="MM/DD/YYYY" id="input-start" required=""/>
										</div>
									</div>
									<div class="col-lg-3 col-sm-6">
										<div class="full field">
											<label class="label">Check-Out</label>
											<input class="input--style-1" type="text" name="checkout" placeholder="MM/DD/YYYY" id="input-end" required=""/>
										</div>
									</div>
									<div class="col-lg-3 col-sm-6">
										<div class="full field">
											<label class="label">guests</label>
											<div class="input-group-icon js-number-input">
												<div class="icon-con">
													<span class="plus">+</span>
													<span class="minus">-</span>
												</div>
												<input class="input--style-1 quantity" type="text" name="guests" value="1 Guests"/>
											</div>
										</div>
									</div>

                                    <div class="col-lg-3 col-sm-6">
										<div class="full field">
											<button class="btn-submit" type="submit">search</button>
										</div>
									</div>
                                    
									
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<!-- section -->
		<section id="our_services" class="section">
			<div class="large_container theme_color_bg white_reveal_ padding-top_4 padding-bottom_2">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="full white_fonts">
									<h2 class="reveal fw_700 wow">Our Services</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- blog service --> 
							<div class="col-md-6 col-lg-3">
								<div class="full blog_service">
									<div class="full text-align_center">
										<svg version="1.1" fill="white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 69.8 60.9" style="enable-background:new 0 0 69.8 60.9;" xml:space="preserve">
											<g>
												<path class="st0" d="M46.8,25H3.6c-1.7,0-3.2,1.4-3.2,3.2v27.4c0,1.7,1.4,3.2,3.2,3.2h43.2c1.7,0,3.2-1.4,3.2-3.2V28.2
													C49.9,26.4,48.5,25,46.8,25z M48.6,55.5c0,1-0.8,1.8-1.8,1.8H3.6c-1,0-1.8-0.8-1.8-1.8V28.2c0-1,0.8-1.8,1.8-1.8h43.2
													c1,0,1.8,0.8,1.8,1.8V55.5z"></path>
												<path class="st0" d="M61.4,12.3H41.2c-3.2,0-5.7,2.6-5.7,5.7v5.7h1.3V18c0-2.4,2-4.4,4.4-4.4h20.2c2.4,0,4.4,2,4.4,4.4v35
													c0,2.4-2,4.4-4.4,4.4H50.9c-0.2,0.5-0.5,0.9-0.9,1.3h11.4c3.1,0,5.7-2.6,5.7-5.7V18C67.1,14.8,64.5,12.3,61.4,12.3z"></path>
												<path class="st0" d="M15.9,32.7h-3.8c-1.1,0-2.1,0.9-2.1,2.1v3.8c0,1.1,0.9,2.1,2.1,2.1h3.8c1.1,0,2.1-0.9,2.1-2.1v-3.8
													C18,33.7,17.1,32.7,15.9,32.7z M16.7,38.6c0,0.4-0.3,0.8-0.8,0.8h-3.8c-0.4,0-0.8-0.3-0.8-0.8v-3.8c0-0.4,0.3-0.8,0.8-0.8h3.8
													c0.4,0,0.8,0.3,0.8,0.8V38.6z"></path>
												<path class="st0" d="M12.2,31.4v-3.7h-1.3v4C11.3,31.5,11.7,31.4,12.2,31.4L12.2,31.4z"></path>
												<path class="st0" d="M17.2,31.7v-4h-1.3v3.7h0C16.4,31.4,16.8,31.5,17.2,31.7z"></path>
												<path class="st0" d="M15.9,42v14.1h1.3V41.7C16.8,41.9,16.4,42,15.9,42L15.9,42z"></path>
												<path class="st0" d="M10.9,41.7v14.3h1.3V42h0C11.7,42,11.3,41.9,10.9,41.7z"></path>
												<path class="st0" d="M38.2,32.7h-3.8c-1.1,0-2.1,0.9-2.1,2.1v3.8c0,1.1,0.9,2.1,2.1,2.1h3.8c1.1,0,2.1-0.9,2.1-2.1v-3.8
													C40.3,33.7,39.3,32.7,38.2,32.7z M38.9,38.6c0,0.4-0.3,0.8-0.8,0.8h-3.8c-0.4,0-0.8-0.3-0.8-0.8v-3.8c0-0.4,0.3-0.8,0.8-0.8h3.8
													c0.4,0,0.8,0.3,0.8,0.8V38.6z"></path>
												<path class="st0" d="M34.5,31.4v-3.7h-1.3v4C33.5,31.5,34,31.4,34.5,31.4L34.5,31.4z"></path>
												<path class="st0" d="M39.5,31.7v-4h-1.3v3.7h0C38.6,31.4,39.1,31.5,39.5,31.7z"></path>
												<path class="st0" d="M38.2,42v14.1h1.3V41.7C39.1,41.9,38.6,42,38.2,42L38.2,42z"></path>
												<path class="st0" d="M33.1,41.7v14.3h1.3V42h0C34,42,33.5,41.9,33.1,41.7z"></path>
												<rect x="45.8" y="20.5" class="st0" width="11" height="1.3"></rect>
												<rect x="57.9" y="14.9" class="st0" width="1.3" height="41.1"></rect>
												<rect x="43.3" y="14.9" class="st0" width="1.3" height="8.8"></rect>
												<rect x="38.1" y="20.5" class="st0" width="4.1" height="1.3"></rect>
												<rect x="60.4" y="20.5" class="st0" width="4" height="1.3"></rect>
												<path class="st0" d="M19.5,24.1c0.4,0,0.8-0.3,0.8-0.8c0-2,2.2-3.6,5-3.6c2.7,0,5,1.6,5,3.6c0,0.4,0.3,0.8,0.8,0.8
													c0.4,0,0.8-0.3,0.8-0.8c0-2.8-2.9-5.1-6.5-5.1c-3.6,0-6.5,2.3-6.5,5.1C18.7,23.8,19,24.1,19.5,24.1z"></path>
												<path class="st0" d="M45.7,4c3.7-2.5,7.4-2.5,11.2,0c0.1,0.1,0.2,0.1,0.4,0.1c0.2,0,0.4-0.1,0.6-0.3c0.2-0.3,0.1-0.7-0.2-0.9
													c-4.2-2.8-8.5-2.8-12.7,0c-0.3,0.2-0.4,0.6-0.2,0.9C45,4.1,45.4,4.2,45.7,4z"></path>
												<path class="st0" d="M46.1,5.3v5.6h1.3V5.3c0-0.4-0.3-0.7-0.7-0.7C46.4,4.7,46.1,5,46.1,5.3z"></path>
												<path class="st0" d="M55.1,5.3v5.6h1.3V5.3c0-0.4-0.3-0.7-0.7-0.7C55.4,4.7,55.1,5,55.1,5.3z"></path>
											</g>
										</svg>
									</div>
									<div class="full heading_blog white_fonts text-align_center">
										<h4>THE BEST LOCATIONS</h4>
									</div>
									<div class="full cont_blog white_fonts text-align_center">
										<p>It is a long established fact that a reader will be distracted by the readable content..</p>
									</div>
									
								</div>
							</div>
							<!-- end service blog -->
							<!-- blog service --> 
							<div class="col-md-6 col-lg-3">
								<div class="full blog_service">
									<div class="full text-align_center">
										<svg version="1.1" id="Layer_2" fill="white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 183.4" style="enable-background:new 0 0 150 183.4;" xml:space="preserve">
											<g>
												<path class="st0" d="M118.1,132.6l0,2.1l-2.7,4.2c-0.3,0.5-0.5,1.1-0.5,1.7v2.6c0,1,0.4,1.9,1.2,2.6l1.2,1
													c-0.8,0.7-1.2,1.6-1.2,2.5c0,0.9,0.4,1.8,1.1,2.5l-1.3,1.1c-0.7,0.7-1.2,1.6-1.2,2.5l0,20.2l-4.3,4.7c-0.1,0.1-0.3,0.3-0.8-0.1
													l-4.2-4.5l0-20.3c0-0.9-0.4-1.8-1.2-2.5l-1.3-1c0.8-0.7,1.2-1.6,1.2-2.5c0-0.9-0.4-1.8-1.1-2.5l1.2-1.1c0.7-0.7,1.2-1.6,1.2-2.5
													l0-2.6c0-0.6-0.2-1.2-0.5-1.7l-2.6-4.1l0-9.1c-1.2-0.5-2.4-1.1-3.4-1.8v10.9c0,0.6,0.2,1.2,0.5,1.7l2.6,4.1l0,2.5l-1.3,1.1
													c-0.8,0.7-1.2,1.6-1.2,2.5c0,0.9,0.4,1.8,1.1,2.5c-0.7,0.7-1.1,1.6-1.1,2.5c0,1,0.4,1.9,1.2,2.5l1.3,1l0,20.3
													c0,0.8,0.3,1.6,0.9,2.3l4.2,4.6c0,0,0.2,0.2,0.2,0.2c0.7,0.8,1.7,1.2,2.8,1.2c1.1,0,2.1-0.4,2.8-1.2c0,0,0.2-0.2,0.2-0.2l4.2-4.5
													c0.6-0.6,0.9-1.4,0.9-2.3l0-14.7l0-5.5l1.3-1.1c0.7-0.7,1.2-1.6,1.2-2.5c0-0.9-0.4-1.8-1.1-2.5c0.7-0.7,1.1-1.6,1.1-2.5
													c0-1-0.4-1.9-1.2-2.5l-1.3-1l0-2.5l2.7-4.2c0.3-0.5,0.5-1.1,0.5-1.7v-10.9c-1.1,0.7-2.2,1.3-3.5,1.8V132.6z"></path>
												<path class="st0" d="M112.1,174v-45.1c0-1-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7V174c0,1,0.8,1.7,1.7,1.7S112.1,174.9,112.1,174z"></path>
												<path class="st0" d="M134.1,75.7c-1,0.6-1.9,1.3-2.9,1.8c0.4,0.7,0.8,1.5,1.2,2.3c1.3,3,1.9,6.1,1.9,9.4c0,8.5-4.6,16.5-12,20.7
													c-0.6,0.4-1,1-0.8,1.8c0,0.3,0.1,0.5,0.1,0.8c0,4.3-5,7.7-11.1,7.7c-6.1,0-11.1-3.5-11.1-7.7c0-0.3,0-0.5,0.1-0.8
													c0.1-0.7-0.2-1.4-0.8-1.8c-7.4-4.3-12-12.2-12-20.7c0-3.2,0.6-6.4,1.9-9.4c3.8-8.8,12.4-14.5,22-14.5c5,0,9.7,1.5,13.6,4.3
													c1.2-0.5,2.3-1.1,3.4-1.8c-4.7-3.7-10.7-5.9-17-5.9c-11,0-20.8,6.5-25.2,16.6c-1.5,3.4-2.2,7-2.2,10.7c0,9.4,4.9,18.1,12.8,23.2
													c0,0,0,0.1,0,0.1c0,6.2,6.5,11.2,14.5,11.2c8,0,14.5-5,14.5-11.2c0,0,0-0.1,0-0.1c7.9-5,12.8-13.8,12.8-23.2
													c0-3.7-0.7-7.3-2.2-10.7C135.1,77.5,134.6,76.6,134.1,75.7z"></path>
												<path class="st0" d="M110.3,72.5c0.3,0,0.6,0,0.8,0.1c2-0.1,4-0.3,5.9-0.7c-1.7-1.8-4.1-2.9-6.7-2.9c-5.2,0-9.4,4.2-9.4,9.4
													c0,5.2,4.2,9.4,9.4,9.4c3.9,0,7.2-2.4,8.7-5.7c-1.5,0.3-3.1,0.5-4.6,0.7c-1.1,1-2.5,1.6-4,1.6c-3.3,0-5.9-2.7-5.9-5.9
													C104.4,75.1,107.1,72.5,110.3,72.5z"></path>
												<path class="st0" d="M21.3,37.9c-0.3,0-0.5,0-0.8,0.1c-0.5,0.2-0.9,0.7-1,1.3c-0.1,0.6-0.1,1.2,0.2,1.7c0.2,0.5,0.5,0.8,0.9,0.9
													c0,0,0.1,0,0.2,0L40,43.1c0.4,0,0.7,0,1.1,0c0.4,0,0.7-0.1,1-0.2l3.1-1.4c0.3-0.1,0.6-0.5,0.7-0.9c0.1-0.5,0.1-0.9-0.1-1.2
													l-4.7-10.7c-0.2-0.4-0.5-0.7-0.8-0.9c-0.4-0.2-0.8-0.3-1.1-0.1l-4.6,2c-0.3,0.1-0.6,0.4-0.7,1c-0.1,0.4,0,0.9,0.2,1.4
													c0.2,0.5,0.5,0.8,0.9,1.1c0.4,0.3,0.8,0.4,1.2,0.2l2.6-1.1l2.9,6.7l-1.6,0.7L21.3,37.9z"></path>
												<path class="st0" d="M28.8,61c0.6,0.8,1.3,1.3,2.2,1.7c0.4,0.2,0.8,0.3,1.3,0.5c0.5,0.1,1,0.2,1.5,0.2c1,0,2.2-0.3,3.4-0.8l0.6-0.2
													c1.3-0.6,2.3-1.3,2.8-2.3c0.4-0.8,0.7-1.7,0.7-2.6c0.6,0.4,1.4,0.7,2.3,0.7c1.1,0.1,2.3-0.2,3.6-0.8c1-0.4,1.8-1,2.4-1.6
													c0.6-0.6,1-1.3,1.2-2.1c0.2-0.7,0.2-1.6,0.1-2.4c-0.1-0.8-0.4-1.7-0.8-2.6c-0.6-1.3-1.2-2.2-1.9-2.9c-0.7-0.7-1.5-1.2-2.2-1.4
													c-0.8-0.3-1.5-0.4-2.2-0.3c-0.7,0.1-1.3,0.2-1.8,0.4c-0.6,0.3-1,0.6-1.2,0.9c-0.1,0.4,0,0.9,0.3,1.7c0.3,0.6,0.6,1,1,1.2
													c0.4,0.1,0.8,0.1,1.3-0.1c0.3-0.1,0.6-0.2,0.9-0.2c0.3,0,0.6,0,0.9,0.1c0.3,0.1,0.6,0.3,0.9,0.7c0.3,0.3,0.6,0.8,0.8,1.3
													c0.4,0.9,0.5,1.7,0.3,2.4c-0.2,0.7-0.9,1.2-1.9,1.7c-0.6,0.3-1.2,0.4-1.7,0.5c-0.5,0-0.8,0-1.2-0.2c-0.3-0.2-0.6-0.4-0.8-0.7
													c-0.2-0.3-0.5-0.7-0.7-1.2c-0.2-0.5-0.5-0.7-0.9-0.8c-0.4,0-0.7,0-1.1,0.2c-0.4,0.2-0.7,0.4-0.9,0.7c-0.2,0.3-0.3,0.7-0.1,1.2
													c0.5,1,0.6,2,0.4,2.8c-0.2,0.8-0.8,1.5-2,2l-0.6,0.2c-1.3,0.6-2.3,0.7-3.1,0.4C31.7,59,31,58.2,30.5,57c-0.5-1.2-0.6-2.1-0.4-2.9
													c0.3-0.8,0.7-1.2,1.3-1.5c0.5-0.2,0.8-0.5,0.9-0.9c0.1-0.4,0-0.9-0.3-1.6c-0.3-0.6-0.6-1-0.9-1.1c-0.4-0.2-0.8-0.1-1.4,0.1
													c-0.6,0.3-1.2,0.7-1.7,1.2c-0.5,0.5-0.9,1.2-1.2,2c-0.3,0.8-0.4,1.7-0.4,2.7c0,1,0.3,2.1,0.8,3.3C27.7,59.4,28.2,60.3,28.8,61z"></path>
												<path class="st0" d="M29.1,77.6c-1.6,0.7-3.3,0.9-5,0.5c-2.5-0.6-4.5-2.2-5.5-4.5l-14.3-32c-1.8-4,0-8.7,4-10.5L68.1,4.3
													c1.6-0.7,3.3-0.9,5-0.5c2.5,0.6,4.5,2.2,5.5,4.5l14.3,32c1.8,4,0,8.7-4,10.5L81,54.3c0.5,1,1.1,2,1.7,3l7.6-3.4
													c5.7-2.6,8.3-9.3,5.8-15l-14.3-32c-1.5-3.3-4.3-5.7-7.9-6.5c-2.4-0.6-4.9-0.3-7.2,0.7L6.9,27.9c-5.7,2.6-8.3,9.3-5.8,15l14.3,32
													c1.5,3.3,4.3,5.7,7.9,6.5c2.4,0.6,4.9,0.3,7.2-0.7l42.7-19.1c-0.6-1-1.1-2-1.7-3L29.1,77.6z"></path>
												<path class="st0" d="M78.6,32c-0.4,1.6-0.7,3.3-0.9,5.1c2.5-1.1,4.4-3.3,5.1-6.2C83.9,26,80.9,21.1,76,20
													c-4.9-1.1-9.7,1.9-10.8,6.8c-0.7,3.1,0.3,6.1,2.3,8.3c0.2-1.9,0.6-3.7,1-5.5c-0.1-0.7-0.1-1.3,0.1-2c0.7-3,3.7-4.9,6.7-4.2
													c3,0.7,4.9,3.7,4.2,6.7C79.3,30.8,79,31.4,78.6,32z"></path>
												<path class="st0" d="M87.9,68.2c-2.4-1.9-4.5-4.1-6.3-6.5l-0.1,0.1l-1.4-2.1l-0.2-0.4c-0.1-0.1-0.1-0.2-0.1-0.3
													c-0.6-0.9-1.1-1.9-1.6-2.8c0-0.1-0.1-0.2-0.1-0.2l-0.2-0.5l-1.4-2.8l0.2-0.1c-1.5-4-2.3-8.2-2.3-12.7c0-4,0.7-7.9,1.9-11.5
													c-0.2-0.8-0.8-1.4-1.6-1.6c-0.6-0.1-1.2,0-1.7,0.4c-1.4,4-2.1,8.3-2.1,12.7c0,6.3,1.5,12.2,4.1,17.4c0.4,0.8,0.8,1.6,1.3,2.4
													c2.5,4.3,5.7,8.1,9.6,11.1C86.3,69.9,87.1,69,87.9,68.2z"></path>
												<path class="st0" d="M110.3,0.2c-9.1,0-17.4,3.1-24.1,8.2l1.4,3.2c6.2-5,14.1-8,22.7-8c20,0,36.2,16.2,36.2,36.2S130.3,76,110.3,76
													c-0.4,0-0.8,0-1.2-0.1c0,0-1.3,0.1-1.3,2.4c0,0.4,0.1,0.7,0.2,1c0.7,0,1.5,0.1,2.2,0.1c21.9,0,39.7-17.8,39.7-39.7
													S132.2,0.2,110.3,0.2z"></path>
											</g>
										</svg>
									</div>
									<div class="full heading_blog white_fonts text-align_center">
										<h4>FAST &amp; EASY BOOKING</h4>
									</div>
									<div class="full cont_blog white_fonts text-align_center">
										<p>It is a long established fact that a reader will be distracted by the readable content..</p>
									</div>
									
								</div>
							</div>
							<!-- end service blog -->
							<!-- blog service --> 
							<div class="col-md-6 col-lg-3">
								<div class="full blog_service">
									<div class="full text-align_center">
										<svg version="1.1" id="Layer_3" fill="white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 63.3 75.7" style="enable-background:new 0 0 63.3 75.7;" xml:space="preserve">
											<g>
												<path class="st0" d="M38.1,36.1c-1.4,0.9-2,2.7-1.2,4.2c1.9,3.5,3.5,7.2,4.8,11v-3.8c-1.1-2.7-2.3-5.3-3.6-7.8
													c-0.5-0.9-0.2-1.9,0.7-2.5c0.3-0.2,0.6-0.2,0.9-0.2c0.6,0,1.2,0.3,1.5,0.9c0.2,0.3,0.4,0.7,0.5,1v-2.4
													C40.8,35.6,39.3,35.4,38.1,36.1z"></path>
												<path class="st0" d="M44.3,74.3h16.3V60.1H44.3V74.3z M45.7,61.4h13.7V73H45.7V61.4z"></path>
												<path class="st0" d="M59.2,35.1c-0.6,0-1.1,0.2-1.5,0.5c-0.4-1-1.5-1.8-2.7-1.8c-0.7,0-1.3,0.2-1.7,0.6c-0.4-1.1-1.5-1.9-2.8-1.9
													c-0.6,0-1.1,0.2-1.6,0.5v-7.3c0-1.6-1.3-2.9-2.9-2.9c-1.6,0-2.9,1.3-2.9,2.9v26.8c0,2.2,0.9,4.2,2.6,5.5v0.9h1.3v-1.2
													c0-0.2-0.1-0.4-0.3-0.5c-1.5-1.1-2.3-2.8-2.3-4.7V25.6c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6v9.7v6.2c0,0.4,0.3,0.7,0.7,0.7
													s0.7-0.3,0.7-0.7v-6.2c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6v1.3v4.8c0,0.4,0.3,0.7,0.7,0.7c0.4,0,0.7-0.3,0.7-0.7v-4.8
													c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6v1v3.8c0,0.4,0.3,0.7,0.7,0.7c0.4,0,0.7-0.3,0.7-0.7v-3.8c0-0.7,0.6-1.3,1.4-1.3
													c0.7,0,1.3,0.6,1.3,1.3v14.6c0,1.9-0.9,3.6-2.3,4.7c-0.2,0.1-0.3,0.3-0.3,0.5v1.2h1.3v-0.9c1.7-1.4,2.6-3.4,2.6-5.5V37.7
													C61.9,36.3,60.7,35.1,59.2,35.1z"></path>
												<path class="st0" d="M57.5,33.4V7.5H5.9V38h29.4c0.1-0.5,0.2-0.9,0.5-1.3H7.2V8.8h49v23.9C56.7,32.9,57.1,33.1,57.5,33.4z"></path>
												<path class="st0" d="M3.8,41.2c-0.5,0-0.8-0.4-0.8-0.8V5.2c0-0.5,0.4-0.8,0.8-0.8h55.9c0.5,0,0.8,0.4,0.8,0.8V34
													c0.5,0.2,0.9,0.4,1.3,0.8V5.2c0-1.2-1-2.2-2.2-2.2H3.8C2.6,3,1.6,4,1.6,5.2v35.2c0,1.2,1,2.2,2.2,2.2h32.8
													c-0.2-0.4-0.4-0.9-0.7-1.3H3.8z"></path>
												<path class="st0" d="M24.8,12l-3.5,5.7h2.4v15.6h15.8V17.7h2.7L38.4,12H24.8z M32.6,31.8h-1.7v-2h1.7V31.8z M38,16.2v15.6h-4.1
													v-3.3h-4.3v3.3h-4.4V16.2H24l1.6-2.7h12l1.8,2.7H38z"></path>
												<rect x="26.6" y="16.3" class="st0" width="1.5" height="2.2"></rect>
												<rect x="29.4" y="16.3" class="st0" width="1.5" height="2.2"></rect>
												<rect x="32.2" y="16.3" class="st0" width="1.5" height="2.2"></rect>
												<rect x="35.1" y="16.3" class="st0" width="1.5" height="2.2"></rect>
												<rect x="26.6" y="20.4" class="st0" width="1.5" height="2.2"></rect>
												<rect x="29.4" y="20.4" class="st0" width="1.5" height="2.2"></rect>
												<rect x="32.2" y="20.4" class="st0" width="1.5" height="2.2"></rect>
												<rect x="35.1" y="20.4" class="st0" width="1.5" height="2.2"></rect>
												<rect x="26.6" y="24.6" class="st0" width="1.5" height="2.2"></rect>
												<rect x="29.4" y="24.6" class="st0" width="1.5" height="2.2"></rect>
												<rect x="32.2" y="24.6" class="st0" width="1.5" height="2.2"></rect>
												<rect x="35.1" y="24.6" class="st0" width="1.5" height="2.2"></rect>
												<rect x="26.6" y="28.4" class="st0" width="1.5" height="2.2"></rect>
												<rect x="35.1" y="28.4" class="st0" width="1.5" height="2.2"></rect>
												<path class="st0" d="M7.6,2h4.1c0.4,0,0.8-0.3,0.8-0.8s-0.3-0.8-0.8-0.8H7.6c-0.4,0-0.8,0.3-0.8,0.8S7.1,2,7.6,2z"></path>
												<path class="st0" d="M14.9,2h7c0.4,0,0.8-0.3,0.8-0.8s-0.3-0.8-0.8-0.8h-7c-0.4,0-0.8,0.3-0.8,0.8S14.5,2,14.9,2z"></path>
												<path class="st0" d="M29.5,38.9c-0.4,0-0.8,0.3-0.8,0.8c0,0.4,0.3,0.8,0.8,0.8h4.4c0.4,0,0.8-0.3,0.8-0.8c0-0.4-0.3-0.8-0.8-0.8
													H29.5z"></path>
												<path class="st0" d="M3.6,9.7v8.8c0,0.4,0.3,0.8,0.8,0.8c0.4,0,0.7-0.3,0.7-0.8V9.7c0-0.4-0.3-0.8-0.7-0.8C4,8.9,3.6,9.3,3.6,9.7z"></path>
												<path class="st0" d="M59,8.9c-0.4,0-0.8,0.3-0.8,0.8v8.8c0,0.4,0.3,0.8,0.8,0.8c0.4,0,0.8-0.3,0.8-0.8V9.7
													C59.7,9.3,59.4,8.9,59,8.9z"></path>
											</g>
										</svg>
									</div>
									<div class="full heading_blog white_fonts text-align_center">
										<h4>Airbnb Resort &amp; ROOMS</h4>
									</div>
									<div class="full cont_blog white_fonts text-align_center">
										<p>It is a long established fact that a reader will be distracted by the readable content..</p>
									</div>
									
								</div>
							</div>
							<!-- end service blog -->
							<!-- blog service --> 
							<div class="col-md-6 col-lg-3">
								<div class="full blog_service">
									<div class="full text-align_center">
										<svg version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" fill="white" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 69.3 58.7" style="enable-background:new 0 0 69.3 58.7;" xml:space="preserve">
											<g>
												<path class="st0" d="M32,44.3c0.4-0.8,0.5-1.6,0.3-2.5l-6.7-23.1c-0.4-1.4-1.7-2.4-3.1-2.4c-0.3,0-0.6,0-0.9,0.1L1.1,22.4l8.5,29.4
													l20.4-5.9C30.9,45.6,31.6,45.1,32,44.3z M10.5,50.2L2.8,23.3l19.2-5.6c1-0.3,2.1,0.3,2.4,1.3L31,42.2c0.1,0.5,0.1,1-0.2,1.5
													c-0.3,0.5-0.7,0.8-1.2,0.9L10.5,50.2z"></path>
												<path class="st0" d="M27.6,33.4L26,33.2C24.9,33,23.7,33,22.5,33c-0.4,0-0.8,0-1.2,0.1c-0.8,0.1-1.2,0.1-1.5,0.2
													c-0.9,0.1-1.7,0.3-2.5,0.6c-2.8,0.8-5.4,2.2-7.7,4.1l-1.2,1l1.6,0.2c0.4,0,0.9,0.1,1.3,0.2c-0.1,0.5-0.2,1-0.2,1.5l-0.2,1.6l1.3-1
													c0.9-0.8,2-1.5,3-2l0.2-0.1c0.1-0.1,0.3-0.1,0.4-0.2c0.2-0.1,0.3-0.1,0.5-0.2c0.2-0.1,0.3-0.2,0.5-0.2c0.3-0.1,0.5-0.2,0.8-0.3
													c0.3-0.1,0.6-0.2,0.9-0.3c0.3-0.1,0.6-0.2,0.9-0.2c0.3-0.1,0.6-0.1,0.8-0.2c0.2,0,0.4-0.1,0.6-0.1c0.2,0,0.3,0,0.5-0.1
													c0.2,0,0.3,0,0.5,0l0.2,0c1.2-0.1,2.4-0.1,3.6,0.1l1.6,0.2l-1-1.3c-0.3-0.4-0.7-0.8-1-1.2c0.3-0.3,0.7-0.6,1-0.9L27.6,33.4z
													M23.9,35.7c0.1,0.1,0.2,0.2,0.4,0.4c-0.8,0-1.5,0-2.3,0l-0.3,0c-0.2,0-0.3,0-0.5,0.1c-0.2,0-0.4,0-0.6,0.1c-0.2,0-0.4,0.1-0.6,0.1
													c-0.3,0.1-0.6,0.1-0.9,0.2c-0.3,0.1-0.7,0.2-1,0.3c-0.3,0.1-0.7,0.2-1,0.3c-0.3,0.1-0.6,0.2-0.9,0.3c-0.2,0.1-0.4,0.2-0.6,0.3
													c-0.2,0.1-0.3,0.2-0.5,0.2c-0.1,0.1-0.3,0.1-0.4,0.2l-0.2,0.1c-0.7,0.4-1.3,0.7-1.9,1.2c0-0.2,0.1-0.3,0.1-0.5l0.1-0.6l-0.6-0.1
													c-0.2,0-0.4-0.1-0.5-0.1c1.8-1.3,3.9-2.3,6-2.9c0.7-0.2,1.5-0.4,2.3-0.5c0,0,0.9-0.1,1.4-0.2c0.4,0,0.8-0.1,1.2-0.1
													c0.6,0,1.2,0,1.8,0c-0.1,0.1-0.3,0.2-0.4,0.4l-0.5,0.5L23.9,35.7z"></path>
												<path class="st0" d="M12.2,34.6c-1.5-1.9-2.7-4-3.6-6.2c2-1.2,4.1-2.1,6.3-2.8c2.2-0.6,4.5-1,6.8-1.1c0.5,2.4,0.6,4.8,0.3,7.2
													c0.1,0,0.2,0,0.4,0c0.3,0,0.7,0,1,0c0.2-2.7,0.1-5.3-0.5-8l-0.1-0.5l-0.5,0c-2.6,0-5.2,0.4-7.7,1.1c-2.5,0.7-4.9,1.8-7.1,3.2
													L7,27.8l0.2,0.5c1,2.5,2.3,4.9,3.9,7C11.5,35,11.8,34.8,12.2,34.6z"></path>
												<path class="st0" d="M21.1,38.8c-0.2,0-0.4,0.1-0.6,0.1c-0.1,0-0.1,0-0.2,0c-0.3,0.7-0.6,1.4-0.9,2.1c-0.7-0.4-1.3-0.9-1.9-1.3
													c-0.1,0-0.2,0.1-0.2,0.1C17.2,39.9,17,40,16.9,40c-0.2,0.1-0.4,0.2-0.5,0.2c0,0-0.1,0-0.1,0.1c1,0.8,2,1.5,3.2,2.2l0.4,0.3l0,0
													l0.2,0.1l0.2-0.5l0.1-0.1c0.6-1.2,1.1-2.4,1.5-3.5c-0.1,0-0.2,0-0.2,0C21.4,38.8,21.3,38.8,21.1,38.8z"></path>
												<path class="st0" d="M63.8,19.8l-11.2-4.5c0,0.4-0.1,0.9-0.1,1.4L63.3,21c0.2,0.1,0.3,0.3,0.3,0.4l-1,2.5c0,0.1-0.3,0.2-0.5,0.1
													l-9.6-3.8c0.3,0.8,0.8,1.5,1.5,1.8l1.4,0.7l6.3,2.5c0.3,0.1,0.5,0.1,0.8,0.1c0.6,0,1.2-0.4,1.4-1l1-2.5
													C65.1,21.1,64.7,20.1,63.8,19.8z"></path>
												<path class="st0" d="M35.2,10.3c0.2,0,0.4,0,0.6,0.1l7.9,3.1c0.1-0.4,0.1-0.9,0.2-1.3l-7.6-3C35.9,9,35.5,9,35.1,9
													c-0.6,0-1.2,0.3-1.7,0.8l-1.7-0.6c-0.4-0.2-0.8,0-1,0.4c-0.2,0.4,0,0.8,0.4,1l1.5,0.6c-0.3,1.2,0.4,2.5,1.6,3l8.9,3.5
													c0-0.5,0.1-0.9,0.1-1.4l-8.5-3.4c-0.7-0.3-1-1-0.7-1.7C34.2,10.7,34.7,10.3,35.2,10.3z"></path>
												<path class="st0" d="M38,29.9c-0.7-0.2-1.2-0.9-1.3-1.7c-0.1-0.8,0.3-1.5,1-1.9c0.2-0.1,0.4-0.4,0.4-0.7c0-0.3-0.2-0.5-0.5-0.6
													c-0.9-0.2-1.3-1-1.4-1.7c-0.1-0.7,0.2-1.6,1.1-2l3.3-1.4c0.3-0.1,0.4-0.4,0.4-0.7c0-0.3-0.3-0.5-0.6-0.6c-1.1-0.1-1.3-0.2-1.4-0.2
													c-1.5-0.2-1.9-1.1-1.9-1.9l-1.3-0.5c0,0.1-0.1,0.3-0.1,0.4c-0.1,1.2,0.6,2.6,2.2,3.1l-1.3,0.5c-1.3,0.6-2.1,1.9-1.9,3.3
													c0.1,1,0.6,1.8,1.3,2.4c-0.6,0.6-0.9,1.5-0.8,2.5c0.1,1,0.5,1.8,1.2,2.3c-0.5,0.7-0.7,1.6-0.5,2.6c0.3,1.1,1.1,1.8,2,2.1
													c0-0.4,0-0.9,0-1.4c-0.4-0.2-0.6-0.6-0.7-1c-0.2-0.6-0.1-1.4,0.7-1.8c0.2-0.1,0.4-0.4,0.3-0.6C38.4,30.2,38.2,30,38,29.9z"></path>
												<path class="st0" d="M65.3,18.1L55,13.8c-0.4-0.2-0.8,0-1,0.4c-0.2,0.4,0,0.8,0.4,1l10.4,4.3c0.7,0.3,1,1.1,0.8,1.8l-0.8,2.2
													c-0.1,0.4,0.1,0.8,0.5,1c0.1,0,0.2,0,0.3,0c0.3,0,0.6-0.2,0.7-0.5l0.8-2.2C67.5,20.3,66.8,18.7,65.3,18.1z"></path>
												<path class="st0" d="M39,57.6h15.5V41.8H39V57.6z M40.3,43.1h12.9v13.2H40.3V43.1z"></path>
												<path class="st0" d="M55.8,24.5l-2.5-1.3c-1.5-0.8-2.5-2.6-2.4-4.5l0.1-2.1c0.2-2.3,0.3-3.7,0.3-4.1c0.3-1.8-0.8-3.5-2.5-3.9
													c-0.2,0-0.4-0.1-0.6-0.1c-1.5,0-2.8,1.2-3.1,2.9c-0.5,3.4-0.9,6.8-1,10.2c0,0.2-0.2,0.5-0.5,0.8l-0.1,0.1c-2.4,2.6-3.3,5.6-3.9,9.2
													c-0.8,4.7,0.5,7.6,1.2,8.7h1.5l0-0.2l-0.2-0.2c0,0-2.2-2.7-1.3-8.1c0.7-4.1,1.6-6.5,3.5-8.5l0.1-0.1c0.4-0.4,0.8-0.9,0.8-1.6
													c0.2-3.4,0.5-6.7,1-10.1c0.2-1,0.9-1.8,1.8-1.8c0.1,0,0.2,0,0.4,0c1,0.2,1.6,1.3,1.5,2.4c-0.1,0.4-0.2,1.8-0.3,4.2l-0.1,2.1
													c-0.2,2.4,1,4.7,3.1,5.7l2.2,1.1c2.9,4.8-3.7,14.6-3.7,14.7L51,40.5h1.6c1.4-2.1,6.6-10.9,3.4-15.8L55.8,24.5z"></path>
												<polygon class="st0" points="23.7,49.1 37.6,49.1 37.6,47.8 28.3,47.8  "></polygon>
												<path class="st0" d="M15.5,1.2H49v6.2c0.1,0,0.1,0,0.2,0c0.4,0.1,0.8,0.2,1.1,0.4v-8H14.2v17.4l1.3-0.4V1.2z"></path>
											</g>
										</svg>
									</div>
									<div class="full heading_blog white_fonts text-align_center">
										<h4>GREAT PRICES</h4>
									</div>
									<div class="full cont_blog white_fonts text-align_center">
										<p>It is a long established fact that a reader will be distracted by the readable content..</p>
									</div>
									
								</div>
							</div>
							<!-- end service blog -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<!-- section -->
		<section class="section padding-top_9 padding-bottom_9">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="full">
							<h2 class="reveal fw_700 wow">Our Rooms</h2>
						</div>
						<div class="full margin-top_2">
							<p>Lorem Ipsum generators on the Internet tend to repeat predefined<br/>chunks as necessary</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		<!-- section -->
        @if(count($houses)>0)
        @foreach($houses as $data_item)


		<section id="rooms_section1" class="section" style="    margin-bottom: 12px;">
			<div class="large_container white_bg padding-top_2 padding-bottom_2">
				<div class="row">
					<div class="container" style="    margin-left: unset;margin-right: unset;">
						<div class="row">
							<div class="col-lg-6">
								<div class="full">
									<img class="img-responsive" src="{{ asset('/storage/uploads/'.$data_item->LatestHouseImage->name) }}" alt="#"/>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="full">
									<h4 class="reveal fw_700 wow margin-top_5">{{  $data_item->name }}</h4>
								</div>
								<div class="full margin-top_3">
									<p class="text-align_justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum..</p>
								</div>
								<div class="full margin-top_3 small_read_more">
									<p class="large_2 price_tag float-left"><span class="fw_700 theme_color">$400</span><br/><span>Per Night</span></p>
									<a class="main_btn float-right margin-top_05" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	

        
        @endforeach
        @endif
		<!-- end section -->

		<!-- end section -->
		<!-- section -->
      


		<section class="section padding-top_9">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="full">
							<h2 class="reveal fw_700 wow">About Airbnb Resort</h2>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- end section -->
		<!-- section -->
		<section id="about_section" class="section margin-top_5">
			<div class="large_container white_bg padding-top_9 padding-bottom_9">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<div class="full">
									<h4 class="reveal fw_700 wow margin-top_5">Hotel</h4>
								</div>
								<div class="full margin-top_3">
									<p class="text-align_justify">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. </p>
								</div>
								<div class="full margin-top_5">
									<a class="main_btn" href="about.html">Read More</a>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="full">
									<img class="r-img wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="1.5s" src="{{ asset('ModifiedAssets/images/house/gym_place_less.jpg') }}" alt="#"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end section -->
		
		<!-- footer -->
		<footer class="main_footer white_reveal_ white_fonts">
			<div class="container">
				<div class="row">
					<!-- widget -->
					<div class="col-xl-4 col-lg-12">
						<div class="full footer_widget">
							<div class="full footer_logo">
								<a href="index.html"><img style="height:100px" src="{{ asset('ModifiedAssets/images/logo.png') }}" alt="#"/></a>
							</div>
							<div class="full margin-top_3">
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum..</p>
							</div>
							<div class="full social_icon_footer margin-top_3">
								<ul class="social_icon">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- end widget --> 
					<!-- widget -->
					<div class="col-xl-4 col-lg-6">
						<div class="full footer_widget">
							<h4 class="reveal fw_700 wow">Useful Link</h4>
							<ul class="full">
								<li><a href="home_1.html">Home</a></li>
								<li><a href="about.html">About</a></li>
								<li><a href="services.html">Services</a></li>
								<li><a href="#">Rooms</a></li>
								<li><a href="package.html">Package</a></li>
								<li><a href="pricing.html">Pricing</a></li>
								<li><a href="faq.html">Faq</a></li>
								<li><a href="team.html">Our Team</a></li>
								<li><a href="tc.html">Term &amp; Condition</a></li>
								<li><a href="pp.html">Privacy &amp; Policy</a></li>
								<li><a href="testimonial.html">Testimonial</a></li>
								<li><a href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<!-- end widget --> 
					<!-- widget -->
					<div class="col-xl-4 col-lg-6">
						<div class="full footer_widget">
							<h4 class="reveal fw_700 wow">Information</h4>
							<div class="full">
								<ul class="contact_information">
									<li>
										<a href="#">
											<svg width="30px" fill="white" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve">
												<path class="st0" d="M76,6.5c-30.3,0-54.9,24.6-54.9,54.9c0,21.6,9.1,43.5,26.2,63.3c12.8,14.8,25.5,23.1,26,23.5
													c0.8,0.5,1.8,0.8,2.7,0.8c0.9,0,1.9-0.3,2.7-0.8c0.5-0.3,13.2-8.7,26-23.5c17.1-19.8,26.2-41.6,26.2-63.3
													C130.9,31.2,106.3,6.5,76,6.5z M76,81.8c-11.6,0-21.1-9.4-21.1-21.1S64.4,39.6,76,39.6s21.1,9.4,21.1,21.1S87.7,81.8,76,81.8z"></path>
											</svg>
											<span>427 Schoen Circles Suite 124 Melbourne Australia</span>
										</a>
									</li>
									<li>
										<a href="tel:+919876543210">
											<svg width="30px" fill="white" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve">
												<path class="st0" d="M130.4,91.7c-4.9-0.6-9.8-1.6-14.5-3c-5.9-1.8-12.3-0.2-16.6,4.1l-9,9C73,92.4,58.6,78,49.2,60.7l9-9
													c4.3-4.3,5.9-10.7,4.1-16.6c-1.4-4.7-2.5-9.6-3-14.5C58.4,12.3,51.3,6,42.8,6H20.6C16,6,11.5,8,8.4,11.4c-2.8,3.1-4.3,7.1-4.3,11.2
													c0,0.5,0,1,0.1,1.5c1.5,15.9,5.7,31.3,12.6,45.8c6.7,14,15.7,26.7,26.7,37.7c11,11,23.7,20,37.7,26.7c14.4,6.9,29.8,11.1,45.8,12.6
													c4.6,0.4,9.3-1.1,12.7-4.3c3.4-3.1,5.4-7.6,5.4-12.2v-22.2C145,99.7,138.7,92.6,130.4,91.7z"></path>
											</svg>
											<span>+91 9876 543 210</span>
										</a>
									</li>
									<li>
										<a href="/cdn-cgi/l/email-protection#3e5a5b53517e59535f5752105d5153">
											<svg width="30px" fill="white" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve">
												<polygon class="st0" points="149,125 149,35.8 98.9,79.5  "></polygon>
												<path class="st0" d="M2.1,34c0,0.1,0,0.1,0,0.1v90.2l50.1-45.4L2.1,34z"></path>
												<path class="st0" d="M84.7,81.2l59.7-52c-0.1,0-0.2-0.1-0.4-0.1H8.7l57.9,51.9C71.7,85.7,79.5,85.7,84.7,81.2z"></path>
												<path class="st0" d="M90,87.3c-4.1,3.6-9.2,5.3-14.3,5.3c-5.2,0-10.4-1.9-14.5-5.6l-3-2.7L6.5,131.2c0.2,0,0.3,0.1,0.5,0.1h136.8
													L92.8,84.9L90,87.3z"></path>
											</svg>
											<span><span class="__cf_email__" data-cfemail="553130383a153238343c397b363a38">[email&#160;protected]</span></span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- end widget --> 
				</div>
			</div>
		</footer>
		<!-- end footer -->
		<div class="cpy">
			<p>Copyright 2022 - Luxury Beach Villa</a></p>
		</div>
		<!-- js section -->
		<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{ asset('ModifiedAssets/js/jquery.min.js') }}"></script>
		<script src="{{ asset('ModifiedAssets/js/bootstrap.min.js') }}"></script>
		<!-- owl slider -->
		<script src="{{ asset('ModifiedAssets/js/owl.carousel.js') }}"></script>
		<!-- bootsrap date picker -->
		<script src="{{ asset('ModifiedAssets/js/moment.min.js') }}"></script>
		<script src="{{ asset('ModifiedAssets/js/daterangepicker.js') }}"></script>
		<script src="{{ asset('ModifiedAssets/js/global.js') }}"></script>
		<!-- nice select -->
		<script src="{{ asset('ModifiedAssets/js/jquery.nice-select.min.js') }}"></script>
		<!-- wow animation -->
		<script src="{{ asset('ModifiedAssets/js/wow.js') }}"></script>
		<!-- custom js -->
		<script src="{{ asset('ModifiedAssets/js/custom.js') }}"></script>
		<script type='text/javascript' style='display:none;' async></script>
		<script type="text/javascript" data-cfasync="false"></script>
	</body>
</html>

