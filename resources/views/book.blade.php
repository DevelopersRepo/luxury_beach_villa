

@extends('layouts.guest.app')
@push('head')
{{-- 
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
--}}
<link rel="stylesheet" href="{{  url('assets/website/css/custom/book.css') }}" >
<style>
	select.form-control:not([size]):not([multiple]) {
  height: calc(2.25rem + 10px);
}
	.row > * {
	top: -10px;
	margin-top: 0px;
	}
	body{
		background: #ededed;	}
	.bt-primary.bt-primary {
	background: #fcc51e;
	color: #614c00;
	}
	.saveforlater_div .saveButton {
	float: right;
	cursor: pointer;
	}
	.bt-primary {
	background: #5C1111;
	color: #fff;
	}
	.vres-btn {
	background-color: #fff;
	border: 0 solid #ccc;
	padding: 10px 15px;
	line-height: 1;
	border-radius: 3px;
	display: inline-block;
	font-size: 14px;
	text-decoration: none;
	color: #333;
	vertical-align: middle;
	letter-spacing: 0;
	transition: all ease .2s;
	box-shadow: 0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);
	-webkit-box-shadow: 0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);
	}

	body {
  padding-left: 0px;
  padding-right: 0px;
}


	.guest-info h4 .sign_in_btn {
	background: #fcc51e;
	color: #614c00;
	}
	.guest-info h4 .sign_in_btn {
	font-size: 14px;
	display: inline-block;
	padding: 8px 15px 6px;
	background-color: #f7c22a;
	color: #403516;
	font-weight: 100;
	text-decoration: none;
	border-radius: 3px;
	margin-left: 4px;
	position: relative;
	}
	a {
	background-color: transparent;
	}
	*, .WebShowHighlighted {
	box-sizing: border-box;
	}
	.rate-hotel, .review-title, .review-section h1, .review-cnt h3, .reviews-for h2, .score-breakdown h2, .review-prof p, .guest-info h4, .acc-bk-title, .vres-book-list h3, .vres-book-cnt p b, .vres-bkBtn-sect a.bt-primary[href], .voucher_txt, .tbl-rm-head, .filter-label, .reply-from, .sec-title, .link {
	color: #0079b9 !important;
	}
	.guest-info h4 {
	font-size: 18px;
	color: #5C1111;
	}
	.payment_wrap {
	line-height: 1.4;
	}
	@media only screen and (max-width: 991px){
	.guest-name-wrap, .mobilediv .mobilelandcode, .payment-roomDetail .guestblk, .payment-roomDetail:last-child .form-wrap {
	margin-bottom: 0;
	}
	}
	.guest-name-wrap {
	display: inline-block;
	width: auto;
	padding: 0;
	margin-bottom: 10px;
	}
	*, .WebShowHighlighted {
	box-sizing: border-box;
	}
	.payment_wrap {
	line-height: 1.4;
	}
	@media only screen and (max-width: 991px){
	/* .guest-name-wrap .guest-label, .payment-form .form-wrap label, .payment-roomDetail .guestblk label, .payment-roomDetail label {
	margin-bottom: 1px;
	} */
	}.guest-name-wrap .guest-label {
	margin-bottom: 4px;
	}
	.guest-name-wrap .guest-label, .guest-name-wrap .guest-text-wrap {
	display: block;
	vertical-align: middle;
	}
	/* .payment-roomDetail label {
	display: inline-block;
	margin-bottom: 4px;
	} */
	*, .WebShowHighlighted {
	box-sizing: border-box;
	}
	.payment_wrap {
	line-height: 1.4;
	}
	.guest-name-wrap .guest-text-wrap {
	position: relative;
	display: inline-block;
	width: 100%;
	min-width: 422px;
	}
	.guest-name-wrap .guest-label, .guest-name-wrap .guest-text-wrap {
	display: block;
	vertical-align: middle;
	}
	*, .WebShowHighlighted {
	box-sizing: border-box;
	}
	.payment_wrap {
	line-height: 1.4;
	}
	/* .payment-roomDetail .guest-text-wrap .vres-select, .payment-roomDetail .guest-text-wrap input {
	margin-right: 10px;
	} */
	/* .payment-roomDetail label.vres-select {
	margin-bottom: 0;
	} */
	/* form label.vres-select {
	border-radius: 3px;
	} */
	/* .vres-forms label.vres-select, form label.vres-select {
	position: relative;
	font-size: 14px;
	display: inline-block;
	min-height: 34px;
	vertical-align: middle;
	border-radius: 0;
	background-color: #fff;
	}
	.vres-forms label.vres-select select, form label.vres-select select {
	position: relative;
	z-index: 1;
	background: 95% center;
	-webkit-appearance: none;
	-moz-appearance: none;
	-ms-appearance: none;
	appearance: none;
	} */
	/* .vres-forms input[type="text"], .vres-forms input[type="password"], .vres-forms input[type="tel"], .vres-forms input[type="email"], body.DroidSans form input[type="text"], form input[type="password"], form input[type="tel"], form input[type="email"], select, textarea {
	font-size: 13px;
	}
	.vres-forms input[type="text"], .vres-forms input[type="password"], .vres-forms input[type="tel"], .vres-forms input[type="email"], body.OpenSans form input[type="text"], form input[type="password"], form input[type="tel"], form input[type="email"], select, textarea {
	font-size: 13px;
	} */
	/* select {
	padding: 0 18px 0 5px;
	} */
	/* .vres-forms input[type="text"], .vres-forms input[type="password"], .vres-forms input[type="tel"], .vres-forms input[type="email"], form input[type="text"], form input[type="password"], form input[type="tel"], form input[type="email"], select, textarea {
	background-color: #fff;
	height: 34px;
	display: inline-block;
	vertical-align: middle;
	font-size: 14px;
	padding: 5px 10px;
	transition: all ease .2s;
	border: 1px solid #ccc;
	border-radius: 3px;
	width: 100%;
	max-width: 150px;
	background-position: 95% center;
	} */
	/* button, select {
	text-transform: none;
	} */
	/* button, input, optgroup, select, textarea {
	color: inherit;
	font: inherit;
	font-size: inherit;
	margin: 0;
	} */
	/* .guest-name-wrap .mandatory, .mobilediv .form-wrap > label .mandatory {
	margin: 0;
	} */
	.mandatory {
	color: #e01a1a;
	margin: 4px;
	}
	/* .payment-roomDetail .guest-text-wrap .vres-select, .payment-roomDetail .guest-text-wrap input {
	margin-right: 10px;
	} */
	.erfer{
	background-color: #fff;
	height: 34px;
	display: inline-block;
	vertical-align: middle;
	font-size: 14px;
	padding: 5px 10px;
	transition: all ease .2s;
	border: 1px solid #ccc;
	border-radius: 3px;
	width: 100%;
	max-width: 150px;
	background-position: 95% center;
	}
	/* input {
	line-height: normal;
	} */
	.modal {
	z-index: 5675755757575;
	background: #0d0808c9;
	}
</style>
@endpush
@push('script')
<script>
	function save_booking_data(){
	
				var ids2 = $('.title_class').map(function(index) {			
					let id = this.id;
						if(!$('#'+id).val()){
							document.getElementById('form-container').scrollIntoView()
							$("#"+id).notify("Please specify the title.");
							return false;
						}
				})
	
	
				var ids3 = $('.first_name_class').map(function(index) {			
					let id = this.id;
						if(!$('#'+id).val()){
							document.getElementById('form-container').scrollIntoView()
							$("#"+id).notify("Please specify the first name.");
							return false;
						}
				})
	
	
				var ids4 = $('.last_name_class').map(function(index) {			
					let id = this.id;
						if(!$('#'+id).val()){
							document.getElementById('form-container').scrollIntoView()
							$("#"+id).notify("Please specify the last name.");
							return false;
						}
				})
	
	
				if(!$('#country_code').val()){
							document.getElementById('form-container').scrollIntoView()
							$("#country_code").notify("Please specify your country code.");
							return false;
						}
	
						if(!$('.phone_number').val()){
							document.getElementById('form-container').scrollIntoView()
							$(".phone_number").notify("Please specify phone.");
							return false;
						}
	
						
						var alert_statement = null;
						alert_statement = "Saving your request!."

						if(!$('.email_address').val()){


						}else{
							if(!validate_email($('.email_address').val())){
							// document.getElementById('form-container').scrollIntoView()
							$(".email_address").notify("Please enter valid email address.");
							return false;
							}else{
							alert_statement = "Saving your request and sending Email to your account!."
							}

						}
	
						
						if(!$(".title_class").val()){
							document.getElementById('form-container').scrollIntoView()
							$(".title_class").notify("Please specify the title.");
							return false;
						}else{


							// paynt methods start here

							
	
							let timerInterval
							Swal.fire({
							title: alert_statement,
							html: 'It will take  <b></b> milliseconds.',
							timer: 5000,
							timerProgressBar: true,
							didOpen: () => {
								Swal.showLoading()
								const b = Swal.getHtmlContainer().querySelector('b')
								timerInterval = setInterval(() => {
								b.textContent = Swal.getTimerLeft()
								}, 100)
							},
							willClose: () => {
								clearInterval(timerInterval)
							}
							}).then((result) => {
					
								document.getElementById("booking-form").submit(); 
								
					
							
							})
	
						}
			


	
	
	
	
	}
	
	
	
	function validate_email(email) {
	   var x = email;
	   var atposition = x.indexOf("@");
	   var dotposition = x.lastIndexOf(".");
	   if (atposition < 1 || dotposition < atposition + 2 || dotposition + 2 >= x.length) {
	       // alert("Please enter a valid e-mail address \n atpostion:" + atposition + "\n dotposition:" + dotposition);
	       return false;
	   } else {
	       return true;
	   }
	}
	
	
	function special_request(id){

Swal.fire({
	 title: 'Special request cannot be guaranteed - but the accommodation will do its best to meet your needs.',
	 input: 'text',
	 inputPlaceholder: 'Special Requests',
	 inputValue : $('#special_request'+id).val(),
	 inputAttributes: {
	   autocapitalize: 'off'
	 },
	 showCancelButton: true,
	 confirmButtonText: 'Save',
	 showLoaderOnConfirm: true,
	 preConfirm: (login) => {
	  $('#special_request'+id).val(login)
	   // return fetch(`//api.github.com/users/${login}`)
	   //   .then(response => {
	   //     if (!response.ok) {
	   //       throw new Error(response.statusText)
	   //     }
	   //     return response.json()
	   //   })
	   //   .catch(error => {
	   //     Swal.showValidationMessage(
	   //       `Request failed: ${error}`
	   //     )
	   //   })
	 },
	 allowOutsideClick: () => !Swal.isLoading()
	}).then((result) => {
	//   if (result.isConfirmed) {
	//     Swal.fire({
	//       title: `${result.value.login}'s avatar`,
	//       imageUrl: result.value.avatar_url
	//     })
	//   }
	})
	}

	
</script>


@endpush
@section('body')
<div class="col-md-12" style="margin-top: 35px;" id="form-container">
	<div class="row" style="  padding: 10px;">
		<div style="display:none" class="col-md-12 not-ready" >Not ready to submit your reservation? 
			<a id="save_later" class="saveButton vres-btn bt-primary float-right">Save for Later</a>
		</div>
		<div class="col-md-8 ">
			<div class="col-md-12 guest-info">
				<form id="booking-form" action="{{ url('store_book_details') }}" method="POST">
					@csrf
					<div class="container">
						<div class="row" style="margin-top: 14px;">
							<h5 class="guestinfotitle" style="margin-bottom: 28px;white-space: pre;color: #0079b9 !important;"><span class="guestinfotitle-heading" style="font-weight: bold">Guest Information </span></h5>
							@php
							$holder = null;
							@endphp
							@if($booking->BookingDetail)
							@foreach($booking->BookingDetail as $key => $details)
							@php
							$holder = $holder ." ".$details->id;
							@endphp
							<input type="hidden" id="holder" name="holder" value="{{  $holder }}">
							<div class="col-md-12 room " style="margin-top:8px;">
								<h7>
								ROOM {{   $key + 1 }} : <span>  {{ $details->House->name }} ({{  $details->number_of_adults + $details->number_of_children }} Guests)</span></h7>
							</div>
							<div class="col-md-12">
								<div class="col-md-12 room" style="padding-left:0px;">
									<h7>Guest Name <span style="color:red">*</span></h7>
								</div>
								<div class="container" style="margin-top:10px;">
									<div class="row">
										<div class="col-md-3" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 0px;
											padding-right: 0px;">
											<select id="title{{ $details->id  }}" name="title{{ $details->id  }}" class="title_class form-control" required class="required" aria-required="true">
												<option value="" selected>--Select--</option>
												<option value="Mr">Mr.</option>
												<option value="Mrs">Mrs.</option>
												<option value="Ms">Ms.</option>
												<option value="Sir">Sir.</option>
												<option value="Sr">Sr.</option>
												<option value="Dr">Dr.</option>
												<option value="Jn">Jn.</option>
												<option value="Mam">Mam.</option>
											</select>
										</div>
										<div class="col-md-3" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 1px;
											padding-right: 1px">
											<input id="first_name{{ $details->id  }}" name="first_name{{ $details->id  }}" class="first_name_class form-control" required type="text" placeholder="First Name">
										</div>
										<div class="col-md-3" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 1px;
											padding-right: 1px">
											<input id="last_name{{ $details->id  }}" name="last_name{{ $details->id  }}" class="last_name_class form-control" required type="text" placeholder="Last Name">
										</div>
										<div class="col-md-3" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 3px;
											padding-right: 3px">
											<input id="special_request{{ $details->id  }}" name="special_request{{ $details->id  }}" class="form-control" type="hidden">
											<label onclick="special_request('{{ $details->id }}')" style="color: #0079b9 !important;">&nbsp;Special Requests <i class="fa fa-plus"></i> </label>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							@endif
							<div class="col-md-12">
								<p>Mobile <span style="color:red">*</span></p>
							</div>
							<div style="margin-top: -17px;">
								<div class="container" style="">
									<div class="row">
										<div class="col-md-6" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 15px;
											padding-right: 17px;">
											<select id="country_code" name="country_code" class="country_code form-control" required aria-required="true">
												<option value="" selected="selected">Country Code</option>
												<option value="+995 44 +7 840, 940|Abkhazia">Abkhazia (+995 44 +7 840, 940)</option>
												<option value="+93|Afghanistan">Afghanistan (+93)</option>
												<option value="+355|Albania">Albania (+355)</option>
												<option value="+213|Algeria">Algeria (+213)</option>
												<option value="+1 684|American Samoa">American Samoa (+1 684)</option>
												<option value="+376|Andorra">Andorra (+376)</option>
												<option value="+244|Angola">Angola (+244)</option>
												<option value="+1 264|Anguilla">Anguilla (+1 264)</option>
												<option value="+1 268|Antigua and Barbuda">Antigua and Barbuda (+1 268)</option>
												<option value="+54|Argentina">Argentina (+54)</option>
												<option value="+374|Armenia">Armenia (+374)</option>
												<option value="+297|Aruba">Aruba (+297)</option>
												<option value="+247|Ascension Island">Ascension Island (+247)</option>
												<option value="+61|Australia">Australia (+61)</option>
												<option value="+672 1x|Australian Antarctic Territory">Australian Antarctic Territory (+672 1x)</option>
												<option value="+43|Austria">Austria (+43)</option>
												<option value="+994|Azerbaijan">Azerbaijan (+994)</option>
												<option value="+1 242|Bahamas">Bahamas (+1 242)</option>
												<option value="+973|Bahrain">Bahrain (+973)</option>
												<option value="+880|Bangladesh">Bangladesh (+880)</option>
												<option value="+1 246|Barbados">Barbados (+1 246)</option>
												<option value="+375|Belarus">Belarus (+375)</option>
												<option value="+32|Belgium">Belgium (+32)</option>
												<option value="+501|Belize">Belize (+501)</option>
												<option value="+229|Benin">Benin (+229)</option>
												<option value="+1 441|Bermuda">Bermuda (+1 441)</option>
												<option value="+975|Bhutan">Bhutan (+975)</option>
												<option value="+591|Bolivia">Bolivia (+591)</option>
												<option value="+599 7|Bonaire">Bonaire (+599 7)</option>
												<option value="+387|Bosnia and Herzegovina">Bosnia and Herzegovina (+387)</option>
												<option value="+267|Botswana">Botswana (+267)</option>
												<option value="+55|Brazil">Brazil (+55)</option>
												<option value="+246|British Indian Ocean Territory">British Indian Ocean Territory (+246)</option>
												<option value="+1 284|British Virgin Islands">British Virgin Islands (+1 284)</option>
												<option value="+673|Brunei">Brunei (+673)</option>
												<option value="+359|Bulgaria">Bulgaria (+359)</option>
												<option value="+226|Burkina Faso">Burkina Faso (+226)</option>
												<option value="+257|Burundi">Burundi (+257)</option>
												<option value="+855|Cambodia">Cambodia (+855)</option>
												<option value="+237|Cameroon">Cameroon (+237)</option>
												<option value="+1|Canada">Canada (+1)</option>
												<option value="+238|Cape Verde">Cape Verde (+238)</option>
												<option value="+1 345|Cayman Islands">Cayman Islands (+1 345)</option>
												<option value="+236|Central African Republic">Central African Republic (+236)</option>
												<option value="+235|Chad">Chad (+235)</option>
												<option value="+56|Chile">Chile (+56)</option>
												<option value="+86|China">China (+86)</option>
												<option value="+61 8 9164|Christmas Island">Christmas Island (+61 8 9164)</option>
												<option value="+61 8 9162|Cocos Islands">Cocos Islands (+61 8 9162)</option>
												<option value="+57|Colombia">Colombia (+57)</option>
												<option value="+682|Cook Islands">Cook Islands (+682)</option>
												<option value="+506|Costa Rica">Costa Rica (+506)</option>
												<option value="+225|Côte d" ivoire'="">Côte d'Ivoire (+225)</option>
												<option value="+385|Croatia">Croatia (+385)</option>
												<option value="+53|Cuba">Cuba (+53)</option>
												<option value="+599 9|Curacao">Curacao (+599 9)</option>
												<option value="+357|Cyprus">Cyprus (+357)</option>
												<option value="+420|Czech Republic">Czech Republic (+420)</option>
												<option value="+243|Democratic Republic of the Congo">Democratic Republic of the Congo (+243)</option>
												<option value="+45|Denmark">Denmark (+45)</option>
												<option value="+253|Djibouti">Djibouti (+253)</option>
												<option value="+1 767|Dominica">Dominica (+1 767)</option>
												<option value="+1 809 / 829 / 849|Dominican Republic">Dominican Republic (+1 809 / 829 / 849)</option>
												<option value="+670|East Timor">East Timor (+670)</option>
												<option value="+593|Ecuador">Ecuador (+593)</option>
												<option value="+20|Egypt">Egypt (+20)</option>
												<option value="+503|El Salvador">El Salvador (+503)</option>
												<option value="+240|Equatorial Guinea">Equatorial Guinea (+240)</option>
												<option value="+291|Eritrea">Eritrea (+291)</option>
												<option value="+372|Estonia">Estonia (+372)</option>
												<option value="+251|Ethiopia">Ethiopia (+251)</option>
												<option value="+500|Falkland Islands">Falkland Islands (+500)</option>
												<option value="+298|Faroe Islands">Faroe Islands (+298)</option>
												<option value="+691|Federated States of Micronesia">Federated States of Micronesia (+691)</option>
												<option value="+679|Fiji">Fiji (+679)</option>
												<option value="+358|Finland">Finland (+358)</option>
												<option value="+33|France">France (+33)</option>
												<option value="+594|French Guiana">French Guiana (+594)</option>
												<option value="+689|French Polynesia">French Polynesia (+689)</option>
												<option value="+241|Gabon">Gabon (+241)</option>
												<option value="+220|Gambia">Gambia (+220)</option>
												<option value="+995|Georgia">Georgia (+995)</option>
												<option value="+49|Germany">Germany (+49)</option>
												<option value="+233|Ghana">Ghana (+233)</option>
												<option value="+350|Gibraltar">Gibraltar (+350)</option>
												<option value="+881|Global Mobile Satellite System">Global Mobile Satellite System (+881)</option>
												<option value="+30|Greece">Greece (+30)</option>
												<option value="+299|Greenland">Greenland (+299)</option>
												<option value="+1 473|Grenada">Grenada (+1 473)</option>
												<option value="+590|Guadeloupe">Guadeloupe (+590)</option>
												<option value="+1 671|Guam">Guam (+1 671)</option>
												<option value="+502|Guatemala">Guatemala (+502)</option>
												<option value="+44 1481|Guernsey">Guernsey (+44 1481)</option>
												<option value="+224|Guinea">Guinea (+224)</option>
												<option value="+245|Guinea-Bissau">Guinea-Bissau (+245)</option>
												<option value="+592|Guyana">Guyana (+592)</option>
												<option value="+509|Haiti">Haiti (+509)</option>
												<option value="+504|Honduras">Honduras (+504)</option>
												<option value="+852|Hong Kong">Hong Kong (+852)</option>
												<option value="+36|Hungary">Hungary (+36)</option>
												<option value="+354|Iceland">Iceland (+354)</option>
												<option value="+91|India">India (+91)</option>
												<option value="+62|Indonesia">Indonesia (+62)</option>
												<option value="+800|International Freephone UIFN">International Freephone UIFN (+800)</option>
												<option value="+979|International Premium Rate Service">International Premium Rate Service (+979)</option>
												<option value="+98|Iran">Iran (+98)</option>
												<option value="+964|Iraq">Iraq (+964)</option>
												<option value="+353|Ireland">Ireland (+353)</option>
												<option value="+44 1624|Isle of Man">Isle of Man (+44 1624)</option>
												<option value="+972|Israel">Israel (+972)</option>
												<option value="+39|Italy">Italy (+39)</option>
												<option value="+1 876|Jamaica">Jamaica (+1 876)</option>
												<option value="+81|Japan">Japan (+81)</option>
												<option value="+44 1534|Jersey">Jersey (+44 1534)</option>
												<option value="+962|Jordan">Jordan (+962)</option>
												<option value="+7 6xx, 7xx|Kazakhstan">Kazakhstan (+7 6xx, 7xx)</option>
												<option value="+254|Kenya">Kenya (+254)</option>
												<option value="+686|Kiribati">Kiribati (+686)</option>
												<option value="+383|Kosovo">Kosovo (+383)</option>
												<!--Prashant - 12/08/2021 - purpose: change country code [RES-2866] -->
												<option value="+965|Kuwait">Kuwait (+965)</option>
												<option value="+996|Kyrgyzstan">Kyrgyzstan (+996)</option>
												<option value="+856|Laos">Laos (+856)</option>
												<option value="+371|Latvia">Latvia (+371)</option>
												<option value="+961|Lebanon">Lebanon (+961)</option>
												<option value="+266|Lesotho">Lesotho (+266)</option>
												<option value="+231|Liberia">Liberia (+231)</option>
												<option value="+218|Libya">Libya (+218)</option>
												<option value="+423|Liechtenstein">Liechtenstein (+423)</option>
												<option value="+370|Lithuania">Lithuania (+370)</option>
												<option value="+352|Luxembourg">Luxembourg (+352)</option>
												<option value="+853|Macau">Macau (+853)</option>
												<option value="+389|Macedonia">Macedonia (+389)</option>
												<option value="+261|Madagascar">Madagascar (+261)</option>
												<option value="+265|Malawi">Malawi (+265)</option>
												<option value="+60|Malaysia">Malaysia (+60)</option>
												<option value="+960|Maldives">Maldives (+960)</option>
												<option value="+223|Mali">Mali (+223)</option>
												<option value="+356|Malta">Malta (+356)</option>
												<option value="+692|Marshall Islands">Marshall Islands (+692)</option>
												<option value="+596|Martinique">Martinique (+596)</option>
												<option value="+222|Mauritania">Mauritania (+222)</option>
												<option value="+230|Mauritius">Mauritius (+230)</option>
												<option value="+262 269 / 639|Mayotte">Mayotte (+262 269 / 639)</option>
												<option value="+52|Mexico">Mexico (+52)</option>
												<option value="+373|Moldova">Moldova (+373)</option>
												<option value="+377|Monaco">Monaco (+377)</option>
												<option value="+976|Mongolia">Mongolia (+976)</option>
												<option value="+382|Montenegro">Montenegro (+382)</option>
												<option value="+1 664|Montserrat">Montserrat (+1 664)</option>
												<option value="+212|Morocco">Morocco (+212)</option>
												<option value="+258|Mozambique">Mozambique (+258)</option>
												<option value="+95|Myanmar">Myanmar (+95)</option>
												<option value="+374 47 / 97|Nagorno-Karabakh">Nagorno-Karabakh (+374 47 / 97)</option>
												<option value="+264|Namibia">Namibia (+264)</option>
												<option value="+674|Nauru">Nauru (+674)</option>
												<option value="+977|Nepal">Nepal (+977)</option>
												<option value="+31|Netherlands">Netherlands (+31)</option>
												<option value="+687|New Caledonia">New Caledonia (+687)</option>
												<option value="+64|New Zealand">New Zealand (+64)</option>
												<option value="+505|Nicaragua">Nicaragua (+505)</option>
												<option value="+227|Niger">Niger (+227)</option>
												<option value="+234|Nigeria">Nigeria (+234)</option>
												<option value="+683|Niue">Niue (+683)</option>
												<option value="+672 3|Norfolk Island">Norfolk Island (+672 3)</option>
												<option value="+850|North Korea">North Korea (+850)</option>
												<option value="+1 670|Northern Mariana Islands">Northern Mariana Islands (+1 670)</option>
												<option value="+47|Norway">Norway (+47)</option>
												<option value="+968|Oman">Oman (+968)</option>
												<option value="+92|Pakistan">Pakistan (+92)</option>
												<option value="+680|Palau">Palau (+680)</option>
												<option value="+970|Palestinian territories">Palestinian territories (+970)</option>
												<option value="+507|Panama">Panama (+507)</option>
												<option value="+675|Papua New Guinea">Papua New Guinea (+675)</option>
												<option value="+595|Paraguay">Paraguay (+595)</option>
												<option value="+51|Peru">Peru (+51)</option>
												<option value="+63|Philippines">Philippines (+63)</option>
												<option value="+48|Poland">Poland (+48)</option>
												<option value="+351|Portugal">Portugal (+351)</option>
												<option value="+1 787 / 939|Puerto Rico">Puerto Rico (+1 787 / 939)</option>
												<option value="+974|Qatar">Qatar (+974)</option>
												<!--<option value='+886|Republic of China (Taiwan)'>Republic of China (Taiwan) (+886)</option>-->
												<option value="+242|Republic of the Congo">Republic of the Congo (+242)</option>
												<option value="+262|Réunion">Réunion (+262)</option>
												<option value="+40|Romania">Romania (+40)</option>
												<option value="+7|Russia">Russia (+7)</option>
												<option value="+250|Rwanda">Rwanda (+250)</option>
												<option value="+599 4|Saba">Saba (+599 4)</option>
												<option value="+290|Saint Helena">Saint Helena (+290)</option>
												<option value="+1 869|Saint Kitts and Nevis">Saint Kitts and Nevis (+1 869)</option>
												<option value="+1 758|Saint Lucia">Saint Lucia (+1 758)</option>
												<option value="+1 784|Saint Vincent and the Grenadines">Saint Vincent and the Grenadines (+1 784)</option>
												<option value="+508|Saint-Pierre and Miquelon">Saint-Pierre and Miquelon (+508)</option>
												<option value="+685|Samoa">Samoa (+685)</option>
												<option value="+378|San Marino">San Marino (+378)</option>
												<option value="+239|Sao Tome and Principe">Sao Tome and Principe</option>
												<option value="+966|Saudi Arabia">Saudi Arabia (+966)</option>
												<option value="+221|Senegal">Senegal (+221)</option>
												<option value="+381|Serbia">Serbia (+381)</option>
												<option value="+248|Seychelles">Seychelles (+248)</option>
												<option value="+232|Sierra Leone">Sierra Leone (+232)</option>
												<option value="+65|Singapore">Singapore (+65)</option>
												<option value="+599 3|Sint Eustatius">Sint Eustatius (+599 3)</option>
												<option value="+599 5|Sint Maarten">Sint Maarten (+599 5)</option>
												<option value="+421|Slovakia">Slovakia (+421)</option>
												<option value="+386|Slovenia">Slovenia (+386)</option>
												<option value="+677|Solomon Islands">Solomon Islands (+677)</option>
												<option value="+252|Somalia">Somalia (+252)</option>
												<option value="+27|South Africa">South Africa (+27)</option>
												<option value="+82|South Korea">South Korea (+82)</option>
												<option value="+211|South Sudan">South Sudan (+211)</option>
												<option value="+34|Spain">Spain (+34)</option>
												<option value="+94|Sri Lanka">Sri Lanka (+94)</option>
												<option value="+249|Sudan">Sudan (+249)</option>
												<option value="+597|Suriname">Suriname (+597)</option>
												<option value="+268|Swaziland">Swaziland (+268)</option>
												<option value="+46|Sweden">Sweden (+46)</option>
												<option value="+41|Switzerland">Switzerland (+41)</option>
												<option value="+963|Syria">Syria (+963)</option>
												<option value="+886|Taiwan">Taiwan (+886)</option>
												<!--Parth - 30 Apr 2019 - RES-1963 - Add Taiwan Country code.-->
												<option value="+992|Tajikistan">Tajikistan (+992)</option>
												<option value="+255|Tanzania">Tanzania (+255)</option>
												<option value="+888|Telecommunications for Disaster Relief by OCHA">Telecommunications for Disaster Relief by OCHA (+888)</option>
												<option value="+66|Thailand">Thailand (+66)</option>
												<option value="+228|Togo">Togo (+228)</option>
												<option value="+690|Tokelau">Tokelau (+690)</option>
												<option value="+676|Tonga">Tonga (+676)</option>
												<option value="+1 868|Trinidad and Tobago">Trinidad and Tobago (+1 868)</option>
												<option value="+290 8|Tristan da Cunha">Tristan da Cunha (+290 8)</option>
												<option value="+216|Tunisia">Tunisia (+216)</option>
												<option value="+90|Turkey">Turkey (+90)</option>
												<option value="+993|Turkmenistan">Turkmenistan (+993)</option>
												<option value="+1 649|Turks and Caicos Islands">Turks and Caicos Islands (+1 649)</option>
												<option value="+688|Tuvalu">Tuvalu (+688)</option>
												<option value="+1 340|U.S. Virgin Islands">U.S. Virgin Islands (+1 340)</option>
												<option value="+256|Uganda">Uganda (+256)</option>
												<option value="+380|Ukraine">Ukraine (+380)</option>
												<option value="+971|United Arab Emirates">United Arab Emirates (+971)</option>
												<option value="+44|United Kingdom">United Kingdom (+44)</option>
												<option value="+1|United States of America">United States of America (+1)</option>
												<option value="+598|Uruguay">Uruguay (+598)</option>
												<option value="+998|Uzbekistan">Uzbekistan (+998)</option>
												<option value="+678|Vanuatu">Vanuatu (+678)</option>
												<option value="+39 06 698|Vatican City">Vatican City (+39 06 698)</option>
												<option value="+58|Venezuela">Venezuela (+58)</option>
												<option value="+84|Vietnam">Vietnam (+84)</option>
												<option value="+681|Wallis and Futuna">Wallis and Futuna (+681)</option>
												<option value="+212 5288 / 5289|Western Sahara">Western Sahara (+212 5288 / 5289)</option>
												<option value="+967|Yemen">Yemen (+967)</option>
												<option value="+260|Zambia">Zambia (+260)</option>
												<option value="+255 24|Zanzibar">Zanzibar (+255 24)</option>
												<option value="+263|Zimbabwe">Zimbabwe (+263)</option>
											</select>
										</div>
										<div class="col-md-6" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 17px;
padding-right: 17px;">
											<input placeholder="Phone number" required class="phone_number form-control" type="number" name="phone" id="phone">
										</div>
										<div class="col-md-3" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 1px;
											padding-right: 1px">
										</div>
										<div class="col-md-3" style="padding-top: 13px;
											padding-bottom: 13px;padding-left: 3px;
											padding-right: 3px">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12" style="margin-top: -25px;">
								<p>Email <span style="color:red">*</span></p>
							</div>
							<div class="col-md-12">
								<div class="row" style="
									margin-bottom: 30px;">
									<div class="col-md-4"><input required placeholder="email" class="email_address form-control" type="email" name="booking_email" id="booking_email"></div>
								</div>
							</div>
							<div class="col-md-12">Cancellation Policy</div>
							<div class="col-md-12">
								<p>You will be charged the total price of the reservation after booking. Amount charged will be non-refundable.</p>
							</div>
							<div class="col-md-12"><input type="checkbox" name=""> I acknowledge and accept the Terms of Cancellation Policy . <span style="color: red">*</span></div>
							<div class="col-md-12">
								{{-- <button class="float-right primary"  style="submit" c>Review your Booking </button> --}}
								<button   type="button" onclick="save_booking_data()" class="float-right primary">Book Now</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12 summary">
				<div class="row" style="margin: inherit;">
					<div class="col-md-12" style="margin-top: 10px;">
						<h5>Your Booking summary</h5>
					</div>
					<div  class="col-md-11" style="
						border-bottom: 1px solid #ccc;margin-top: 10px;margin: auto;
						margin-top: auto;
						margin-bottom: auto;
						margin-top: 10px;
						margin-bottom: 10px;"></div>
					<div class="col-md-12">
						<h6 style="color: #0079b9;font-weight: bold;">Luxury Beach Villa</h6>
					</div>
					<div class="col-md-12" style="margin-top: 8px;">
						<p style="color:black">Mbezi Beach White sands road, Dar es Salaam.</p>
					</div>
					<div class="col-md-12" style="margin-top:-9px;"> <i class="fa fa-phone"></i> +255756883598 </div>
					<div class="col-md-12" style="margin-top:7px;"> <i class="fa fa-envelope"></i>&nbsp;luxury@luxurybeachvilla.co.tz  </div>
					<div class="col-md-12" style="margin-top:29px;">
						<div class="row">
							<div class="col-md-4" ><strong>Check In</strong><br> {{  $booking->arrival_date }}  </div>
							<div class="col-md-4" ><strong>Check Out</strong><br> {{  $booking->departure_date }} </div>
							<div class="col-md-4" ><strong>No Of Night(s)</strong><br> {{  $booking->number_of_days }} </div>
						</div>
					</div>
					<div class="col-md-12" style="margin-top:7px;"><a href="{{ url('/') }}"><button class="primary">Change Dates</button></a></div>
					<div  class="col-md-11" style="
						border-bottom: 1px solid #ccc;margin-top: 10px;margin: auto;
						margin-top: auto;
						margin-bottom: auto;
						margin-top: 10px;
						margin-bottom: 10px;"></div>
					<div class="col-md-12" >Rooms & Rates ( Price for 1 Night )</div>
					<div style="margin-bottom: 13px;">
						@php
						$price_code = 0;
						$price_total = 0;
						@endphp
						@if($booking->BookingDetail)
						@foreach($booking->BookingDetail as $key => $data)
						<div>
							<div class="col-md-12"><strong style="font-weight: bold">Room {{  $key +1  }}</strong> :</div>
							<div class="col-md-12">Rateplan Name : {{ $data->House->name }}</div>
							<div class="col-md-12"><strong>Pax Details </strong>: {{  $data->number_of_adults }} Adults , {{ $data->number_of_children }} Child</div>
							<div class="col-md-12" style="color: red">(Non-Refundable)</div>
							<div class="col-md-12" style="margin-top: 21px;">
								<div class="row" style="padding-left: 16px;
									padding-right: 16px;">
									<div class="col-md-6" style="border: 1px solid #ced4da;padding-top: 9px;
										padding-bottom: 9px;">Total Charges: </div>
									<div class="col-md-6" style="border: 1px solid #ced4da;padding-top: 9px;
										padding-bottom: 9px;">{{  $data->House->Currency->code  }} {{  $data->House->cost* $booking->number_of_days   }} </div>
								</div>
							</div>
						</div>
						@php
						$price_code = $data->House->Currency->code;
						$price_total = $price_total + $data->House->cost;
						@endphp
						@endforeach
						@endif
					</div>
					<div class="col-md-6">Total Room Charges</div>
					<div class="col-md-6">{{  $price_code  }} {{  $price_total * $booking->number_of_days  }} </div>
					<div class="col-md-6">Total Price (Inc. Of Taxes)</div>
					<div class="col-md-6">{{  $price_code  }} {{  $price_total * $booking->number_of_days  }} </div>
					<div class="col-md-6">Total Payable Now</div>
					<div class="col-md-6">{{  $price_code  }} {{  $price_total * $booking->number_of_days  }} </div>
					<div class="col-md-6">Amount due at time of check-in</div>
					<div class="col-md-6">{{  $price_code  }} 0.00 </div>
					<div class="col-md-12">Price Breakdown / Rate Details</div>
					<div class="col-md-12" style="margin-top: 15px;margin-top: 15px;
						background: #e1e1e1;
						
						margin-top: ;
						padding-top: 19px;box-shadow: 0 2px 6px rgba(0, 0, 0, 0.15);">
						<div class="row" style="font-size: large;
							font-weight: bold;">
							<div class="col-md-6" style="color: #0079b9;text-align: left;">Pay Now:</div>
						
							<div class="col-md-6" style="color: #0079b9;text-align: left;">{{  $price_code  }} {{  $price_total * $booking->number_of_days  }} </div>
						</div>
					</div>
					<!-- <div class="col-md-8"><input type="text" name="promotion" ></div>
						<div class="col-md-4">$ 201.00</div> -->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

