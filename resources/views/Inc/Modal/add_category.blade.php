<div class="modal fade" id="add_category" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Category</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
            <form method="POST" action="{{  route('category.store') }}" enctype='multipart/form-data'>
				@csrf
				<div class="modal-body">
					<div class="form-floating" >
						<label style="margin-top: -11px;" for="name">Name:</label>
						<input type="text" class="form-control" placeholder="name" id="name" placeholder="Eg Jacuzzi" name="name">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>


					

					<div class="form-floating" style="padding-top: 10px;">
						<label for="description" >Description:</label>
						<textarea   name="description" class="form-control" style="height: 100px;" maxlength="150" placeholder="Descriptions for this facility" rows="5" id="description"></textarea>
					</div>

                    
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>

      
      </div>
    </div>
  </div>