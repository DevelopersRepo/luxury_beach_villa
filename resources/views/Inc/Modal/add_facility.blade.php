<div class="modal fade" id="add_facility" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Facility</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
            <form method="POST" action="{{  route('facility.store') }}" enctype='multipart/form-data'>
				@csrf
				<div class="modal-body">

					<div class="form-floating" >
						<label for="name" style="margin-top: -11px;">Name:</label>
						<input type="text" placeholder="name" class="form-control" id="name" placeholder="Eg Swimming pool" name="name">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>
					

					
					<div class="form-floating" style="padding-top: 10px;">
						<label for="description" style="background: #fff;
						display: block;width: 95%;
						z-index: 20;
						height: 52px;
						margin-top: 11px;
						margin-left: 3px;">Feature(s):</label>
				
						

						<select name="feature[]"  id="feature" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
							{{-- <label for="description" >Feature(s):</label> --}}
								{{-- <optgroup selected="">Feature(s):</optgroup> --}}
								@if(count($features)>0)
								@foreach($features as $data)
							<option style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->name }}</option>
							@endforeach
							@endif
							

						  </select>



					</div>


					<div class="form-floating" style="padding-top: 10px;">
						<label for="description" >Description:</label>
						<textarea   name="description" class="form-control" style="height: 100px;" maxlength="150" placeholder="Descriptions for this facility" rows="5" id="description"></textarea>
					</div>
                    

					<div class="form-floating" style="padding-top: 10px;">
						{{-- <label for="file">Select picture (If Any):</label> --}}
						<input type="file" style="padding-top: 20px;"  class="form-control" id="file" name="file">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>


					
					<div class="form-floating" style="padding-top: 10px;">
						
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
							<label class="form-check-label" for="securityNotify">
							  Visible on website.
							</label>
						  </div>
					</div>


					


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>

      
      </div>
    </div>
  </div>