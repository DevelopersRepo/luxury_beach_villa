<div class="modal fade" id="create_gallery_category" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Gallery Category</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
            <form method="POST" action="{{  route('gallery_category.store') }}">
				@csrf
				<div class="modal-body">
					<div class="form-group">
						<label>Name:</label>
						<input type="text" placeholder="Eg Surroundings" class="form-control input-default shadow-input" name="name" id="name" required >                   
					</div>
					<div class="form-group" style="padding-top: 10px;">
						<label>Description:</label>
						<textarea  name="description" class="form-control" maxlength="150" placeholder="Description about this category" rows="3" id="description"></textarea>
					</div>
                    
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>

      
      </div>
    </div>
  </div>