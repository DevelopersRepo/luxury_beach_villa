

<div class="modal fade" id="add_house" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable ">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Room</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form method="POST"  action="{{  route('house.store') }}" enctype='multipart/form-data'>
					@csrf
					<div class="form-floating" >
						<label for="name" style="margin-top: -11px;">Name:</label>
						<input type="text" placeholder="name" class="form-control" id="name" placeholder="Eg Swimming pool" name="name">
					</div>
					<div style="display: inline-flex;">
						<div class="form-floating" style="padding-top: 10px;margin-right:10px;"  >
							<label for="floatingSelect1">Price Per Room:</label>
							<input type="number" required  class="form-control" id="cost"  name="cost">
						</div>
						<div class="form-floating" style="padding-top: 10px;margin-right:10px;">
							<label for="currency_id">Currency:</label>
							<select name="currency_id" id="currency_id" required class="form-select" required id="currency_id" aria-label="Floating label select example">
								@if(count($currencies)>0)
								<option selected="" value="">--Select Currency--</option>
								@foreach($currencies as $data)
								<option value={{  $data->id  }}>{{  $data->code }}</option>
								@endforeach
								@endif
							</select>
							
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="currency_id" style="font-size: x-small;">Reference (Eg Per Month):</label>
							<input type="text" required placeholder="Eg Per Month"  class="form-control" id="Price_reference"  name="Price_reference">
						</div>
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<label for="number_of_rooms">Number Of Room:</label>
						<input required type="text"  class="form-control" id="number_of_rooms" name="number_of_rooms">
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<label for="max_number_of_children">Max Number Of Children Per Room:</label>
						<input required type="text"  class="form-control" id="max_number_of_children" name="max_number_of_children">
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<label for="max_number_of_adults">Max Number Of Adult Per Room:</label>
						<input required type="text"  class="form-control" id="max_number_of_adults" name="max_number_of_adults">
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<label for="name">Location:</label>
						<input required type="text" placeholder="name" class="form-control" id="location" value="{{  $organisation_location->name }}" name="location">
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<label for="name">Category:</label>
						<select name="category_id" id="category_id" required class="form-select" required id="floatingSelect2" aria-label="Floating label select example">
							@if(count($categories)>0)
							<option selected="" value="">--Select category--</option>
							@foreach($categories as $data)
							<option value={{  $data->id  }}>{{  $data->name }}</option>
							@endforeach
							@endif
						</select>
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<div class="form-floating" style="padding-top: 10px;">
							<input type="file" style="padding-top: 20px;border: dashed;border-color: #66A6BC;"  class="form-control" id="file" name="file">
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="activity" style="background: #fff;
								display: block;width: 95%;
								z-index: 20;
								height: 52px;
								margin-top: 11px;
								margin-left: 3px;">Activities(s):</label>
							<select name="activity[]"  id="activity" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
								@if(count($activities)>0)
								<option selected value="">--Select Activity--</option>
								@foreach($activities as $data)
								<option style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->title }}</option>
								@endforeach
								@endif
							</select>
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="facility" style="background: #fff;
								display: block;width: 95%;
								z-index: 20;
								height: 52px;
								margin-top: 11px;
								margin-left: 3px;">Facility(s):</label>
							<select name="facility[]"  id="facility" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
								@if(count($facilities)>0)
								<option selected value="">--Select Facility--</option>
								@foreach($facilities as $data)
								<option style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->name }}</option>
								@endforeach
								@endif							
							</select>
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="description" >Description:</label>
							<textarea   name="description" class="form-control" style="height: 100px;" maxlength="150" placeholder="Descriptions for this facility" rows="5" id="description"></textarea>
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label style="margin-top: -8px;
								margin-left: 10px;" for="isAvailable"  class="form-check-label" >Available:</label>
							<input class="form-check-input" checked="" type="checkbox" name="isAvailable" id="isAvailable">
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
								<label class="form-check-label" for="securityNotify">
								Visible on website.
								</label>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
				</form>
				</div>
			</div>
		</div>
	</div>
	
	