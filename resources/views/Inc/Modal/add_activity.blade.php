

<div class="modal fade" id="add_activity" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Activity</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form method="POST" action="{{  route('activity.store') }}" enctype='multipart/form-data'>
				@csrf
				<div class="modal-body">
					<div class="form-floating" >
						<label for="title" style="margin-top: -11px;">Title:</label>
						<input type="text" required placeholder="title" class="form-control" id="title" placeholder="Eg Clean Beach" name="title">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>


					<div class="form-floating" style="padding-top: 10px;">
						<label for="description" >Front Description:</label> 
						
						
						<textarea  required  name="front_description" class="form-control" style="height: 60px;" maxlength="150" placeholder="" rows="5" id="front_description"></textarea>
						
					</div>



					<div class="form-floating"  style="padding-top: 10px;">
						{{-- <label for="file" style="margin-left: 20px;">Image (1620x2700):</label> --}}
						<input type="file" required style="padding-top: 20px;" class="form-control" id="front_file" name="front_file">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>



				






					<div class="form-floating" style="padding-top: 10px;">
						<label for="feature" style="background: #fff;
							display: block;width: 96%;
							z-index: 20;
							height: 52px;
							margin-top: 11px;
							margin-left: 3px;">Facility(s):</label>
						<select name="facility[]"  id="facility" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
							{{-- <label for="description" >Feature(s):</label> --}}
							{{-- 
							<optgroup selected="">Feature(s):</optgroup>
							--}}
							@if(count($facilities)>0)
							@foreach($facilities as $data)
							<option style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->name }}</option>
							@endforeach
							@else
							<option style="" value="">No any facility registered.</option>
							@endif
						</select>
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<label for="description" >Description:</label> 
						
						
						<textarea   name="activity_desc" class="form-control" style="height: 100px;" maxlength="150" placeholder="Descriptions for this activity" rows="5" id="activity_desc"></textarea>
						
					</div>
					<div class="form-floating" style="padding-top: 10px;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
							<label class="form-check-label" for="securityNotify">
							Visible on website.
							</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

