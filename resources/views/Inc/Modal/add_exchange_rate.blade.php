<div class="modal fade" id="add_exchange_rate" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Echange Rate</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
        <form method="POST" action="{{  route('exchange.store') }}" enctype='multipart/form-data'>
				@csrf
				<div class="modal-body">
					<div class="form-floating" >
						<label style="margin-top: -11px;" for="name">Price:</label>
						<input type="text" class="form-control" placeholder="name" id="name" placeholder="Eg Jacuzzi" name="name">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>

      
      </div>
    </div>
  </div>