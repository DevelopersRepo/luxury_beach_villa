<div class="modal fade" id="add_image" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Image</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
            <form method="POST" action="{{  route('image.store') }}" enctype='multipart/form-data'>
				@csrf
				<div class="modal-body">
					
					<div class="form-floating"  style="padding-top: 10px;display: contents;">
						{{-- <label for="file" style="margin-left: 20px;">Image:</label> --}}
						<input type="file" style="padding-top: 20px;" class="form-control" id="file" name="file">
						{{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
					</div>


					<div class="form-floating" style="padding-top: 10px;">
						<label  for="category_id">Category:</label>
						<select class="form-select" name="category_id" id="category_id" aria-label="Default select example">

							@if(count($galleryCategory)>0)
							<option value="">Select category</option>

							@foreach($galleryCategory as $data)
							<option value={{ $data->id }}>{{  $data->name  }}</option>
							@endforeach
							@else
							<option value="" >Please add category</option>
							
							@endif
						  </select>
					</div>


					<div class="form-floating" style="padding-top: 10px;">
						<label for="quality_level">Resolution:</label>
						<select class="form-select" id="quality_level"  name="quality_level" aria-label="Default select example">
							<option selected="800" value="0">Default</option>
							<option  value="200">High</option>
							<option value="100">Medium</option>
							<option value="50">Low</option>
						  </select>
					 </div>



					<div class="form-floating" style="padding-top: 10px;">
						<label for="description">Description:</label>
						<textarea  name="description" class="form-control" style="height: 100px;" maxlength="150" placeholder="Description for this image" rows="3" id="description"></textarea>
					</div>


					<div class="form-floating" style="padding-top: 10px;">
						
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
							<label class="form-check-label" for="securityNotify">
							  Visible on website.
							</label>
						  </div>
					</div>
                    
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>

      
      </div>
    </div>
  </div>