{{-- main  modal goes here --}}
@if(count($houses)>0)
@foreach($houses as $data_item)
<div class="modal fade jd-modal" id="house_main_modal{{  $data_item->id }}" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">{{  $data_item->name }}</h5>
				{{-- <button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button> --}}
			</div>
			
			<div class="modal-body" >
				{{-- <div class="container"> --}}
					
					{{-- <div class="tab-content"> --}}
						
						<div id="menucontent{{  $data_item->id }}3" class="tab-pane show in fade" style="padding-top: 24px">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-10" style="margin: auto;">
										<form id="quatation_form{{  $data_item->id }}" action="{{  route('quatation_request') }}"  method="post"  class="php-email-form">
											@csrf
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>First Name:</label>
															<input name="house_id_holder" id="house_id_holder" type="hidden" value="{{ $data_item->id }}"   placeholder="">
															<input name="first_name_quot{{  $data_item->id }}" type="text" class="form-control" id="first_name_quot{{  $data_item->id }}" placeholder="First Name">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Last Name:</label>
															<input name="last_name_quot{{  $data_item->id }}" type="text" class="form-control" id="last_name_quot{{  $data_item->id }}" placeholder="Last Name">
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Country Code:</label>
															<select id="country_code_quot{{  $data_item->id }}" name="country_code_quot{{  $data_item->id }}" class="country_code_quot{{  $data_item->id }} form-control" required aria-required="true">
																<option value="" selected="selected">Country Code</option>
																<option value="+995 44 +7 840, 940|Abkhazia">Abkhazia (+995 44 +7 840, 940)</option>
																<option value="+93|Afghanistan">Afghanistan (+93)</option>
																<option value="+355|Albania">Albania (+355)</option>
																<option value="+213|Algeria">Algeria (+213)</option>
																<option value="+1 684|American Samoa">American Samoa (+1 684)</option>
																<option value="+376|Andorra">Andorra (+376)</option>
																<option value="+244|Angola">Angola (+244)</option>
																<option value="+1 264|Anguilla">Anguilla (+1 264)</option>
																<option value="+1 268|Antigua and Barbuda">Antigua and Barbuda (+1 268)</option>
																<option value="+54|Argentina">Argentina (+54)</option>
																<option value="+374|Armenia">Armenia (+374)</option>
																<option value="+297|Aruba">Aruba (+297)</option>
																<option value="+247|Ascension Island">Ascension Island (+247)</option>
																<option value="+61|Australia">Australia (+61)</option>
																<option value="+672 1x|Australian Antarctic Territory">Australian Antarctic Territory (+672 1x)</option>
																<option value="+43|Austria">Austria (+43)</option>
																<option value="+994|Azerbaijan">Azerbaijan (+994)</option>
																<option value="+1 242|Bahamas">Bahamas (+1 242)</option>
																<option value="+973|Bahrain">Bahrain (+973)</option>
																<option value="+880|Bangladesh">Bangladesh (+880)</option>
																<option value="+1 246|Barbados">Barbados (+1 246)</option>
																<option value="+375|Belarus">Belarus (+375)</option>
																<option value="+32|Belgium">Belgium (+32)</option>
																<option value="+501|Belize">Belize (+501)</option>
																<option value="+229|Benin">Benin (+229)</option>
																<option value="+1 441|Bermuda">Bermuda (+1 441)</option>
																<option value="+975|Bhutan">Bhutan (+975)</option>
																<option value="+591|Bolivia">Bolivia (+591)</option>
																<option value="+599 7|Bonaire">Bonaire (+599 7)</option>
																<option value="+387|Bosnia and Herzegovina">Bosnia and Herzegovina (+387)</option>
																<option value="+267|Botswana">Botswana (+267)</option>
																<option value="+55|Brazil">Brazil (+55)</option>
																<option value="+246|British Indian Ocean Territory">British Indian Ocean Territory (+246)</option>
																<option value="+1 284|British Virgin Islands">British Virgin Islands (+1 284)</option>
																<option value="+673|Brunei">Brunei (+673)</option>
																<option value="+359|Bulgaria">Bulgaria (+359)</option>
																<option value="+226|Burkina Faso">Burkina Faso (+226)</option>
																<option value="+257|Burundi">Burundi (+257)</option>
																<option value="+855|Cambodia">Cambodia (+855)</option>
																<option value="+237|Cameroon">Cameroon (+237)</option>
																<option value="+1|Canada">Canada (+1)</option>
																<option value="+238|Cape Verde">Cape Verde (+238)</option>
																<option value="+1 345|Cayman Islands">Cayman Islands (+1 345)</option>
																<option value="+236|Central African Republic">Central African Republic (+236)</option>
																<option value="+235|Chad">Chad (+235)</option>
																<option value="+56|Chile">Chile (+56)</option>
																<option value="+86|China">China (+86)</option>
																<option value="+61 8 9164|Christmas Island">Christmas Island (+61 8 9164)</option>
																<option value="+61 8 9162|Cocos Islands">Cocos Islands (+61 8 9162)</option>
																<option value="+57|Colombia">Colombia (+57)</option>
																<option value="+682|Cook Islands">Cook Islands (+682)</option>
																<option value="+506|Costa Rica">Costa Rica (+506)</option>
																<option value="+225|Côte d" ivoire'="">Côte d'Ivoire (+225)</option>
																<option value="+385|Croatia">Croatia (+385)</option>
																<option value="+53|Cuba">Cuba (+53)</option>
																<option value="+599 9|Curacao">Curacao (+599 9)</option>
																<option value="+357|Cyprus">Cyprus (+357)</option>
																<option value="+420|Czech Republic">Czech Republic (+420)</option>
																<option value="+243|Democratic Republic of the Congo">Democratic Republic of the Congo (+243)</option>
																<option value="+45|Denmark">Denmark (+45)</option>
																<option value="+253|Djibouti">Djibouti (+253)</option>
																<option value="+1 767|Dominica">Dominica (+1 767)</option>
																<option value="+1 809 / 829 / 849|Dominican Republic">Dominican Republic (+1 809 / 829 / 849)</option>
																<option value="+670|East Timor">East Timor (+670)</option>
																<option value="+593|Ecuador">Ecuador (+593)</option>
																<option value="+20|Egypt">Egypt (+20)</option>
																<option value="+503|El Salvador">El Salvador (+503)</option>
																<option value="+240|Equatorial Guinea">Equatorial Guinea (+240)</option>
																<option value="+291|Eritrea">Eritrea (+291)</option>
																<option value="+372|Estonia">Estonia (+372)</option>
																<option value="+251|Ethiopia">Ethiopia (+251)</option>
																<option value="+500|Falkland Islands">Falkland Islands (+500)</option>
																<option value="+298|Faroe Islands">Faroe Islands (+298)</option>
																<option value="+691|Federated States of Micronesia">Federated States of Micronesia (+691)</option>
																<option value="+679|Fiji">Fiji (+679)</option>
																<option value="+358|Finland">Finland (+358)</option>
																<option value="+33|France">France (+33)</option>
																<option value="+594|French Guiana">French Guiana (+594)</option>
																<option value="+689|French Polynesia">French Polynesia (+689)</option>
																<option value="+241|Gabon">Gabon (+241)</option>
																<option value="+220|Gambia">Gambia (+220)</option>
																<option value="+995|Georgia">Georgia (+995)</option>
																<option value="+49|Germany">Germany (+49)</option>
																<option value="+233|Ghana">Ghana (+233)</option>
																<option value="+350|Gibraltar">Gibraltar (+350)</option>
																<option value="+881|Global Mobile Satellite System">Global Mobile Satellite System (+881)</option>
																<option value="+30|Greece">Greece (+30)</option>
																<option value="+299|Greenland">Greenland (+299)</option>
																<option value="+1 473|Grenada">Grenada (+1 473)</option>
																<option value="+590|Guadeloupe">Guadeloupe (+590)</option>
																<option value="+1 671|Guam">Guam (+1 671)</option>
																<option value="+502|Guatemala">Guatemala (+502)</option>
																<option value="+44 1481|Guernsey">Guernsey (+44 1481)</option>
																<option value="+224|Guinea">Guinea (+224)</option>
																<option value="+245|Guinea-Bissau">Guinea-Bissau (+245)</option>
																<option value="+592|Guyana">Guyana (+592)</option>
																<option value="+509|Haiti">Haiti (+509)</option>
																<option value="+504|Honduras">Honduras (+504)</option>
																<option value="+852|Hong Kong">Hong Kong (+852)</option>
																<option value="+36|Hungary">Hungary (+36)</option>
																<option value="+354|Iceland">Iceland (+354)</option>
																<option value="+91|India">India (+91)</option>
																<option value="+62|Indonesia">Indonesia (+62)</option>
																<option value="+800|International Freephone UIFN">International Freephone UIFN (+800)</option>
																<option value="+979|International Premium Rate Service">International Premium Rate Service (+979)</option>
																<option value="+98|Iran">Iran (+98)</option>
																<option value="+964|Iraq">Iraq (+964)</option>
																<option value="+353|Ireland">Ireland (+353)</option>
																<option value="+44 1624|Isle of Man">Isle of Man (+44 1624)</option>
																<option value="+972|Israel">Israel (+972)</option>
																<option value="+39|Italy">Italy (+39)</option>
																<option value="+1 876|Jamaica">Jamaica (+1 876)</option>
																<option value="+81|Japan">Japan (+81)</option>
																<option value="+44 1534|Jersey">Jersey (+44 1534)</option>
																<option value="+962|Jordan">Jordan (+962)</option>
																<option value="+7 6xx, 7xx|Kazakhstan">Kazakhstan (+7 6xx, 7xx)</option>
																<option value="+254|Kenya">Kenya (+254)</option>
																<option value="+686|Kiribati">Kiribati (+686)</option>
																<option value="+383|Kosovo">Kosovo (+383)</option>
																<!--Prashant - 12/08/2021 - purpose: change country code [RES-2866] -->
																<option value="+965|Kuwait">Kuwait (+965)</option>
																<option value="+996|Kyrgyzstan">Kyrgyzstan (+996)</option>
																<option value="+856|Laos">Laos (+856)</option>
																<option value="+371|Latvia">Latvia (+371)</option>
																<option value="+961|Lebanon">Lebanon (+961)</option>
																<option value="+266|Lesotho">Lesotho (+266)</option>
																<option value="+231|Liberia">Liberia (+231)</option>
																<option value="+218|Libya">Libya (+218)</option>
																<option value="+423|Liechtenstein">Liechtenstein (+423)</option>
																<option value="+370|Lithuania">Lithuania (+370)</option>
																<option value="+352|Luxembourg">Luxembourg (+352)</option>
																<option value="+853|Macau">Macau (+853)</option>
																<option value="+389|Macedonia">Macedonia (+389)</option>
																<option value="+261|Madagascar">Madagascar (+261)</option>
																<option value="+265|Malawi">Malawi (+265)</option>
																<option value="+60|Malaysia">Malaysia (+60)</option>
																<option value="+960|Maldives">Maldives (+960)</option>
																<option value="+223|Mali">Mali (+223)</option>
																<option value="+356|Malta">Malta (+356)</option>
																<option value="+692|Marshall Islands">Marshall Islands (+692)</option>
																<option value="+596|Martinique">Martinique (+596)</option>
																<option value="+222|Mauritania">Mauritania (+222)</option>
																<option value="+230|Mauritius">Mauritius (+230)</option>
																<option value="+262 269 / 639|Mayotte">Mayotte (+262 269 / 639)</option>
																<option value="+52|Mexico">Mexico (+52)</option>
																<option value="+373|Moldova">Moldova (+373)</option>
																<option value="+377|Monaco">Monaco (+377)</option>
																<option value="+976|Mongolia">Mongolia (+976)</option>
																<option value="+382|Montenegro">Montenegro (+382)</option>
																<option value="+1 664|Montserrat">Montserrat (+1 664)</option>
																<option value="+212|Morocco">Morocco (+212)</option>
																<option value="+258|Mozambique">Mozambique (+258)</option>
																<option value="+95|Myanmar">Myanmar (+95)</option>
																<option value="+374 47 / 97|Nagorno-Karabakh">Nagorno-Karabakh (+374 47 / 97)</option>
																<option value="+264|Namibia">Namibia (+264)</option>
																<option value="+674|Nauru">Nauru (+674)</option>
																<option value="+977|Nepal">Nepal (+977)</option>
																<option value="+31|Netherlands">Netherlands (+31)</option>
																<option value="+687|New Caledonia">New Caledonia (+687)</option>
																<option value="+64|New Zealand">New Zealand (+64)</option>
																<option value="+505|Nicaragua">Nicaragua (+505)</option>
																<option value="+227|Niger">Niger (+227)</option>
																<option value="+234|Nigeria">Nigeria (+234)</option>
																<option value="+683|Niue">Niue (+683)</option>
																<option value="+672 3|Norfolk Island">Norfolk Island (+672 3)</option>
																<option value="+850|North Korea">North Korea (+850)</option>
																<option value="+1 670|Northern Mariana Islands">Northern Mariana Islands (+1 670)</option>
																<option value="+47|Norway">Norway (+47)</option>
																<option value="+968|Oman">Oman (+968)</option>
																<option value="+92|Pakistan">Pakistan (+92)</option>
																<option value="+680|Palau">Palau (+680)</option>
																<option value="+970|Palestinian territories">Palestinian territories (+970)</option>
																<option value="+507|Panama">Panama (+507)</option>
																<option value="+675|Papua New Guinea">Papua New Guinea (+675)</option>
																<option value="+595|Paraguay">Paraguay (+595)</option>
																<option value="+51|Peru">Peru (+51)</option>
																<option value="+63|Philippines">Philippines (+63)</option>
																<option value="+48|Poland">Poland (+48)</option>
																<option value="+351|Portugal">Portugal (+351)</option>
																<option value="+1 787 / 939|Puerto Rico">Puerto Rico (+1 787 / 939)</option>
																<option value="+974|Qatar">Qatar (+974)</option>
																<!--<option value='+886|Republic of China (Taiwan)'>Republic of China (Taiwan) (+886)</option>-->
																<option value="+242|Republic of the Congo">Republic of the Congo (+242)</option>
																<option value="+262|Réunion">Réunion (+262)</option>
																<option value="+40|Romania">Romania (+40)</option>
																<option value="+7|Russia">Russia (+7)</option>
																<option value="+250|Rwanda">Rwanda (+250)</option>
																<option value="+599 4|Saba">Saba (+599 4)</option>
																<option value="+290|Saint Helena">Saint Helena (+290)</option>
																<option value="+1 869|Saint Kitts and Nevis">Saint Kitts and Nevis (+1 869)</option>
																<option value="+1 758|Saint Lucia">Saint Lucia (+1 758)</option>
																<option value="+1 784|Saint Vincent and the Grenadines">Saint Vincent and the Grenadines (+1 784)</option>
																<option value="+508|Saint-Pierre and Miquelon">Saint-Pierre and Miquelon (+508)</option>
																<option value="+685|Samoa">Samoa (+685)</option>
																<option value="+378|San Marino">San Marino (+378)</option>
																<option value="+239|Sao Tome and Principe">Sao Tome and Principe</option>
																<option value="+966|Saudi Arabia">Saudi Arabia (+966)</option>
																<option value="+221|Senegal">Senegal (+221)</option>
																<option value="+381|Serbia">Serbia (+381)</option>
																<option value="+248|Seychelles">Seychelles (+248)</option>
																<option value="+232|Sierra Leone">Sierra Leone (+232)</option>
																<option value="+65|Singapore">Singapore (+65)</option>
																<option value="+599 3|Sint Eustatius">Sint Eustatius (+599 3)</option>
																<option value="+599 5|Sint Maarten">Sint Maarten (+599 5)</option>
																<option value="+421|Slovakia">Slovakia (+421)</option>
																<option value="+386|Slovenia">Slovenia (+386)</option>
																<option value="+677|Solomon Islands">Solomon Islands (+677)</option>
																<option value="+252|Somalia">Somalia (+252)</option>
																<option value="+27|South Africa">South Africa (+27)</option>
																<option value="+82|South Korea">South Korea (+82)</option>
																<option value="+211|South Sudan">South Sudan (+211)</option>
																<option value="+34|Spain">Spain (+34)</option>
																<option value="+94|Sri Lanka">Sri Lanka (+94)</option>
																<option value="+249|Sudan">Sudan (+249)</option>
																<option value="+597|Suriname">Suriname (+597)</option>
																<option value="+268|Swaziland">Swaziland (+268)</option>
																<option value="+46|Sweden">Sweden (+46)</option>
																<option value="+41|Switzerland">Switzerland (+41)</option>
																<option value="+963|Syria">Syria (+963)</option>
																<option value="+886|Taiwan">Taiwan (+886)</option>
																<!--Parth - 30 Apr 2019 - RES-1963 - Add Taiwan Country code.-->
																<option value="+992|Tajikistan">Tajikistan (+992)</option>
																<option value="+255|Tanzania">Tanzania (+255)</option>
																<option value="+888|Telecommunications for Disaster Relief by OCHA">Telecommunications for Disaster Relief by OCHA (+888)</option>
																<option value="+66|Thailand">Thailand (+66)</option>
																<option value="+228|Togo">Togo (+228)</option>
																<option value="+690|Tokelau">Tokelau (+690)</option>
																<option value="+676|Tonga">Tonga (+676)</option>
																<option value="+1 868|Trinidad and Tobago">Trinidad and Tobago (+1 868)</option>
																<option value="+290 8|Tristan da Cunha">Tristan da Cunha (+290 8)</option>
																<option value="+216|Tunisia">Tunisia (+216)</option>
																<option value="+90|Turkey">Turkey (+90)</option>
																<option value="+993|Turkmenistan">Turkmenistan (+993)</option>
																<option value="+1 649|Turks and Caicos Islands">Turks and Caicos Islands (+1 649)</option>
																<option value="+688|Tuvalu">Tuvalu (+688)</option>
																<option value="+1 340|U.S. Virgin Islands">U.S. Virgin Islands (+1 340)</option>
																<option value="+256|Uganda">Uganda (+256)</option>
																<option value="+380|Ukraine">Ukraine (+380)</option>
																<option value="+971|United Arab Emirates">United Arab Emirates (+971)</option>
																<option value="+44|United Kingdom">United Kingdom (+44)</option>
																<option value="+1|United States of America">United States of America (+1)</option>
																<option value="+598|Uruguay">Uruguay (+598)</option>
																<option value="+998|Uzbekistan">Uzbekistan (+998)</option>
																<option value="+678|Vanuatu">Vanuatu (+678)</option>
																<option value="+39 06 698|Vatican City">Vatican City (+39 06 698)</option>
																<option value="+58|Venezuela">Venezuela (+58)</option>
																<option value="+84|Vietnam">Vietnam (+84)</option>
																<option value="+681|Wallis and Futuna">Wallis and Futuna (+681)</option>
																<option value="+212 5288 / 5289|Western Sahara">Western Sahara (+212 5288 / 5289)</option>
																<option value="+967|Yemen">Yemen (+967)</option>
																<option value="+260|Zambia">Zambia (+260)</option>
																<option value="+255 24|Zanzibar">Zanzibar (+255 24)</option>
																<option value="+263|Zimbabwe">Zimbabwe (+263)</option>
															</select>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Phone:</label>
															<input name="phone_quot{{  $data_item->id }}" type="text" class="form-control" id="phone_quot{{  $data_item->id }}" placeholder="Phone">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Number Of Children(s):</label>
															<select name="number_of_children_quot{{  $data_item->id }}"  class="form-control" id="number_of_children_quot{{  $data_item->id }}" >
																<option value="" selected>--Select--</option>
																<option value="0">0</option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
															</select>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Number Of Adult(s):</label>
															<select name="number_of_adult_quot{{  $data_item->id }}"  class="form-control" id="number_of_adult_quot{{  $data_item->id }}" >
																<option value="" selected>--Select--</option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Email:</label>
															<input name="user_email_quot{{  $data_item->id }}" type="text" class="form-control" id="user_email_quot{{  $data_item->id }}" placeholder="Email Address">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Number Of Room(s):</label>
															<select name="number_of_rooms_quot{{  $data_item->id }}"  class="form-control" id="number_of_rooms_quot{{  $data_item->id }}" >
																<option value="" selected>--Select--</option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
															</select>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Check In:</label>
															<input name="check__in_date_quot{{  $data_item->id }}" type="date" class="check__in_date_quot_class{{  $data_item->id }} form-control" id="check__in_date_quot{{  $data_item->id }}" placeholder="Check In">
														</div>
														<div class="form-input" style="padding: 3px;">
															<label>Check Out:</label>
															<input name="check__out_date_quot{{  $data_item->id }}" type="date" class="check__out_date_quot_class form-control" id="check__out_date_quot{{  $data_item->id }}" placeholder="Check Out">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-input" style="padding: 3px;">
															<label>Message:</label>
															<textarea name="message_quot{{  $data_item->id }}" rows="3"  class="form-control" placeholder="Message" id="message_quot{{  $data_item->id }}" >
															</textarea>
														</div>
													</div>
												</div>
											</div>

											<div class="form-input" style="padding: 3px;">
												<div class="col-md-12">
													<div class="form-input" style="padding-top: 10px;">
														<button onclick="submit_quatation('{{  $data_item->id }}')" type="button"  class="btn btn-primary" style="background: #fcc51e;
															color: #614c00;
															width: 100%;
															padding: 9px;border-color: #6860603d;
															border-radius: 2px;" >Enquire</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						
                        
					{{-- </div> --}}
				{{-- </div> --}}
			</div>
		</div>
	</div>
</div>
@endforeach
@endif
{{-- main modal ends here --}}



@if(count($booked)>0)
@foreach($booked as $data_book_modal)
<div class="modal fade jd-modal" id="house_details_modal{{  $data_book_modal->id }}" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Guest(s) Details</h5>
				{{-- <button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button> --}}
			</div>
			
			<div class="modal-body" >
				{{-- <div class="container"> --}}
					
					{{-- <div class="tab-content"> --}}
						
						<div id="" class="tab-pane show in fade" style="padding-top: 24px">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-10" style="margin: auto;">
										

										@php
										$guests = [];
										if(count($data_book_modal->BookingDetail)>0){
										    foreach($data_book_modal->BookingDetail as $key=>$data1){
										        $real_key = $key + 1;
										        echo "<span> Room ". $real_key ." -  Category:".$data1->House->name." | Price: " .$data1->House->Currency->code . " ". $data1->House->cost . " | Adult: ". $data1->number_of_adults . " | Children: ". $data1->number_of_children . "</span> <hr>";
										    }
										}
										// $guests = implode(" , ", $guests);

										
									@endphp


										{{-- {{  $guests  }} --}}
									</div>
								</div>
							</div>
						</div>
						
                        
					{{-- </div> --}}
				{{-- </div> --}}
			</div>
		</div>
	</div>
</div>
@endforeach
@endif
{{-- main modal ends here --}}