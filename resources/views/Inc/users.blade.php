@extends('layouts.user.app') 
@push('head') {{-- <link href="{{ asset('assets_user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet" /> --}} @endpush @push('script') {{--
    <script src="{{ asset('assets_user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    --}} @endpush 
    
    
    
    @push('script')
    <script>

    function open_modal(element){
        $('#'+element).modal('show');
    }
        </script>
        @endpush 



    @section('body')
    
    <main id="main" class="main">
        <div class="pagetitle">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">Users</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->    
        @include('alert')
        <div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_user')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add User</button>
</div>




        <section class="section dashboard">
            <div class="row">
                @if(count($users)>0)

                <div class="card">
                    <div class="card-body">
                        
                        <div class="">
                           
                            <div class="">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th scope="col" data-sortable="" style="width: 5.68966%;"><a href="#" class="dataTable-sorter">#</a></th>
                                            <th scope="col" data-sortable="" style="width: 27.931%;" class="desc"><a href="#" class="dataTable-sorter">Name</a></th>
                                            <th scope="col" data-sortable="" style="width: 37.7586%;" class=""><a href="#" class="dataTable-sorter">Email</a></th>
                                            <th scope="col" data-sortable="" style="width: 37.7586%;" class=""><a href="#" class="dataTable-sorter">Role</a></th>
                                            <th scope="col" data-sortable="" style="width: 37.7586%;" class=""><a href="#" class="dataTable-sorter">Action</a></th>
                                            {{-- <th scope="col" data-sortable="" style="width: 19.3103%;"><a href="#" class="dataTable-sorter">Action</a></th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                      
                                        @foreach($users as $key=>$data)

                                        <tr>
                                            <th scope="row">{{  $key+1 }}</th>
                                            <td>{{  $data->name }}</td>
                                            <td>
                                                {{  $data->email }}
                                            </td>

                                            <td>Admin</td>
                                            {{-- <td>Online</td> --}}
                                           

                                            <td>   
                                                
                                                
                                                <button type="button" class="btn btn-primary" style="    white-space: nowrap;">View</button>
                                                   
                                                
                                            </td>
                                            
                                        </tr>

                                        
                                       
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                           
                        </div>



                    </div>
                </div>
                @else
                <p><strong>No any user!.</strong></p>
                @endif
            </div>
        </section>
        
    </main>


    <div class="modal fade" id="add_user" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add User</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
           
                
                <form method="POST" action="{{  route('user.store') }}" enctype='multipart/form-data'>
                    @csrf
                    <div class="modal-body">
                        <div class="form-floating" >
                            <label style="margin-top: -11px;" for="name">Name:</label>
                            <input type="text" class="form-control" placeholder="name" id="name" placeholder="Eg John" name="name">
                            {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                        </div>
    
                        <div class="form-floating"  style="margin-top: 10px;">
                            <label style="margin-top: -11px;" for="name">Email:</label>
                            <input type="text" class="form-control" placeholder="Email" id="email" placeholder="Eg John@example.com" name="email">
                            {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                        </div>
<p>*Password for this user will be sent to his email address</p>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
    
          
          </div>
        </div>
      </div>


    @endsection
    