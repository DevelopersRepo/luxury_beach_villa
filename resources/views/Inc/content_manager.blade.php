@extends('layouts.user.app') 
@push('head') 

{{-- <link href="{{ asset('assets_user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet" /> --}} @endpush @push('script') {{--
    <script src="{{ asset('assets_user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    --}} 
    <style>
    .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
        color: #fff;
        background-color: #bebfc1d4;
      }

      </style>
    @endpush 


    @section('blade_script')
    @include('lib.activity_image_upload_adapter')
    @endsection
    @push('script') 
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>

<script>

    function delete_image(item_id){
        $.ajax({
	            url:"{{ url('delete_image') }}",
                type: 'DELETE',
	            data:{image_id:item_id,  "_token": "{{ csrf_token() }}",},
	            success: function(result) {
                    if(result.message){
                        location.reload();
                    }


	                // if(result.message){
	                //     $('#publication' + publication_id).css('display', 'none');
	                //     alertify.alert('You have successful delete this post.',							
	                //             ).setHeader('Success!.');
	                //         }else{
	                //             alertify.error("Sorry, something is wrong!. You can not delete this post, please contact system administrator.")		
	                //         }	           
	            },
	            error: function(data) {
	                alert("Sorry, something is wrong!. You can not delete this post, please contact system administrator.")		
	            },
	        });
    }
	function delete_item(item_id){
	        $.ajax({
	            url:"{{ url('delete_house') }}",
                type: 'DELETE',
	            data:{house_id:item_id,  "_token": "{{ csrf_token() }}",},
	            success: function(result) {
                    if(result.message){
                        location.reload();
                    }


	                // if(result.message){
	                //     $('#publication' + publication_id).css('display', 'none');
	                //     alertify.alert('You have successful delete this post.',							
	                //             ).setHeader('Success!.');
	                //         }else{
	                //             alertify.error("Sorry, something is wrong!. You can not delete this post, please contact system administrator.")		
	                //         }	           
	            },
	            error: function(data) {
	                alert("Sorry, something is wrong!. You can not delete this post, please contact system administrator.")		
	            },
	        });
	
	    }        

    </script>

    <script>
       

        function ActivityImageUploadAdapterPlugin( editor ) {
            editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
                // Configure the URL to the upload script in your back-end here!
                return new MyUploadAdapter( loader );
            };
        }

        ClassicEditor
            .create( document.querySelector('#activity_desc'), {
                extraPlugins: [ ActivityImageUploadAdapterPlugin ],
             })
            .catch( error => {
                console.error( error );
            } );
    </script>




    <script>

if (!localStorage.getItem("current_tab")) {
    change_local_tab_details("house-tab")   
    $('#' + localStorage.getItem("current_tab")+ "-tab").addClass('active');
    $('#bordered-' + localStorage.getItem("current_tab")).addClass('active');
    $('#bordered-' + localStorage.getItem("current_tab")).addClass('show');

} else {

    $('#' + localStorage.getItem("current_tab")+ "-tab").addClass('active');
    $('#bordered-' + localStorage.getItem("current_tab")).addClass('active');
    $('#bordered-' + localStorage.getItem("current_tab")).addClass('show');
}


$('#v-pills-1-tab').addClass('active');
$('#v-pills-1').addClass('active');
$('#v-pills-1').addClass('show');


function change_local_tab_details(value) {
    localStorage.setItem("current_tab", value)
}


function open_modal(element){
    $('#'+element).modal('show');
}
        </script>
        @endpush 



    
    @section('body')
    
    <main id="main" class="main">
        <div class="pagetitle">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{  url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{  url('home/content_manager') }}">Content Manager</a></li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
    
        <section class="section dashboard">
            <div class="row">
                <div class="card">
                    <div class="card-body" style="padding-top: 22px;">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered" id="borderedTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="house-tab" data-bs-toggle="tab" data-bs-target="#bordered-house" type="button" role="tab" aria-controls="house" aria-selected="true" onclick="change_local_tab_details('house')">Room</button>
                            </li>
                            {{-- <li class="nav-item" role="presentation">
                                <button class="nav-link" id="category-tab" data-bs-toggle="tab" data-bs-target="#bordered-category" type="button" role="tab" aria-controls="category" aria-selected="false"  onclick="change_local_tab_details('category')">Category</button>
                            </li> --}}
    
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="activity-tab" data-bs-toggle="tab" data-bs-target="#bordered-activity" type="button" role="tab" aria-controls="activity" aria-selected="false"  onclick="change_local_tab_details('activity')">Activities</button>
                            </li>
    
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="facility-tab" data-bs-toggle="tab" data-bs-target="#bordered-facility" type="button" role="tab" aria-controls="facility" aria-selected="false"  onclick="change_local_tab_details('facility')">Facilities</button>
                            </li>
    
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="feature-tab" data-bs-toggle="tab" data-bs-target="#bordered-feature" type="button" role="tab" aria-controls="feature" aria-selected="false" onclick="change_local_tab_details('feature')">Features</button>
                            </li>

                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="gallery-tab" data-bs-toggle="tab" data-bs-target="#bordered-gallery" type="button" role="tab" aria-controls="gallery" aria-selected="false" onclick="change_local_tab_details('gallery')">Gallery</button>
                            </li>

                        </ul>
                        <div class="tab-content pt-2" id="borderedTabContent">
                            <div class="tab-pane fade" id="bordered-house" role="tabpanel" aria-labelledby="house-tab">
                                @include('Inc.Tab.house')                               
                            </div>
                           
                            <div class="tab-pane fade " id="bordered-category" role="tabpanel" aria-labelledby="category-tab">
                                @include('Inc.Tab.category')
                            </div>

                            <div class="tab-pane fade " id="bordered-activity" role="tabpanel" aria-labelledby="activity-tab">
                                @include('Inc.Tab.activity')
                            </div>

                            <div class="tab-pane fade " id="bordered-facility" role="tabpanel" aria-labelledby="facility-tab">
                                @include('Inc.Tab.facility')
                            </div>
                            <div class="tab-pane fade " id="bordered-feature" role="tabpanel" aria-labelledby="feature-tab">
                                @include('Inc.Tab.feature')
                            </div>
                            <div class="tab-pane fade " id="bordered-gallery" role="tabpanel" aria-labelledby="gallery-tab">
                                @include('Inc.Tab.gallery')
                               
                            </div>
                        </div>
                        <!-- End Bordered Tabs -->
                    </div>
                </div>
            </div>
        </section>
    </main>

    @include('Inc.Modal.add_gallery_category')
    @include('Inc.Modal.add_image')
    @include('Inc.Modal.add_feature')
    @include('Inc.Modal.add_facility')
    @include('Inc.Modal.add_activity')
    @include('Inc.Modal.add_category')
    @include('Inc.Modal.add_house')

    @endsection
    