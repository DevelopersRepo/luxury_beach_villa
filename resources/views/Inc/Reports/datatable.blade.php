@extends('layouts.user.app') 


@push('head')

<!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/user/assets/css/daterangepicker.css')}}">


{{-- <link href="{{ asset('assets/user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet"> --}}

<link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css" rel="stylesheet">

@endpush



@push('script')

{{-- datatable --}}
{{-- <script src="{{ asset('assets/user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script> --}}
{{-- datatable ends --}}

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
 
    




<script src="{{ asset('assets/user/assets/js/moment.min.js') }}"></script>

<script src="{{ asset('assets/user/assets/js/daterangepicker.js') }}"></script>



{{-- datatable --}}
<script>

$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        "aaSorting": [],
	      "responsive": true, 
		  "lengthChange": false, 
		  "autoWidth": false,
       "buttons": [ 
	      {
	extend: "copy",
	text: "Copy",
	exportOptions: {
	    columns: ":visible"
	 }
	},
	  
	      {
	extend: "excel",
	text: "Excel",
	exportOptions: {
	    columns: ":visible"
	 }
	      },
	     
	      {
	extend: "print",
	text: "PDF",
	exportOptions: {
	    columns: ":visible"
	 }
	      },"colvis",
	
	]
    } );
} );
    // On each draw, update the data in the chart
  
  </script>
{{-- datatable ends --}}
<script>

$(function() {

var start = moment().subtract(29, 'days');
var end = moment();

function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

$('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
       'Last 3 Months': [ moment().subtract(3, 'month').startOf('month'),moment(), moment()]
    }
}, cb);

cb(start, end);

});



    </script>



@endpush

@section('body')
        
<main id="main" class="main">
    <div class="pagetitle">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
        </nav>
    </div>
    <!-- End Page Title -->    
    @include('alert')


<div class='row'>

    <div class="card">
        <div class="card-body">
          {{-- <h5 class="card-title">Charts</h5> --}}

          <!-- Bordered Tabs -->
          <ul class="nav nav-tabs nav-tabs-bordered" id="borderedTab" role="tablist">
            <li class="nav-item" role="presentation">
              <a href="{{ url('reports/index') }}"><button class="nav-link" >Charts</button></a>
            </li>
            <li class="nav-item" role="presentation">
             <a href="{{ url('reports/datatable') }}"> <button class="nav-link active" type="button" >Datatable</button></a>
            </li>
            {{-- <li class="nav-item" role="presentation">
              <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Contact</button>
            </li> --}}
          </ul>
          <div class="tab-content pt-2" id="borderedTabContent">
            
            <div class="tab-pane active show" id="bordered-profile" role="tabpanel" aria-labelledby="profile-tab">
                {{-- datatable goes here --}}
                @include('Inc.Reports.Sections.filter')

                <table class="table datatable" id="example">
                  <thead>
                    <tr>
                      <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Name</a></th>
                      <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Reservation No</a></th>

                      <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Email</a></th>
                      <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Phone</a></th>
                      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Total Cost</a></th>
                      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Arrival Date</a></th>
                      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Departure Date</a></th>
                      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">No Of Night(s)</a></th>
                      {{-- <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Booked At</a></th> --}}
                      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Status</a></th>


                    </tr>
                  </thead>
                  <tbody>

                    @if(count($bookings)>0)
                    @foreach($bookings as $data)
                
                
                    @php

                    $booking_id = $data->id;
                            
                    $title = $data->FirstBookingDetailRecord->title_id;
                    $first_name = $data->FirstBookingDetailRecord->first_name;
                    $last_name = $data->FirstBookingDetailRecord->last_name;
                    $user_name = $title ." ". $first_name." ". $last_name;


                @endphp

                @if($first_name==null)
                @continue
                @endif
                    <tr>
                      

                      

                        <td style="text-transform: lowercase; font-size:12px;">{{  $user_name }}</td>
                        <td>{{ $data->booking_code }}</td>

                        <td>{{  $data->email }}</td>

                        @php
                              $country_code = $data->code;
                                $phone_number =  $data->phone;
                                $phone = $country_code . " - " . $phone_number;

                        @endphp
                        <td>{{  $phone }}</td>

                        @php
                          
                            $price_total = 0;
                            $cost = null;
                            $price_code = null;
                            if(count($data->BookingDetail)>0){
                                foreach($data->BookingDetail as $key=>$data2){
                                    $price_code = $data2->House->Currency->code;
                                    $price_total = $price_total + $data2->House->cost;
                                }
                            }

                              $cost = $price_total * $data->number_of_days;
                                $cost = $price_code ." ".$cost;
                        @endphp
                        
                        <td>{{ $cost }}</td>
                        <td>{{ $data->arrival_date }}</td>
                        <td>{{ $data->departure_date }}</td>
                        
                        <td>
                            {{  $data->number_of_days }}
                            {{-- @if($data->status== 0)
                            <span class="badge bg-secondary">Reviewed</span>

                            @else
                            <span class="badge bg-primary">Reviewed</span>
                            @endif</td> --}}
                      
                        </td>
                        {{-- <td>{{  $data->created_at }}</td> --}}
                        <td>
                            @if($data->status== 0)

                            <span class="badge bg-secondary" style="padding: 10px;margin: 5px;">Pending</span>
                              @elseif($data->status== 1)
                              <span class="badge bg-primary" style="padding: 10px;margin: 5px;">Checked In</span>

                            @elseif($data->status== 2)
                            <span class="badge bg-danger" style="padding: 10px;margin: 5px;">Checked Out</span>

                            {{-- <a  onclick="show_details_modal('{{  $data->id }}')" ><span class="badge bg-secondary" style="padding: 10px;margin: 5px;">View</span> --}}

                            @endif
                        </td>
                        
                    </tr>

                      
                    
                    
                    @endforeach
                    @endif
                    
                  </tbody>
                </table>
{{-- datan=table ends here --}}
                
               </div>
           
          </div><!-- End Bordered Tabs -->

        </div>
      </div>


</div>


</main>


<div class="modal fade" id="add_user" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add User</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
            <form method="POST" action="{{  route('user.store') }}" enctype='multipart/form-data'>
                @csrf
                <div class="modal-body">
                    <div class="form-floating" >
                        <label style="margin-top: -11px;" for="name">Name:</label>
                        <input type="text" class="form-control" placeholder="name" id="name" placeholder="Eg John" name="name">
                        {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                    </div>

                    <div class="form-floating"  style="margin-top: 10px;">
                        <label style="margin-top: -11px;" for="name">Email:</label>
                        <input type="text" class="form-control" placeholder="Email" id="email" placeholder="Eg John@example.com" name="email">
                        {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                    </div>
<p>*Password for this user will be sent to his email address</p>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

      
      </div>
    </div>
  </div>


@endsection
