
              <div class="row" style="background: #c9c5c53d;
              padding-bottom: 10px;">

                <div class="col-md-3">
                    <label for="title" class="col-md-2 col-form-label text-md-right"><strong>Date</strong></label>
                    <div class="col form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="bi bi-calendar-day"></i>
                                </span>
                                </div>
                                <input type="text"  name="date" class="form-control float-right" id="reportrange">
                            </div>
                            <!-- /.input group -->
                    </div>
                </div>

                  <div class="col-md-3">
                      <label for="title" class="col-md-4 col-form-label text-md-right"><strong>Category</strong></label>
                      <div class="col form-group">
                              <div class="input-group">
                                  <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="bi bi-tags"></i>
                                  </span>
                                  </div>
                                  <select  class="form-control">
                                      <option selected>--All--</option>
                                      <option>Check In</option>
                                      <option>Check Out</option>
                                      <option>Pending</option>
                                  </select>
                              </div>
                              <!-- /.input group -->
                      </div>
                  </div>


                  <div class="col-md-3">
                      <label for="title" class="col-md-6 col-form-label text-md-right"><strong>Room Type</strong></label>
                      <div class="col form-group">
                              <div class="input-group">
                                  <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="bi bi-house"></i>
                                  </span>
                                  </div>
                                  <select  class="form-control">
                                      <option selected>--All--</option>
                                      
                                      @if(count($house)>0)
                                          @foreach($house as $data)
                                          <option>{{  $data->name }}</option>
                                          @endforeach
                                      @endif
                                      
                                  </select>
                              </div>
                              <!-- /.input group -->
                        </div>
                  </div>


                  <div class="col-md-3">
                      <label for="title" class="col-md-4 col-form-label text-md-right">&nbsp;</label>
                      <div class="col form-group">
                              <div class="input-group">
                              
                                  <button onclick="filter_now()" style="width: -moz-available;" class="btn btn-primary btn-block">Filter</button> 
                                  
                              </div>
                              <!-- /.input group -->
                      </div>
                  </div>




            </div>