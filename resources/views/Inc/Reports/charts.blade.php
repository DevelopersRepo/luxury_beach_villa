@extends('layouts.user.app') 


@push('head')

<!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/user/assets/css/daterangepicker.css')}}">


  {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/searchpanes/2.0.2/css/searchPanes.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.4.0/css/select.dataTables.min.css">
 --}}

  <style>

/* monthly average templature */

.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

/* monthly average templature ends here */
    </style>


{{-- chart 2 --}}

<style>
.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  margin: 1em auto;
}

#container {
  height: 400px;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
    </style>

    {{-- chart 3 --}}
    <style>

#container {
  height: 400px;
}

.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}


        </style>

        {{-- chart4 --}}

        <style>

.highcharts-figure,
.highcharts-data-table table {
  min-width: 360px;
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}


            </style>
  <link href="{{ asset('assets/user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">

@endpush



@push('script')

{{-- datatable --}}
<script src="{{ asset('assets/user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
{{-- datatable ends --}}
<script src="{{ asset('assets/user/assets/js/moment.min.js') }}"></script>

<script src="{{ asset('assets/user/assets/js/daterangepicker.js') }}"></script>


{{-- datatable --}}
<script>

 
    // On each draw, update the data in the chart
  
  </script>
{{-- datatable ends --}}
<script>

$(function() {

var start = moment().subtract(29, 'days');
var end = moment();

function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}

$('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
       'Last 3 Months': [ moment().subtract(3, 'month').startOf('month'),moment(), moment()]
    }
}, cb);

cb(start, end);

});



    </script>

{{-- monthly average temprature --}}
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
{{-- mothly average template ends here --}}





<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/searchpanes/2.0.2/js/dataTables.searchPanes.min.js"></script>
<script src="https://cdn.datatables.net/select/1.4.0/js/dataTables.select.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>


<script>
// chart 1

Highcharts.chart('container1', {
  chart: {
    type: 'spline'
  },
  title: {
    text: 'Monthly Bookings'
  },
  subtitle: {
    text: 'Source: luxurybeachvilla.com'
  },
  xAxis: {
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    accessibility: {
      description: 'Months of the year'
    }
  },
  yAxis: {
    title: {
      text: 'Number Of People'
    },
    labels: {
      formatter: function () {
        return this.value;
      }
    }
  },
  tooltip: {
    crosshairs: true,
    shared: true
  },
  plotOptions: {
    spline: {
      marker: {
        radius: 4,
        lineColor: '#666666',
        lineWidth: 1
      }
    }
  },
  series: [{
    name: 'Standard Family Room',
    marker: {
      symbol: 'square'
    },
    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, {
      y: 26.5,
      marker: {
        // symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
      },
      accessibility: {
        description: 'Sunny symbol, this is the warmest point in the chart.'
      }
    }, 23.3, 18.3, 13.9, 9.6]

  }, {
    name: 'Single Deluxe Room',
    marker: {
      symbol: 'diamond'
    },
    data: [{
      y: 3.9,
      marker: {
        // symbol: 'url(https://www.highcharts.com/samples/graphics/snow.png)'
      },
      accessibility: {
        description: 'Snowy symbol, this is the coldest point in the chart.'
      }
    }, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
  }]
});


    </script>

{{-- chart2 --}}
    <script>

Highcharts.chart('container2', {
  title: {
    text: 'Category Chart'
  },
  subtitle: {
    text: 'Source: luxurybeachvilla.com'
  },
  xAxis: {
    categories: ['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']
  },
  labels: {
    items: [{
      html: 'Pie Chart Distribution',
      style: {
        left: '50px',
        top: '18px',
        color: ( // theme
          Highcharts.defaultOptions.title.style &&
          Highcharts.defaultOptions.title.style.color
        ) || 'black'
      }
    }]
  },
  series: [{
    type: 'column',
    name: 'Checked In',
    data: [3, 2, 1, 3, 4]
  }, {
    type: 'column',
    name: 'Checked  Out',
    data: [2, 3, 5, 7, 6]
  }, {
    type: 'column',
    name: 'Unspecified',
    data: [4, 3, 3, 9, 0]
  }, {
    type: 'spline',
    name: 'Pending',
    data: [3, 2.67, 3, 6.33, 3.33],
    marker: {
      lineWidth: 2,
      lineColor: Highcharts.getOptions().colors[3],
      fillColor: 'white'
    }
  }, {
    type: 'pie',
    name: 'Total consumption',
    data: [{
      name: 'Jane',
      y: 13,
      color: Highcharts.getOptions().colors[0] // Jane's color
    }, {
      name: 'John',
      y: 23,
      color: Highcharts.getOptions().colors[1] // John's color
    }, {
      name: 'Joe',
      y: 19,
      color: Highcharts.getOptions().colors[2] // Joe's color
    }],
    center: [100, 80],
    size: 100,
    showInLegend: false,
    dataLabels: {
      enabled: false
    }
  }]
});


        </script>

        {{-- chart 3 --}}

        <script>

Highcharts.chart('container3', {
  chart: {
    type: 'column',
    options3d: {
      enabled: true,
      alpha: 15,
      beta: 15,
      viewDistance: 25,
      depth: 40
    }
  },
  subtitle: {
    text: 'Source: luxurybeachvilla.com'
  },
  title: {
    text: 'Family Distribution Chart'
  },

  xAxis: {
    categories: ['Family Room', 'Single Deluxe', 'Standard Twin Room', 'Superior Double', 'Standard Single'],
    labels: {
      skew3d: true,
      style: {
        fontSize: '16px'
      }
    }
  },

  yAxis: {
    allowDecimals: false,
    min: 0,
    title: {
      text: 'Number of individuals',
      skew3d: true
    }
  },

  tooltip: {
    headerFormat: '<b>{point.key}</b><br>',
    pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: {point.y} / {point.stackTotal}'
  },

  plotOptions: {
    column: {
      stacking: 'normal',
      depth: 40
    }
  },

  series: [{
    name: 'Children',
    data: [5, 3, 4, 7, 2],
    stack: 'male'
  }, {
    name: 'Adolesence',
    data: [3, 4, 4, 2, 5],
    stack: 'male'
  }, {
    name: 'Adult',
    data: [2, 5, 6, 2, 1],
    stack: 'female'
  }, {
    name: 'Older',
    data: [3, 0, 4, 4, 3],
    stack: 'female'
  }]
});
            </script>

            <script>
// chart 4

// alert(eight_yrs)
Highcharts.chart('container4', {

title: {
  text: 'Monthly Income (USD)'
},

subtitle: {
  text: 'Source: luxurybeachvilla.com'
},

yAxis: {
  title: {
    text: 'Income'
  }
},

xAxis: {
  accessibility: {
    rangeDescription: 'Range: 2010 to 2017'
  }
},

legend: {
  layout: 'vertical',
  align: 'right',
  verticalAlign: 'middle'
},

plotOptions: {
  series: {
    label: {
      connectorAllowed: false
    },
    pointStart:{{ intval($eight_years_ago) }}
  }
},

series:@json($monthly_data),

responsive: {
  rules: [{
    condition: {
      maxWidth: 500
    },
    chartOptions: {
      legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'bottom'
      }
    }
  }]
}

});


                </script>

@endpush

@section('body')
        
<main id="main" class="main">
    <div class="pagetitle">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
        </nav>
    </div>
    <!-- End Page Title -->    
    @include('alert')
   
   






<div class='row'>

    <div class="card">
        <div class="card-body">
          {{-- <h5 class="card-title">Charts</h5> --}}

          <!-- Bordered Tabs -->
          <ul class="nav nav-tabs nav-tabs-bordered" id="borderedTab" role="tablist">
            <li class="nav-item" role="presentation">
              <a href="{{ url('reports/index') }}"><button class="nav-link active" >Charts</button></a>
            </li>
            <li class="nav-item" role="presentation">
             <a href="{{ url('reports/datatable') }}"> <button class="nav-link" type="button" >Datatable</button></a>
            </li>
            {{-- <li class="nav-item" role="presentation">
              <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Contact</button>
            </li> --}}
          </ul>
          <div class="tab-content pt-2" id="borderedTabContent">
            <div class="tab-pane fade active show" id="bordered-home" role="tabpanel" aria-labelledby="home-tab">
              
              {{-- filter section --}}
@include('Inc.Reports.Sections.filter')
              <div class="col-md-12">
                <div class="container" style="background: #c9c5c53d;
                margin-top: 10px;
                border-radius: 4px;">
                <div class="row">
                       <div class="col-md-6">
                            <figure class="highcharts-figure">
                                <div id="container3"></div>
                               
                            </figure>
                        </div>
                        <div class="col-md-6">
                            <figure class="highcharts-figure">
                                <div id="container4"></div>                               
                            </figure>
                        </div>

                        <div class="col-md-6">
                            <figure class="highcharts-figure">
                                <div id="container1"></div>
                              
                            </figure>
                        </div>

                        <div class="col-md-6">
                            <figure class="highcharts-figure">
                                <div id="container2"></div>
                               
                            </figure>
                        </div>
                </div>
                </div>
              </div>
                {{-- Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde. --}}
            </div>
            <div class="tab-pane fade" id="bordered-profile" role="tabpanel" aria-labelledby="profile-tab">
{{-- datatable goes here --}}
@include('Inc.Reports.Sections.filter')

<table class="table datatable" id="example">
  <thead>
    <tr>
      <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Name</a></th>
      <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Reservation No</a></th>

      <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Email</a></th>
      <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Phone</a></th>
      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Total Cost</a></th>
      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Arrival Date</a></th>
      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Departure Date</a></th>
      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">No Of Night(s)</a></th>
      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Booked At</a></th>
      <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Status</a></th>


    </tr>
  </thead>
  <tbody>

    @if(count($bookings)>0)
    @foreach($bookings as $data)
 
 
    @php

    $booking_id = $data->id;
            
    $title = $data->FirstBookingDetailRecord->title_id;
    $first_name = $data->FirstBookingDetailRecord->first_name;
    $last_name = $data->FirstBookingDetailRecord->last_name;
    $user_name = $title ." ". $first_name." ". $last_name;


@endphp

@if($first_name==null)
@continue
@endif
    <tr>
       

       

        <td style="text-transform: capitalize">{{  $user_name }}</td>
        <td>{{ $data->booking_code }}</td>

        <td>{{  $data->email }}</td>

        @php
               $country_code = $data->code;
                $phone_number =  $data->phone;
                $phone = $country_code . " - " . $phone_number;

        @endphp
        <td>{{  $phone }}</td>

        @php
           
            $price_total = 0;
            $cost = null;
            $price_code = null;
            if(count($data->BookingDetail)>0){
                foreach($data->BookingDetail as $key=>$data2){
                    $price_code = $data2->House->Currency->code;
                    $price_total = $price_total + $data2->House->cost;
                }
            }

               $cost = $price_total * $data->number_of_days;
                 $cost = $price_code ." ".$cost;
        @endphp
        
        <td>{{ $cost }}</td>
        <td>{{ $data->arrival_date }}</td>
        <td>{{ $data->departure_date }}</td>
        
        <td>
            {{  $data->number_of_days }}
            {{-- @if($data->status== 0)
            <span class="badge bg-secondary">Reviewed</span>

            @else
            <span class="badge bg-primary">Reviewed</span>
            @endif</td> --}}
      
        </td>
        <td>{{  $data->created_at->DiffForHumans() }}, <strong>{{  $data->created_at }}</strong> </td>
        <td>
            @if($data->status== 0)

            <span class="badge bg-secondary" style="padding: 10px;margin: 5px;">Pending</span>
              @elseif($data->status== 1)
              <span class="badge bg-primary" style="padding: 10px;margin: 5px;">Checked In</span>

            @elseif($data->status== 2)
            <span class="badge bg-danger" style="padding: 10px;margin: 5px;">Checked Out</span>

            {{-- <a  onclick="show_details_modal('{{  $data->id }}')" ><span class="badge bg-secondary" style="padding: 10px;margin: 5px;">View</span> --}}

            @endif
        </td>
        
    </tr>

      
    
    
    @endforeach
    @endif
    
  </tbody>
</table>
{{-- datan=table ends here --}}
                
                            </div>
            <div class="tab-pane fade" id="bordered-contact" role="tabpanel" aria-labelledby="contact-tab">
              Saepe animi et soluta ad odit soluta sunt. Nihil quos omnis animi debitis cumque. Accusantium quibusdam perspiciatis qui qui omnis magnam. Officiis accusamus impedit molestias nostrum veniam. Qui amet ipsum iure. Dignissimos fuga tempore dolor.
            </div>
          </div><!-- End Bordered Tabs -->

        </div>
      </div>


</div>


</main>


<div class="modal fade" id="add_user" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add User</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
       
            
            <form method="POST" action="{{  route('user.store') }}" enctype='multipart/form-data'>
                @csrf
                <div class="modal-body">
                    <div class="form-floating" >
                        <label style="margin-top: -11px;" for="name">Name:</label>
                        <input type="text" class="form-control" placeholder="name" id="name" placeholder="Eg John" name="name">
                        {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                    </div>

                    <div class="form-floating"  style="margin-top: 10px;">
                        <label style="margin-top: -11px;" for="name">Email:</label>
                        <input type="text" class="form-control" placeholder="Email" id="email" placeholder="Eg John@example.com" name="email">
                        {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                    </div>
<p>*Password for this user will be sent to his email address</p>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

      
      </div>
    </div>
  </div>


@endsection
