

<div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_category')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add Category</button>
</div>
<div class="">
	<div class="row " style="margin-top: 10px;">
		@if(count($categories)>0)
		<div class="">
			
			<div class="">
				<table class="table datatable">
					<thead>
						<tr>
							<th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Name</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Code</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Created By</a></th>
							<th scope="col" data-sortable="" ><a href="#" class="dataTable-sorter">Created At</a></th>
							{{-- <th scope="col" data-sortable="" style="width: 37.7586%;" class=""><a href="#" class="dataTable-sorter">Status</a></th> --}}
							{{-- <th scope="col" data-sortable="" style="width: 19.3103%;"><a href="#" class="dataTable-sorter">Action</a></th> --}}
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $data)
						<tr>
							<td style="text-transform: capitalize;">{{  $data->name }}</td>
							<td>
								45FL{{  $data->id }}D
							</td>
							<td>{{  $data->Creator->name }}</td>
							<td>{{  $data->created_at->DiffForHumans() }}</td>
							
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
		</div>
		@else
		<p><strong>No any category!.</strong></p>
		@endif  
	</div>
</div>
{{-- gallery done here --}}

