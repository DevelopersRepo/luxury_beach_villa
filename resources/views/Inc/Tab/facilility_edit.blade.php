@extends('layouts.user.app') 
@push('head') 

{{-- <link href="{{ asset('assets_user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet" /> --}} @endpush @push('script') {{--
    <script src="{{ asset('assets_user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    --}} 
    <style>
  
      </style>
    @endpush 


    @section('blade_script')
    @endsection
    @push('script') 

   



    <script>

</script>
        @endpush 



    
    @section('body')
    
    <main id="main" class="main">
        <div class="pagetitle">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">Content Manager</li>
                    <li class="breadcrumb-item active">Facility</li>
                    <li class="breadcrumb-item active">Edit</li>

                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
    
        <section class="section dashboard">
            <div class="row">
                <div class="card">
                    <div class="col-md-8" style="padding-top: 22px;margin:auto">
                        <!-- Bordered Tabs -->
                       
                        
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Facility</h5>
                          </div>
                         
                              
                          {!! Form::open(['id'=>'editable_form', 'action'=> ['App\Http\Controllers\FacilityController@update',$facility->id], 'method'=>'PUT']) !!}
						{{ Form::hidden('_method' ,'PUT') }}
                              {{-- <form method="POST" action="{{  route('facility.update.'.$facility->id) }}" enctype='multipart/form-data'> --}}
                              
                                  <div class="modal-body">
                  
                                      <div class="form-floating" >
                                          <label for="name" style="margin-top: -11px;">Name:</label>
                                          <input type="text" value="{{  $facility->name }}"  class="form-control" id="name" placeholder="Eg Swimming pool" name="name">
                                      </div>
                                      
                  
                                      
                                      <div class="form-floating" style="padding-top: 10px;">
                                          <label for="description" style="background: #fff;
                                          display: block;width: 96%;
                                          z-index: 20;
                                          height: 52px;
                                          margin-top: 10.5px;
                                          margin-left: 3px;">Feature(s):</label>
                                  
                                          
                  
                                          <select name="feature[]"  id="feature" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
                                              {{-- <label for="description" >Feature(s):</label> --}}
                                                  {{-- <optgroup selected="">Feature(s):</optgroup> --}}
                                                  @if(count($features)>0)
                                                  <option style="" value="">--Select Features--</option>
                                                  @foreach($features as $feature_data)

                                                  
                                                            @if(\App\Models\FacilityFeature::where("feature_id", $feature_data->id)->where("facility_id", $facility->id)->first())
                                                            <option style="text-transform: capitalize;font-weight: bold;" selected value={{ $feature_data->id }}>{{ $feature_data->name }}</option>
                                                            @else
                                                            <option style="text-transform: capitalize;font-weight: bold;" value={{ $feature_data->id }}>{{ $feature_data->name }}</option>
                                                            @endif
                                                  
                                                 
                                                    

                                            
                                            
                                              @endforeach
                                              @endif
                                              
                  
                                            </select>
                  
                  
                  
                                      </div>
                  
                  
                                      <div class="form-floating" style="padding-top: 10px;">
                                          <label for="description" >Description:</label>
                                          <textarea  name="description" class="form-control"  style="height: 100px;" maxlength="150" rows="5" id="description" >{{  $facility->description }}</textarea>
                                      </div>
                                      
                  
                                      <div class="form-floating" style="padding-top: 10px;">
                                          {{-- <label for="file">Select picture (If Any):</label> --}}
                                          <input type="file" style="padding-top: 20px;" value="{{  $facility->image_name }}"  class="form-control" id="file" name="file">
                                          {{-- {{ Form::file('file',null,['autofocus'=>'','required'=>'','class'=>'form-control','placeholder'=>'eg fastest cars in the world',  'maxlength'=>'1000', 'minlength'=>'1']) }} --}}
                                      </div>
                  
                  
                                      
                                      <div class="form-floating" style="padding-top: 10px;">
                                          
                                          <div class="form-check">
                                              <input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
                                              <label class="form-check-label" for="securityNotify">
                                                Visible on website.
                                              </label>
                                            </div>
                                      </div>
                  
                  
                                      
                  
                  
                                  </div>
                                  <div class="modal-footer">
                                      <a href={{  url('home/content_manager') }}><button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button></a>
                                      <button type="submit" class="btn btn-primary">Update</button>
                                  </div>
                                  {!! Form::close() !!} 

                  



                        <!-- End Bordered Tabs -->
                    </div>
                </div>
            </div>
        </section>
    </main>

    

    @endsection
    