

<div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_activity')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add Activity</button>
</div>
<div class="">
	<div class="row " style="margin-top: 10px;">
		@if(count($activities)>0)
		<div class="">
			
			<div class="">
				<table class="table datatable">
					<thead>
						<tr>

							<th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Title</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Code</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Facility</a></th>

							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Description</a></th>

							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Created By</a></th>
							<th scope="col" data-sortable="" ><a href="#" class="dataTable-sorter">Created At</a></th>
						</tr>
					</thead>
					<tbody>
						@foreach($activities as $data)
						<tr>
							
							<td style="text-transform: capitalize;">{{  $data->title }}</td>
							<td>
								45FL{{  $data->id }}D
							</td>
							<td>
								
								@if($data->Facility)
								@foreach($data->Facility as $data)
								<span class="badge bg-secondary">{{  $data->name }}</span>
								
								@endforeach

								@endif
							
							</td>

							<td>
								
								<span class="badge" style="cursor: pointer;background:#00BFFF;">
									<i class="bi bi-info-circle me-1"></i>
									 click to ready</span>
								
								</td>
							<td>{{  $data->Creator->name }}</td>
							<td>{{  $data->created_at->DiffForHumans() }}</td>
							
						</tr>
						@endforeach
					</tbody>
				</table>

				
				<div style="text-align: right;">
					{{  $activities->links() }}
					</div>
		
			
			</div>
			
		</div>
		@else
		<p><strong>No any registered activity!.</strong></p>
		@endif  
	</div>
</div>
{{-- gallery done here --}}

