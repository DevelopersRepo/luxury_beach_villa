

@extends('layouts.user.app') 
@push('head') 
{{-- 
<link href="{{ asset('assets_user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet" />
--}} @endpush @push('script') {{--
<script src="{{ asset('assets_user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
--}} 
<style>
</style>
@endpush 
@section('blade_script')
@endsection
@push('script') 
<script></script>
@endpush 
@section('body')
<main id="main" class="main">
	<div class="pagetitle">
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{  url('/') }}">Home</a></li>
				<li class="breadcrumb-item active"><a href="{{  url('home/content_manager') }}">Content Manager</a></li>
				<li class="breadcrumb-item active">House</li>
				<li class="breadcrumb-item active">Edit</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section dashboard">
		<div class="row">
			<div class="card">
				<div class="col-md-8" style="padding-top: 22px;margin:auto">
					<!-- Bordered Tabs -->
					<div class="modal-header">
						<h5 class="modal-title">Edit House</h5>
					</div>
					{!! Form::open(['id'=>'editable_form','enctype'=>'multipart/form-data', 'action'=> ['App\Http\Controllers\HouseController@update',$house->id], 'method'=>'PUT']) !!}
					{{ Form::hidden('_method' ,'PUT') }}
					{{-- 
					<form method="POST" action="{{  route('facility.update.'.$facility->id) }}" enctype='multipart/form-data'>
					--}}
					<div class="modal-body">
						
						<div class="form-floating" >
							<label for="name" style="margin-top: -11px;">Name:</label>
							<input type="text" placeholder="name" value="{{  $house->name }}" class="form-control" id="name" placeholder="Eg Swimming pool" name="name">
						</div>
						<div style="display: inline-flex;">
							<div class="form-floating" style="padding-top: 10px;margin-right:10px;"  >
								<label for="floatingSelect1">Price Per Room:</label>
								<input type="number" required value="{{  $house->cost }}"  class="form-control" id="cost"  name="cost">
							</div>
							<div class="form-floating" style="padding-top: 10px;margin-right:10px;">
								<label for="currency_id">Currency:</label>
								<select name="currency_id" id="currency_id"  required class="form-select" required id="currency_id" aria-label="Floating label select example">
									@if(count($currencies)>0)
									
									@foreach($currencies as $data)
									@if($house->Currency->id === $data->id)
									<option value={{  $data->id  }} selected="">{{  $data->code }}</option>
									@else
									<option value={{  $data->id  }}>{{  $data->code }}</option>
									@endif
									@endforeach
									@endif
								</select>
								
							</div>
							<div class="form-floating" style="padding-top: 10px;">
								<label for="currency_id" style="font-size: x-small;">Reference (Eg Per Month):</label>
								<input type="text" required placeholder="Eg Per Month" value="{{  $house->Price_reference }}"  class="form-control" id="Price_reference"  name="Price_reference">
							</div>
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="number_of_rooms">Number Of Room:</label>
							<input required type="text"  class="form-control" id="number_of_rooms" value="{{  $house->number_of_rooms }}"  name="number_of_rooms">
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="max_number_of_children">Max Number Of Children Per Room:</label>
							<input required type="text"  class="form-control" id="max_number_of_children"  value="{{  $house->max_number_of_children }}" name="max_number_of_children">
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="max_number_of_adults">Max Number Of Adult Per Room:</label>
							<input required type="text"  class="form-control" id="max_number_of_adults"  value="{{  $house->max_number_of_adults }}" name="max_number_of_adults">
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="name">Location:</label>
							<input required type="text" placeholder="name" class="form-control" id="location" value="{{  $organisation_location->name }}" name="location">
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<label for="name">Category:</label>
							<select name="category_id" id="category_id" required class="form-select" required id="floatingSelect2" aria-label="Floating label select example">
								@if(count($categories)>0)
								<option value="">--Select category--</option>
								@foreach($categories as $data)

								@if($data->id== $house->category_id)
								<option selected="" value="{{  $data->id  }}"> {{ $data->name }}</option>
								@else
								<option value={{  $data->id  }}>{{  $data->name }}</option>								
								@endif

								@endforeach
								@endif
							</select>
						</div>
						<div class="form-floating" style="padding-top: 10px;">
							<div class="form-floating" style="padding-top: 10px;">
								@php
								$house_image = null;
									$house_image_count = count($house->HouseImage);
									if($house_image_count!=null){
										foreach($house->HouseImage as $data_image){
											$house_image = $data_image->name;
										}
									}
								@endphp
								<input type="file"   value="{{  $house_image }}" style="padding-top: 20px;border: dashed;border-color: #66A6BC;"  class="form-control" id="file" name="file">
							</div>
							<div class="form-floating" style="padding-top: 10px;">
								<label for="activity" style="background: #fff;
									display: block;width: 95%;
									z-index: 20;
									height: 52px;
									margin-top: 11px;
									margin-left: 3px;">Activities(s):</label>
								<select name="activity[]"  id="activity" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
									@if(count($activities)>0)
									<option   style="" value="" >--Select Activity--</option>
									@foreach($activities as $data)

											@if(\App\Models\ActivityHouse::where("activity_id", $data->id)->where("house_id", $house->id)->first())

											<option selected=""  style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->title }}</option>
											@else
											<option style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->title }}</option>
											@endif
	


									@endforeach
									@endif
								</select>
							</div>
							<div class="form-floating" style="padding-top: 10px;">
								<label for="facility" style="background: #fff;
									display: block;width: 96%;
									z-index: 20;
									height: 52px;
									margin-top: 10.5px;
									margin-left: 3px;">Facility(s):</label>
								<select name="facility[]"  id="facility" style="height: calc(10.5rem + 2px);padding-top: 40px;padding-top: 54px;" class="form-control" multiple="" aria-label="multiple select example">
									@if(count($facilities)>0)
									<option value="">--Select Facility--</option>
									@foreach($facilities as $data)

										@if(\App\Models\FacilityHouse::where("facility_id", $data->id)->where("house_id", $house->id)->first())
										<option selected=""  style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->name }}</option>
										@else
										<option style="text-transform: capitalize;font-weight: bold;" value={{ $data->id }}>{{ $data->name }}</option>
										@endif


									@endforeach
									@endif							
								</select>
							</div>
							<div class="form-floating" style="padding-top: 10px;">
								<label for="description" >Description:</label>
								<textarea   name="description" class="form-control"   style="height: 100px;" maxlength="150" placeholder="Descriptions for this facility" rows="5" id="description">{{  $house->description }}</textarea>
							</div>
							<div class="form-floating" style="padding-top: 10px;">
								<label style="margin-top: -8px;margin-left: 10px;" for="isAvailable"  class="form-check-label" >Available:</label>
								<input class="form-check-input" 
								
								@if($house->isAvailable == "on")
								checked=""
								@endif
								
								type="checkbox" value="{{  $house->isAvailable }}" name="isAvailable" id="isAvailable">
							</div>
							<div class="form-floating" style="padding-top: 10px;">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" id="securityNotify" checked="" disabled="">
									<label class="form-check-label" for="securityNotify">
									Visible on website.
									</label>
								</div>
							</div>


					</div>
					<div class="modal-footer">
						<a href={{  url('home/content_manager') }}><button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button></a>
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
					{!! Form::close() !!} 
					<!-- End Bordered Tabs -->
				</div>
			</div>
		</div>
	</section>
</main>
@endsection

