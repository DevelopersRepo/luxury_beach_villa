

<div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_house')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add Room</button>
</div>
<div class="">
	<div class="row " style="margin-top: 10px;">
        @if(count($houses)>0)
		<div class="">
			
			<div class="">
				<table class="table datatable">
					<thead>
						<tr>

							<th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Name</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Code</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Cost</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Status</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Images</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Location</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Activity</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Facility</a></th>
							<th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Description</a></th>

							<th scope="col" data-sortable="" ><a href="#" class="dataTable-sorter">Created At</a></th>
							<th scope="col" data-sortable="" ><a href="#" class="dataTable-sorter">Created At</a></th>
						</tr>
					</thead>
					<tbody>
						@foreach($houses as $data)
						<tr>
							
							<td style="text-transform: capitalize;">{{  $data->name }}</td>
							<td>
								45FL{{  $data->id }}D
							</td>
                            <td style="text-transform: capitalize;">{{  $data->Currency->code }} {{  $data->cost }}</td>
                            <td style="text-transform: capitalize;">
                                
                                @if($data->isAvailable=="on")
                                <span class="badge bg-success">Available</span>
                                @else
                                <span class="badge bg-danger">Not available</span>

                                @endif
                            
                            </td>
                            <td style="text-transform: capitalize;"> </td>
                            <td style="text-transform: capitalize;">{{  $data->Location->name }}</td>


							<td>
								
								@if($data->Activity)
								@foreach($data->Activity as $res)
								<span class="badge bg-secondary">{{  $res->title }}</span>
								
								@endforeach

								@endif
							
							
							</td>

							<td>
								
								
                                
								
							</td>
							<td>{{  $data->description }}</td>

							<td>{{  $data->created_at->DiffForHumans() }}</td>
							<td>
								<div style="display: inline-flex;">
								<a href="{{ url('home/content_manager/house/'. $data->id.'/edit') }}"><span class="badge bg-primary" style="padding: 10px;">Edit</span></a>
								
								<a onclick="delete_item('{{ $data->id }}')"  ><span class="badge bg-danger" style="padding: 10px;margin-left:5px">Delete</span></a>
								</div>
								
								</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
		</div>
		@else
		<p><strong>No any registered activity!.</strong></p>
		@endif  

        
	</div>
</div>
{{-- gallery done here --}}

