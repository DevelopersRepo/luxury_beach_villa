

<div style="z-index:200;bottom:11px; right:10px;position:fixed;height:58px;width:100%;padding-top: 13px;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_image')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add Image</button>
</div>
{{-- gallery goes here --}}
<div class="d-flex align-items-start">
	<div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		@php
		$galleryCategoryCounter = 1;
		@endphp
		@if(count($galleryCategory)>0)
		@foreach($galleryCategory as $data)
		<button class="nav-link" id="v-pills-{{ $galleryCategoryCounter }}-tab" data-bs-toggle="pill" data-bs-target="#v-pills-{{ $galleryCategoryCounter }}" type="button" role="tab" aria-controls="v-pills-{{ $galleryCategoryCounter }}" aria-selected="true">{{  $data->name }}</button>
		@php
		$galleryCategoryCounter ++;
		@endphp
		@endforeach
		@endif
		<button type="button" class="nav-link" style="font-size: x-large;
			font-weight: bold;"  onclick="open_modal('create_gallery_category')">+</button>
	</div>
	<div class="tab-content" id="v-pills-tabContent">
		@php
		$galleryCategoryCounter2 = 1;
		@endphp
		@if(count($galleryCategory)>0)
		@foreach($galleryCategory as $data)
		<div class="tab-pane fade" id="v-pills-{{  $galleryCategoryCounter2 }}" role="tabpanel" aria-labelledby="v-pills-{{   $galleryCategoryCounter2 }}-tab">
			<div class="row align-items-top">
				@php
				$imagesList = App\Models\Image::where('category_id', $data->id)->simplePaginate();
				@endphp
				@if(count($imagesList)>0)
				@foreach($imagesList as $data)
				<div class="col-md-3">
					<!-- Card with an image on top -->
					<div class="card">
						<img src="{{ asset('/storage/uploads/'.$data->name) }}" class="card-img-top" alt="">
						<div class="card-body">
							<p class="card-text">{{ $data->description }}</p>

							{{-- <span class="dropdown-item" style="color: red;"  >Delete</span> --}}

							<button onclick="delete_image({{  $data->id  }})" class="btn btn-sm btn-danger">Delete</button>


						</div>
					</div>
					<!-- End Card with an image on top -->
					<!-- Card with an image on bottom -->                                            
				</div>

				@endforeach
				@else
				<p><strong>No image uploaded in this category</strong></p>
				@endif
			</div>
		</div>
		@php
		$galleryCategoryCounter2 ++;
		@endphp 
		@endforeach

		@else
		<p><strong>No any image!.</strong></p>
		

		@endif
	</div>
</div>
{{-- gallery done here --}}

