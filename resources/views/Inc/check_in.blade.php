@extends('layouts.user.app') 
@push('head') {{-- <link href="{{ asset('assets_user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet" /> --}} @endpush @push('script') {{--
    <script src="{{ asset('assets_user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    --}} @endpush 

@push('script')
<script>
function show_check_in_details_modal(id){
		$('#house_details_modal' + id).modal('show');
}
    </script>
@endpush

    @section('body')
    
    <main id="main" class="main">
        <div class="pagetitle">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active">Check In</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
    
        {{-- <div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_house')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add House</button>
</div> --}}


        <section class="section dashboard">
            <div class="row">
                @if(count($checkin)>0)
                <div class="card">
                    <div class="card-body">
                        
                        <div class="dataTable-wrapper dataTable-loading no-footer sortable searchable fixed-columns">
                          
                            <div class="">
                                <table class="table datatable ">
                                    <thead>
                                        <tr>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Name</a></th>
                                            <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Reservation No</a></th>

                                            <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Email</a></th>
                                            <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Phone</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Total Cost</a></th>

                                            
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Arrival Date</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Departure Date</a></th>

                                            <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">No Of Night(s)</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Booked At</a></th>
                                            {{-- <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Reserved with</a></th> --}}
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Action</a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      

                                        @foreach($checkin as $data)
                          

                                        
                                        @php
                                            
                                         $title = $data->FirstBookingDetailRecord->title_id;
                                        $first_name = $data->FirstBookingDetailRecord->first_name;
                                        $last_name = $data->FirstBookingDetailRecord->last_name;
                                        $user_name = $title ." ". $first_name." ". $last_name;
                                        @endphp



                                        <tr>

                                            <td >{{ $user_name }}</td>
                                            <td>{{ $data->booking_code }}</td>

                                            <td>{{  $data->email }}</td>
                                            @php
                                            $country_code = $data->code;
                                             $phone_number =  $data->phone;
                                             $phone = $country_code . " - " . $phone_number;

                                     @endphp
                                     <td>{{  $phone }}</td>
                                     @php
                                               
                                     $price_total = 0;
                                     $cost = null;
                                     $price_code = null;
                                     if(count($data->BookingDetail)>0){
                                         foreach($data->BookingDetail as $key=>$data2){
                                             $price_code = $data2->House->Currency->code;
                                             $price_total = $price_total + $data2->House->cost;
                                         }
                                     }

                                        $cost = $price_total * $data->number_of_days;
                                          $cost = $price_code ." ".$cost;
                                 @endphp
                                 
                                 <td>{{ $cost }}</td>
                                
                
                                             <td>{{ $data->arrival_date }}</td>
                                            <td>{{ $data->departure_date }}</td>
                                            

                                             <td>{{  $data->number_of_days }}</td>
                                             <td>{{  $data->created_at->DiffForHumans() }}, <strong>{{  $data->created_at }}</strong></td>

                                            <td>
                                            @if($data->status == 1)
                                            <a  onclick="show_check_in_details_modal('{{  $data->id }}')" ><span class="badge bg-secondary" style="padding: 10px;margin: 5px;">View</span>

                                            <a href="{{ url('home/booking/checkout/'. $data->id) }}"><span class="badge bg-danger" style="padding: 10px;">Check Out</span>

                                            @endif
                                        </td>

                                        </tr>

                                        @endforeach

                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>



                    </div>
                </div>

                @else
                <p><strong>No any guest checked in!.</strong></p>
                @endif
            </div>
        </section>
    </main>

    
@if(count($checkin)>0)
@foreach($checkin as $data_book_modal)
<div class="modal fade jd-modal" id="house_details_modal{{  $data_book_modal->id }}" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Guest(s) Details</h5>
				{{-- <button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button> --}}
			</div>
			
			<div class="modal-body" >
				{{-- <div class="container"> --}}
					
					{{-- <div class="tab-content"> --}}
						
						<div id="" class="tab-pane show in fade" style="padding-top: 24px">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-10" style="margin: auto;">
										

										@php
										$guests = [];
										if(count($data_book_modal->BookingDetail)>0){
										    foreach($data_book_modal->BookingDetail as $key=>$data1){
										        $real_key = $key + 1;
										        echo "<span> Room ". $real_key ." -  Category:".$data1->House->name." | Price: " .$data1->House->Currency->code . " ". $data1->House->cost . " | Adult: ". $data1->number_of_adults . " | Children: ". $data1->number_of_children . "</span> <hr>";
										    }
										}
										// $guests = implode(" , ", $guests);

										
									@endphp


										{{-- {{  $guests  }} --}}
									</div>
								</div>
							</div>
						</div>
						
                        
					{{-- </div> --}}
				{{-- </div> --}}
			</div>
		</div>
	</div>
</div>
@endforeach
@endif
{{-- main modal ends here --}}


    @endsection
    