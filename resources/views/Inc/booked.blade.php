@extends('layouts.user.app') 

@push('head') {{-- <link href="{{ asset('assets_user/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet" /> --}} @endpush @push('script') {{--
    <script src="{{ asset('assets_user/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    --}} @endpush 
    
    @push('script')


    <script>

function show_enquire_modal(house_id){
		// $('#house_id_holder').val(house_id)
		$('#house_main_modal' + house_id).modal('show');
		// $('#menu'+house_id+'3').removeClass('fade');
		// add_Active(house_id + '3')
	}	


    function show_details_modal(id){
		$('#house_details_modal' + id).modal('show');
		// $('#menu'+house_id+'3').removeClass('fade');
		// add_Active(house_id + '3')
	}	


	function submit_quatation(house_id){
	
    if(!$('#first_name_quot' + house_id).val()){
          $("#first_name_quot"+ house_id).notify('Please specify your first name.');
          return false;		
      }

      if(!$('#last_name_quot'+ house_id).val()){
          $("#last_name_quot"+ house_id).notify('Please specify your last name.');
          return false;		
      }


      if(!$('#country_code_quot'+ house_id).val()){
          $("#country_code_quot"+ house_id).notify('Please specify country code.');
          return false;		
      }

      if(!$('#number_of_adult_quot'+ house_id).val()){
          $("#number_of_adult_quot"+ house_id).notify('Please specify number of adults.');
          return false;		
      }

      if(!$('#phone_quot'+ house_id).val()){
          $("#phone_quot"+ house_id).notify('Please specify phone number.');
          return false;		
      }
                  
      if(!$('#number_of_children_quot'+ house_id).val()){
          $("#number_of_children_quot"+ house_id).notify('Please specify number of children.');
          return false;		
      }
      
      if(!$('#number_of_rooms_quot'+ house_id).val()){
          $("#number_of_rooms_quot"+ house_id).notify('Please specify number of rooms.');
          return false;		
      }
      
      if(!$('#user_email_quot'+ house_id).val()){
          $("#user_email_quot"+ house_id).notify('Please specify your email address.');
          return false;		
      }

      
if(!validate_email($('#user_email_quot'+ house_id).val())){
 $("#user_email_quot"+ house_id).notify("Please enter valid email address.");
 return false;
}



if(!$('#check__in_date_quot'+ house_id).val()){
          $("#check__in_date_quot"+ house_id).notify('Please specify check in date.');
          return false;		
      }

      

if(!$('#check__out_date_quot'+ house_id).val()){
          $("#check__out_date_quot"+ house_id).notify('Please specify check out date.');
          return false;		
      }



      if(!$('#confirm_code_quot'+ house_id).val()){
          $("#confirm_code_quot"+ house_id).notify('Please re-write the code above.');
          return false;		
      }else{


      let timerInterval
      Swal.fire({
      title: 'Saving booking infos!',
      html: 'It will take  <b></b> milliseconds.',
      timer: 2000,
      timerProgressBar: true,
      didOpen: () => {
          Swal.showLoading()
          const b = Swal.getHtmlContainer().querySelector('b')
          timerInterval = setInterval(() => {
          b.textContent = Swal.getTimerLeft()
          }, 100)
      },
      willClose: () => {
          clearInterval(timerInterval)
      }
      }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
          document.getElementById("quatation_form" + house_id).submit(); 
                     $('#house_main_modal' + house_id).modal('hide');
          }
      })




      }



      


      
}



    </script>
    @endpush

    
    @section('body')
    
    <main id="main" class="main">
        <div class="pagetitle">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{  url('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Booking</li>
                </ol>
            </nav>
        </div>
        <!-- End Page Title -->
    
        {{-- <div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;">
	<button type="button" class="btn btn-secondary" onclick="open_modal('add_house')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Add House</button>
</div> --}}


<div style="z-index: 200;
bottom: 3px;
right: 0px;
position: fixed;
height: 66px;
width: 100%;
padding-top: 13px;
background: white;tex-align:right;">

<select class="form-control"  style="text-align:center;border: 2px solid #ffffff;
box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;width: unset;background-color: #6c757d;color:white;">
    <option  selected="">-- Add Customer--</option>
  
    @if(count($houses)>0)


    @foreach($houses as $dataItem)
    <option  onclick="show_enquire_modal('{{  $dataItem->id }}')" value="{{  $dataItem->id }}">{{  $dataItem->name }}</option>
    @endforeach
    
    @endif
    {{-- <option value="1">One</option>
    <option value="2">Two</option>
    <option value="3">Three</option> --}}
  </select>
{{-- 

	<button type="button" class="btn btn-secondary" onclick="open_modal('add_customer')" style="border: 2px solid #ffffff;
		box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.2);padding:7px;position:fixed;right:65px;" >Register customer</button> --}}
</div>
        <section class="section dashboard">
            <div class="row">
                @if(count($booked)>0)
                <div class="card">
                    <div class="card-body">
                        
                        <div class="">
                            <div class="">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            {{-- <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">#</a></th> --}}
                                            <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Name</a></th>
                                            <th scope="col" data-sortable="" style="" class="desc"><a href="#" class="dataTable-sorter">Reservation No</a></th>

                                            <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Email</a></th>
                                            <th scope="col" data-sortable="" style="" class=""><a href="#" class="dataTable-sorter">Phone</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Total Cost</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Arrival Date</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Departure Date</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">No Of Night(s)</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Booked At</a></th>
                                            <th scope="col" data-sortable="" style=""><a href="#" class="dataTable-sorter">Action</a></th>

                                            </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($booked as $data)
                                        @php

                                        $booking_id = $data->id;
                                                
                                        $title = $data->FirstBookingDetailRecord->title_id;
                                        $first_name = $data->FirstBookingDetailRecord->first_name;
                                        $last_name = $data->FirstBookingDetailRecord->last_name;
                                        $user_name = $title ." ". $first_name." ". $last_name;


                                    @endphp

                                    @if($first_name==null)
                                    @continue
                                    @endif
                                        <tr>
                                           

                                           

                                            <td style="text-transform: capitalize">{{  $user_name }}</td>
                                            <td>{{ $data->booking_code }}</td>

                                            <td>{{  $data->email }}</td>

                                            @php
                                                   $country_code = $data->code;
                                                    $phone_number =  $data->phone;
                                                    $phone = $country_code . " - " . $phone_number;

                                            @endphp
                                            <td>{{  $phone }}</td>

                                            @php
                                               
                                                $price_total = 0;
                                                $cost = null;
                                                $price_code = null;
                                                if(count($data->BookingDetail)>0){
                                                    foreach($data->BookingDetail as $key=>$data2){
                                                        $price_code = $data2->House->Currency->code;
                                                        $price_total = $price_total + $data2->House->cost;
                                                    }
                                                }

                                                   $cost = $price_total * $data->number_of_days;
                                                     $cost = $price_code ." ".$cost;
                                            @endphp
                                            
                                            <td>{{ $cost }}</td>
                                            <td>{{ $data->arrival_date }}</td>
                                            <td>{{ $data->departure_date }}</td>
                                            
                                            <td>
                                                {{  $data->number_of_days }}
                                                {{-- @if($data->status== 0)
                                                <span class="badge bg-secondary">Reviewed</span>

                                                @else
                                                <span class="badge bg-primary">Reviewed</span>
                                                @endif</td> --}}
                                          
                                            </td>
                                            <td>{{  $data->created_at->DiffForHumans() }}, <strong>{{  $data->created_at }}</strong> </td>
                                            <td>
                                                @if($data->status== 0)
                                                <a  onclick="show_details_modal('{{  $data->id }}')" ><span class="badge bg-secondary" style="padding: 10px;margin: 5px;">View</span></a>
                                                    <a href="{{ url('home/booking/'. $booking_id) }}"><span class="badge bg-primary" style="padding: 10px;margin: 5px;">Check In</span></a>

                                                @endif
                                            </td>
                                            
                                        </tr>

                                        @endforeach

                                        
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>



                    </div>
                </div>

                @else
                <p><strong>No any booking!.</strong></p>
                @endif
            </div>
        </section>
    </main>

    @include('Inc.Modal.add_customer')

    @endsection
    