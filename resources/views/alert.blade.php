<script>

    
            setTimeout(function(){
                $('.alert').fadeOut('fast');
            },500);
    
    
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    
    
    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert" style="margin-bottom:-10px;">
            <p style="color:yellow;font-size:18px;" class="label label-danger">
                {{ $error }}
            </p>
            <div>
        @endforeach
    @endif
    
    
    @if(session('success'))
    <div class="alert" style="margin-bottom:-10px;">
        <p style="color:green;font-size:18px;" class="label label-success">
             {{ session('success' )}}
        </p>
    </div>
    
    @endif
    
    @if(session('error'))
    <div class="alert" style="margin-bottom:-10px;">
    
        <p  style="color:red;font-size:18px;"  class="label label-danger">
             {{ session('error' )}}
        </p>
    </div>
    @endif 