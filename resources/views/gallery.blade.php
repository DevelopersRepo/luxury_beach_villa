

@extends('layouts.guest.app')
@push('head')
<style>
.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(54, 65, 70, 0.08);
}
.dsa{
	background: #009cea;
    border: 0;
    padding: 10px 24px;
    color: #fff;
    transition: 0.4s;
    border-radius: 4px;
}

.btn {
    box-shadow: 0 6px 5px rgb(0 0 0 / 12%);
}

.btn-secondary {
    border: 0px solid rgba(134, 142, 150, 0.75);
}
	</style>
@endpush
@include('layouts.guest.nav_inner_page')

@section('body')

<section id="breadcrumbs" class="breadcrumbs" style="margin-top: -6px;">
	<div class="container">

	  <ol>
		<li><a href="index.html">Home</a></li>
		<li>View Gallery</li>
	  </ol>
	  <h2>Gallery</h2>

	</div>
  </section>


  <section id="gallery" class="portfolio">
    <div class="container">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Gallery</h2>
      </div>
      <div class="row" style="margin-top: -33px;">
        <div class="col-lg-12 d-flex justify-content-center aos-init aos-animate" style="margin-top: -15px;" data-aos="fade-up" data-aos-delay="100">
          <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">All</li>
            
            {{-- $galleryCategory<li data-filter=".filter-app">Rooms</li> --}}
  
            @php
            $galleryCategoryCounter = 1;
            @endphp
            @if(count($galleryCategory)>0)
            @foreach($galleryCategory as $data)
            <li data-filter=".filter-{{ $data->id }}">{{  $data->name }}</li>
            @php
            $galleryCategoryCounter ++;
            @endphp
            @endforeach
            @endif
  
  
  
            {{-- <li data-filter=".filter-card">Fitness</li> --}}
            {{-- <li data-filter=".filter-app">Surroundings</li> --}}
            {{-- <li data-filter=".filter-web">Business Center</li> --}}
          </ul>
        </div>
      </div>
      <div class="row portfolio-container aos-init aos-animate" data-aos="fade-up" data-aos-delay="200" style="position: relative; height: 756px;">
        {{-- <div class="col-md-3 portfolio-item filter-app" style="position: absolute; left: 0px; top: 0px;">
          <div class="portfolio-wrap">
            <img src="{{  asset('assets/website/assets/img/breakfast-hotel-g182986909_1920.jpg')  }}" class="img-fluid" alt="">
            <div class="portfolio-info">
            </div>
            <div class="portfolio-links">
              <a href="{{  asset('assets/website/assets/img/breakfast-hotel-g182986909_1920.jpg')  }}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 1"><i class="bx bx-file"></i></a>
            </div>
          </div>
        </div> --}}
        {{-- <div class="col-md-3 portfolio-item filter-web" style="position: absolute; left: 320px; top: 0px;">
          <div class="portfolio-wrap">
            <img src="{{  asset('assets/website/assets/img/lbv-bg.jpg')  }}" class="img-fluid" alt="">
            <div class="portfolio-info">
            </div>
            <div class="portfolio-links">
              <a href="{{  asset('assets/website/assets/img/lbv-bg.jpg')  }}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="Web 3"><i class="bx bx-file"></i></a>
            </div>
          </div>
        </div>
        <div class="col-md-3 portfolio-item filter-app" style="position: absolute; left: 640px; top: 0px;">
          <div class="portfolio-wrap">
            <img src="{{  asset('assets/website/assets/img/city_tour.jpg')  }}" class="img-fluid" alt="">
            <div class="portfolio-info">
            </div>
            <div class="portfolio-links">
              <a href="{{  asset('assets/website/assets/img/city_tour.jpg')  }}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="App 2"><i class="bx bx-file"></i></a>
            </div>
          </div>
        </div>
        <div class="col-md-3 portfolio-item filter-card" style="position: absolute; left: 0px; top: 252px;">
          <div class="portfolio-wrap">
            <img src="{{  asset('assets/website/assets/img/breakfast-hotel-g182986909_1920.jpg')  }}" class="img-fluid" alt="">
            <div class="portfolio-info">
            </div>
            <div class="portfolio-links">
              <a href="{{  asset('assets/website/assets/img/breakfast-hotel-g182986909_1920.jpg')  }}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="Card 2"><i class="bx bx-file"></i></a>
            </div>
          </div>
        </div> --}}
  
  
        @php
        $galleryCounter = 1;
        @endphp
        @if(count($gallery)>0)
        @foreach($gallery as $data)
  
        <div class="col-md-3 portfolio-item filter-{{  $data->GalleryCategory->id }}" style="position: absolute; left: 320px; top: 252px;">
          <div class="portfolio-wrap">
            <img src="{{ asset('/storage/uploads/'.$data->name) }}" class="img-fluid" alt="">
            <div class="portfolio-info">
            </div>
            <div class="portfolio-links">
              <a href="{{ asset('/storage/uploads/'.$data->name) }}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="Web 2"><i class="bx bx-file"></i></a>
            </div>
          </div>
        </div>
  
  
  
        @php
        $galleryCounter ++;
        @endphp 
        @endforeach
    
        @else
      
    
        @endif
  
  
        
        
        
      </div>
    </div>
  </section>
@endsection

