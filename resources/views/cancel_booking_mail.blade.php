

<html>
	<head>
		{{-- 
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		--}}
		<style>
		
			</style>
	</head>
	<body style="padding-top: 52px;">
		<table cellpadding="0" cellspacing="0" border="0" width="600" class="mobile" style="margin: 0 auto;border-style: solid;
			border-width: thin;
			border-color: #dadce0;
			border-radius: 8px;
			padding: 40px 20px;background: #fbfbfb;" align="center">
			<tr>
				<td style="padding-left: 20px;
				padding-right: 20px;">
					<p style="text-align: center;margin-bottom: -18px;">			
						<img style="max-height: 123px;" src="https://luxurybeachvilla.co.tz/assets/website/img/fav2.png" alt="">
					</p>

					@if($user_role=="admin")
					<h3 style="text-align: center;">Booking Cancellation.</h3>
					@endif
					


					@if($user_role=="customer")
					<h3 style="text-align: left;text-transform:capitalize;color: black;" >Hello {{  $user_name }}!,</h3>
				
					<p style="text-align: left;color:black;">Thanks for contacting us.</p>
					<p style="text-align: left;color:black;">We're sorry to hear you wish to cancel your subscription with us. 
					If you would like to cancel due to quality issues, we would like to do anything we can to resolve the issue.</p>
					<p style="text-align: left;color:black;">Are you available for a quick call to discuss potential resolutions? Let me know what works best for you.</p>
					<p style="text-align: left;color:black;"> We appreciate your time! </p>
					@elseif($user_role=="admin")
					<p style="text-align: left;color:black;"> {{  $main_paragraph  }} </p>
					@endif

						{{-- <p style="text-align: left;"> {{  $main_paragraph }} </p> --}}
					<svg style="margin-bottom: 15px;" xmlns="http://www.w3.org/2000/svg" class="color-bar-footer" width="100%" height="3" viewBox="0 0 1441 3" preserveAspectRatio="none" style="">
						<g fill="none" fill-rule="evenodd">
							<rect width="144" height="3" fill="#0B88C3"></rect>
							<rect width="144" height="3" x="144" fill="#00D377"></rect>
							<rect width="144" height="3" x="288" fill="#00B950"></rect>
							<rect width="144" height="3" x="432" fill="#FFD247"></rect>
							<rect width="144" height="3" x="576" fill="#FF9800"></rect>
							<rect width="144" height="3" x="720" fill="#FF464F"></rect>
							<rect width="144" height="3" x="864" fill="#FF82B3"></rect>
							<rect width="144" height="3" x="1008" fill="#FF4385"></rect>
							<rect width="144" height="3" x="1152" fill="#A47DCC"></rect>
							<rect width="145" height="3" x="1296" fill="#875BB5"></rect>
						</g>
					</svg>
					<p style="text-align: center;margin-bottom: -19px;color: #5f6368;">Please visit our website for any missing details 
						<a href="https://luxurybeachvilla.co.tz/" style="font-weight: bold;font-family: 'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;
							color: #00BFFF;">luxurybeachvilla.co.tz </a>
							  <br>
							  <small style="color: #5f6368;">© @php echo date("Y"); @endphp Luxury Beach Villa,  Mbezi Beach White Sands Road, Dar Es Salaam.
							  </small>
					</p>
				</td>
			</tr>
		
		</table>
	</body>

