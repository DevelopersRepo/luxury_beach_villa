<html>
	<head>
		{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> --}}
	</head>
	<body style="padding-top: 52px;">
		<table cellpadding="0" cellspacing="0" border="0" width="600" class="mobile" style="margin: 0 auto;border-style: solid;
			border-width: thin;
			border-color: #dadce0;
			border-radius: 8px;
			padding: 40px 20px;background: #fbfbfb;" align="center">
			<tr>
				<td>
					<p style="text-align: center;margin-bottom: -18px;">			
						<img style="max-height: 123px;" src="https://luxurybeachvilla.co.tz/assets/website/img/fav2.png" alt="">
					</p>
					<h3 style="text-align: center;text-transform:capitalize;color: black;">Hello {{  $name }}!,</h3>
					<p style="text-align: center;"> {{  $main_paragraph }} </p>
					<svg style="margin-bottom: 15px;" xmlns="http://www.w3.org/2000/svg" class="color-bar-footer" width="100%" height="3" viewBox="0 0 1441 3" preserveAspectRatio="none" style="">
						<g fill="none" fill-rule="evenodd">
							<rect width="144" height="3" fill="#0B88C3"></rect>
							<rect width="144" height="3" x="144" fill="#00D377"></rect>
							<rect width="144" height="3" x="288" fill="#00B950"></rect>
							<rect width="144" height="3" x="432" fill="#FFD247"></rect>
							<rect width="144" height="3" x="576" fill="#FF9800"></rect>
							<rect width="144" height="3" x="720" fill="#FF464F"></rect>
							<rect width="144" height="3" x="864" fill="#FF82B3"></rect>
							<rect width="144" height="3" x="1008" fill="#FF4385"></rect>
							<rect width="144" height="3" x="1152" fill="#A47DCC"></rect>
							<rect width="145" height="3" x="1296" fill="#875BB5"></rect>
						</g>
					</svg>
					<table cellpadding="0" cellspacing="0" border="0" width="100%" style=" font-size:20px; padding: 0 0 0 15%;font-size: inherit;">
						
						<tr>
							<td style="padding-bottom: 3%;text-align:right;"><strong>Username</strong> :</td>
							<td style="padding-bottom: 3%;padding-left: 2%;">{{  $email }}</td>
						</tr>
						<tr>
							<td style="padding-bottom: 3%;text-align:right;"><strong>Email</strong> :</td>
							<td style="padding-bottom: 3%;padding-left: 2%;">{{  $password }}</td>
						</tr>
						<tr>
							<td style="padding-bottom: 3%;text-align:right;"><strong>Link</strong> :</td>
							<td style="padding-bottom: 3%;padding-left: 2%;">{{  url('/') }}</td>
						</tr>
					
					</table>
					<p style="text-align: center;color: #5f6368;margin-bottom: -19px;">Please visit our website for any missing details 
						<a href="https://luxurybeachvilla.co.tz/" style="font-weight: bold;font-family: 'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;
							color: #00BFFF;">luxurybeachvilla.co.tz </a>
                            <br>
                            <small style="color: #5f6368;">© @php echo date("Y"); @endphp Luxury Beach Villa,  Mbezi Beach White Sands Road, Dar Es Salaam.
                            </small>
                            
					</p>
				</td>
			</tr>
		
            
		</table>
	</body>

