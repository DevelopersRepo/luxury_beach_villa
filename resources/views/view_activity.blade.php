

@extends('layouts.guest.app')
@push('head')
<style>
.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(54, 65, 70, 0.08);
}
.dsa{
	background: #009cea;
    border: 0;
    padding: 10px 24px;
    color: #fff;
    transition: 0.4s;
    border-radius: 4px;
}

.btn {
    box-shadow: 0 6px 5px rgb(0 0 0 / 12%);
}

.btn-secondary {
    border: 0px solid rgba(134, 142, 150, 0.75);
}
	</style>
@endpush

@section('body')
@include('layouts.guest.nav_inner_page')

<section id="breadcrumbs" class="breadcrumbs" style="margin-top: -6px;">
	<div class="container">

	  <ol>
		<li><a href="index.html">Home</a></li>
		<li>View Activity</li>
	  </ol>
	  <h2>View Activity</h2>

	</div>
  </section>


  <section id="portfolio-details" class="portfolio-details">
	

	

			<div class="container" style="margin-top: 24px;">
				<div class="row">

					
			<div class="col-md-12 hsdbcksd" data-aos="zoom-in">
				<div class="icon-box portfolio-info">
					<div class="row">
						@if($activity->description)
						{!!  $activity->description  !!}
						@else
						Sorry!, No description for this activity.
						@endif
					</div>
				</div>
			</div>
			
			
		
			
			

		</div>
	  
	</div>
  </section>
  <hr>
@endsection

