

@extends('layouts.guest.app')
@push('head')
<style>
.portfolio-details .portfolio-info {
  padding: 30px;
  box-shadow: 0px 0 30px rgba(54, 65, 70, 0.08);
}
.dsa{
	background: #009cea;
    border: 0;
    padding: 10px 24px;
    color: #fff;
    transition: 0.4s;
    border-radius: 4px;
}

.btn {
    box-shadow: 0 6px 5px rgb(0 0 0 / 12%);
}

.btn-secondary {
    border: 0px solid rgba(134, 142, 150, 0.75);
}
	</style>
@endpush
@include('layouts.guest.nav_inner_page')

@section('body')

<section id="breadcrumbs" class="breadcrumbs" style="margin-top: -6px;background: #fff;">
	<div class="container">

	  <ol>
		<li><a href="index.html">Home</a></li>
		<li>View Contacts</li>
	  </ol>
	  <h2>Contacts</h2>

	</div>
  </section>

@endsection

