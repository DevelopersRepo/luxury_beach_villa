

@extends('layouts.guest.app')
@push('head')
<link href="{{  asset('assets/website/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">
<link href="{{  asset('assets/website/assets/css/custom/bootstrap.min.css') }}" rel="stylesheet">
<style>
	.portfolio-details .portfolio-info {
	padding: 30px;
	box-shadow: 0px 0 30px rgba(54, 65, 70, 0.08);
	}
	.dsa{
	background: #797978;
	border: 0;
	padding: 5px;
	color: #fff;
	transition: 0.4s;
	border-radius: 4px;
	}
	.btn {
	box-shadow: 0 6px 5px rgb(0 0 0 / 12%);
	}
	.btn-secondary {
	border: 0px solid rgba(134, 142, 150, 0.75);
	}
	.portfolio-details .portfolio-details-slider .swiper-pagination .swiper-pagination-bullet-active {
	background-color: #009cea;
	}
	.portfolio-details .portfolio-info ul {
	font-size: 12px;
	}
	.jfbvkjf{
	padding-right: 10px;
	padding-left: 10px;
	}
	.dfvcsdcs{
	display: flex;
	padding: 10px;
	background: #f0f0f08a;
	border-radius: 1px;
	margin-bottom: 3px;
	}
	.cvscs{
	width: initial;
	padding-left: 10px;
	padding-right: 10px;
	}
	.main-icon{
	color: #4CAF50;
	font-size: x-large;
	margin-right: 0px;
	}
	.main_list{
	display: contents;
	list-style: none;
	font-size: small;
	}
	.lis{
	margin-top: -10px;
	}
	/* @media (min-width: 992px)
	.modal-lg, .modal-xl {
	max-width: 1000px;
	} */
</style>
@endpush
@push('script')
<script src="{{  asset('assets/website/assets/js/custom/room_selection_handler.js') }}"></script>
<script src="{{  asset('assets/website/assets/js/custom/bootstrap.min.js') }}"></script>
<script>
	$('.next_room_default').val(0)
</script>
@endpush
@section('body')
@include('layouts.guest.nav_inner_page')
<section id="breadcrumbs" class="breadcrumbs" style="margin-top: -6px;">
	<div class="container">
		<ol>
			<li><a href="index.html">Home</a></li>
			<li>Check Available</li>
		</ol>
		<h2>Check Available</h2>
	</div>
</section>
<section id="portfolio-details" class="portfolio-details">
	<div class="container" style="margin-top: -20px;">
		<form method="POST" action="{{ url('check_available_post') }}">
			@csrf
			<div class="row" style="padding: 10px;
				background: #f4f4f4;
				border-radius: 7px;">
				<div class="col-lg-3 col-md-6">
					<div class="box aos-init aos-animate" data-aos="zoom-in">
						<div class="btn-wrap">
							{{-- 
							<div class="form-group">
								--}}
								<div class="" style="display: flex;">
									<input type="text" name="from_date" value="{{  $from_date }}" placeholder="Arrive Date" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" style="font-size: x-small;"  class="form-control btn btn-secondary" id="from_date" required="">
								</div>
								{{-- 
							</div>
							--}}
							{{-- 
							<button type="submit" class="btn btn-secondary">
								<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 svg_icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
									<path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
								</svg>
								From
							</button>
							--}}
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 mt-4 mt-md-0">
					<div class="box featured aos-init aos-animate" data-aos="zoom-in" data-aos-delay="100">
						<div class="btn-wrap">
							<div class="" style="display: flex;">
								<input type="text" name="to_date"  value="{{  $to_date }}" placeholder="Departure Date" onfocus="(this.type='date')" onblur="if(this.value==''){this.type='text'}" style="font-size: x-small;"  class="form-control btn btn-secondary" id="to_date" required="">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 mt-4 mt-lg-0">
					<div class="box aos-init aos-animate" data-aos="zoom-in" data-aos-delay="200">
						<div class="btn-wrap">
							<div class="" style="display: flex;">
								{{-- <input type="number" name="number_of_guest" oninput="number_range_checker()" placeholder="No Of Guest" min="1" max="12" onfocus="(this.type='number')" onblur="if(this.value==''){this.type='text'}"  class="form-control btn btn-secondary" id="number_of_guest" required=""> --}}
								<select name="number_of_adult" value="{{  $number_of_adult }}" id="number_of_adult" style="text-transform: capitalize;font-weight: bold;font-size: x-small;
									font-family: 'Nunito', sans-serif;" required="" class="form-control btn btn-secondary">
									<option style="text-transform: capitalize;font-weight: bold;" >--Select Adult--</option>
									@for($i=1;$i<11;$i++)								
									<option style="text-transform: capitalize;font-weight: bold;" 
									@if($i==$number_of_adult)
									selected=""
									@endif
									value="{{ $i }}">{{ $i }}</option>
									@endfor
								</select>
							</div>
							{{-- 
							<button type="submit" class="btn btn-secondary">
								<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 svg_icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
									<path stroke-linecap="round" stroke-linejoin="round" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
								</svg>
								People
							</button>
							--}}
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 mt-4 mt-lg-0">
					<div class="box aos-init aos-animate" data-aos="zoom-in" data-aos-delay="200">
						<div class="btn-wrap">
							<div class="" style="display: flex;">
								{{-- <input type="number" name="number_of_guest" oninput="number_range_checker()" placeholder="No Of Guest" min="1" max="12" onfocus="(this.type='number')" onblur="if(this.value==''){this.type='text'}"  class="form-control btn btn-secondary" id="number_of_guest" required=""> --}}
								<select name="number_of_children"  id="number_of_children" value="{{  $number_of_children }}" style="text-transform: capitalize;font-weight: bold;font-size: x-small;
									font-family: 'Nunito', sans-serif;" required="" class="form-control btn btn-secondary">
									<option style="text-transform: capitalize;font-weight: bold;" >--Select Children--</option>
									@for($i=1;$i<11;$i++)								
									<option style="text-transform: capitalize;font-weight: bold;" 
									@if($i==$number_of_children)
									selected=""
									@endif
									value="{{ $i }}">{{ $i }}</option>
									@endfor
								</select>
								{{-- 
								<select name="facility"  id="facility" style="" class="form-control btn btn-secondary">
									--}}
									{{-- <label for="description" >Feature(s):</label> --}}
									{{-- 
									<optgroup selected="">Feature(s):</optgroup>
									--}}
									{{-- @if(count($facilities)>0)
									@foreach($facilities as $data) --}}
									{{-- 
									<option style="text-transform: capitalize;font-weight: bold;" selected="">No of People</option>
									@for($i=0; $i<10; $i++)
									<option style="text-transform: capitalize;font-weight: bold;">{{ $i }}</option>
									@endfor --}}
									{{-- @endforeach
									@else --}}
									{{-- 
									<option style="" value="">No any facility registered.</option>
									--}}
									{{-- @endif --}}
									{{-- 
								</select>
								--}}
							</div>
							{{-- 
							<button type="submit" class="btn btn-secondary">
								<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 svg_icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
									<path stroke-linecap="round" stroke-linejoin="round" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
								</svg>
								People
							</button>
							--}}
						</div>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 mt-4 mt-lg-0">
					<div class="box aos-init aos-animate" data-aos="zoom-in"  data-aos-delay="300">
						<div class="btn-wrap">
							<a href="{{  url('check_available') }}">
								<button type="submit"style="font-size: x-small;" class="btn btn-primary">
									Search
									{{-- 
									<svg   xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 svg_icon" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
										<path stroke-linecap="round" stroke-linejoin="round" d="M14 5l7 7m0 0l-7 7m7-7H3" />
									</svg>
									--}}
								</button>
							</a>
						</div>
					</div>
				</div>
				{{-- 
			</div>
			--}}
		</form>
	</div>
	<div class="container" style="margin-top: 24px;">
		<div class="row">
			<div class="col-md-9">
				@if(count($houses)>0)
				@foreach($houses as $data)
				<div class="row hsdbcksd" data-aos="zoom-in" style="margin-bottom: 4px;">
					<div class="icon-box portfolio-info">
						<div class="row">
							<div class="col-lg-4 portfolio-details-slider swiper" sty>
								{{-- 
								<div class="portfolio-details-slider swiper">
									<div class="swiper-wrapper align-items-center">
										<div class="swiper-slide"> --}}
											<img src="{{  asset('assets/website/assets/img/imported/22866_20210602213329_0832523001622669609_893_Double_Room1.png')  }}" alt="">
											{{-- 
										</div>
										<div class="swiper-slide">
											<img src="{{  asset('assets/website/assets/img/house800x600/20211229_121653.jpg')  }}" alt="">
										</div>
										<div class="swiper-slide">
											<img src="{{  asset('assets/website/assets/img/house800x600/20211207_200647.jpg')  }}" alt="">
										</div>
									</div>
									<div class="swiper-pagination"></div>
								</div>
								<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-horizontal"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span></div>
								<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span> --}}
							</div>
							{{-- 
							<div class="col-lg-6 aos-init aos-animate" data-aos="zoom-in">
								<img src="{{  asset('assets/website/assets/img/quick_trips/surroundings.jpg')  }}" class="img-fluid hide-on-mobile" alt="">
							</div>
							--}}
							<div class="col-lg-8 d-flex flex-column justify-contents-center aos-init aos-animate" data-aos="fade-left">
								<div class="">
									<h3 id="item_name_{{  $data->id }}">{{  $data->name }}</h3>
									<ul>
										<li><strong>Room capacity</strong>: (  <small>Adult:</small> {{  $data->max_number_of_adults }}, <small>Children:</small> {{  $data->max_number_of_children }}, <small>Room:</small> 1) </li>
										<li><strong>Room Left</strong>: <span id="room_left{{  $data->id }}">{{  $data->number_of_rooms }}</span></li>
										{{-- 
										<li><strong>Category</strong>: {{  $data->Category->name }}</li>
										--}}
										<li><strong>Location</strong>: {{  $data->Location->name }}</li>
										<li><strong>Price</strong>: <strong id="item_currency_{{  $data->id }}">{{  $data->Currency->code }} </strong><strong  id="item_price_{{  $data->id }}">{{  $data->cost }}</strong>, {{  $data->Price_reference  }} (<strong style="color: #e3a646;">Room Rates Inclusive of Tax</strong>)</li>
									</ul>
									<hr>
									<div style="margin-bottom: 10px;
										display: inline-flex;
										width: 100%;">
										<div>
											<a  data-toggle="modal" data-target="#android-developer" >	<button type="button" class="dsa" style="color: #454a4c;background: #79797800;" ><strong>Room infos</strong></button></a>
											<a href="{{  url('/book/'.$data->id) }}" >	<button type="button" class="dsa" style="color: #00BFFF;background: #79797800;"><strong>Enquire</strong></button></a>
										</div>
										<a onclick="add_room_in_list('{{ $data->id }}','{{ $data->number_of_rooms }}')" style="float:right;padding-right: 0px;margin: auto;" id="room_adder{{ $data->id }}"><button type="button" class="dsa" >Add Room</button></a>
										@php
										$number_of_rooms = $data->number_of_rooms;
										@endphp
										<div  class="form-group" id="main_switcher{{ $data->id }}" style="text-align: right;width: 72px;
											display: -webkit-inline-box;
											float: right;padding-right: 0px;margin: auto;display:none;">
											{{-- <strong>4 Room(s) left</strong> --}}
											<button type="button" class="btn btn-default btn-sm cvscs"  onclick="remove_room_details('{{ $data->id }}','{{  $number_of_rooms }}')">-</button>
											<input type="text" style="" class="form-control next_room_default" disabled="" name="next_room_remaining_rooms_in_house_{{ $data->id }}"  id="next_room_remaining_rooms_in_house_{{ $data->id }}" value="0"/>
											<input type="hidden" style="" class="form-control" name="temp_last_room_value_holder{{ $data->id }}"  id="temp_last_room_value_holder{{ $data->id }}" value=""/>
											<button type="button" class="btn btn-default btn-sm cvscs" onclick="add_room_details('{{ $data->id }}','{{  $number_of_rooms }}')">+</button>
										</div>
									</div>
									<div class="" id="room_main_section_{{  $data->id }}">									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@else
				Sorry!, No available house yet.
				@endif
			</div>
			<div class="col-md-3">
				<div class="portfolio-info">
					<h6><strong>Booking Summary</strong></h6>
					<ul>
						<li><strong>Dates</strong>: <span id="cart_date"></span> </li>
						<li><strong>Night </strong>: <span id="cart_reference"></span></li>
						<li><strong>Room </strong>: <span id="cart_item"></span></li>
						<input type="hidden" id="cart_item_id" name="cart_item_id">
						<li>
							<h6 style="font-family: 'Nunito', sans-serif;"><strong>TOTAL </strong>: <span id="cart_price"></span></h6>
						</li>
						<li>
							@if(count($houses)>0)
							<a href="{{  url('/book/'.$data->id) }}" style="float:right;padding-right: 0px;margin: auto;width: 100%;display:none" id="book_btn"><button type="button" class="dsa" style="width: inherit;">Book</button></a>
							@endif
							{{-- 
							<h6 style="font-family: 'Nunito', sans-serif;">
							</h6>
							--}}
						</li>
						{{-- 
						<li><strong>Project URL</strong>: <a href="#">www.example.com</a></li>
						--}}
					</ul>
					{{-- 
					<li><strong>Category</strong>: Web design</li>
				</div>
				--}}
			</div>
		</div>
	</div>
</section>
<div class="modal fade jd-modal" id="android-developer" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00BFFF;
				color: white;
				font-weight: bold;">
				<h5 class="modal-title">Deluxe Double Room with Breakfast</h5>
				<button type="button" class="close" data-dismiss="modal">
				<span>×</span>
				</button>
			</div>
			<div class="modal-body" >
				{{-- 
				<nav id="navbar" class="navbar order-last order-lg-0">
					<ul>
						<li><a class="nav-link scrollto " href="#hero">Home</a></li>
						<li class="dropdown"><a href="#"><span>About</span> <i class="bi bi-chevron-down"></i></a>
						</li>
						<li><a class="nav-link scrollto" href="#services">Services</a></li>
						<li><a class="nav-link scrollto " href="#portfolio">Portfolio</a></li>
						<li><a class="nav-link scrollto" href="#pricing">Pricing</a></li>
						<li><a class="nav-link scrollto" href="#contact">Contact</a></li>
					</ul>
					<i class="bi bi-list mobile-nav-toggle"></i>
				</nav>
				--}}
				<ul class="nav nav-tabs flex-column" style="display: -webkit-inline-box;margin-top: -22px;width:100%;width: -moz-available;">
					<li  class="nav-item aos-init aos-animate" style="margin-top: 8px;padding: 6px;" data-aos="fade-up">
						<a class="nav-link active show" data-bs-toggle="tab" href="#tab-1">
						<strong>Room Details</strong>
						</a>
					</li>
					<li  class="nav-item mt-2 aos-init aos-animate" style="padding: 6px;" data-aos="fade-up" data-aos-delay="100">
						<a class="nav-link" data-bs-toggle="tab" href="#tab-2">
						<strong>Photo Gallery</strong>
						</a>
					</li>
					<li  class="nav-item mt-2 aos-init aos-animate" style="padding: 6px;" data-aos="fade-up" data-aos-delay="200">
						<a class="nav-link" data-bs-toggle="tab" href="#tab-3">
						<strong>Airbnb Resort Map</strong>
						</a>
					</li>
					{{-- 
					<li  class="nav-item mt-2 aos-init aos-animate" style="padding: 6px;" data-aos="fade-up" data-aos-delay="300">
						<a class="nav-link" data-bs-toggle="tab" href="#tab-4">
						zhdcvj
						</a>
					</li>
					--}}
				</ul>
				<div class="col-lg-12 order-1 order-lg-2 aos-init aos-animate" data-aos="zoom-in">
					<div class="tab-content">
						<div class="tab-pane active show" id="tab-1">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								<div class="col-md-3">
									<figure>
										<img src="{{  asset('assets/website/assets/img/house800x600/20211229_121653.jpg')  }}" alt="" class="img-fluid">
									</figure>
								</div>
								<div class="col-md-9">
									<div class="">
										<h6><strong>Room Description: </strong></h6>
										<p>Double Room with Breakfast</p>
										<p><strong> Room Capacity: </strong> 2 Adult(s), 0 Child(ren) / Room</p>
										<h6><strong>Room Amenities: </strong></h6>
										<div class="row footer-links" style="padding-left: -3px;
											padding-right: 15px;">
											<div class="col-md-4 footer-links">
												<ul class="main_list">
													<li class="lis"><i class="bi bi-check main-icon"></i> Wi-Fi in designated areasFree</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Billiards</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Multilingual staff</li>
												</ul>
											</div>
											<div class="col-md-4">
												<ul class="main_list">
													<li class="lis"><i class="bi bi-check main-icon"></i>Wi-Fi in designated areasFree</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Billiards</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Multilingual staff</li>
												</ul>
											</div>
											<div class="col-md-4">
												<ul class="main_list">
													<li class="lis"><i class="bi bi-check main-icon"></i>Wi-Fi in designated areasFree</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Billiards</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Multilingual staff</li>
												</ul>
											</div>
										</div>
										<h6 style="margin-top: 11px;"><strong>Airbnb Resort Amenities: </strong></h6>
										<div class="row footer-links" style="padding-left: -3px;
											padding-right: 15px;">
											<div class="col-md-4 footer-links">
												<ul class="main_list">
													<li class="lis"><i class="bi bi-check main-icon"></i>Wi-Fi in designated areasFree</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Billiards</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Multilingual staff</li>
												</ul>
											</div>
											<div class="col-md-4">
												<ul class="main_list">
													<li class="lis"><i class="bi bi-check main-icon"></i>Front desk (24 hours)</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Luggage storage</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Designated smoking area</li>
												</ul>
											</div>
											<div class="col-md-4">
												<ul class="main_list">
													<li class="lis"><i class="bi bi-check main-icon"></i>Gym</li>
													<li class="lis"><i class="bi bi-check main-icon"></i>Outdoor swimming pool</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-2">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								<div class="col-md-8 portfolio-details-slider swiper" sty>
									<div class="portfolio-details-slider swiper">
										<div class="swiper-wrapper align-items-center">
											<div class="swiper-slide">
												<img src="{{  asset('assets/website/assets/img/imported/22866_20210602213329_0832523001622669609_893_Double_Room1.png')  }}" alt="">
											</div>
											<div class="swiper-slide">
												<img src="{{  asset('assets/website/assets/img/imported/22866_20210602213334_0582447001622669614_906_Double_Room2.png')  }}" alt="">
											</div>
											<div class="swiper-slide">
												<img src="{{  asset('assets/website/assets/img/imported/22866_20210602213340_0806624001622669620_959_Double_Room3.png')  }}" alt="">
											</div>
											<div class="swiper-slide">
												<img src="{{  asset('assets/website/assets/img/imported/22866_20210602213345_0420295001622669625_694_Double_Room4.png')  }}" alt="">
											</div>
										</div>
										<div class="swiper-pagination"></div>
									</div>
									<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-horizontal"><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span></div>
									<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-3">
							<div class="row" style="padding-top: 11px;
								padding-bottom: 11px;">
								{{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.6000988917567!2d39.222497314771715!3d-6.696349995158616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0!2zNsKwNDEnNDYuOSJTIDM5wrAxMycyOC45IkU!5e0!3m2!1sen!2stz!4v1647865919144!5m2!1sen!2stz" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe> --}}
								<iframe aria-hidden="true" tabindex="-1" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: medium none;" frameborder="0"></iframe>
							</div>
						</div>
						{{-- 
						<div class="tab-pane" id="tab-4">
							<figure>
								<img src="{{  asset('assets/website/assets/img/house800x600/20211229_121653.jpg')  }}" alt="" class="img-fluid">
							</figure>
						</div>
						--}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

