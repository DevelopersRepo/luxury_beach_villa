<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    use HasFactory;
        
    public function Creator(){
        return $this->belongsTo(User::class, 'created_by');
    }
   
  
    public function Feature(){
        return $this->belongsToMany(Feature::class);
     }


     
}
