<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{


    use HasFactory;

    public function Booking(){
        return $this->belongsTo(Booking::class, 'booking_id');
    }
    
    public function CheckedInBooking(){
        return $this->hasMany(Booking::class, 'booking_id');
    }
   

    public function House(){
        return $this->belongsTo(House::class, 'house_id');
    }
}

