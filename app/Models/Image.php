<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    
    public function GalleryCategory(){
        return $this->belongsTo(GalleryCategory::class, 'category_id');
     }


}
