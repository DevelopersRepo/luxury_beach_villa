<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use HasFactory;

    
    public function Creator(){
        return $this->belongsTo(User::class, 'created_by');
    }

    
  
    public function Facility(){
        return $this->belongsToMany(Facility::class);
     }
}
