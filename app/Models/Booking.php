<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    public function FirstBookingDetailRecord(){
        return $this->hasOne(BookingDetail::class, 'booking_id')->orderBy('updated_at', 'desc');
    }


    public function BookingDetail(){
        return $this->hasMany(BookingDetail::class, 'booking_id');
    }



}
