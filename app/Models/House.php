<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    public function Creator(){
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function BookingDetail(){
        return $this->belongsTo(BookingDetail::class, 'house_id');
    }


    public function SpecialBookingDetail(){
        return $this->hasMany(BookingDetail::class, 'house_id');
    }

    public function Currency(){
        return $this->belongsTo(Currency::class, 'currency_id');
    }
    
    public function Activity(){
        return $this->belongsToMany(Activity::class);
     }

    public function Location(){
        return $this->belongsTo(Location::class, 'location_id');
    }

    public function Category(){
        return $this->belongsTo(Category::class, 'category_id');
    }
    
    public function HouseImage(){
        return $this->hasMany(HouseImage::class, 'house_id');
    }
    
    public function LatestHouseImage(){
        return $this->hasOne(HouseImage::class, 'house_id')->orderBy('updated_at', 'desc');
    }

}
