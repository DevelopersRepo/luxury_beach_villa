<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'file' => 'image|required|max:7999',
        ]);

        // lower value high compression - less mbs
        // high value low compresssion - high mbs
        $x = $request->quality_level;
        if($request->hasFile('file')){
                //get file name
                $file = $request->file('file');
                $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
                $fileData = \Image::make($file);

                if($x==0){
                    $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore));

                }else{
                    $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                

                }
                
                
        }else{
            $fileNameToStore = null;
        }

        $image = new Image;
        $image->name = $fileNameToStore;        
        $image->category_id = $request->input('category_id');
        if($request->input('description')){
            $image->description = $request->input('description');
        }
        $image->created_by = auth()->user()->id;
        $image->save();
        return redirect()->back();   



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
