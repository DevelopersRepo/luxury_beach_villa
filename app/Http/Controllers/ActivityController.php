<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // $this->validate($request,[
        //     'file' => 'image|max:50999',
        // ]);

        // lower value high compression - less mbs
        // high value low compresssion - high mbs
        // $x = 10;
        // $fileNameToStore =null;
        // if($request->hasFile('file')){
        //         //get file name
        //         $file = $request->file('file');
        //         $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
        //         $fileData = \Image::make($file);
        //         $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                
        // }
        

        $activity = new Activity;
        $activity->title = $request->input('title');
        $activity->front_description = $request->input('front_description');



        if($request->hasFile('front_file')){
            //get file name
            $file = $request->file('front_file');
            $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
            $fileData = \Image::make($file);
            $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), 10);                
        }else{
            $fileNameToStore = null;
        }
            
        $activity->front_file = $fileNameToStore;


        // if($fileNameToStore){
        //     $activity->image_name = $fileNameToStore;  
        // }

     


        if($request->input('activity_desc')){
            $activity->description = $request->input('activity_desc');
        }
        $activity->created_by = auth()->user()->id;

        $activity->save();




        $facility_count = $request['facility']; 
        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $activity_id = $activity->id;
             
            // return $request;

            if($facility_count){
        foreach($facility_count as $facility_counter_data) {
            // $data_data_check = FacilityFeature::where('feature_id', $facility_counter_data)->where('facility_id', $facility_id)->first();
            //    if(!$data_data_check){
                $facil_data=array('facility_id'=>$facility_counter_data,"activity_id"=>$activity_id, 'created_at'=>$created_at,'updated_at'=>$updated_at);
                DB::table('activity_facility')->insert($facil_data);
            //   }
        }}


        return redirect()->back();   





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        //
    }
}
