<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ActivityImage;

class JsonResponseImageController extends Controller
{
    //

    public function upload(Request $request){

        $x = 10;
        if($request->hasFile('upload')){
                //get file name
                $file = $request->file('upload');
                $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
                $fileData = \Image::make($file);
                $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                
        }else{
            $fileNameToStore = null;
        }

        $image = new ActivityImage;
        $image->name = $fileNameToStore;    
        $image->created_by = auth()->user()->id;
        $image->save();
        return response()->json([
            'url'=>asset('/storage/uploads/'.$fileNameToStore)
        ]); 


    }
}
