<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $bytes = random_bytes(5);
        // $random_hex = bin2hex($bytes);
        $name = $request->name;
        $email = $request->email;
        $password = 'luxury1234';

        // user check
        $usercheck = User::where('email', $email)->first();
        if($usercheck){
            return redirect()->back()->with('error', 'User already exist');
            // return false;
        }


        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password  =  Hash::make($password);
        $user->save();


        $main_paragraph = "Welcome Luxury Beach villa, Here's your Login Credentials.";
        //  email to customer
        $data = array('name'=>$name,'email'=>$email, 'password'=>$password,
          'main_paragraph'=>$main_paragraph
         );

        
        Mail::send('user_registration_email', $data, function($message) use($email) {
            $message->to($email, $email)->subject
                ('Login Credentials!.');
            $message->from(env('MAIL_FROM_ADDRESS'),'Luxury Beach Villa');
        });


        return redirect()->back()->with('success', 'User us successful added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
