<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $book = new Booking;
        $book->first_name = $request->first_name;
        $book->middle_name = $request->middle_name;
        $book->last_name = $request->last_name;
        $book->email = $request->email;
        $book->address = $request->address;
        $book->city = $request->city;
        $book->state = $request->state;
        $book->zip = $request->zip;
        $book->gender = $request->gender;
        $book->arrival_date = $request->arrival_date;
        $book->departure_date = $request->departure_date;
        $book->number_of_people = $request->number_of_people;
        $book->card_number = $request->card_number;
        $book->card_expiration = $request->card_expiration;
        $book->vcc_number = $request->vcc_number;
        $book->save();
        if($book->save()){
            return redirect('/')->with('success', "Thanks ".$book->last_name."!, we've receive your booking informations. We'll notify you through ".$book->email." after reviewing it.");

        }else{
            return redirect('/')->with('error', "Sorry, something is wrong in your details. Contact +255756883598 for more help");

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $book = Booking::find($id);
        $book->status = 1;
        $book->save();
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
