<?php

namespace App\Http\Controllers;

use App\Models\BookingDetail;
use Illuminate\Http\Request;
use App\Models\Booking;

class BookingDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // check unique value
        $major_id_holder_data  = $request->major_id_holder;

        $booking = new Booking;
        $booking->arrival_date = $request->check_in;
        $booking->departure_date = $request->check_out;
        $booking->number_of_days = $request->cart_number_of_days_holder;        
        $booking->save();

        $booking_id = $booking->id;   
        $unique_value = $request->unique_value;
      
        for($i= 0; $i <count($major_id_holder_data); $i++){        

            $bookingDetails = new BookingDetail;
            $bookingDetails->house_id = $major_id_holder_data[$i]["house_id"];
            $bookingDetails->number_of_adults = $major_id_holder_data[$i]["const_adult_id_holder"];
            $bookingDetails->number_of_children = $major_id_holder_data[$i]["const_children_id_holder"];
            $bookingDetails->booking_id = $booking_id;
            $bookingDetails->unique_value = $unique_value;
            $bookingDetails->save();
            
        }

        return response(["booking_id"=>$booking_id,"unique_value"=>"true"]);
       
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookingDetail  $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function show(BookingDetail $bookingDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookingDetail  $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(BookingDetail $bookingDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BookingDetail  $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookingDetail $bookingDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookingDetail  $bookingDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookingDetail $bookingDetail)
    {
        //
    }
}
