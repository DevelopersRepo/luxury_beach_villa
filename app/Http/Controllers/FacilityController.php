<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;
use App\Models\FacilityFeature;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Feature;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'file' => 'image|max:50999',
        ]);

        // lower value high compression - less mbs
        // high value low compresssion - high mbs
        $x = 10;
        $fileNameToStore =null;
        if($request->hasFile('file')){
                //get file name
                $file = $request->file('file');
                $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
                $fileData = \Image::make($file);
                $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                
        }
        

        $facility = new Facility;
        $facility->name = $request->input('name');

        if($fileNameToStore){
            $facility->image_name = $fileNameToStore;  
        }

     
        if($request->input('description')){
            $facility->description = $request->input('description');
        }
        $facility->created_by = auth()->user()->id;

        $facility->save();




           $feature_count = $request['feature']; 
        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
            $facility_id = $facility->id;
             
            // return $request;

            if($feature_count){
                foreach($feature_count as $feature_counter_data) {
                    // $data_data_check = FacilityFeature::where('feature_id', $feature_counter_data)->where('facility_id', $facility_id)->first();
                
                //    if(!$data_data_check){
                    $level_data=array('feature_id'=>$feature_counter_data,"facility_id"=>$facility_id, 'created_at'=>$created_at,'updated_at'=>$updated_at);
                    DB::table('facility_feature')->insert($level_data);
                //   }
                }
    }

        return redirect()->back();   





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
     $features = Feature::orderBy('updated_at', 'desc')->get();
     $facility = Facility::find($id);     
     return view('Inc.Tab.facilility_edit')->with(array('facility'=>$facility, 'features'=>$features));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $facility = Facility::find($id);
        $facility->name = $request->name;
        $facility->description = $request->description;

        // check if previous and new change matches
        $fileNameToStore =null;
        if($facility->image_name != $request->file){
            $x = 100;
            if($request->hasFile('file')){
                    //get file name
                    $file = $request->file('file');
                    $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
                    $fileData = \Image::make($file);
                    $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                
            }

        }

        if($fileNameToStore){
            $facility->image_name = $fileNameToStore;  
        }

        $facility->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility)
    {
        //
    }
}
