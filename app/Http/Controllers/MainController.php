<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GalleryCategory;
use App\Models\Feature;
use App\Models\Facility;
use App\Models\Activity;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Location;
use App\Models\Image;
use App\Models\House;
use App\Models\Booking;
use App\Models\HouseImage;
use App\Models\ActivityHouse;
use App\Models\FacilityHouse;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\BookingDetail;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Support\Facades\Redirect;
use App\Models\ExchangeRate;
use Illuminate\Support\Facades\URL;

class MainController extends Controller
{
    //


    

    public function save_exchange_rate(Request $request){
$rate = new ExchangeRate();

//  "am here";
$rate->value  = $request->value;
// return $request->value;
$rate->to_currency  = "TZS";
$rate->creator_id = Auth::user()->id; 
$rate->save();
return redirect()->back();
 }


    public function design2(){

        
        
    return Redirect::away("https://luxurybeachvilla.co.tz/luxury_beach_villa2_index");

       // $galleryCategory = GalleryCategory::orderBy('updated_at', 'desc')->get();
        // $gallery = Image::orderBy('updated_at', 'desc')->take(4)->get();
        // $organisation_location = Location::where('isDefault', 1)->first();        
        // $activities = Activity::orderBy('updated_at', 'desc')->take(3)->get();
        // $facility = Facility::orderBy('updated_at', 'desc')->simplePaginate(4);
        // return view('welcome',       
        // [
        //     'facility'=>$facility,
        //     'organisation_location'=>$organisation_location,
        //     'activities' =>  $activities,
        //     'galleryCategory' =>  $galleryCategory,
        //     'gallery' =>  $gallery,
        // ]);    

        // $galleryCategory = GalleryCategory::orderBy('updated_at', 'desc')->get();
        // $gallery = Image::orderBy('updated_at', 'desc')->simplePaginate();
        // return view('gallery',       
        // [    
        //     'galleryCategory' =>  $galleryCategory,        
        //     'gallery' =>  $gallery,
        // ]);

        
        // $today = date("d-m-Y");
        // $from_date = $today;
        // $to_date = date('d-m-Y', strtotime($today . ' +1 day'));
        // $number_of_adult = 5;
        // $number_of_children = 2;
        // $houses = House::orderBy('created_at', 'desc')->simplePaginate();           
        // $organisation_location = Location::where('isDefault', 1)->first();        
        // return view('Imp.index',       
        // [
        //     'number_of_adult'=>$number_of_adult,
        //     'number_of_children'=>$number_of_children,
        //     'from_date'=>$from_date,
        //     'to_date'=>$to_date,
        //     'organisation_location'=>$organisation_location,
        //     'houses' =>  $houses,
        //     'gallery' =>  $gallery,
        // ]);    




}

public function view_reports(){
    $house = House::all();
    $bookings = Booking::orderBy('updated_at', 'desc')->get();

    $current_year = date("y");
    $current_year = "20".$current_year;
    $current_year = intval($current_year);
    $eight_years_ago = intval($current_year - 8);
    
    $monthly_data = [];

    $current_year_house = House::orderBy('updated_at','desc')->get();
    $name_counter = [];  
    if(count($current_year_house)>0){
        foreach($current_year_house as $data){
            //   CheckedInBooking
            if(count($data->SpecialBookingDetail)){
                $name_counter = [];              
                $count = 0;
                $current_year = $current_year;
                $hold_yr = 0;
                $cc = 0;
                foreach($data->SpecialBookingDetail as $res){
                    if(count($name_counter)==8){
                        break;
                    }    
                        if($res->created_at->year == $hold_yr){
                            $cc ++;
                            $name_counter [] = $cc;
                        }else{
                            $name_counter [] = $cc;
                        }
                        $hold_yr = $res->created_at->year;  
                }
            }
              $monthly_data [] = array("name"=>$data->name, "data"=>$name_counter);            
        }
    }
    // return json_encode($monthly_data);

    return view('Inc.Reports.charts')->with(array('bookings'=>$bookings,'monthly_data'=>$monthly_data, 
    'eight_years_ago'=>$eight_years_ago,
                                        'current_year'=>$current_year, 'house'=>$house));
}


public function datatable_reports(){
    $house = House::all();
    $bookings = Booking::orderBy('updated_at', 'desc')->get();
    return view('Inc.Reports.datatable')->with(array('bookings'=>$bookings, 'house'=>$house));
}



    public function cancel_trip_description(Request $request){
        $id = $request->cancelled_booking_id;
        $booking = Booking::find($id);
        $booking->cancellation_reason = $request->cancel_booking_description;
        $booking->save();
        return redirect()->back();
    }

        public function quatation_request(Request $request){

      
            $booking_id = null;
            $house_id = $request->house_id_holder;                       

            $bookings = new Booking;
            $bookings->email = $request["user_email_quot" .$house_id];
            $bookings->phone = $request["phone_quot".$house_id];
            $bookings->code = $request["country_code_quot".$house_id];
            $bookings->arrival_date = $request["check__in_date_quot".$house_id];
            $bookings->departure_date = $request["check__out_date_quot".$house_id];

            $bytes = random_bytes(3);
            $random_hex = bin2hex($bytes);
            $random_hex_checker = Booking::where('booking_code', $random_hex);
            if($random_hex_checker){
                $bytes2 = random_bytes(3);
                $random_hex = bin2hex($bytes2);
            }
            $bookings->booking_code = $random_hex;
            $bookings->save();

            $booking_id = $bookings->id;


            $booking_details = new BookingDetail;
            // $booking_details->title_id = $request["title". $id];
            $booking_details->house_id = $house_id;
            $booking_details->booking_id = $booking_id;
            $booking_details->special_request = $request["message_quot".$house_id];
            $booking_details->first_name = $request["first_name_quot".$house_id];
            $booking_details->last_name = $request["last_name_quot".$house_id];           
            $booking_details->number_of_children = $request["number_of_children_quot".$house_id];
            $booking_details->number_of_adults = $request["number_of_adult_quot".$house_id];
            $booking_details->save();

            
    
            $booking = Booking::find($booking_id);
            if(!empty($booking)){    
                $guests = [];
                if(count($booking->BookingDetail)>0){
                    foreach($booking->BookingDetail as $key=>$data){
                        $real_key = $key + 1;
                        $guests [] = "Room ". $real_key ." -  Category: ".$data->House->name." | Price: " .$data->House->Currency->code . " ". $data->House->cost . " | Adult: ". $data->number_of_adults . " | Children: ". $data->number_of_children;
                    }
                }
                $guests = implode(" , ", $guests);
            }
        
    
        
            // send emails
            $bookings = Booking::find($booking_id);
            $title = $bookings->FirstBookingDetailRecord->title_id;
            $first_name = $bookings->FirstBookingDetailRecord->first_name;
            $last_name = $bookings->FirstBookingDetailRecord->last_name;
            $user_name = $title ." ". $first_name." ". $last_name;
            $nationality = $bookings->code;
            $email = $bookings->email;
            $phone = $bookings->phone;
            $datetime = $bookings->created_at;
    
    
    
            $main_paragraph = "Thanks for booking with us, We're working on your request.";
            //  email to customer
            $data = array('name'=>$user_name, 'booking_id'=>$random_hex, 'guests'=>$guests,
            'title'=>$title, 'first_name'=>$first_name, 'last_name'=>$last_name,
            'email'=>$email, 'phone'=>$phone, 'nationality'=>$nationality,
              'datetime'=> $datetime, 'main_paragraph'=>$main_paragraph
             );
    
            
            Mail::send('mail', $data, function($message) use($email) {
                $message->to($email, $email)->subject
                    ('Thanks for booking with us!.');
                $message->from(env('MAIL_FROM_ADDRESS'),'Luxury Beach Villa');
            });
    
            $user_name = "";
            // email to luxury booking email
            $main_paragraph = "Please reserve and confirm the following request.";       
            $data = array('name'=>$user_name, 'booking_id'=>$random_hex, 'guests'=>$guests,
            'title'=>$title, 'first_name'=>$first_name, 'last_name'=>$last_name,
            'email'=>$email, 'phone'=>$phone, 'nationality'=>$nationality,
              'datetime'=> $datetime, 'main_paragraph'=>$main_paragraph
             );
    
            
            
            Mail::send('mail', $data, function($message) use($email, $user_name) {
                $message->to(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_ADDRESS'))->subject
                    ('Booking Request');
                $message->from(env('MAIL_FROM_ADDRESS'),$user_name);
            });
    
    
    
    
        return redirect('/');


        }


    public function cancel_booking(Request $request){

        $email = $request->cancel_email_reserve;
        $id = $request->cancel_number_reserve;

        $booking = Booking::orderBy('updated_at', 'desc')->where('booking_code', $id)->where('email', $email)->first();
       
       if($booking){
        $booking->status = 1;
        $booking->save();
        $random_hex = $booking->booking_code ;

        
        $title = $booking->FirstBookingDetailRecord->title_id;
        $country_code = $booking->code;
        $phone_number =  $booking->phone;
        $phone = $country_code . " - " . $phone_number;
        $first_name = $booking->FirstBookingDetailRecord->first_name;
        $last_name = $booking->FirstBookingDetailRecord->last_name;
        $user_name = $title ." ". $first_name." ". $last_name;



        $booking_id = $booking->id;
        
        $user_role = "customer";

        $main_paragraph = "Thanks for booking with us, We're working on your request.";
            //  email to customer
            $data = array('user_role'=>$user_role,  'user_name'=>$user_name, 'booking_id'=>$random_hex, 'main_paragraph'=>$main_paragraph,
    
             );
    
            
            Mail::send('cancel_booking_mail', $data, function($message) use($email) {
                $message->to($email, $email)->subject
                    ('Thanks for booking with us!.');
                $message->from(env('MAIL_FROM_ADDRESS'),'Luxury Beach Villa');
            });
    
        $user_role = "admin";
            // email to luxury booking email
            $main_paragraph = "We would like to report a booking cancellation for ".$user_name.", with Reservation Number " . $random_hex . " and Phone Number " . $phone;       
            $data = array('user_role'=>$user_role, 'user_name'=>$user_name,  'booking_id'=>$random_hex, 'main_paragraph'=>$main_paragraph,
    
        );
    
            
            
            Mail::send('cancel_booking_mail', $data, function($message) use($email, $user_name) {
                $message->to(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_ADDRESS'))->subject
                    ('Booking Cancellation');
                $message->from(env('MAIL_FROM_ADDRESS'),$user_name);
            });
    
    

        return response(["message"=>"success", "booking_id"=>$booking_id]);
       } 
       return response(["message"=>"fail"]);


    }

    public function login_checker(Request $request){
        $email = $request->email;
        $coming_password = $request->password;
      
        $user_checker = User::where('email', $email)->first();
        if(!empty($user_checker)){
            // return response(["message"=>$user_checker ]);   

            if(Hash::check($coming_password, $user_checker->password)){
                
                return response(["message"=>"success"]);   
                
            }
            else{
                return response(["message"=>"fail"]);   
    
            }            

        }else{
            return response(["message"=>"fail"]);   

        }




    }
    
    public function store_book_details(Request $request){
    
      
        // store book details
        $arr_holder = $request->holder;
      
        $arr_holder =  explode(" ",$arr_holder);
        $booking_id = null;
        
        if(count($arr_holder)>0){
        foreach($arr_holder as $id){
            $booking_details = BookingDetail::find($id);
            $booking_details->title_id = $request["title". $id];
            $booking_details->first_name = $request["first_name". $id];
            $booking_details->last_name = $request["last_name". $id];           
            $booking_details->special_request = $request["special_request". $id];

            $booking_details->save();
            $booking_id = $booking_details->Booking->id;
        }

        $booking = Booking::find($booking_id);
        if(!empty($booking)){

            $bytes = random_bytes(3);
            $random_hex = bin2hex($bytes);
            $random_hex_checker = Booking::where('booking_code', $random_hex);
            if($random_hex_checker){
                $bytes2 = random_bytes(3);
                $random_hex = bin2hex($bytes2);
            }

            $booking->booking_code = $random_hex;
            $booking->email = $request["booking_email"];
            $booking->phone = $request["phone"];
            $booking->code = $request["country_code"];
            $booking->save();


            $guests = [];
            $price_total = 0;
            $price_code = null;
            if(count($booking->BookingDetail)>0){
                foreach($booking->BookingDetail as $key=>$data){
                    $cost_item = null;
                    $real_key = $key + 1;
                    $price_code = $data->House->Currency->code;
					$price_total = $price_total + $data->House->cost;
                    $cost_item =  $data->House->cost * $data->Booking->number_of_days;
                    $guests [] = "Room ". $real_key ." -  Category: ".$data->House->name." | Price: " .$data->House->Currency->code . " ". $cost_item . " | Adult: ". $data->number_of_adults . " | Children: ". $data->number_of_children;
                }
            }
            $guests = implode(" , ", $guests);
        }
    }

    
        // send emails
        $bookings = Booking::find($booking_id);
        $title = $bookings->FirstBookingDetailRecord->title_id;
        $first_name = $bookings->FirstBookingDetailRecord->first_name;
        $last_name = $bookings->FirstBookingDetailRecord->last_name;
        $user_name = $title ." ". $first_name." ". $last_name;
        $nationality = $bookings->code;
        $cost = $price_total * $bookings->number_of_days;
        $cost_pay = $cost;
        $cost = $price_code ." ".$cost;
        $check_in = $bookings->arrival_date;
        $check_out = $bookings->departure_date;
        $number_of_days = $bookings->number_of_days;
        $email = $bookings->email;
        $phone = $bookings->phone;
        $datetime = $bookings->created_at;

        // dd($cost);
        if($email==null){
            return redirect('/');
       


        $main_paragraph = "Thanks for booking with us, We're working on your request.";
        //  email to customer
        $data = array('name'=>$user_name, 'booking_id'=>$random_hex, 'guests'=>$guests,
        'title'=>$title, 'first_name'=>$first_name, 'last_name'=>$last_name,
        'email'=>$email, 'phone'=>$phone,'number_of_days'=>$number_of_days,'check_in'=>$check_in,'check_out'=>$check_out, 'cost'=>$cost, 'nationality'=>$nationality,
          'datetime'=> $datetime, 'main_paragraph'=>$main_paragraph
         );

        
        Mail::send('mail', $data, function($message) use($email) {
            $message->to($email, $email)->subject
                ('Thanks for booking with us!.');
            $message->from(env('MAIL_FROM_ADDRESS'),'Luxury Beach Villa');
        });

    }
    

    

        $user_name = "";
        // email to luxury booking email
        $main_paragraph = "Please reserve and confirm the following request.";       
        $data = array('name'=>$user_name, 'booking_id'=>$random_hex, 'guests'=>$guests,
        'title'=>$title, 'first_name'=>$first_name, 'last_name'=>$last_name,
        'email'=>$email, 'phone'=>$phone,'number_of_days'=>$number_of_days,'check_in'=>$check_in,'check_out'=>$check_out,'cost'=>$cost, 'nationality'=>$nationality,
          'datetime'=> $datetime, 'main_paragraph'=>$main_paragraph
         );

        
        
        // Mail::send('mail', $data, function($message) use($email, $user_name) {
        //     $message->to(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_ADDRESS'))->subject
        //         ('Booking Request');
        //     $message->from(env('MAIL_FROM_ADDRESS'),$user_name);
        // });


    $full_name = $first_name ." ". $last_name;
    $city = "Dar Es Salam";
    $amount = $cost_pay * ExchangeRate::orderBy('updated_at', 'desc')->first()->value;
    $currency = "TZS";
    $this->pay($email, $phone, $amount, $first_name, $last_name, $full_name, $city, $currency);
    }


    public static function pay($email, $phone, $amount, $first_name, $last_name, $full_name, $city, $currency) {

        //Set your appropirate timezone
        

        // Vendor: TILL60790672
        // API Key: LBV-8Ylj0rQnRhVUkPSa
        // API Secret: P71Yuh62-7l34-7L9T-HY85-h78G1b14527
        // Base URL: https://apigw.selcommobile.com/v1



        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $order_id = '';
        for ($i = 0; $i < 10; $i++) {
            $order_id .= $characters[rand(0, $charactersLength - 1)];
        }



        date_default_timezone_set('Africa/Dar_es_Salaam');

        $api_key = 'LBV-8Ylj0rQnRhVUkPSa';
        $api_secret = 'P71Yuh62-7l34-7L9T-HY85-h78G1b14527';

        $base_url = "https://apigw.selcommobile.com";
        $api_endpoint = "/v1/checkout/create-order";
        $url = $base_url.$api_endpoint;

        
        $req=[
            'vendor'=>	'TILL60790672',	//Mandatory
            'order_id'=> $order_id,	//Mandatory
            'buyer_email'=> $email,	//Mandatory
            'buyer_name'=>$full_name,	//Mandatory
            // 'buyer_userid'=>	'joejohn20',	//Option
            'buyer_phone'=>	$phone,	//Mandatory
            // 'gateway_buyer_uuid'=>	'A1233232',	//Option
            'amount'=>	$amount,	//Mandatory
            'currency'=>	$currency,	//Mandatory
            'payment_methods'=>"ALL",	//Mandatory
            'redirect_url'=> base64_encode('https://luxurybeachvilla.co.tz/payment/done/'.$order_id),	//Optional
            'cancel_url'=> base64_encode('https://luxurybeachvilla.co.tz/payment/cancel/'.$order_id),	//Optional
            'billing.firstname'=>$first_name,	//Mandatorypayment/
            'billing.lastname'=>$last_name,	//Mandatory
            'billing.address_1'=>$city,	//Mandatory
            // 'billing.address_2'=>	'Upanga Area',	//Optional
            'billing.city'=>$city,	//Mandatory
            'billing.state_or_region'=>$city,	//Mandatory
            'billing.postcode_or_pobox'=>	'43434',	//Mandatory
            'billing.country'=>	'TZ',	//Mandatory
            'billing.phone'=>	$phone,//Mandatory
            'header_colour' =>'#ff9500',
            'link_colour' =>'#f65169',
            'button_colour'=>'#f65169',

            'no_of_items'=>	'1',	//Mandatory

        ];


        // $req1 = array("utilityref"=>"12345", "transid"=>"transid", "amount"=>"amount");
        $authorization = base64_encode($api_key);
        $timestamp = date('c'); //2019-02-26T09:30:46+03:00
        $isPost=1;

        $signed_fields  = implode(',', array_keys($req));
        
        $_this = new self;
        
        $digest = $_this->computeSignature($req, $signed_fields, $timestamp, $api_secret);

        $response = $_this->sendJSONPost($url, $isPost, json_encode($req), $authorization, $digest, $signed_fields, $timestamp);


        // echo json_encode([
        //          'success' => true,
        //           'url' => base64_decode($response['data'][0]['payment_gateway_url']),
        // ]);
        
        // echo  base64_decode($response['data'][0]['payment_gateway_url']);
        
        return Redirect::to(base64_decode($response['data'][0]['payment_gateway_url']))->send();
        
        // return "hello";
        // return base64_decode($response['data'][0]['payment_gateway_url']);
          }
        
        
          public function payment_done($id)
          {
            $gallery = Image::orderBy('updated_at', 'desc')->simplePaginate();
            $Booking = Booking::where('transaction_id',$id)->where('payment_status',"0")->first();

            if($Booking){
                Booking::where('transaction_id',$id)->update(['payment_status'=>2]);
                $msg = "Your booking has been confirmed. Check your email for details.";
                $status = "true";
                

                return view('payment_status',compact('gallery','msg','status'));
        
            }
        
            else{
                $msg = "We couldn't make your booking. please try again!";
                $status = "fail";
                return view('payment_status',compact('gallery','msg','status'));
        
            }
        
          }
        
        
        
          public function cancel_payment($id){
        
            $Booking = Booking::where('transaction_id',$id)->where('payment_status',"0")->first();
            if($Booking){
             Booking::where('transaction_id',$id)->update(['payment_status'=>3]);
    

         
            // Auth::loginUsingId($trans->user_id,TRUE);
        
            // return Redirect::to('https://www.pakainfo.com');
            return "payment cancel successfull";
        
            }
        
            else{
                // Auth::loginUsingId($trans->user_id,TRUE);
                return "payment cancel Failed";
        
            }
        
          }
        
        
        
        
        
          public static function sendJSONPost($url, $isPost, $json, $authorization, $digest, $signed_fields, $timestamp) {
            $headers = array(
              "Content-type: application/json;charset=\"utf-8\"", "Accept: application/json", "Cache-Control: no-cache",
              "Authorization: SELCOM $authorization",
              "Digest-Method: HS256",
              "Digest: $digest",
              "Timestamp: $timestamp",
              "Signed-Fields: $signed_fields",
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            if($isPost){
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch,CURLOPT_TIMEOUT,90);
            $result = curl_exec($ch);
            curl_close($ch);
            $resp = json_decode($result, true);
            return $resp;
         }
        
           public static function computeSignature($parameters, $signed_fields, $request_timestamp, $api_secret){
                $fields_order = explode(',', $signed_fields);
                $sign_data = "timestamp=$request_timestamp";
                foreach ($fields_order as $key) {
                $sign_data .= "&$key=".$parameters[$key];
                }
        
        
            //RS256 Signature Method
            #$private_key_pem = openssl_get_privatekey(file_get_contents("path_to_private_key_file"));
            #openssl_sign($sign_data, $signature, $private_key_pem, OPENSSL_ALGO_SHA256);
            #return base64_encode($signature);
        
            //HS256 Signature Method
            return base64_encode(hash_hmac('sha256', $sign_data, $api_secret, true));
         }
        
        
        


    public function booking_details($id){
        $gallery = Image::orderBy('updated_at', 'desc')->simplePaginate();
        $booking = Booking::find($id);
        return view('book')->with(array('gallery'=>$gallery, 'booking'=>$booking));
    }
    public function save_booking(Request $request){
       
    }
    
 public function booking_details_update(Request $request){

    $booking_details_id = $request->booking_details_id;
    $unique_value = $request->unique_value;
    $bookingDetails = BookingDetail::where('unique_value', $unique_value)->first();
    // $bookingDetails->house_id = $request->house_id;
    $bookingDetails->number_of_children = $request->children_id;
    $bookingDetails->save();
    return response(["success"]);
 }
    public function check_user(Request $request){
        return response(["email"=>$request->email]);
    }
    public function homepage(){
   
        $gallery = Image::orderBy('updated_at', 'desc')->simplePaginate();
        // return view('gallery',       
        // [    
        //     'galleryCategory' =>  $galleryCategory,        
        //     'gallery' =>  $gallery,
        // ]);

        
        $today = date("d-m-Y");
        $from_date = $today;
        $to_date = date('d-m-Y', strtotime($today . ' +1 day'));
        $number_of_adult = 5;
        $number_of_children = 2;
        $houses = House::orderBy('created_at', 'desc')->simplePaginate();           
        $organisation_location = Location::where('isDefault', 1)->first();        
        return view('welcome',       
        [
            'number_of_adult'=>$number_of_adult,
            'number_of_children'=>$number_of_children,
            'from_date'=>$from_date,
            'to_date'=>$to_date,
            'organisation_location'=>$organisation_location,
            'houses' =>  $houses,
            'gallery' =>  $gallery,
        ]);    

    }

    
    
    public function map(){
        
        return view('map');
    }


    public function gallery(){
        $galleryCategory = GalleryCategory::orderBy('updated_at', 'desc')->get();
        $gallery = Image::orderBy('updated_at', 'desc')->simplePaginate();
        return view('gallery',       
        [    
            'galleryCategory' =>  $galleryCategory,        
            'gallery' =>  $gallery,
        ]);
    }

    public function contacts(){
        return view('contacts');
    }

    

    public function checking_out_guest($id){
        $book = Booking::find($id);
        $book->status = 2;
        $book->save();
        return redirect()->back();
        // return 'hi';
    }

    public function view_activity($id){
        $activity = Activity::find($id);

        return view('view_activity')->with('activity',$activity);
    }

    public function delete_house(Request $request){
        $house_id  = $request->house_id;
        $house = House::find($house_id);
            if ($house != null ){
                // delete house
                $house->delete();

                // delete images
                $house_image = HouseImage::where('house_id', $house_id)->get();
                if(count($house_image)>0){
                    foreach($house_image as $house_image_data){
                        $house_image_data->delete();
                    }
                }

                // delete activities
                $take_previous_activities = ActivityHouse::where('house_id', $house_id)->get();
                if(count($take_previous_activities)>0){
                    foreach($take_previous_activities as $take_previous_activities_data){
                        $take_previous_activities_data->delete();
                    }
                }

                // delete facility
                

                $take_previous_facility = FacilityHouse::where('house_id', $house_id)->get();
                if(count($take_previous_facility)>0){
                    foreach($take_previous_facility as $take_previous_facility_data){
                        $take_previous_facility_data->delete();
                    }
                }


                return response(['message'=>'success']);
            }
            // else
            // {
            //    return response([]);
            // }
       
    }


    public function delete_image(Request $request){
        $image_id = $request->image_id;
        $image = Image::find($image_id);
        if ($image != null ){
            // delete house
            $image->delete();
            return response(['message'=>'success']);
        }
            return response(['message'=>'fail']);

    }
    public function book($house_id){
        $organisation_location = Location::where('isDefault', 1)->first();        
        $house = House::find($house_id);           
        return view('book',       
        [
            'organisation_location'=>$organisation_location,
            'house' =>  $house,
        ]);
    }


    public function booked(){
        // $select_unreview_bookings = Booking::where('status',0)->orderBy('created_at', 'desc')->simplePaginate();           
        $booked = Booking::orderBy('created_at', 'desc')->where('status',0)->simplePaginate();           
        $houses = House::orderBy('created_at', 'desc')->get();           
        $gallery  = Image ::orderBy('updated_at', 'desc')->get();        
        return view('Inc.booked',       
        [
            'gallery' =>  $gallery,
            'booked' =>  $booked,
            'houses' =>  $houses,
        ]);
    }

    public function check_in(){
        $checkin = Booking::orderBy('created_at', 'desc')->where('status', 1)->simplePaginate();           
        return view('Inc.check_in',       
        [
            'checkin' =>  $checkin,
        ]);
    }

    
    public function check_out(){
        $check_out = Booking::orderBy('created_at', 'desc')->where('status', 2)->simplePaginate();           
        return view('Inc.check_out',       
        [
            'check_out' =>  $check_out,
        ]);
    }

    
    public function users(){
        $users = User::all();
        return view('Inc.users')->with(array('users'=>$users));
    }


    public function content_manager(){
        $galleryCategory = GalleryCategory::orderBy('updated_at', 'desc')->get();
        $features = Feature::orderBy('updated_at', 'desc')->simplePaginate();
        $facilities = Facility::orderBy('updated_at', 'desc')->simplePaginate();
        $activities = Activity::orderBy('updated_at', 'desc')->simplePaginate();        
        $categories = Category::orderBy('updated_at', 'desc')->get();        
        $gallery  = Image ::orderBy('updated_at', 'desc')->get();        
        $currencies = Currency::orderBy('updated_at', 'desc')->get();        
        $organisation_location = Location::where('isDefault', 1)->first();        
        $houses = House::orderBy('created_at', 'desc')->simplePaginate();           
     
        return view('Inc.content_manager',       
        [
            'gallery'=>  $gallery,
            'houses'=>  $houses,
            'organisation_location'=>  $organisation_location,
            'currencies'=>  $currencies,
            'categories' =>  $categories,
            'activities' =>  $activities,
            'galleryCategory' =>  $galleryCategory,
            'facilities' =>  $facilities,
            'features' =>  $features
            ]
    );
    }

    public function check_available_post_func(Request $request){

        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $number_of_adult = $request->number_of_adult;
        $number_of_children = $request->number_of_children;
        $houses = House::orderBy('created_at', 'desc')->where('max_number_of_children','>=', $number_of_children)->where('max_number_of_adults','>=', $number_of_adult)->simplePaginate();           
        $organisation_location = Location::where('isDefault', 1)->first();        
        return view('view_available',       
        [
            'number_of_adult'=>$number_of_adult,
            'number_of_children'=>$number_of_children,
            'from_date'=>$from_date,
            'to_date'=>$to_date,
            'organisation_location'=>$organisation_location,
            'houses' =>  $houses,
        ]);
    }

    
    public function check_available(){
        $today = date("d-m-Y");
        $from_date = $today;
        $to_date = date('d-m-Y', strtotime($today . ' +1 day'));
        $number_of_adult = 5;
        $number_of_children = 2;
        $houses = House::orderBy('created_at', 'desc')->simplePaginate();           
        $organisation_location = Location::where('isDefault', 1)->first();        
        return view('view_available',       
        [
            'number_of_adult'=>$number_of_adult,
            'number_of_children'=>$number_of_children,
            'from_date'=>$from_date,
            'to_date'=>$to_date,
            'organisation_location'=>$organisation_location,
            'houses' =>  $houses,
        ]);

    }




}
