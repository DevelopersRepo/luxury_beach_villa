<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $booking_count = Booking::select('id')->where('email', '!=', null)->where('status', 0)->count();           
        $check_in_count = Booking::select('id')->where('email', '!=', null)->where('status', 1)->count();           
        $check_out_count = Booking::select('id')->where('email', '!=', null)->where('status', 2)->count();           
        return view('home',       
        [
            'booking_count' =>  $booking_count,
            'check_in_count' =>  $check_in_count,
            'check_out_count' =>  $check_out_count,
        ]);


    }

    function RateStore()
    {
        
    }
}
