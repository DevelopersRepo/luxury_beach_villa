<?php

namespace App\Http\Controllers;

use App\Models\House;
use App\Models\HouseImage;
use App\Models\Facility;
use App\Models\Feature;
use App\Models\Location;
use App\Models\Activity;
use App\Models\Category;
use App\Models\Currency;
use App\Models\ActivityHouse;
use App\Models\FacilityHouse;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $house = new House;
        $house->name = $request->name;        
        $house->cost = $request->cost;        
        $house->currency_id = $request->currency_id;   
        $house->Price_reference = $request->Price_reference;        
        $house->number_of_rooms = $request->number_of_rooms;        
        $house->max_number_of_children = $request->max_number_of_children;        
        $house->max_number_of_adults = $request->max_number_of_adults;        

        $location  = $request->location;
        $locationCheck = Location::where('name', $location)->first();
        if($locationCheck){
            $location_id = $locationCheck->id;
        }else{
            $newLoc = new Location;
            $newLoc->name = $location;
            $newLoc->save();
            $location_id = $newLoc->id;
        }
        $house->location_id = $location_id;        
        $house->max_number_of_people = $house->max_number_of_children + $house->max_number_of_adults;
        $house->category_id = $request->category_id;        













        $house->isAvailable = $request->isAvailable;      
        $house->created_by = auth()->user()->id;
        if($request->input('description')){
            $house->description = $request->input('description');
        }
        $house->save();

        
        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $house_id = $house->id;
       
        $x = 10;
        if($request->hasFile('file')){
            
            $file = $request->file('file');
            $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
            $fileData = \Image::make($file);
            $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                
  
            $image = new HouseImage;
            $image->name = $fileNameToStore;    
            $image->house_id = $house_id;
            $image->save();

        }



        $take_previous_activities = ActivityHouse::where('house_id', $house_id)->get();
        if(count($take_previous_activities)>0){
            foreach($take_previous_activities as $take_previous_activities_data){
                $take_previous_activities_data->delete();
            }
        }

        $activity_count = $request['activity']; 
        if(count($activity_count)>0){
            foreach($activity_count as $activity) {
            if($activity){
                $activity_data=array('activity_id'=>$activity,"house_id"=>$house_id, 'created_at'=>$created_at,'updated_at'=>$updated_at);
                DB::table('activity_house')->insert($activity_data);
            } 
          }           
        }


        
        $take_previous_facility = FacilityHouse::where('house_id', $house_id)->get();
        if(count($take_previous_facility)>0){
            foreach($take_previous_facility as $take_previous_facility_data){
                $take_previous_facility_data->delete();
            }
        }



        $facility_count = $request['facility']; 
        if(count($facility_count)>0){
          foreach($facility_count as $facility) {
            if($facility){
                $facility_data=array('facility_id'=>$facility,"house_id"=>$house_id, 'created_at'=>$created_at,'updated_at'=>$updated_at);
                DB::table('facility_house')->insert($facility_data);
            }
        }
            
        }



        return redirect()->back();   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        
        $categories = Category::orderBy('updated_at', 'desc')->get();
        $features = Feature::orderBy('updated_at', 'desc')->get();
        $facilities = Facility::orderBy('updated_at', 'desc')->get();     
        $activities = Activity::orderBy('updated_at', 'desc')->get();     
        $currencies = Currency::orderBy('updated_at', 'desc')->get();        
        $organisation_location = Location::where('isDefault', 1)->first();        
        $house = House::find($id);     
        return view('Inc.Tab.house_edit')->with(array('house'=>$house,'currencies'=>$currencies,'organisation_location'=>$organisation_location,'categories'=>$categories, 'activities'=>$activities,'facilities'=>$facilities, 'features'=>$features));
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $house = House::find($id);
        $house->name = $request->name;        
        $house->cost = $request->cost;        
        $house->currency_id = $request->currency_id;   
        $house->Price_reference = $request->Price_reference;        
        $house->number_of_rooms = $request->number_of_rooms;        
        $house->max_number_of_children = $request->max_number_of_children;        
        $house->max_number_of_adults = $request->max_number_of_adults;        

        $location  = $request->location;
        $locationCheck = Location::where('name', $location)->first();
        if($locationCheck){
            $location_id = $locationCheck->id;
        }else{
            $newLoc = new Location;
            $newLoc->name = $location;
            $newLoc->save();
            $location_id = $newLoc->id;
        }
        $house->location_id = $location_id;        
        $house->max_number_of_people = $house->max_number_of_children + $house->max_number_of_adults;
        $house->category_id = $request->category_id;        
        $house->description = $request->description;        

        if($request->isAvailable){
            $house->isAvailable = $request->isAvailable;  
        }

        $house->created_by = auth()->user()->id;
        $house->save();

        
        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $house_id = $house->id;
       
        $x = 20;

        if($request->hasFile('file')){
            
            $file = $request->file('file');
            $fileNameToStore = time() . "_" . $file->getClientOriginalName();          
            $fileData = \Image::make($file);
            $fileData->save(storage_path('app/public/uploads/'.$fileNameToStore), $x);                
  
            $image = new HouseImage;
            $image->name = $fileNameToStore;    
            $image->house_id = $house_id;
            $image->save();

        }

        $take_previous_activities = ActivityHouse::where('house_id', $id)->get();
        if(count($take_previous_activities)>0){
            foreach($take_previous_activities as $take_previous_activities_data){
                $take_previous_activities_data->delete();
            }
        }

        $activity_count = $request['activity']; 
        if(!empty($activity_count)){
            foreach($activity_count as $activity){
                if($activity){
                    $activity_data = array('activity_id'=>$activity,"house_id"=>$house_id, 'created_at'=>$created_at,'updated_at'=>$updated_at);
                    DB::table('activity_house')->insert($activity_data);
                }            
            }
        }




        

        $take_previous_facility = FacilityHouse::where('house_id', $id)->get();
        if(count($take_previous_facility)>0){
            foreach($take_previous_facility as $take_previous_facility_data){
                $take_previous_facility_data->delete();
            }
        }



        $facility_count = $request['facility']; 
        if(count($facility_count)>0){
            foreach($facility_count as $facility) {
                if($facility){
                    $facility_data=array('facility_id'=>$facility,"house_id"=>$house_id, 'created_at'=>$created_at,'updated_at'=>$updated_at);
                    DB::table('facility_house')->insert($facility_data);
                }
                
            }
        }



        return redirect()->back();   
   


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {
        //
    }
}
