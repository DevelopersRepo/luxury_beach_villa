-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 11, 2022 at 12:05 PM
-- Server version: 10.5.16-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `luxurybeachvilla_luxurybeachvilla_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `front_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `front_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `title`, `created_by`, `front_file`, `front_description`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ISLAND TRIP', 1, '1647852097_island-trip.jpg', 'You\'ll enjoy clear beach, turquoise-blue water, shadow sandbars perfect for wading etc', '<p>You\'ll enjoy clear beach, turquoise-blue water, shadow sandbars perfect for wading etc</p>', NULL, '2022-03-21 05:41:38', '2022-03-21 05:41:38'),
(2, 'CLEANING BEACH', 1, '1647852131_clean-beach.jpg', 'Let\'s maintain the health and beauty of Dar beaches all over the year.', '<p>Let\'s maintain the health and beauty of Dar beaches all over the year.</p>', NULL, '2022-03-21 05:42:11', '2022-03-21 05:42:11'),
(3, 'CITY TOUR', 1, '1647852154_city_tour.jpg', 'Learn about Dar Es Salaam past and present with this guided tour of the Tanzanian capital.', '<p>Learn about Dar Es Salaam past and present with this guided tour of the Tanzanian capital.</p>', NULL, '2022-03-21 05:42:35', '2022-03-21 05:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `activity_facility`
--

CREATE TABLE `activity_facility` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `activity_house`
--

CREATE TABLE `activity_house` (
  `id` int(10) UNSIGNED NOT NULL,
  `activity_id` bigint(20) NOT NULL,
  `house_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_house`
--

INSERT INTO `activity_house` (`id`, `activity_id`, `house_id`, `created_at`, `updated_at`) VALUES
(52, 3, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `activity_images`
--

CREATE TABLE `activity_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `departure_date` date DEFAULT NULL,
  `payment_type` enum('online','cash') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cash',
  `number_of_people` int(11) DEFAULT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_expiration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vcc_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isReviewed` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT 0,
  `reviewed_by` bigint(20) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `special_request` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `require_pick_up` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pick_up_date_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flight_details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancellation_reason` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `email`, `phone`, `arrival_date`, `number_of_days`, `departure_date`, `payment_type`, `number_of_people`, `card_number`, `card_expiration`, `vcc_number`, `isReviewed`, `status`, `reviewed_by`, `code`, `booking_code`, `special_request`, `require_pick_up`, `pick_up_date_time`, `flight_details`, `cancellation_reason`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '2022-05-06', NULL, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-06 14:09:14', '2022-05-06 14:09:14'),
(2, NULL, NULL, '2022-05-06', NULL, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-06 14:09:55', '2022-05-06 14:09:55'),
(3, NULL, NULL, '2022-05-06', NULL, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-06 14:16:54', '2022-05-06 14:16:54'),
(4, NULL, NULL, '2022-05-06', 10, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-06 15:05:36', '2022-05-06 15:05:36'),
(5, 'onassiskenan@gmail.com', '63468694', '2022-05-06', 10, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+93|Afghanistan', 'bc921d', NULL, NULL, NULL, NULL, NULL, '2022-05-06 15:08:33', '2022-05-09 12:37:37'),
(6, 'onassiskenan@gmail.com', '063468693', '2022-05-06', 10, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+672 1x|Australian Antarctic Territory', 'e8273a', NULL, NULL, NULL, NULL, NULL, '2022-05-06 15:38:04', '2022-05-09 12:36:37'),
(7, NULL, NULL, '2022-05-06', 1, '2022-05-07', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-06 16:28:36', '2022-05-06 16:28:36'),
(8, 'onassiskenan@gmail.com', '063468693', '2022-05-06', 10, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+43|Austria', 'f2672d', NULL, NULL, NULL, NULL, NULL, '2022-05-06 16:32:27', '2022-05-09 12:36:53'),
(9, 'onassiskenan@gmail.com', '063468693', '2022-05-06', 10, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+994|Azerbaijan', 'd6d3a5', NULL, NULL, NULL, NULL, NULL, '2022-05-06 16:33:44', '2022-05-09 12:33:38'),
(10, NULL, NULL, '2022-05-19', 8, '2022-05-27', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-09 12:07:43', '2022-05-09 12:07:43'),
(11, NULL, NULL, '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-09 12:17:04', '2022-05-09 12:17:04'),
(12, 'onassiskenan@gmail.com', '063468693', '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+995 44 +7 840, 940|Abkhazia', 'c67afa', NULL, NULL, NULL, NULL, NULL, '2022-05-09 12:38:02', '2022-05-09 12:44:17'),
(13, 'onassiskenan@gmail.com', '063468693', '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+994|Azerbaijan', 'dbe713', NULL, NULL, NULL, NULL, NULL, '2022-05-09 12:44:55', '2022-05-09 12:50:28'),
(14, 'onassiskenan@gmail.com', '063468693', '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+994|Azerbaijan', '3e6101', NULL, NULL, NULL, NULL, NULL, '2022-05-09 12:51:41', '2022-05-09 12:53:31'),
(15, 'onassiskenan@gmail.com', '063468693', '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+43|Austria', '06401d', NULL, NULL, NULL, NULL, NULL, '2022-05-09 12:53:55', '2022-05-09 16:26:30'),
(16, 'onassiskenan@gmail.com', '063468693', '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 2, NULL, '+93|Afghanistan', 'ba71d2', NULL, NULL, NULL, NULL, NULL, '2022-05-09 13:19:42', '2022-05-09 13:23:11'),
(17, NULL, NULL, '2022-05-09', 1, '2022-05-10', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-09 16:35:02', '2022-05-09 16:35:02'),
(18, 'sangahekela98@gmail.com', '0659520807', '2022-05-10', 1, '2022-05-11', 'cash', NULL, NULL, NULL, NULL, '0', 1, NULL, '+255|Tanzania', 'b5e868', NULL, NULL, NULL, NULL, NULL, '2022-05-09 16:36:47', '2022-05-11 22:34:14'),
(19, NULL, NULL, '2022-05-10', 6, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-10 12:29:28', '2022-05-10 12:29:28'),
(20, 'onassiskenan@gmail.com', '063468693', '2022-05-10', 6, '2022-05-16', 'cash', NULL, NULL, NULL, NULL, '0', 1, NULL, '+255|Tanzania', '26e2b4', NULL, NULL, NULL, NULL, NULL, '2022-05-10 12:30:10', '2022-05-10 12:45:39'),
(21, 'asme.gebre@gmail.com', '47599256947', '2022-05-12', 10, '2022-05-22', 'cash', NULL, NULL, NULL, NULL, '0', 1, NULL, '+47|Norway', 'f2e098', NULL, NULL, NULL, NULL, NULL, '2022-05-11 22:25:59', '2022-05-11 22:33:47'),
(22, 'teklay@htc.co.tz', '00255756883598', '2022-05-20', 10, '2022-05-30', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', 'b2603a', NULL, NULL, NULL, NULL, NULL, '2022-05-18 09:48:36', '2022-05-18 09:53:37'),
(23, NULL, NULL, '2022-05-20', 1, '2022-05-21', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-20 15:43:40', '2022-05-20 15:43:40'),
(24, NULL, NULL, '2022-06-02', 1, '2022-06-03', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-06-02 11:50:17', '2022-06-02 11:50:17'),
(25, NULL, '0711750551', '2022-06-02', 1, '2022-06-03', 'cash', NULL, NULL, NULL, NULL, '0', 1, NULL, '+43|Austria', 'b35616', NULL, NULL, NULL, NULL, NULL, '2022-06-02 11:50:38', '2022-06-02 11:56:40'),
(26, NULL, '0711750551', '2022-06-02', 1, '2022-06-03', 'cash', NULL, NULL, NULL, NULL, '0', 1, NULL, '+255|Tanzania', 'bbfc10', NULL, NULL, NULL, NULL, NULL, '2022-06-02 11:56:24', '2022-06-02 12:05:57'),
(27, NULL, NULL, '2022-07-27', 1, '2022-07-28', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-27 16:03:03', '2022-07-27 16:03:03'),
(28, 'laison.marko@cits.co.tz', '757252339', '2022-07-27', 1, '2022-07-28', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '146a84', NULL, NULL, NULL, NULL, NULL, '2022-07-27 16:04:06', '2022-07-27 16:52:29'),
(29, 'martinkaboja@gmail.com', '710426568', '2022-07-29', 1, '2022-07-30', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '606886', NULL, NULL, NULL, NULL, NULL, '2022-07-29 13:14:49', '2022-07-29 13:15:32'),
(30, NULL, NULL, '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-01 11:31:25', '2022-08-01 11:31:25'),
(31, 'martinkaboja@gmail.com', '710426568', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '797245', NULL, NULL, NULL, NULL, NULL, '2022-08-01 11:39:28', '2022-08-01 11:40:07'),
(32, 'martinkaboja@gmail.com', '710426568', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '4851a2', NULL, NULL, NULL, NULL, NULL, '2022-08-01 11:44:28', '2022-08-01 11:45:05'),
(33, 'martinkaboja@gmail.com', '071042656', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '6a03a0', NULL, NULL, NULL, NULL, NULL, '2022-08-01 12:21:07', '2022-08-01 12:42:40'),
(34, 'martinkaboja@gmail.com', '710426568', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '29cb1a', NULL, NULL, NULL, NULL, NULL, '2022-08-01 12:42:57', '2022-08-01 12:43:25'),
(35, 'martinkaboja@gmail.com', '71042656', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '2e65ab', NULL, NULL, NULL, NULL, NULL, '2022-08-01 12:44:08', '2022-08-01 12:50:00'),
(36, 'martinkaboja@gmail.com', '0710426568', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', 'd86e86', NULL, NULL, NULL, NULL, NULL, '2022-08-01 12:53:55', '2022-08-01 14:51:35'),
(37, 'martinkaboja@gmail.com', '710426568', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', 'a057c2', NULL, NULL, NULL, NULL, NULL, '2022-08-01 15:55:10', '2022-08-01 15:55:41'),
(38, NULL, NULL, '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-01 15:58:02', '2022-08-01 15:58:02'),
(39, 'martinkaboja@gmail.com', '0710426568', '2022-08-01', 1, '2022-08-02', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '423458', NULL, NULL, NULL, NULL, NULL, '2022-08-01 15:59:52', '2022-08-01 16:02:13'),
(40, 'martinkaboja@gmail.com', '0710426568', '2022-08-04', 1, '2022-08-05', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+355|Albania', 'b75b1f', NULL, NULL, NULL, NULL, NULL, '2022-08-04 11:04:17', '2022-08-04 11:04:36'),
(41, 'lizabellekaboja@gmail.com', '672426113', '2022-08-04', 8, '2022-08-12', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', 'cfd141', NULL, NULL, NULL, NULL, NULL, '2022-08-04 11:35:31', '2022-08-04 11:36:23'),
(42, 'onassiskenan@gmail.com', '683468699', '2022-08-04', 1, '2022-08-05', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, '+255|Tanzania', '1e997f', NULL, NULL, NULL, NULL, NULL, '2022-08-04 13:08:20', '2022-08-04 13:10:14'),
(43, NULL, NULL, '2022-08-11', 1, '2022-08-12', 'cash', NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-08-10 12:33:27', '2022-08-10 12:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `booking_details`
--

CREATE TABLE `booking_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` bigint(20) NOT NULL,
  `special_request` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_id` bigint(20) DEFAULT NULL,
  `title_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_of_children` int(11) DEFAULT NULL,
  `number_of_adults` int(11) DEFAULT NULL,
  `unique_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking_details`
--

INSERT INTO `booking_details` (`id`, `booking_id`, `special_request`, `house_id`, `title_id`, `first_name`, `last_name`, `phone`, `email`, `number_of_children`, `number_of_adults`, `unique_value`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856953872', '2022-05-06 14:09:14', '2022-05-06 14:09:14'),
(2, 1, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856953872', '2022-05-06 14:09:14', '2022-05-06 14:09:14'),
(3, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856953872', '2022-05-06 14:09:14', '2022-05-06 14:09:14'),
(4, 2, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856995590', '2022-05-06 14:09:56', '2022-05-06 14:09:56'),
(5, 2, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856995590', '2022-05-06 14:09:56', '2022-05-06 14:09:56'),
(6, 2, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856995590', '2022-05-06 14:09:56', '2022-05-06 14:09:56'),
(7, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651856995590', '2022-05-06 14:09:56', '2022-05-06 14:09:56'),
(8, 3, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651857414422', '2022-05-06 14:16:54', '2022-05-06 14:16:54'),
(9, 3, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651857414422', '2022-05-06 14:16:54', '2022-05-06 14:16:54'),
(10, 3, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651857414422', '2022-05-06 14:16:54', '2022-05-06 14:16:54'),
(11, 4, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651860324558', '2022-05-06 15:05:36', '2022-05-06 15:05:36'),
(12, 5, NULL, 5, 'Mrs', 'vdfv', 'fdvfd', NULL, NULL, 0, 2, '1651860512951', '2022-05-06 15:08:33', '2022-05-06 15:12:32'),
(13, 5, NULL, 5, 'Mrs', 'vfdvdf', 'vdf', NULL, NULL, 0, 2, '1651860512951', '2022-05-06 15:08:33', '2022-05-06 15:12:32'),
(14, 5, NULL, 2, 'Mr', 'vdfv', 'dfvfd', NULL, NULL, 0, 2, '1651860512951', '2022-05-06 15:08:33', '2022-05-06 15:12:32'),
(15, 5, NULL, 2, 'Ms', 'vdf', 'vdfvfd', NULL, NULL, 0, 2, '1651860512951', '2022-05-06 15:08:33', '2022-05-06 15:12:32'),
(16, 6, NULL, 5, 'Mrs', 'dfg', 'dfgdf', NULL, NULL, 0, 2, '1651862284354', '2022-05-06 15:38:04', '2022-05-06 16:27:25'),
(17, 6, NULL, 5, 'Mrs', 'gfdg', 'fdgdf', NULL, NULL, 0, 2, '1651862284354', '2022-05-06 15:38:04', '2022-05-06 16:27:25'),
(18, 6, NULL, 2, 'Mrs', 'gfdg', 'dfgdf', NULL, NULL, 0, 2, '1651862284354', '2022-05-06 15:38:04', '2022-05-06 16:27:25'),
(19, 7, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651865316089', '2022-05-06 16:28:36', '2022-05-06 16:28:36'),
(20, 7, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651865316089', '2022-05-06 16:28:36', '2022-05-06 16:28:36'),
(21, 7, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651865316089', '2022-05-06 16:28:36', '2022-05-06 16:28:36'),
(22, 7, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1651865316089', '2022-05-06 16:28:36', '2022-05-06 16:28:36'),
(23, 8, NULL, 5, 'Mr', 'sdfsf', 'sffs', NULL, NULL, 0, 2, '1651865547474', '2022-05-06 16:32:27', '2022-05-06 16:33:04'),
(24, 8, NULL, 5, 'Mr', 'fsdf', 'dfd', NULL, NULL, 0, 2, '1651865547474', '2022-05-06 16:32:27', '2022-05-06 16:33:04'),
(25, 8, NULL, 2, 'Dr', 'sdfs', 'fsdfsd', NULL, NULL, 7, 2, '1651865547474', '2022-05-06 16:32:27', '2022-05-06 16:33:04'),
(26, 9, NULL, 2, 'Sr', 'we', 'dwedw', NULL, NULL, 2, 2, '1651865623897', '2022-05-06 16:33:44', '2022-05-06 16:34:29'),
(27, 9, NULL, 2, 'Mr', 'dwe', 'edwe', NULL, NULL, 3, 2, '1651865623897', '2022-05-06 16:33:44', '2022-05-06 16:34:29'),
(28, 9, NULL, 1, 'Dr', 'dwed', 'wedw', NULL, NULL, 0, 2, '1651865623897', '2022-05-06 16:33:44', '2022-05-06 16:34:29'),
(29, 10, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1652098062528', '2022-05-09 12:07:43', '2022-05-09 12:07:43'),
(30, 10, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1652098062528', '2022-05-09 12:07:43', '2022-05-09 12:07:43'),
(31, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, 7, 2, '1652098062528', '2022-05-09 12:07:44', '2022-05-09 12:07:44'),
(32, 11, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, 2, '1652098624433', '2022-05-09 12:17:04', '2022-05-09 12:17:04'),
(33, 12, NULL, 5, 'Mr', 'sd', 'fsd', NULL, NULL, 2, 2, '1652099881404', '2022-05-09 12:38:02', '2022-05-09 12:38:29'),
(34, 12, NULL, 5, 'Mrs', 'fdsf', 'sdf', NULL, NULL, 1, 2, '1652099881404', '2022-05-09 12:38:02', '2022-05-09 12:38:29'),
(35, 12, NULL, 2, 'Mr', 'fdsfsd', 'fsdf', NULL, NULL, 0, 2, '1652099881404', '2022-05-09 12:38:02', '2022-05-09 12:38:29'),
(36, 13, NULL, 5, 'Mrs', 'fsdf', 'dsf', NULL, NULL, 3, 2, '1652100294606', '2022-05-09 12:44:55', '2022-05-09 12:45:17'),
(37, 13, NULL, 5, 'Mr', 'fsdf', 'dsfsd', NULL, NULL, 1, 2, '1652100294606', '2022-05-09 12:44:55', '2022-05-09 12:45:17'),
(38, 13, NULL, 2, 'Mam', 'fsdf', 'sdf', NULL, NULL, 0, 2, '1652100294606', '2022-05-09 12:44:55', '2022-05-09 12:45:17'),
(39, 14, NULL, 5, 'Ms', 'dfvdf', 'vdfvd', NULL, NULL, 6, 2, '1652100700735', '2022-05-09 12:51:41', '2022-05-09 12:52:06'),
(40, 14, NULL, 5, 'Mr', 'dfvdfv', 'dfvdf', NULL, NULL, 2, 2, '1652100700735', '2022-05-09 12:51:41', '2022-05-09 12:52:06'),
(41, 14, NULL, 2, 'Dr', 'dfvd', 'fvdf', NULL, NULL, 9, 2, '1652100700735', '2022-05-09 12:51:41', '2022-05-09 12:52:06'),
(42, 15, NULL, 5, 'Mrs', 'er', 'gffe', NULL, NULL, 8, 2, '1652100834942', '2022-05-09 12:53:55', '2022-05-09 12:54:22'),
(43, 15, NULL, 5, 'Mr', 'ref', 'erfer', NULL, NULL, 0, 2, '1652100834942', '2022-05-09 12:53:55', '2022-05-09 12:54:22'),
(44, 15, NULL, 2, 'Ms', 'ref', 'rf', NULL, NULL, 0, 2, '1652100834942', '2022-05-09 12:53:55', '2022-05-09 12:54:22'),
(45, 16, NULL, 5, 'Mr', 'fsd', 'f', NULL, NULL, 0, 2, '1652102381846', '2022-05-09 13:19:42', '2022-05-09 13:20:16'),
(46, 16, NULL, 5, 'Sir', 'f', 'fsd', NULL, NULL, 0, 2, '1652102381846', '2022-05-09 13:19:42', '2022-05-09 13:20:16'),
(47, 16, NULL, 5, 'Mrs', 'sdfs', 'df', NULL, NULL, 0, 2, '1652102381846', '2022-05-09 13:19:42', '2022-05-09 13:20:16'),
(48, 16, NULL, 5, 'Ms', 'f', 'sdfsd', NULL, NULL, 0, 2, '1652102381846', '2022-05-09 13:19:42', '2022-05-09 13:20:16'),
(49, 16, NULL, 5, 'Sr', 'sdfs', 'fsdfsdf', NULL, NULL, 0, 2, '1652102381846', '2022-05-09 13:19:42', '2022-05-09 13:20:16'),
(50, 16, NULL, 5, 'Mr', 'sdf', 'fsdf', NULL, NULL, 0, 2, '1652102381846', '2022-05-09 13:19:42', '2022-05-09 13:20:16'),
(51, 17, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1652103300896', '2022-05-09 16:35:02', '2022-05-09 16:35:02'),
(52, 18, NULL, 5, 'Ms', 'Joyner', 'Sanga', NULL, NULL, 0, 2, '1652103405863', '2022-05-09 16:36:47', '2022-05-09 16:37:46'),
(53, 19, NULL, 5, NULL, NULL, NULL, NULL, NULL, 2, 3, '1652174966664', '2022-05-10 12:29:28', '2022-05-10 12:29:28'),
(54, 19, NULL, 5, NULL, NULL, NULL, NULL, NULL, 2, 3, '1652174966664', '2022-05-10 12:29:28', '2022-05-10 12:29:28'),
(55, 19, NULL, 2, NULL, NULL, NULL, NULL, NULL, 1, 3, '1652174966664', '2022-05-10 12:29:28', '2022-05-10 12:29:28'),
(56, 20, NULL, 5, 'Mr', 'fdfd', 'fdfd', NULL, NULL, 2, 3, '1652175008164', '2022-05-10 12:30:10', '2022-05-10 12:31:01'),
(57, 20, NULL, 5, 'Mr', 'fdfd', 'dfdf', NULL, NULL, 2, 3, '1652175008164', '2022-05-10 12:30:10', '2022-05-10 12:31:01'),
(58, 20, NULL, 2, 'Mrs', 'fdfd', 'fdf', NULL, NULL, 2, 3, '1652175008164', '2022-05-10 12:30:10', '2022-05-10 12:31:01'),
(59, 21, NULL, 5, 'Mr', 'Teklay', 'Gezahagn', NULL, NULL, 1, 2, '1652297159971', '2022-05-11 22:25:59', '2022-05-11 22:29:27'),
(60, 21, NULL, 2, 'Mr', 'Asmelash', 'Abraha', NULL, NULL, 1, 2, '1652297159971', '2022-05-11 22:25:59', '2022-05-11 22:29:27'),
(61, 21, NULL, 1, 'Mr', 'Isayas', 'Tekel', NULL, NULL, 1, 2, '1652297159971', '2022-05-11 22:25:59', '2022-05-11 22:29:27'),
(62, 22, NULL, 5, 'Mr', 'Teklay', 'Gezahagn', NULL, NULL, 0, 2, '1652856516282', '2022-05-18 09:48:37', '2022-05-18 09:53:37'),
(63, 22, NULL, 1, 'Mr', 'Samuel', 'Mesfin', NULL, NULL, 0, 2, '1652856516282', '2022-05-18 09:48:37', '2022-05-18 09:53:37'),
(64, 23, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1653050619122', '2022-05-20 15:43:40', '2022-05-20 15:43:40'),
(65, 23, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1653050619122', '2022-05-20 15:43:40', '2022-05-20 15:43:40'),
(66, 24, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1654159816535', '2022-06-02 11:50:17', '2022-06-02 11:50:17'),
(67, 24, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1654159816535', '2022-06-02 11:50:17', '2022-06-02 11:50:17'),
(68, 25, NULL, 5, 'Mr', 'Juma', 'Ally', NULL, NULL, 0, 2, '1654159836999', '2022-06-02 11:50:38', '2022-06-02 11:52:08'),
(69, 25, NULL, 5, 'Mr', 'Hussein', 'Mohamedi', NULL, NULL, 0, 2, '1654159836999', '2022-06-02 11:50:38', '2022-06-02 11:52:08'),
(70, 25, NULL, 2, 'Mrs', 'Lukia', 'Mbaga', NULL, NULL, 0, 2, '1654159836999', '2022-06-02 11:50:38', '2022-06-02 11:52:08'),
(71, 25, NULL, 2, 'Mr', 'Martin', 'Haule', NULL, NULL, 0, 2, '1654159836999', '2022-06-02 11:50:38', '2022-06-02 11:52:08'),
(72, 26, NULL, 2, 'Mr', 'Haule juma', 'Mwamri', NULL, NULL, 0, 2, '1654160184046', '2022-06-02 11:56:25', '2022-06-02 12:04:27'),
(73, 26, NULL, 2, 'Mr', 'Christopher', 'Dalika', NULL, NULL, 0, 2, '1654160184046', '2022-06-02 11:56:25', '2022-06-02 12:04:27'),
(74, 26, NULL, 1, 'Mr', 'Fadhili', 'Mwanga', NULL, NULL, 0, 2, '1654160184046', '2022-06-02 11:56:25', '2022-06-02 12:04:27'),
(75, 26, NULL, 1, 'Mr', 'Godson', 'Haule', NULL, NULL, 0, 2, '1654160184046', '2022-06-02 11:56:25', '2022-06-02 12:04:27'),
(76, 27, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1658926983808', '2022-07-27 16:03:03', '2022-07-27 16:03:03'),
(77, 27, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1658926983808', '2022-07-27 16:03:03', '2022-07-27 16:03:03'),
(78, 28, NULL, 5, 'Mr', 'laison', 'marko', NULL, NULL, 0, 2, '1658927045362', '2022-07-27 16:04:06', '2022-07-27 16:04:54'),
(79, 29, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659089689258', '2022-07-29 13:14:49', '2022-07-29 13:15:32'),
(80, 30, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1659331881728', '2022-08-01 11:31:25', '2022-08-01 11:31:25'),
(81, 30, NULL, 2, NULL, NULL, NULL, NULL, NULL, 0, 2, '1659331881728', '2022-08-01 11:31:25', '2022-08-01 11:31:25'),
(82, 31, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659332367761', '2022-08-01 11:39:28', '2022-08-01 11:40:07'),
(83, 31, NULL, 2, 'Mrs', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659332367761', '2022-08-01 11:39:28', '2022-08-01 11:40:07'),
(84, 32, NULL, 2, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659332667536', '2022-08-01 11:44:28', '2022-08-01 11:45:05'),
(85, 33, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659334866480', '2022-08-01 12:21:07', '2022-08-01 12:21:34'),
(86, 34, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659336176721', '2022-08-01 12:42:57', '2022-08-01 12:43:25'),
(87, 35, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659336247966', '2022-08-01 12:44:08', '2022-08-01 12:44:35'),
(88, 36, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659347635444', '2022-08-01 12:53:55', '2022-08-01 12:54:18'),
(89, 37, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659358510030', '2022-08-01 15:55:10', '2022-08-01 15:55:41'),
(90, 38, NULL, 5, NULL, NULL, NULL, NULL, NULL, 0, 2, '1659358682022', '2022-08-01 15:58:02', '2022-08-01 15:58:02'),
(91, 39, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659358792582', '2022-08-01 15:59:52', '2022-08-01 16:02:13'),
(92, 40, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659600257198', '2022-08-04 11:04:17', '2022-08-04 11:04:36'),
(93, 41, NULL, 5, 'Mr', 'MARTIN LAURENT', 'KABOJA', NULL, NULL, 0, 2, '1659602130712', '2022-08-04 11:35:31', '2022-08-04 11:36:23'),
(94, 41, NULL, 2, 'Mrs', 'lizabell', 'kaboja', NULL, NULL, 0, 2, '1659602130712', '2022-08-04 11:35:31', '2022-08-04 11:36:23'),
(95, 42, NULL, 5, 'Mr', 'onasis', 'mvanga', NULL, NULL, 0, 2, '1659607698613', '2022-08-04 13:08:20', '2022-08-04 13:09:07'),
(96, 42, NULL, 5, 'Mr', 'martin', 'kaboja', NULL, NULL, 0, 2, '1659607698613', '2022-08-04 13:08:20', '2022-08-04 13:09:07'),
(97, 43, NULL, 5, NULL, NULL, NULL, NULL, NULL, 1, 3, '1660124007308', '2022-08-10 12:33:27', '2022-08-10 12:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `code`, `description`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Apartment Building', NULL, 'Apartment building (U.S) /Block of Flats (U.K)', 1, NULL, '2022-03-21 05:44:01', '2022-03-21 05:44:01'),
(2, 'Cabin', NULL, 'Cabin', 1, NULL, '2022-03-21 05:44:13', '2022-03-21 05:44:13'),
(3, 'Castle', NULL, 'Castle', 1, NULL, '2022-03-21 05:44:23', '2022-03-21 05:44:23'),
(4, 'Chalet', NULL, 'Chalet', 1, NULL, '2022-03-21 05:44:32', '2022-03-21 05:44:32'),
(5, 'Hut', NULL, 'Hut', 1, NULL, '2022-03-21 05:44:45', '2022-03-21 05:44:45'),
(6, 'Mansion', NULL, 'Mansion', 1, NULL, '2022-03-21 05:44:58', '2022-03-21 05:44:58');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Tanzania Shilling', 'TZS', '2022-03-21 02:36:06', '2022-03-21 02:36:06'),
(3, 'AMerican Dollar', 'USD', '2022-03-21 02:36:06', '2022-03-21 02:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `exchange_rates`
--

CREATE TABLE `exchange_rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `operation_start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operation_end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('buying','selling') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'buying',
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exchange_rates`
--

INSERT INTO `exchange_rates` (`id`, `operation_start_date`, `operation_end_date`, `creator_id`, `from_currency`, `to_currency`, `category`, `value`, `created_at`, `updated_at`) VALUES
(6, NULL, NULL, '1', NULL, 'TZS', 'buying', 2300, '2022-08-04 09:38:26', '2022-08-04 09:38:26'),
(7, NULL, NULL, '1', NULL, 'TZS', 'buying', 2350, '2022-08-04 09:42:24', '2022-08-04 09:42:24'),
(8, NULL, NULL, '1', NULL, 'TZS', 'buying', 2400, '2022-08-04 13:06:52', '2022-08-04 13:06:52'),
(9, NULL, NULL, '1', NULL, 'TZS', 'buying', 2300, '2022-08-04 13:07:17', '2022-08-04 13:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`, `image_name`, `created_by`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Swimming pool', NULL, 1, 'Swimming pool', NULL, '2022-03-21 05:10:07', '2022-03-21 05:10:07'),
(2, 'Airport shuttle', NULL, 1, 'Airport shuttle', NULL, '2022-03-21 05:10:54', '2022-03-21 05:10:54'),
(5, 'Living room', NULL, 1, 'Living room', NULL, '2022-03-21 05:15:43', '2022-03-21 05:15:43'),
(6, 'Bathroom', NULL, 1, 'Bathroom', NULL, '2022-03-21 05:16:39', '2022-03-21 05:16:39'),
(7, 'Bedroom', NULL, 1, 'Bedroom', NULL, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(8, 'Entertainment', NULL, 1, 'Entertainment', NULL, '2022-03-21 05:20:13', '2022-03-21 05:20:13'),
(9, 'Outdoors', NULL, 1, 'Nature and all of its colours, scents and textures is one of the biggest trends of this year with regards to interior and landscape design.', NULL, '2022-03-21 05:25:46', '2022-03-29 05:06:10'),
(10, 'Sea View Terrace', NULL, 1, 'Sea View Terrace', NULL, '2022-03-21 05:31:19', '2022-03-21 05:31:19'),
(11, 'Modern Kitchen', NULL, 1, 'Modern Kitchen', NULL, '2022-03-21 05:32:58', '2022-03-21 05:32:58'),
(12, 'Parking', NULL, 1, 'Parking', NULL, '2022-03-21 05:33:38', '2022-03-21 05:33:38'),
(13, 'Cleaning services', NULL, 1, 'Cleaning services', NULL, '2022-03-21 05:36:11', '2022-03-21 05:36:11'),
(14, 'Business facilities', NULL, 1, 'Business facilities', NULL, '2022-03-21 05:36:31', '2022-03-21 05:36:31'),
(15, 'Safety & security', NULL, 1, 'Safety & security', NULL, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(16, 'Non-Smoking Throughout', NULL, 1, 'We offer accommodation for non smokers. You also do not like cigarette smoke or smoky smelling rooms? Then there is the offer for you nonsmokers.', NULL, '2022-03-21 05:38:10', '2022-03-29 06:22:19'),
(17, 'Car Hire', NULL, 1, 'You can rent a car in our Hotel if you want to fully explore in and around the city. There’s a lot of things to do and places to see in Dar.', NULL, '2022-03-21 05:38:27', '2022-03-29 06:22:55'),
(18, 'Wellness', NULL, 1, 'The whole experience of the visitors during their stay is based on their physical and mental well-being.', NULL, '2022-03-21 05:39:16', '2022-03-29 05:28:26');

-- --------------------------------------------------------

--
-- Table structure for table `facility_feature`
--

CREATE TABLE `facility_feature` (
  `id` int(10) UNSIGNED NOT NULL,
  `feature_id` bigint(20) NOT NULL,
  `facility_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facility_feature`
--

INSERT INTO `facility_feature` (`id`, `feature_id`, `facility_id`, `created_at`, `updated_at`) VALUES
(1, 58, 1, '2022-03-21 05:10:07', '2022-03-21 05:10:07'),
(2, 20, 5, '2022-03-21 05:15:43', '2022-03-21 05:15:43'),
(3, 6, 5, '2022-03-21 05:15:43', '2022-03-21 05:15:43'),
(4, 7, 6, '2022-03-21 05:16:39', '2022-03-21 05:16:39'),
(5, 14, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(6, 13, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(7, 12, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(8, 11, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(9, 10, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(10, 9, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(11, 8, 7, '2022-03-21 05:18:46', '2022-03-21 05:18:46'),
(12, 18, 8, '2022-03-21 05:20:13', '2022-03-21 05:20:13'),
(13, 17, 8, '2022-03-21 05:20:13', '2022-03-21 05:20:13'),
(14, 16, 8, '2022-03-21 05:20:13', '2022-03-21 05:20:13'),
(15, 15, 8, '2022-03-21 05:20:13', '2022-03-21 05:20:13'),
(16, 19, 9, '2022-03-21 05:25:46', '2022-03-21 05:25:46'),
(17, 22, 10, '2022-03-21 05:31:19', '2022-03-21 05:31:19'),
(18, 21, 10, '2022-03-21 05:31:19', '2022-03-21 05:31:19'),
(19, 20, 10, '2022-03-21 05:31:19', '2022-03-21 05:31:19'),
(20, 26, 11, '2022-03-21 05:32:58', '2022-03-21 05:32:58'),
(21, 25, 11, '2022-03-21 05:32:58', '2022-03-21 05:32:58'),
(22, 24, 11, '2022-03-21 05:32:58', '2022-03-21 05:32:58'),
(23, 23, 11, '2022-03-21 05:32:58', '2022-03-21 05:32:58'),
(24, 28, 12, '2022-03-21 05:33:38', '2022-03-21 05:33:38'),
(25, 31, 13, '2022-03-21 05:36:11', '2022-03-21 05:36:11'),
(26, 30, 13, '2022-03-21 05:36:11', '2022-03-21 05:36:11'),
(27, 29, 13, '2022-03-21 05:36:11', '2022-03-21 05:36:11'),
(28, 32, 14, '2022-03-21 05:36:31', '2022-03-21 05:36:31'),
(29, 40, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(30, 39, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(31, 38, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(32, 37, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(33, 36, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(34, 35, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(35, 34, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(36, 33, 15, '2022-03-21 05:37:21', '2022-03-21 05:37:21'),
(37, 56, 16, '2022-03-21 05:38:10', '2022-03-21 05:38:10'),
(38, 55, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(39, 54, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(40, 53, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(41, 52, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(42, 51, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(43, 50, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(44, 49, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(45, 48, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(46, 47, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(47, 46, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16'),
(48, 45, 18, '2022-03-21 05:39:16', '2022-03-21 05:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `facility_house`
--

CREATE TABLE `facility_house` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` bigint(20) NOT NULL,
  `house_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facility_house`
--

INSERT INTO `facility_house` (`id`, `facility_id`, `house_id`, `created_at`, `updated_at`) VALUES
(506, 17, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(507, 16, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(508, 18, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(509, 9, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(510, 15, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(511, 14, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(512, 13, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(513, 12, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(514, 11, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(515, 10, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(516, 8, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(517, 7, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(518, 6, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(519, 5, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(520, 2, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(521, 1, 1, '2022-04-28 07:52:23', '2022-04-28 07:52:23'),
(522, 17, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(523, 16, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(524, 18, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(525, 9, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(526, 15, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(527, 14, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(528, 13, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(529, 12, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(530, 11, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(531, 10, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(532, 8, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(533, 7, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(534, 6, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(535, 5, 2, '2022-04-28 07:53:17', '2022-04-28 07:53:17'),
(536, 17, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(537, 16, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(538, 18, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(539, 9, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(540, 15, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(541, 14, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(542, 13, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(543, 12, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(544, 11, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(545, 10, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(546, 8, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(547, 7, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(548, 6, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(549, 5, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(550, 2, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41'),
(551, 1, 5, '2022-05-11 22:36:41', '2022-05-11 22:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `featured_images`
--

CREATE TABLE `featured_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `name`, `created_by`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Large Screen Smart Tv', 1, NULL, NULL, '2022-03-21 04:43:18', '2022-03-21 04:43:18'),
(2, 'Dinning table for 12 people', 1, NULL, NULL, '2022-03-21 04:43:28', '2022-03-21 04:43:28'),
(3, 'Jacuzzi', 1, NULL, NULL, '2022-03-21 04:43:42', '2022-03-21 04:43:42'),
(4, 'Non-smoking rooms', 1, NULL, NULL, '2022-03-21 04:44:23', '2022-03-21 04:44:23'),
(5, 'Large Wordrobe', 1, NULL, NULL, '2022-03-21 04:44:46', '2022-03-21 04:44:46'),
(6, 'Dinning table for 12 people', 1, NULL, NULL, '2022-03-21 04:46:01', '2022-03-21 04:46:01'),
(7, 'Jacuzzi', 1, NULL, NULL, '2022-03-21 04:46:13', '2022-03-21 04:46:13'),
(8, 'Garden View', 1, NULL, NULL, '2022-03-21 04:46:33', '2022-03-21 04:46:33'),
(9, 'Sea View', 1, NULL, NULL, '2022-03-21 04:46:53', '2022-03-21 04:46:53'),
(10, 'Non-smoking rooms', 1, NULL, NULL, '2022-03-21 04:47:04', '2022-03-21 04:47:04'),
(11, 'Large Wordrobe', 1, NULL, NULL, '2022-03-21 04:47:22', '2022-03-21 04:47:22'),
(12, 'Safe box', 1, NULL, NULL, '2022-03-21 04:47:42', '2022-03-21 04:47:42'),
(13, 'Working desk', 1, NULL, NULL, '2022-03-21 04:47:58', '2022-03-21 04:47:58'),
(14, 'Dressing mirror', 1, NULL, NULL, '2022-03-21 04:48:13', '2022-03-21 04:48:13'),
(15, 'Large Screen Smart TV', 1, NULL, NULL, '2022-03-21 04:49:33', '2022-03-21 04:49:33'),
(16, 'DSTV with 1 50 Channels available', 1, NULL, NULL, '2022-03-21 04:49:45', '2022-03-21 04:49:45'),
(17, 'Netflix', 1, NULL, NULL, '2022-03-21 04:49:55', '2022-03-21 04:49:55'),
(18, 'High Speed WiFi Internet', 1, NULL, NULL, '2022-03-21 04:50:06', '2022-03-21 04:50:06'),
(19, 'Outdoor furniture', 1, NULL, NULL, '2022-03-21 04:50:24', '2022-03-21 04:50:24'),
(20, 'Large Screen Smart TV', 1, NULL, NULL, '2022-03-21 04:50:41', '2022-03-21 04:50:41'),
(21, 'Pool Table Game', 1, NULL, NULL, '2022-03-21 04:50:52', '2022-03-21 04:50:52'),
(22, 'Dart Board', 1, NULL, NULL, '2022-03-21 04:51:14', '2022-03-21 04:51:14'),
(23, 'Full Kitchen Utensils', 1, NULL, NULL, '2022-03-21 04:51:36', '2022-03-21 04:51:36'),
(24, 'Fridges', 1, NULL, NULL, '2022-03-21 04:51:46', '2022-03-21 04:51:46'),
(25, 'Oven', 1, NULL, NULL, '2022-03-21 04:51:59', '2022-03-21 04:51:59'),
(26, 'Microwave', 1, NULL, NULL, '2022-03-21 04:52:09', '2022-03-21 04:52:09'),
(27, 'Free WiFi is available in all areas', 1, NULL, NULL, '2022-03-21 04:52:27', '2022-03-21 04:52:27'),
(28, 'Parking available', 1, NULL, NULL, '2022-03-21 04:58:40', '2022-03-21 04:58:40'),
(29, 'Daily housekeeping', 1, NULL, NULL, '2022-03-21 04:58:53', '2022-03-21 04:58:53'),
(30, 'Ironing service Additional charge', 1, NULL, NULL, '2022-03-21 05:00:48', '2022-03-21 05:00:48'),
(31, 'Laundry Additional charge', 1, NULL, NULL, '2022-03-21 05:00:56', '2022-03-21 05:00:56'),
(32, 'Business centre Additional charge', 1, NULL, NULL, '2022-03-21 05:01:17', '2022-03-21 05:01:17'),
(33, 'Fire extinguishers', 1, NULL, NULL, '2022-03-21 05:01:32', '2022-03-21 05:01:32'),
(34, 'CCTV outside property', 1, NULL, NULL, '2022-03-21 05:01:43', '2022-03-21 05:01:43'),
(35, 'CCTV in common areas', 1, NULL, NULL, '2022-03-21 05:01:53', '2022-03-21 05:01:53'),
(36, 'Heat alarms', 1, NULL, NULL, '2022-03-21 05:02:04', '2022-03-21 05:02:04'),
(37, 'Smoke alarms', 1, NULL, NULL, '2022-03-21 05:02:14', '2022-03-21 05:02:14'),
(38, 'Security alarm', 1, NULL, NULL, '2022-03-21 05:02:24', '2022-03-21 05:02:24'),
(39, 'Access control', 1, NULL, NULL, '2022-03-21 05:02:34', '2022-03-21 05:02:34'),
(40, '24-hour security', 1, NULL, NULL, '2022-03-21 05:02:47', '2022-03-21 05:02:47'),
(41, 'Designated smoking area', 1, NULL, NULL, '2022-03-21 05:03:18', '2022-03-21 05:03:18'),
(42, 'Air conditioning', 1, NULL, NULL, '2022-03-21 05:03:27', '2022-03-21 05:03:27'),
(43, 'Non-smoking throughout', 1, NULL, NULL, '2022-03-21 05:03:45', '2022-03-21 05:03:45'),
(44, 'Car hire', 1, NULL, NULL, '2022-03-21 05:03:53', '2022-03-21 05:03:53'),
(45, 'Personal trainer', 1, NULL, NULL, '2022-03-21 05:04:48', '2022-03-21 05:04:48'),
(46, 'Fitness classes', 1, NULL, NULL, '2022-03-21 05:04:58', '2022-03-21 05:04:58'),
(47, 'Yoga classes', 1, NULL, NULL, '2022-03-21 05:05:15', '2022-03-21 05:05:15'),
(48, 'Fitness', 1, NULL, NULL, '2022-03-21 05:05:27', '2022-03-21 05:05:27'),
(49, 'Full body massage', 1, NULL, NULL, '2022-03-21 05:05:43', '2022-03-21 05:05:43'),
(50, 'Hand massage', 1, NULL, NULL, '2022-03-21 05:05:59', '2022-03-21 05:05:59'),
(51, 'Head massage', 1, NULL, NULL, '2022-03-21 05:06:07', '2022-03-21 05:06:07'),
(52, 'Couples massage', 1, NULL, NULL, '2022-03-21 05:06:20', '2022-03-21 05:06:20'),
(53, 'Foot massage', 1, NULL, NULL, '2022-03-21 05:06:32', '2022-03-21 05:06:32'),
(54, 'Neck massage', 1, NULL, NULL, '2022-03-21 05:06:41', '2022-03-21 05:06:41'),
(55, 'Back massage', 1, NULL, NULL, '2022-03-21 05:06:51', '2022-03-21 05:06:51'),
(56, 'Sun loungers or beach chairs', 1, NULL, NULL, '2022-03-21 05:07:30', '2022-03-21 05:07:30'),
(57, 'Massage', 1, NULL, NULL, '2022-03-21 05:07:41', '2022-03-21 05:07:41'),
(58, 'Fitness centre', 1, NULL, NULL, '2022-03-21 05:07:59', '2022-03-21 05:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `phone`, `subject`, `message`, `country_code`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'ecsdcsdc', 'onassiskenan@gmail.com', '0711750551', 'sdcs', 'sc', '+995 44 +7 840, 940|Abkhazia', 0, NULL, '2022-04-22 03:36:25', '2022-04-22 03:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_categories`
--

CREATE TABLE `gallery_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_categories`
--

INSERT INTO `gallery_categories` (`id`, `name`, `created_by`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Living rooms', 1, 'Living room', NULL, '2022-03-22 03:17:40', '2022-03-22 03:17:40'),
(2, 'Sea View Terrace', 1, 'Sea View Terrace', NULL, '2022-03-22 03:18:01', '2022-03-22 03:18:01'),
(3, 'Safety & security', 1, 'Safety & security', NULL, '2022-03-22 03:19:46', '2022-03-22 03:19:46'),
(4, 'Business facilities', 1, 'Business facilities', NULL, '2022-03-22 03:20:16', '2022-03-22 03:20:16'),
(5, 'Outdoor', 1, 'Outdoor', NULL, '2022-03-23 06:52:39', '2022-03-23 06:52:39');

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE `houses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` int(11) NOT NULL,
  `Price_reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `location_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `max_number_of_people` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isAvailable` enum('on','off') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'on',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `max_number_of_children` int(11) NOT NULL,
  `max_number_of_adults` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`id`, `name`, `cost`, `Price_reference`, `created_by`, `currency_id`, `location_id`, `category_id`, `max_number_of_people`, `description`, `isAvailable`, `created_at`, `updated_at`, `number_of_rooms`, `max_number_of_children`, `max_number_of_adults`) VALUES
(1, 'Twin Room with Breakfast', 67, 'Per Month', 1, 3, 1, 1, 10, NULL, 'on', '2022-03-21 06:10:11', '2022-04-28 07:52:23', 8, 0, 10),
(2, 'Double Room with Breakfast', 67, 'Per Month', 1, 3, 1, 1, 10, 'vdfvdvdv', 'on', '2022-03-21 06:11:06', '2022-04-28 07:53:17', 8, 0, 10),
(5, 'Deluxe Double Room with Breakfast', 80, 'Price for 1 Night', 1, 3, 1, 1, 4, NULL, 'on', '2022-03-31 09:56:59', '2022-05-11 22:36:41', 2, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `house_images`
--

CREATE TABLE `house_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_id` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `house_images`
--

INSERT INTO `house_images` (`id`, `name`, `house_id`, `category_id`, `description`, `created_at`, `updated_at`) VALUES
(1, '1649684745_thumb_22866_20210602213202_0970712001622669522_751_Deluxe_Double_Room3.png', 5, NULL, NULL, '2022-04-11 10:45:45', '2022-04-11 10:45:45'),
(3, '1649685142_thumb_22866_20210602213329_0832523001622669609_893_Double_Room1.png', 2, NULL, NULL, '2022-04-11 10:52:22', '2022-04-11 10:52:22'),
(4, '1649685164_thumb_22866_20210602213245_0698256001622669565_353_Twin_Room2.png', 1, NULL, NULL, '2022-04-11 10:52:44', '2022-04-11 10:52:44'),
(5, '1659354163_DSC05266.jpg', 1, NULL, NULL, '2022-08-01 11:42:45', '2022-08-01 11:42:45'),
(6, '1659354204_DSC05266.jpg', 1, NULL, NULL, '2022-08-01 11:43:27', '2022-08-01 11:43:27'),
(7, '1659354280_DSC05266.jpg', 1, NULL, NULL, '2022-08-01 11:44:42', '2022-08-01 11:44:42'),
(8, '1659354329_DSC05266.jpg', 1, NULL, NULL, '2022-08-01 11:45:31', '2022-08-01 11:45:31'),
(9, '1659354469_DSC05266.jpg', 1, NULL, NULL, '2022-08-01 11:47:52', '2022-08-01 11:47:52'),
(10, '1659354504_DSC04958.jpg', 2, NULL, NULL, '2022-08-01 11:48:24', '2022-08-01 11:48:24'),
(11, '1659354517_DSC05230.jpg', 5, NULL, NULL, '2022-08-01 11:48:39', '2022-08-01 11:48:39');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `category_id`, `created_by`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(8, '1648030497_20220226_185040.jpg', 1, 1, NULL, NULL, '2022-03-23 07:14:57', '2022-03-23 07:14:57'),
(9, '1648030513_20220226_141743.jpg', 1, 1, NULL, NULL, '2022-03-23 07:15:13', '2022-03-23 07:15:13'),
(10, '1648030536_20211229_121653.jpg', 2, 1, NULL, NULL, '2022-03-23 07:15:36', '2022-03-23 07:15:36'),
(12, '1648030561_20211207_203029.jpg', 5, 1, NULL, NULL, '2022-03-23 07:16:01', '2022-03-23 07:16:01'),
(13, '1648030723_20211207_200647.jpg', 5, 1, NULL, NULL, '2022-03-23 07:18:43', '2022-03-23 07:18:43'),
(14, '1648031158_20211213_173244.jpg', 5, 1, NULL, NULL, '2022-03-23 07:25:59', '2022-03-23 07:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `image_house_house`
--

CREATE TABLE `image_house_house` (
  `id` int(10) UNSIGNED NOT NULL,
  `house_image_id` bigint(20) NOT NULL,
  `house_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDefault` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `latitude`, `longitude`, `name`, `isDefault`, `code`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Mbezi Beach White sands road, Dar es Salaam', '1', NULL, '2022-03-21 02:36:06', '2022-03-21 02:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_03_15_090201_create_articles_table', 1),
(6, '2022_03_15_090311_create_categories_table', 1),
(7, '2022_03_15_095249_create_organizations_table', 1),
(8, '2022_03_16_133800_create_gallery_categories_table', 1),
(9, '2022_03_17_090307_create_images_table', 1),
(10, '2022_03_17_101450_create_features_table', 1),
(11, '2022_03_17_135733_create_facilities_table', 1),
(12, '2022_03_18_053217_create_facility_feature_table', 1),
(13, '2022_03_18_060054_create_activities_table', 1),
(14, '2022_03_18_085235_create_activity_facility_table', 1),
(16, '2022_03_18_112131_create_currencies_table', 1),
(17, '2022_03_18_114233_create_locations_table', 1),
(18, '2022_03_20_095329_create_activity_images_table', 1),
(21, '2022_03_20_100824_create_activity_house_table', 1),
(22, '2022_03_20_100900_create_facility_house_table', 1),
(24, '2022_03_18_105246_create_houses_table', 2),
(33, '2022_03_31_085159_add_column_in_houses_table', 7),
(34, '2022_03_20_095831_create_house_images_table', 8),
(49, '2022_03_21_143430_create_feedback_table', 10),
(59, '2022_04_08_093320_create_booking_details_table', 11),
(60, '2022_03_20_190203_create_bookings_table', 12),
(61, '2022_05_09_152332_create_remaining_rooms_table', 13),
(62, '2022_03_18_074853_create_featured_images_table', 14),
(63, '2022_03_20_100127_create_image_house_house_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_time_editor_id` bigint(20) NOT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_password` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encryption_protocol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_mailphp_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `host` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `remaining_rooms`
--

CREATE TABLE `remaining_rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `house_id` bigint(20) NOT NULL,
  `total_number_of_rooms` int(11) NOT NULL,
  `remaining_rooms` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `remaining_rooms`
--

INSERT INTO `remaining_rooms` (`id`, `house_id`, `total_number_of_rooms`, `remaining_rooms`, `created_at`, `updated_at`) VALUES
(1, 1, 8, 7, '2022-05-09 12:25:26', '2022-05-11 22:33:47'),
(2, 2, 8, 7, '2022-05-09 12:25:26', '2022-05-11 22:33:47'),
(3, 5, 8, 6, '2022-05-09 12:25:26', '2022-05-11 22:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Luxury Beach Villa Admin', 'luxury@luxurybeachvilla.co.tz', '2022-03-21 02:36:05', '$2y$10$Ueio5tob0odc2ry6Y.QIE.0J5WqQgZM8BmABLCM0Prnee9dtLi.uu', 'mjhFsKTSEk2VC6XGwpofCJwtyMRmUQz2FgpvyAcfPnsg1HBTp1hHhPTSyeRN', '2022-03-21 02:36:05', '2022-03-21 02:36:05'),
(2, 'Luxury Beach Villa Admin2', 'luxury2@luxurybeachvilla.co.tz', '2022-03-21 02:36:06', '$2y$10$ga3iNwLJVyQMIDUFymprGOofv7CvQyhT/zfua.E6sLE9oWyV/0IBi', NULL, '2022-03-21 02:36:06', '2022-03-21 02:36:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_facility`
--
ALTER TABLE `activity_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_house`
--
ALTER TABLE `activity_house`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_images`
--
ALTER TABLE `activity_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_details`
--
ALTER TABLE `booking_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exchange_rates`
--
ALTER TABLE `exchange_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_feature`
--
ALTER TABLE `facility_feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_house`
--
ALTER TABLE `facility_house`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `featured_images`
--
ALTER TABLE `featured_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `house_images`
--
ALTER TABLE `house_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_house_house`
--
ALTER TABLE `image_house_house`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `remaining_rooms`
--
ALTER TABLE `remaining_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `activity_facility`
--
ALTER TABLE `activity_facility`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_house`
--
ALTER TABLE `activity_house`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `activity_images`
--
ALTER TABLE `activity_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `booking_details`
--
ALTER TABLE `booking_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exchange_rates`
--
ALTER TABLE `exchange_rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `facility_feature`
--
ALTER TABLE `facility_feature`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `facility_house`
--
ALTER TABLE `facility_house`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=552;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `featured_images`
--
ALTER TABLE `featured_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `houses`
--
ALTER TABLE `houses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `house_images`
--
ALTER TABLE `house_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `image_house_house`
--
ALTER TABLE `image_house_house`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `remaining_rooms`
--
ALTER TABLE `remaining_rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
