<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('on_construction');
// });

// Route::get('/rough', function () {
//     return view('rough');
// });

Route::get('/reports/index', [App\Http\Controllers\MainController::class, 'view_reports'])->middleware('auth');
Route::get('/reports/datatable', [App\Http\Controllers\MainController::class, 'datatable_reports'])->middleware('auth');

Route::get('/', [App\Http\Controllers\MainController::class, 'homepage'])->name('home');

//payment routes

Route::get('/payment/done/{id}', [App\Http\Controllers\MainController::class, 'payment_done']);
Route::get('/payment/cancel/{id}', [App\Http\Controllers\MainController::class, 'cancel_payment']);

Route::post('/cancel_trip_description', [App\Http\Controllers\MainController::class, 'cancel_trip_description'])->name('cancel_trip_description');

Route::get('/map', [App\Http\Controllers\MainController::class, 'map']);
Route::post('/check_user', [App\Http\Controllers\MainController::class, 'check_user']);
Route::post('/save_booking', [App\Http\Controllers\MainController::class, 'booking_details_update']);
Route::post('/store_book_details', [App\Http\Controllers\MainController::class, 'store_book_details']);
Route::post('/cancel_booking', [App\Http\Controllers\MainController::class, 'cancel_booking'])->name('cancel_booking');
Route::post('/quatation_request', [App\Http\Controllers\MainController::class, 'quatation_request'])->name('quatation_request');

Route::post('/exchange_rate_store',  [App\Http\Controllers\HomeController::class, 'RateStore'])->name('exchange.store');
Route::get('/contacts', [App\Http\Controllers\MainController::class, 'contacts']);
Route::get('/gallery', [App\Http\Controllers\MainController::class, 'gallery']);
Route::post('/check_available_post', [App\Http\Controllers\MainController::class, 'check_available_post_func']);
Route::delete('/delete_house', [App\Http\Controllers\MainController::class, 'delete_house']);
Route::delete('/delete_image', [App\Http\Controllers\MainController::class, 'delete_image']);

Route::get('/check_available', [App\Http\Controllers\MainController::class, 'check_available']);
Route::get('/book/{id}', [App\Http\Controllers\MainController::class, 'book']);
Route::get('/activity/{id}', [App\Http\Controllers\MainController::class, 'view_activity']);

Route::get('/home/booking/checkout/{id}', [App\Http\Controllers\MainController::class, 'checking_out_guest']);
Route::post('booking_details_update', [App\Http\Controllers\MainController::class, 'booking_details_update']);
Route::get('booking_details/{id}', [App\Http\Controllers\MainController::class, 'booking_details']);
Route::post('login_checker', [App\Http\Controllers\MainController::class, 'login_checker']);

Auth::routes();

Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/check_in', [App\Http\Controllers\MainController::class, 'check_in'])->middleware('auth');
Route::get('/home/check_out', [App\Http\Controllers\MainController::class, 'check_out'])->middleware('auth');
Route::get('/home/booked', [App\Http\Controllers\MainController::class,  'booked'])->middleware('auth');
Route::get('/home/users', [App\Http\Controllers\MainController::class, 'users'])->middleware('auth');
Route::get('/home/content_manager', [App\Http\Controllers\MainController::class, 'content_manager'])->middleware('auth');
Route::resource('/home/content_manager/feature', App\Http\Controllers\FeatureController::class)->middleware('auth');
Route::resource('/home/content_manager/gallery_category', App\Http\Controllers\GalleryCategoryController::class)->middleware('auth');
Route::resource('/home/content_manager/image', App\Http\Controllers\ImageController::class)->middleware('auth');
Route::resource('/home/content_manager/facility', App\Http\Controllers\FacilityController::class)->middleware('auth');
Route::resource('/home/content_manager/activity', App\Http\Controllers\ActivityController::class)->middleware('auth');
Route::resource('/home/content_manager/category', App\Http\Controllers\CategoryController::class)->middleware('auth');
Route::resource('/home/content_manager/house', App\Http\Controllers\HouseController::class)->middleware('auth');
Route::resource('/home/booking', App\Http\Controllers\BookingController::class);
Route::post('/home/image_upload', [App\Http\Controllers\JsonResponseImageController::class, 'upload'])->name('json_upload_image');
Route::resource('/feedback', App\Http\Controllers\FeedbackController::class);
Route::resource('/booking_details', App\Http\Controllers\BookingDetailController::class);
Route::resource('/home/user', App\Http\Controllers\UserController::class);
Route::post('/save_exchange_rate', [App\Http\Controllers\MainController::class, 'save_exchange_rate']);

Route::get('/online_migrate', function() {
    $exitCode = Artisan::call('migrate');    
    // $exitCode = Artisan::call('migrate:fresh --seed');    
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('optimize:clear');
    return 'changes commited';
});