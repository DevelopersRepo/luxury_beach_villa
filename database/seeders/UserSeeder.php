<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

           DB::table('users')->insert([[
            'id'=>1,
            'name'=>'Luxury Beach Villa Admin',
            'email'=>'luxury@luxurybeachvilla.co.tz',
            'password'=>bcrypt('123456789'),
            'email_verified_at'=> Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'=> Carbon::now()->format('Y-m-d H:i:s')
    ],
    [
        'id'=>2,
        'name'=>'Luxury Beach Villa Admin2',
        'email'=>'luxury2@luxurybeachvilla.co.tz',
        'password'=>bcrypt('123456789'),
        'email_verified_at'=> Carbon::now()->format('Y-m-d H:i:s'),
        'created_at'=> Carbon::now()->format('Y-m-d H:i:s'),
        'updated_at'=> Carbon::now()->format('Y-m-d H:i:s')
]
    ]);



    }
}
