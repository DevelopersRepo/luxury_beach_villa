<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');           
            $table->string('name');
            $table->integer('cost');
            $table->string('Price_reference');
            $table->bigInteger('created_by');
            $table->integer('currency_id');
            $table->bigInteger('location_id');
            $table->bigInteger('category_id');
            $table->bigInteger('max_number_of_people');
            $table->longText('description')->nullable();
            $table->enum('isAvailable',["on", "off"])->default("on");
            $table->softDeletes();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
