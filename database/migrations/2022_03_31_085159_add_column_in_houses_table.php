<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('houses', function (Blueprint $table) {
            //
            $table->integer('number_of_rooms');
            $table->integer('max_number_of_children');
            $table->integer('max_number_of_adults');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->dropColumn('number_of_rooms');
            $table->dropColumn('max_number_of_children');
            $table->dropColumn('max_number_of_adults');
//
        });
    }
}
