<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id'); 
            // $table->string('first_name')->nullable();           
            // $table->string('middle_name')->nullable();           
            // $table->string('last_name')->nullable();           
            // $table->bigInteger('house_id')->nullable();           
            $table->string('email')->nullable();           
            $table->string('phone')->nullable();           
            // $table->string('address')->nullable();           
            // $table->string('city')->nullable();           
            // $table->string('state')->nullable();           
            // $table->integer('zip')->nullable();           
            // $table->integer('gender')->nullable();           
            $table->date('arrival_date')->nullable();           
            $table->integer('number_of_days')->nullable();           
            $table->date('departure_date')->nullable();           
            $table->enum('payment_type', ["online","cash"])->default("cash");           
            $table->integer('number_of_people')->nullable();           
            $table->string('card_number')->nullable();           
            $table->string('card_expiration')->nullable();           
            $table->string('vcc_number')->nullable();   
            $table->enum('isReviewed', [0,1])->default(0);   
            $table->integer('status')->default(0); 
            $table->bigInteger('reviewed_by')->nullable(); 
            $table->string('code')->nullable();  
            $table->string('booking_code')->nullable();  
            $table->longText('special_request')->nullable(); 
            $table->string('require_pick_up')->nullable(); 
            $table->string('pick_up_date_time')->nullable();  
            $table->longText('flight_details')->nullable();  
            $table->longText('cancellation_reason')->nullable();  
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
