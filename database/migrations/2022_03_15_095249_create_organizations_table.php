<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');           
            $table->string('name');
            $table->bigInteger('last_time_editor_id');
            $table->bigInteger('location_id')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->longText('mail_password')->nullable();
            $table->string('encryption_protocol')->nullable();
            $table->string('from_mailphp_address')->nullable();
            $table->string('port')->nullable();
            $table->string('host')->nullable();
            $table->softDeletes();
           $table->string('driver')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
